ALTER TABLE hnd_administrative.hnd_property ALTER COLUMN hasregistrationdata SET DEFAULT false;

ALTER TABLE hnd_cadastre.hnd_spatialzone ALTER COLUMN locklevel SET DEFAULT 'UNLOCKED';

ALTER TABLE hnd_cadastre.hnd_parcel ALTER COLUMN access SET DEFAULT false;
ALTER TABLE hnd_cadastre.hnd_parcel ALTER COLUMN businessclass SET DEFAULT false;
ALTER TABLE hnd_cadastre.hnd_parcel ALTER COLUMN drainage SET DEFAULT false;
ALTER TABLE hnd_cadastre.hnd_parcel ALTER COLUMN electricity SET DEFAULT false;
ALTER TABLE hnd_cadastre.hnd_parcel ALTER COLUMN lighting SET DEFAULT false;
ALTER TABLE hnd_cadastre.hnd_parcel ALTER COLUMN garbagetruck SET DEFAULT false;
ALTER TABLE hnd_cadastre.hnd_parcel ALTER COLUMN heritage SET DEFAULT false;
ALTER TABLE hnd_cadastre.hnd_parcel ALTER COLUMN landscapevalue SET DEFAULT false;
ALTER TABLE hnd_cadastre.hnd_parcel ALTER COLUMN location SET DEFAULT false;
ALTER TABLE hnd_cadastre.hnd_parcel ALTER COLUMN phone SET DEFAULT false;
ALTER TABLE hnd_cadastre.hnd_parcel ALTER COLUMN sidewalks SET DEFAULT false;
ALTER TABLE hnd_cadastre.hnd_parcel ALTER COLUMN streets SET DEFAULT false;
ALTER TABLE hnd_cadastre.hnd_parcel ALTER COLUMN topography SET DEFAULT false;
ALTER TABLE hnd_cadastre.hnd_parcel ALTER COLUMN tvcable SET DEFAULT false;
ALTER TABLE hnd_cadastre.hnd_parcel ALTER COLUMN vulnerable SET DEFAULT false;
ALTER TABLE hnd_cadastre.hnd_parcel ALTER COLUMN water SET DEFAULT false;

ALTER TABLE hnd_cadastre.hnd_landuse ALTER COLUMN fillopacity SET DEFAULT 0.1;
ALTER TABLE hnd_cadastre.hnd_landuse ALTER COLUMN strokewidth SET DEFAULT 0.5;


//SELECT omoa.export_schema();
//SELECT ptocortes.export_schema();
