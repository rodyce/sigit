package org.sigit.services;

import java.util.UUID;

import org.sigit.interop.ws.commons.GeneralResponseMsg;
import org.sigit.interop.ws.transaction.types.v1.AdministrativeSourceList;
import org.sigit.interop.ws.transaction.types.v1.AdministrativeSourceList.AdministrativeSource;
import org.sigit.interop.ws.transaction.types.v1.TransactionList;
import org.sigit.interop.ws.transaction.types.v1.TransactionResponse;

public interface MunicipalTransactionService {
    String getTransactionData(String presentationId) throws GeneralResponseMsg;
    
    AdministrativeSourceList getTransactionSources(String presentationId) throws GeneralResponseMsg;

    AdministrativeSource getTransactionSource(String presentationId, String sID) throws GeneralResponseMsg;

    UUID getTransactionSourceArchiveId(String presentationId, String sID) throws GeneralResponseMsg;

    String receiveTransaction(String presentationId, String entityName) throws GeneralResponseMsg;

    String getNextTransaction(boolean idOnly) throws GeneralResponseMsg;

    TransactionList getPendingTransactions(boolean deliverForExternalApprovalOnly) throws GeneralResponseMsg;

    void processTransactionResponse(TransactionResponse transactionResponse) throws GeneralResponseMsg;

    void processTransactionResponseXML(String transactionResponseXML) throws GeneralResponseMsg;

}