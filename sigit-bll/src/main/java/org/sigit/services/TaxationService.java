package org.sigit.services;

import java.math.BigDecimal;
import java.util.Date;

import org.sigit.interop.ws.commons.GeneralResponseMsg;
import org.sigit.interop.ws.taxation.types.v1.TaxationInfo;
import org.sigit.interop.ws.taxation.types.v1.TaxationInfoList;

public interface TaxationService {

    TaxationInfo getTaxationInfo(String parcelNationalIdentifier) throws GeneralResponseMsg;

    TaxationInfoList getTaxationInfoUpdatedAfter(Date date) throws GeneralResponseMsg;

    TaxationInfo updateParcelTaxationInfo(String parcelNationalIdentifier, BigDecimal commercialAppraisal,
            BigDecimal fiscalAppraisal, BigDecimal taxationBalanceDue) throws GeneralResponseMsg;

}