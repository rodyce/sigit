package org.sigit.services;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.sigit.dao.hnd.cadastre.HND_ParcelDAO;
import org.sigit.interop.ws.commons.GeneralResponseMsg;
import org.sigit.interop.ws.commons.types.GeneralResponse;
import org.sigit.interop.ws.taxation.types.v1.TaxationInfo;
import org.sigit.interop.ws.taxation.types.v1.TaxationInfoList;
import org.sigit.model.hnd.cadastre.HND_Parcel;
import org.springframework.stereotype.Service;

@Service
public class TaxationServiceImpl implements TaxationService {
    private static final Properties messageBundle;

    static {
        messageBundle = new Properties();
        InputStream inStream = TaxationService.class.getResourceAsStream("/messages_es.properties");
        try {
            if (inStream != null)
                messageBundle.load(inStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @PostConstruct
    private void init() {
    }
    
    /* (non-Javadoc)
     * @see org.sigit.services.TaxationService#getTaxationInfo(java.lang.String)
     */
    @Override
    public TaxationInfo getTaxationInfo(String parcelNationalIdentifier) throws GeneralResponseMsg {
        HND_Parcel parcel = obtainParcelFromCadastralKey(parcelNationalIdentifier);
        return calcTIFromParcel(parcel);
    }
    
    /* (non-Javadoc)
     * @see org.sigit.services.TaxationService#getTaxationInfoUpdatedAfter(java.util.Date)
     */
    @Override
    public TaxationInfoList getTaxationInfoUpdatedAfter(Date date) throws GeneralResponseMsg {
        List<HND_Parcel> parcels = HND_ParcelDAO.loadParcelByTaxationLastUpdateAfter(date);
        TaxationInfoList til = new TaxationInfoList();
        
        for (HND_Parcel parcel : parcels)
            til.getTaxation().add(calcTIFromParcel(parcel));
        
        return til;
    }

    /* (non-Javadoc)
     * @see org.sigit.services.TaxationService#updateParcelTaxationInfo(java.lang.String, java.math.BigDecimal, java.math.BigDecimal, java.math.BigDecimal)
     */
    @Override
    public TaxationInfo updateParcelTaxationInfo(
            String parcelNationalIdentifier,
            BigDecimal commercialAppraisal,
            BigDecimal fiscalAppraisal,
            BigDecimal taxationBalanceDue) throws GeneralResponseMsg {
        
        HND_Parcel parcel = obtainParcelFromCadastralKey(parcelNationalIdentifier);
        if (commercialAppraisal != null) parcel.setCommercialAppraisal(commercialAppraisal);
        if (fiscalAppraisal != null) parcel.setFiscalAppraisal(fiscalAppraisal);
        if (taxationBalanceDue != null) parcel.setTaxationBalanceDue(taxationBalanceDue);
        parcel.setTaxationInfoSource("WEB SERVICE");
        HND_ParcelDAO.save(parcel);
        
        TaxationInfo ti = calcTIFromParcel(parcel);
        return ti;
    }
    
    
    private TaxationInfo calcTIFromParcel(HND_Parcel parcel) {
        TaxationInfo ti = new TaxationInfo();
        
        ti.setParcelNationalIdentifier(parcel.getCadastralKey());
        ti.setCommercialAppraisal(parcel.getCommercialAppraisal());
        ti.setFiscalAppraisal(parcel.getFiscalAppraisal());
        ti.setTaxationBalanceDue(parcel.getTaxationBalanceDue());
        ti.setTaxationInfoLastUpdate(parcel.getTaxationInfoLastUpdate());
        ti.setTaxationInfoSource(parcel.getTaxationInfoSource());
        
        return ti;
    }
    
    private HND_Parcel obtainParcelFromCadastralKey(String cadastralKey) throws GeneralResponseMsg {
        List<HND_Parcel> parcels = HND_ParcelDAO.loadParcelsByCadastralKey(cadastralKey);
        if (parcels == null || parcels.size() == 0)
            throw new GeneralResponseMsg( messageBundle.getProperty("ws.taxation.parcel_not_found"),
                    getGeneralResponseMap().get("ws.taxation.parcel_not_found") );
        
        HND_Parcel parcel = parcels.get(0);
        
        return parcel;
    }


    private static Map<String, GeneralResponse> generalResponseMap;
    private static org.sigit.interop.ws.commons.types.ObjectFactory commonsTypesOF = new org.sigit.interop.ws.commons.types.ObjectFactory();
    
    private static Map<String, GeneralResponse> getGeneralResponseMap() {
        if (generalResponseMap == null) {
            generalResponseMap = new ConcurrentHashMap<String, GeneralResponse>();
            
            generalResponseMap.put("ws.taxation.parcel_not_found", commonsTypesOF.createGeneralResponse("SIGIT-TAX-01", messageBundle.getProperty("ws.taxation.parcel_not_found")));
        }
        return generalResponseMap;
    }
}
