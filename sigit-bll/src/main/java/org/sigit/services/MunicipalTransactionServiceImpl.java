package org.sigit.services;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.sigit.dao.hnd.administrative.HND_MunicipalTransactionDAO;
import org.sigit.dao.hnd.administrative.HND_TransactionLogDAO;
import org.sigit.dao.hnd.ladmshadow.ParcelDAO;
import org.sigit.dao.hnd.special.SystemConfigurationDAO;
import org.sigit.interop.gmlproducers.TransactionGMLProducer;
import org.sigit.interop.ws.commons.GeneralResponseMsg;
import org.sigit.interop.ws.commons.types.GeneralResponse;
import org.sigit.interop.ws.transaction.types.v1.AdministrativeSourceList;
import org.sigit.interop.ws.transaction.types.v1.TransactionList;
import org.sigit.interop.ws.transaction.types.v1.TransactionList.Transaction;
import org.sigit.interop.ws.transaction.types.v1.TransactionResponse;
import org.sigit.interop.ws.transaction.types.v1.TransactionResponse.Details.ParcelData;
import org.sigit.logic.workflow.BPMController;
import org.sigit.logic.workflow.BusinessProcess;
import org.sigit.logic.workflow.BusinessProcessManager;
import org.sigit.model.commons.IAdministrativeSource;
import org.sigit.model.commons.IExtArchive;
import org.sigit.model.hnd.administrative.HND_ActivityType;
import org.sigit.model.hnd.administrative.HND_MunicipalTransaction;
import org.sigit.model.hnd.administrative.HND_TransactionLog;
import org.sigit.model.hnd.ladmshadow.Parcel;
import org.sigit.model.hnd.special.SystemConfiguration;
import org.sigit.model.ladm.administrative.LA_AdministrativeSource;
import org.springframework.stereotype.Component;

@Component
public class MunicipalTransactionServiceImpl implements MunicipalTransactionService {
    private static JAXBContext transactionResponseJAXBContext;
    
    private static final Properties messageBundle;
    private SystemConfiguration systemConfiguration;
    
    private BPMController bpmController;
    
    static {
        try {
            transactionResponseJAXBContext = JAXBContext.newInstance(TransactionResponse.class);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        
        messageBundle = new Properties();
        InputStream inStream = MunicipalTransactionService.class.getResourceAsStream("/messages_es.properties");
        try {
            if (inStream != null)
                messageBundle.load(inStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private SystemConfiguration getSystemConfiguration() {
        if (systemConfiguration == null) {
            systemConfiguration = SystemConfigurationDAO.loadSystemConfiguration();
        }
        return systemConfiguration;
    }
    
    
    /* (non-Javadoc)
     * @see org.sigit.services.MunicipalTransactionServiceImpl#getTransactionData(java.lang.String)
     */
    @Override
    public String getTransactionData(String presentationId) throws GeneralResponseMsg {
        return getTrxDataXML(presentationId, false, null);
    }
    
    /* (non-Javadoc)
     * @see org.sigit.services.MunicipalTransactionServiceImpl#receiveTransaction(java.lang.String)
     */
    @Override
    public String receiveTransaction(String presentationId, String entityName) throws GeneralResponseMsg {
        return getTrxDataXML(presentationId, true, entityName);
    }

    /* (non-Javadoc)
     * @see org.sigit.services.MunicipalTransactionServiceImpl#getNextTransaction(boolean)
     */
    @Override
    public String getNextTransaction(boolean idOnly) throws GeneralResponseMsg {
        HND_MunicipalTransaction trx = HND_MunicipalTransactionDAO.loadNextRequest();

        if (trx == null) {
            throw new GeneralResponseMsg( messageBundle.getProperty("ws.getNextTransaction_err"),
                    getGeneralResponseMap().get("ws.no_next_deliver_for_external_approval") );
        }
        
        String response;
        if (idOnly) {
            response = String.format("%s-%s", getSystemConfiguration().getWorkingNamespace(), trx.getPresentationNo());
        } else {
            //When 'getting' the transaction, the 'entityName' argument may be null. This is unlike when 'receiving'
            //the transaction
            response = getTrxDataXML(trx.getPresentationNo().toString(), false, null);
        }
        
        return response;
    }
    
    /* (non-Javadoc)
     * @see org.sigit.services.MunicipalTransactionServiceImpl#getPendingTransactions(boolean)
     */
    @Override
    public TransactionList getPendingTransactions(boolean deliverForExternalApprovalOnly) throws GeneralResponseMsg {
        List<HND_MunicipalTransaction> transactionList = null;
        if (deliverForExternalApprovalOnly) {
            transactionList = HND_MunicipalTransactionDAO
            .loadRequestsByCurrentActivity(HND_ActivityType.DELIVER_FOR_EXTERNAL_APPROVAL);
        } else {
            transactionList = HND_MunicipalTransactionDAO
            .loadRequestsByCurrentActivity(HND_ActivityType.DELIVER_FOR_EXTERNAL_APPROVAL, HND_ActivityType.EXTERNAL_APPROVAL);
        }
        
        return pendingTrxXml(transactionList);
    }
    
    @Override
    public AdministrativeSourceList getTransactionSources(String presentationId) throws GeneralResponseMsg {
        PresentationIdData pid = new PresentationIdData(presentationId);
        HND_MunicipalTransaction transaction = HND_MunicipalTransactionDAO.loadRequestByPresentationNo(pid.presentationNo);
        
        return createAdministrativeSourceList( transaction.getSources() );
    }
    
    @Override
    public AdministrativeSourceList.AdministrativeSource getTransactionSource(
            String presentationId, String sID) throws GeneralResponseMsg {
        IAdministrativeSource adminSource = getTransactionSourceUtil(presentationId, sID);
        return createAdministrativeSource( adminSource );
    }
    
    @Override
    public UUID getTransactionSourceArchiveId(String presentationId, String sID) throws GeneralResponseMsg {
        IAdministrativeSource adminSource = getTransactionSourceUtil(presentationId, sID);
        IExtArchive extArchive = adminSource.getArchive();
        if (extArchive == null) {
            String msg = String.format("Administrative source [%s] does not have an associated archive", sID);
            System.err.println(msg);
            throw new IllegalStateException(msg);
        }
        
        return extArchive.getsID();
    }
    

    private IAdministrativeSource getTransactionSourceUtil(
            String presentationId, String sID) throws GeneralResponseMsg {
        try {
            PresentationIdData pid = new PresentationIdData(presentationId);
            HND_MunicipalTransaction transaction = HND_MunicipalTransactionDAO.loadRequestByPresentationNo(pid.presentationNo);
            UUID uuid = UUID.fromString(sID);
            
            IAdministrativeSource adminSource = null;
            for (IAdministrativeSource as : transaction.getSources()) {
                if (as.getsID().equals(uuid)) {
                    adminSource = as;
                    break;
                }
            }
            if (adminSource == null) {
                throw new GeneralResponseMsg(
                        String.format("%s [%s]",
                                messageBundle.getProperty("ws.getAdministrativeSource_err"), sID),
                        getGeneralResponseMap().get("ws.bad_administrative_source_code"));
            }
            return adminSource;
        } catch (IllegalArgumentException iae) {
            iae.printStackTrace();
            throw new GeneralResponseMsg(
                    String.format("%s [%s]",
                            messageBundle.getProperty("ws.getAdministrativeSource_err"), sID),
                    getGeneralResponseMap().get("ws.bad_administrative_source_code"));
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException(messageBundle.getProperty("ws.getAdministrativeSource_err"));
        }
    }



    /* (non-Javadoc)
     * @see org.sigit.services.MunicipalTransactionServiceImpl#processTransactionResponse(org.sigit.interop.ws.transaction.types.v1.TransactionResponse)
     */
    @Override
    public void processTransactionResponse(TransactionResponse transactionResponse) throws GeneralResponseMsg {

        String presentationNo = transactionResponse.getPresentationId();
        PresentationIdData pid = new PresentationIdData(presentationNo);
        
        String username = transactionResponse.getEntityName();

        //HND_MunicipalTransaction trx = HND_TransactionDAO.loadRequestByPresentationId(presentationNo);
        HND_MunicipalTransaction transaction = HND_MunicipalTransactionDAO.loadRequestByPresentationNo(pid.presentationNo);
        if (transaction.getCurrentActivity() == HND_ActivityType.EXTERNAL_APPROVAL) {
            //proceed only if state is EXTERNAL_APPROVAL
            //STEP 1: Resume the business process
            UUID trxLogKey = getTrxUnfinishedLogId(pid);
            BusinessProcess.instance().resumeProcess(BPMController.MUNICIPAL_TRANSACTION_WORKFLOW, trxLogKey);
            
            //STEP 2: Set the cadastral keys
            for (ParcelData pd : transactionResponse.getDetails().getParcelData()) {
                Parcel parcel = ParcelDAO.loadParcelAfterTransactionByPresentationNoAndSuID(pid.presentationNo, UUID.fromString(pd.getFeatureId()));
                parcel.setCadastralKey(pd.getNewCadastralKey());
                ParcelDAO.save(parcel);
            }
            
            //STEP 3: Copy from shadow schema to front schema and complete transaction data
            String xmlResponsePayload;
            StringWriter sw = new StringWriter();
            try {
                transactionResponseJAXBContext.createMarshaller().marshal(transactionResponse, sw);
            } catch (JAXBException e1) {
                e1.printStackTrace();
                throw new GeneralResponseMsg( messageBundle.getProperty("ws.processTransactionResponse_err"),
                        getGeneralResponseMap().get("ws.could_not_process_transaction_response") );
            }
            xmlResponsePayload = sw.toString();
            bpmController.endMunicipalTransactionProcess(transaction.getId(), transactionResponse.isApproved(), xmlResponsePayload);

            //STEP 5: Signal the business process. Should end
            signalWorkflow(pid, username);
        }
        else {
            throw new GeneralResponseMsg( messageBundle.getProperty("ws.processTransactionResponse_err"),
                    getGeneralResponseMap().get("ws.illegal_state") );
        }
    }
    
    /* (non-Javadoc)
     * @see org.sigit.services.MunicipalTransactionServiceImpl#processTransactionResponseXML(java.lang.String)
     */
    @Override
    public void processTransactionResponseXML(String transactionResponseXML) throws GeneralResponseMsg {
        try {
            Unmarshaller unmarshaller = transactionResponseJAXBContext.createUnmarshaller();
            TransactionResponse tr = (TransactionResponse) unmarshaller.unmarshal(new StringReader(transactionResponseXML));
            processTransactionResponse(tr);
        } catch (JAXBException e) {
            throw new GeneralResponseMsg( messageBundle.getProperty("ws.processTransactionResponse_err"),
                    getGeneralResponseMap().get("ws.bad_transaction_response_xml") );
        }
    }

    private String getTrxDataXML(String presentationId, boolean changeState, String receivingEntityName) throws GeneralResponseMsg {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(16834);
        
        PresentationIdData pid = new PresentationIdData(presentationId);
        
        try {
            if (getSystemConfiguration().getWorkingNamespace().equalsIgnoreCase(pid.municipalCode) || pid.municipalCode.equals("")) {
                //The presentation code belongs to this municipality
                
                HND_MunicipalTransaction trx = HND_MunicipalTransactionDAO.loadRequestByPresentationNo(pid.presentationNo);
                if (trx == null)
                    throw new GeneralResponseMsg( messageBundle.getProperty("ws.getTrxDataXML_err"),
                            getGeneralResponseMap().get("ws.transaction_not_found") );
    
                TransactionGMLProducer trxGMLProducer = new TransactionGMLProducer(getSystemConfiguration().getWorkingNamespace(), trx);
                trxGMLProducer.produceXml(baos);
                
                if (changeState) {
                    if (trx.getCurrentActivity() == HND_ActivityType.DELIVER_FOR_EXTERNAL_APPROVAL) {
                        signalWorkflow(pid, receivingEntityName);
                    } else {
                        throw new GeneralResponseMsg( messageBundle.getProperty("ws.processTransactionResponse_err"),
                                getGeneralResponseMap().get("ws.illegal_state") );
                    }
                }
            } else {
                //TODO: Route to another municipality
                throw new GeneralResponseMsg("The presentation code does not belong to this municipality.");
            }
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
            throw new GeneralResponseMsg( messageBundle.getProperty("ws.getTrxDataXML_err"),
                    getGeneralResponseMap().get("ws.could_not_produce_xml") );
        }

        return baos.toString();
    }

    private TransactionList pendingTrxXml(List<HND_MunicipalTransaction> transactionList) throws GeneralResponseMsg {
        TransactionList tl = new TransactionList();
        for (HND_MunicipalTransaction hndTrx : transactionList) {
            Transaction t = new Transaction();
            t.setPresentationId(String.format("%s-%s",
                    getSystemConfiguration().getWorkingNamespace(),
                    hndTrx.getPresentationNo()));
            
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(hndTrx.getPresentationDate());
            try {
                t.setPresentationDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(cal));
            } catch (DatatypeConfigurationException e) {
                e.printStackTrace();
                throw new GeneralResponseMsg( messageBundle.getProperty("ws.getTrxDataXML_err"),
                        getGeneralResponseMap().get("ws.could_not_produce_xml") );
            }
            t.setCurrentActivity(hndTrx.getCurrentActivity().toString());
            
            tl.getTransaction().add(t);
        }
        
        return tl;
    }
    
    private AdministrativeSourceList createAdministrativeSourceList(
            Set<LA_AdministrativeSource> set) throws GeneralResponseMsg {
        AdministrativeSourceList resultList = new AdministrativeSourceList();
        
        for (IAdministrativeSource adminSource : set) {
            AdministrativeSourceList.AdministrativeSource as = createAdministrativeSource(adminSource);
            resultList.getAdministrativeSource().add(as);
        }
        
        return resultList;
    }
    
    private AdministrativeSourceList.AdministrativeSource createAdministrativeSource(
            IAdministrativeSource adminSource) throws GeneralResponseMsg {
        AdministrativeSourceList.AdministrativeSource as = new AdministrativeSourceList.AdministrativeSource();
        as.setSID(adminSource.getsID().toString());
        as.setAvailabilityStatus(adminSource.getAvailabilityStatus().toString());
        as.setType(adminSource.getType().toString());

        try {
            GregorianCalendar cal = new GregorianCalendar();
            
            if (adminSource.getAcceptance() != null) {
                cal.setTime(adminSource.getAcceptance());
                as.setAcceptance(DatatypeFactory.newInstance().newXMLGregorianCalendar(cal));
            }
            
            if (adminSource.getRecordation() != null) {
                cal.setTime(adminSource.getRecordation());
                as.setRecordation(DatatypeFactory.newInstance().newXMLGregorianCalendar(cal));
            }
            
            if (adminSource.getSubmission() != null) {
                cal.setTime(adminSource.getSubmission());
                as.setSubmission(DatatypeFactory.newInstance().newXMLGregorianCalendar(cal));
            }
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
            throw new GeneralResponseMsg( messageBundle.getProperty("ws.getTrxDataXML_err"),
                    getGeneralResponseMap().get("ws.could_not_produce_xml") );
        }
        
        return as;
    }

    private void signalWorkflow(PresentationIdData pid, String username) throws GeneralResponseMsg {
        try {
            //Try to resume process without the jurisdiction code
            UUID activityUuid = getTrxUnfinishedLogId(pid);
            BusinessProcessManager.instance().resumeProcess(
                    BPMController.MUNICIPAL_TRANSACTION_WORKFLOW,
                    activityUuid, username);
            BusinessProcessManager.instance().endTask(
                    BPMController.MUNICIPAL_TRANSACTION_WORKFLOW,
                    activityUuid);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            throw new GeneralResponseMsg( messageBundle.getProperty("ws.getTrxDataXML_err"),
                    getGeneralResponseMap().get("ws.bad_entity_name"));
        } catch (NoSuchElementException e) {
            e.printStackTrace();
            throw new GeneralResponseMsg( messageBundle.getProperty("ws.getTrxDataXML_err"),
                    getGeneralResponseMap().get("ws.bad_transaction_id"));
        }
    }

    private UUID getTrxUnfinishedLogId(PresentationIdData pid) {
        HND_TransactionLog trxLog = null;
        for (HND_TransactionLog tl : HND_TransactionLogDAO.loadTransactionLogsByPresentationNo(pid.presentationNo)) {
            if (tl.getDateFinished() != null) {
                trxLog = tl;
            }
        }
        if (trxLog == null) {
            throw new NoSuchElementException("Transaction log entry not found");
        }
        return trxLog.getId();
    }
    
    private static class PresentationIdData {
        private static final String PRESENTATION_ID_SEPARATOR1 = ":";
        private static final String PRESENTATION_ID_SEPARATOR2 = "-";
        
        public final String presentationId;
        public final String municipalCode;
        public final long presentationNo;
        
        public PresentationIdData(String presentationId) throws GeneralResponseMsg {
            this.presentationId = presentationId;
            try {
                if (presentationId.contains(PRESENTATION_ID_SEPARATOR1)) {
                    String[] strs = presentationId.split(PRESENTATION_ID_SEPARATOR1);
                    municipalCode = strs[0];
                    presentationNo = Long.valueOf(strs[1]);
                } else if (presentationId.contains(PRESENTATION_ID_SEPARATOR2)) {
                    String[] strs = presentationId.split(PRESENTATION_ID_SEPARATOR2);
                    municipalCode = strs[0];
                    presentationNo = Long.valueOf(strs[1]);
                } else {
                    municipalCode = "";
                    presentationNo = Long.valueOf(presentationId);
                }
            }
            catch (NumberFormatException e) {
                throw new GeneralResponseMsg( messageBundle.getProperty("ws.processTransactionResponse_err"),
                        getGeneralResponseMap().get("ws.bad_transaction_id") );
            }
        }

        public String getPresentationId() {
            return presentationId;
        }
    }
    
    
    private static Map<String, GeneralResponse> generalResponseMap;
    private static org.sigit.interop.ws.commons.types.ObjectFactory commonsTypesOF = new org.sigit.interop.ws.commons.types.ObjectFactory();
    
    private static Map<String, GeneralResponse> getGeneralResponseMap() {
        if (generalResponseMap == null) {
            generalResponseMap = new ConcurrentHashMap<String, GeneralResponse>();
            
            generalResponseMap.put("ws.bad_entity_name", commonsTypesOF.createGeneralResponse("SIGIT-TXN-01", messageBundle.getProperty("ws.bad_entity_name")));
            generalResponseMap.put("ws.bad_administrative_source_code", commonsTypesOF.createGeneralResponse("SIGIT-TXN-01", messageBundle.getProperty("ws.bad_administrative_source_code")));
            generalResponseMap.put("ws.bad_transaction_id", commonsTypesOF.createGeneralResponse("SIGIT-TXN-01", messageBundle.getProperty("ws.bad_transaction_id")));
            generalResponseMap.put("ws.illegal_state", commonsTypesOF.createGeneralResponse("SIGIT-TXN-02", messageBundle.getProperty("ws.illegal_state")));
            generalResponseMap.put("ws.no_next_deliver_for_external_approval", commonsTypesOF.createGeneralResponse("SIGIT-TXN-03", messageBundle.getProperty("ws.no_next_deliver_for_external_approval")));
            generalResponseMap.put("ws.transaction_not_found", commonsTypesOF.createGeneralResponse("SIGIT-TXN-04", messageBundle.getProperty("ws.transaction_not_found")));
            generalResponseMap.put("ws.could_not_produce_xml", commonsTypesOF.createGeneralResponse("SIGIT-TXN-05", messageBundle.getProperty("ws.could_not_produce_xml")));
            generalResponseMap.put("ws.could_not_process_transaction_response", commonsTypesOF.createGeneralResponse("SIGIT-TXN-06", messageBundle.getProperty("ws.could_not_process_transaction_response")));
            generalResponseMap.put("ws.could_not_resume_transaction_process", commonsTypesOF.createGeneralResponse("SIGIT-TXN-07", messageBundle.getProperty("ws.could_not_resume_transaction_process")));
            generalResponseMap.put("ws.bad_transaction_response_xml", commonsTypesOF.createGeneralResponse("SIGIT-TXN-08", messageBundle.getProperty("bad_transaction_response_xml")));
        }
        return generalResponseMap;
    }
}
