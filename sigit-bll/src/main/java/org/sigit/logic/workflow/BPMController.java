package org.sigit.logic.workflow;

import java.util.UUID;

public interface BPMController {
    String MUNICIPAL_TRANSACTION_WORKFLOW = "transactionWorkflow";
    
    void endMunicipalTransactionProcess(UUID transactionId, boolean approved, String responseData);
}
