package org.sigit.logic.workflow;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.HashMap;
import java.util.UUID;

import org.sigit.dao.ladm.external.ExtArchiveDAO;
import org.sigit.model.ladm.external.ExtArchive;
import org.sigit.util.AppOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component("documentViewer")
@Scope("session")
public class DocumentViewer implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private static HashMap<String, String> mimeMap = new HashMap<String, String>();
    private static final String downloadMIME = "application/x-download";
    
    private UUID archiveId;
    private ExtArchive archive;
    private byte[] file2ViewBinaryData;
    private String appropriateMIME;
    private boolean forceDownload = false;
    private AttachedFileData fileData;
    
    @Autowired
    private AppOptions appOptions;
    
    @Autowired
    private ArchiveRepository archiveRepository;

    static {
        mimeMap.put(".doc", "application/msword");
        mimeMap.put(".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        mimeMap.put(".jpeg", "image/jpeg");
        mimeMap.put(".jpg", "image/jpeg");
        mimeMap.put(".pdf", "application/pdf");
        mimeMap.put(".png", "image/png");
        mimeMap.put(".tif", "image/tiff");
        mimeMap.put(".tiff", "image/tiff");
        mimeMap.put(".xls", "application/vnd.ms-excel");
        mimeMap.put(".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    }
    
    public UUID getArchiveId() {
        return archiveId;
    }

    @Transactional
    public void setArchiveId(UUID archiveId) {
        if (archiveId == null) {
            return;
        }
        if (this.archiveId == null || archive == null || !this.archiveId.equals(archiveId)) {
            this.archiveId = archiveId;

            archive = ExtArchiveDAO.loadExtArchiveByID(this.archiveId);
            file2ViewBinaryData = null;
            appropriateMIME = null;
        }
    }
    
    
    public byte[] getFile2ViewBinaryData() {
        if (file2ViewBinaryData == null) {
            if (archive != null) {
                if (archive.isExternal()) {
                    RandomAccessFile raf = null;
                    try {
                        String fname = new String( archive.getData() );
                        File file = new File(appOptions.getUploadFileSavePath() + '/' + fname);
                        raf = new RandomAccessFile(file, "r");
                        file2ViewBinaryData = new byte[(int)raf.length()];
    
                        int bytesRead = 0;
                        while (bytesRead < file2ViewBinaryData.length) {
                            int bread = raf.read(file2ViewBinaryData, bytesRead, file2ViewBinaryData.length - bytesRead);
                            if (bread == -1) break;
                            bytesRead += bread;
                        }
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } finally {
                        if (raf != null) {
                            try {
                                raf.close();
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    file2ViewBinaryData = archiveRepository.getArchiveData(archive);
                }
            } else {
                file2ViewBinaryData = fileData.getData();
            }
        }
        return file2ViewBinaryData;
    }
    
    
    public ExtArchive getArchive() {
        return archive;
    }
    
    public void setArchive(ExtArchive archive) {
        this.archive = archive;
    }
    
    public AttachedFileData getFileData() {
        return fileData;
    }
    public void setFileData(AttachedFileData fileData) {
        if (this.fileData != fileData) {
            this.fileData = fileData;
            this.archive = (ExtArchive)fileData.getArchive();
            this.file2ViewBinaryData = null;
            this.appropriateMIME = null;
        }
    }
    
    public String getAppropriateMIME() {
        if (forceDownload)
            return downloadMIME;
        
        if (appropriateMIME == null) {
            String fileName = getFileData().getFileName();
            String fileExt = fileName.substring(fileName.lastIndexOf('.')).toLowerCase();
            
            if (!mimeMap.containsKey(fileExt))
                return downloadMIME;
            else
                appropriateMIME = mimeMap.get(fileExt); 
        }
        return appropriateMIME;
    }
    
    
    public boolean isForceDownload() {
        return forceDownload;
    }
    public void setForceDownload(boolean forceDownload) {
        this.forceDownload = forceDownload;
    }
    
    
    public void downloadFile() {
        /*
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletResponse response = (HttpServletResponse) externalContext
                .getResponse();

        response.reset();
        
        response.setContentType(getAppropriateMIME());
        
        response.setHeader("Content-disposition",
                "attachment; filename=\"shape.zip\"");

        BufferedOutputStream output = new BufferedOutputStream(out);

        try {
            writeZipFile(shapeFiles, output);
        } finally {
            output.close();
        }
        */
    }
    
    public void viewFile(OutputStream out, Object data) {
        byte[] bytes = getFile2ViewBinaryData();
        if (bytes == null || bytes.length == 0) return;
        
        BufferedOutputStream output = new BufferedOutputStream(out);

        try {
            output.write(bytes);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                output.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
        /*
         * TODO: Ver si se utiliza en lectura por Stream desde campos LOB
        final int BUFFER = 4192;
        
        byte[] buff = new byte[BUFFER];
        BufferedInputStream origin = null;

        try {
            FileInputStream fis = new FileInputStream(file2View);
            origin = new BufferedInputStream(fis, BUFFER);
            
            int count;
            while ((count = origin.read(buff, 0, BUFFER)) != -1)
                output.write(buff, 0, count);
        }
        catch (FileNotFoundException fnfe) {
            return;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (null != origin) origin.close();
                output.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        */
    }
    
}
