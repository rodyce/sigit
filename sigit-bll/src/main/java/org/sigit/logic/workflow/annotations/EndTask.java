package org.sigit.logic.workflow.annotations;

public @interface EndTask {
    
    String transition() default "";
    
}
