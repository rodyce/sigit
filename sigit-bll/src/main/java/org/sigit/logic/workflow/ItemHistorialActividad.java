package org.sigit.logic.workflow;

import java.util.Date;

public class ItemHistorialActividad {
    private String tarea;
    private String actor;
    private Date fechaAsignado;
    private Date fechaFinalizado;
    
    
    public String getTarea() {
        return tarea;
    }
    public void setTarea(String tarea) {
        this.tarea = tarea;
    }
    public String getActor() {
        return actor;
    }
    public void setActor(String actor) {
        this.actor = actor;
    }
    public Date getFechaAsignado() {
        return fechaAsignado;
    }
    public void setFechaAsignado(Date fechaAsignado) {
        this.fechaAsignado = fechaAsignado;
    }
    public Date getFechaFinalizado() {
        return fechaFinalizado;
    }
    public void setFechaFinalizado(Date fechaFinalizado) {
        this.fechaFinalizado = fechaFinalizado;
    }
}
