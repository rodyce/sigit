package org.sigit.logic.workflow;

import java.io.File;
import java.io.Serializable;
import java.util.UUID;

import org.sigit.model.commons.IExtArchive;
import org.sigit.model.ladm.administrative.LA_AdministrativeSourceType;

public class AttachedFileData implements Serializable {
    private static final long serialVersionUID = 7391954314733540565L;

    transient private File file;
    
    private UUID id;
    private UUID sourceId;
    private String fileName;
    private String fullPath;
    private String descripcion;
    private String crc;
    private LA_AdministrativeSourceType adminSourceType;
    private IExtArchive archive;
    private byte[] data;
    
    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }
    
    public UUID getSourceId() {
        return sourceId;
    }
    public void setSourceId(UUID sourceId) {
        this.sourceId = sourceId;
    }
    
    public File getFile() {
        return file;
    }
    public void setFile(File file) {
        this.file = file;
    }
    
    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    public String getFullPath() {
        return fullPath;
    }
    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }
    
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String getCrc() {
        return crc;
    }
    public void setCrc(String crc) {
        this.crc = crc;
    }
    
    public LA_AdministrativeSourceType getAdminSourceType() {
        return adminSourceType;
    }
    public void setAdminSourceType(LA_AdministrativeSourceType adminSourceType) {
        this.adminSourceType = adminSourceType;
    }
    
    public IExtArchive getArchive() {
        return archive;
    }
    public void setArchive(IExtArchive archive) {
        this.archive = archive;
    }
    
    public byte[] getData() {
        return data;
    }
    public void setData(byte[] data) {
        this.data = data;
    }
}
