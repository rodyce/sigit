package org.sigit.logic.workflow;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.sigit.commons.di.CtxComponent;
import org.sigit.logic.security.IAuthenticator;
import org.sigit.model.hnd.administrative.HND_Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;


@Service
@Scope("sigit-conversation")
public class BusinessProcess implements Serializable {
    private static final long serialVersionUID = 1L;

    @Autowired
    private IAuthenticator authenticator;
    
    @Autowired
    private BusinessProcessManager bpManager;
    
    private UUID currentActivityId;
    private String processDefinition;
    
    public static BusinessProcess instance() {
        return CtxComponent.getInstance(BusinessProcess.class);
    }
    
    @Transactional
    public boolean resumeProcess(String processDefinition, UUID trxLogKey) {
        try {
            bpManager.resumeProcess(
                    processDefinition, trxLogKey, authenticator.getCredentials().getUsername());
            this.currentActivityId = trxLogKey;
            this.processDefinition = processDefinition;
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
    
    @Transactional
    public void createProcess(final String processDefinition, final HND_Transaction transaction) {
        bpManager.createProcess(processDefinition, transaction);
        this.processDefinition = processDefinition;
    }
    
    @Transactional
    public void endTask() {
        endTask("");
    }
    @Transactional
    public void endTask(String transition) {
        bpManager.endTask(processDefinition, currentActivityId, transition);
    }
    
    @Transactional
    public List<Task> getTaskList(String processName) {
        return bpManager.getTaskList(processName, authenticator.getCredentials().getUsername());
    }
    
    @Transactional
    public List<Task> getPooledTaskList(String processDefinition) {
        return bpManager.getPooledTaskList(processDefinition, authenticator.getCredentials().getUserRoleTypeSet());
    }
    
    @Transactional
    public void assignTaskToCurrentUser(Task task) {
        bpManager.assignTaskToUser(task, authenticator.getCredentials().getUsername());
    }
}
