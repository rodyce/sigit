package org.sigit.logic.workflow.annotations;

public @interface CreateProcess {
    
    String definition();
    
    String processKey();
    
}
