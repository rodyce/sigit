package org.sigit.logic.workflow;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.hibernate.HibernateException;
import org.sigit.dao.hnd.special.ArchiveInternalRepositoryDAO;
import org.sigit.dao.ladm.external.ExtArchiveDAO;
import org.sigit.model.commons.IExtArchive;
import org.sigit.model.ladm.administrative.LA_AdministrativeSource;
import org.sigit.model.ladm.administrative.LA_AvailabilityStatusType;
import org.sigit.model.ladm.external.ExtArchive;
import org.sigit.util.AppOptions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("archiveRepository")
public class ArchiveRepository {
    private static HashMap<String, String> mimeMap = new HashMap<String, String>();
    private static final String downloadMIME = "application/x-download";

    static {
        mimeMap.put(".doc", "application/msword");
        mimeMap.put(".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        mimeMap.put(".jpeg", "image/jpeg");
        mimeMap.put(".jpg", "image/jpeg");
        mimeMap.put(".pdf", "application/pdf");
        mimeMap.put(".png", "image/png");
        mimeMap.put(".tif", "image/tiff");
        mimeMap.put(".tiff", "image/tiff");
        mimeMap.put(".xls", "application/vnd.ms-excel");
        mimeMap.put(".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    }

    
    @Transactional
    public LA_AdministrativeSource addToRepository(AppOptions appOptions, AttachedFileData afd, boolean external) {
        if (external) {
            //TODO Add support for external file repository
            //return addToRepositoryExternal(appOptions, afd);
            throw new IllegalStateException("External repository in maintenance mode");
        }
        
        return addToRepositoryInternal(appOptions, afd);
    }

    @Transactional(readOnly=true)
    public List<String> getArchiveIds() {
        List<ExtArchive> archives = ExtArchiveDAO.loadAll();
        List<String> uuids = new ArrayList<String>();
        
        for (ExtArchive archive : archives) {
            uuids.add(archive.getsID().toString());
        }
        
        return uuids;
    }

    @Transactional
    public String getArchiveName(UUID uuid) {
        ExtArchive archive = ExtArchiveDAO.loadExtArchiveByID(uuid);
        if (archive == null) {
            throw new NoSuchElementException("archive not found");
        }
        return getArchiveName(archive);
    }
    public String getArchiveName(IExtArchive extArchive) {
        return extArchive.getName();
    }

    @Transactional
    public byte[] getArchiveData(UUID uuid) {
        ExtArchive archive = ExtArchiveDAO.loadExtArchiveByID(uuid);
        if (archive == null) {
            throw new NoSuchElementException("archive not found");
        }
        return getArchiveData(archive);
    }
    @Transactional
    public byte[] getArchiveData(IExtArchive extArchive) {
        byte[] file2ViewBinaryData = null;
        if (extArchive.isExternal()) {
            RandomAccessFile raf = null;
            try {
                //FIXME
                String fname = new String( extArchive.getData() );
                File file = new File(/*TODO appOptions.getUploadFileSavePath() + '/' + */fname);
                raf = new RandomAccessFile(file, "r");
                file2ViewBinaryData = new byte[(int)raf.length()];

                int bytesRead = 0;
                while (bytesRead < file2ViewBinaryData.length) {
                    int bread = raf.read(file2ViewBinaryData, bytesRead, file2ViewBinaryData.length - bytesRead);
                    if (bread == -1) break;
                    bytesRead += bread;
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                if (raf != null)
                {
                    try {
                        raf.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        } else {
            //It is an internal file
            if (extArchive.getData() == null) {
                //This means that the archive contents is in the internal repository
                try {
                    file2ViewBinaryData = ArchiveInternalRepositoryDAO.loadBytesByExtArchive(
                            (ExtArchive)extArchive);
                } catch (HibernateException he) {
                    throw new IllegalStateException("Archive contents could not be retrived or non-existent");
                }
            } else {
                file2ViewBinaryData = extArchive.getData();
            }
        }

        return file2ViewBinaryData;
    }
    
    public String getAppropriateMIME(String fileName, boolean isDownload) {
        if (isDownload)
        {
            return downloadMIME;
        }
        
        String fileExt = fileName.substring(fileName.lastIndexOf('.')).toLowerCase();
        
        if (!mimeMap.containsKey(fileExt)) {
            return downloadMIME;
        }
        return mimeMap.get(fileExt);
    }

    
    private static LA_AdministrativeSource addToRepositoryInternal(AppOptions appOptions, AttachedFileData afd) {
        LA_AdministrativeSource newAdminSource = null;
        
        if (afd.getData() != null) {
            newAdminSource = newArchiveAdminSource(afd, afd.getData(), false);
        } else {
            File fIn = afd.getFile();
            if (fIn != null) {
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(fIn);
                    byte[] bytes = new byte[(int)fIn.length()];
                    int bytesRead = 0;
                    while (bytesRead < bytes.length) {
                        int bread = fis.read(bytes, bytesRead, bytes.length - bytesRead);
                        if (bread == -1) break;
                        bytesRead += bread;
                    }
                    newAdminSource = newArchiveAdminSource(afd, bytes, false);
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } finally {
                    if (fis != null)
                        try {
                            fis.close();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    
                    afd.getFile().delete();
                }
            }
        }
        
        return newAdminSource;
    }
    
    private static LA_AdministrativeSource addToRepositoryExternal(AppOptions appOptions, AttachedFileData afd) {
        LA_AdministrativeSource newAdminSource = null;
        
        File fIn, fOut, fRepoDir, fOutDir;
        fIn = afd.getFile();
        if (fIn != null) {
            fRepoDir = new File(appOptions.getUploadFileSavePath());
            
            int nextFileIdx = countFilesInDir(fRepoDir) + 1;
            fOutDir = new File(appOptions.getUploadFileSavePath());
            
            if (!fOutDir.exists()) fOutDir.mkdirs();
            
            fOut = new File(fOutDir.getAbsolutePath() + '/' + String.format("%08d", nextFileIdx) + ".DAT");
            
            //TODO: DEFINIR POLITICA DE MANEJOR DE EXCEPCIONES
            try {
                copyFile(new FileInputStream(fIn), new FileOutputStream(fOut));
                afd.getFile().delete();
                afd.setFullPath(fOut.getCanonicalPath());
                newAdminSource = newArchiveAdminSource(afd, fOut.getName().getBytes(), true);
            }
            catch (FileNotFoundException fnfe) {
                FacesContext.getCurrentInstance().addMessage("oops",
                        new FacesMessage("No se encontro un archivo"));
            }
            catch (IOException ioe) {
                FacesContext.getCurrentInstance().addMessage("oops",
                        new FacesMessage("No se pudo adjuntar uno o mas archivos"));
            }
        }

        
        return newAdminSource;
    }
    
    private static LA_AdministrativeSource newArchiveAdminSource(AttachedFileData afd, byte[] fileData, boolean external) {
        Date today = new Date();

        LA_AdministrativeSource newAdminSource = new LA_AdministrativeSource();
        newAdminSource.setAvailabilityStatus(LA_AvailabilityStatusType.ARCHIVE_CONVERTED);
        newAdminSource.setType(afd.getAdminSourceType());
        newAdminSource.setRecordation(today);
        newAdminSource.setSubmission(today);
        
        ExtArchive extArchive = new ExtArchive();
        extArchive.setAcceptance(today);
        extArchive.setExternal(external);
        extArchive.setRecordation(today);
        extArchive.setSubmission(today);
        extArchive.setName(afd.getFileName());
        if (external) {
            //TODO implement
            //extArchive.setData(fileData);
        }
        extArchive.setDescription(afd.getDescripcion());
        
        newAdminSource.setArchive(extArchive);
        
        ArchiveInternalRepositoryDAO.saveData(extArchive, fileData);
        
        return newAdminSource;
    }
    
    private static void copyFile(FileInputStream sourceStream,
            FileOutputStream destinationStream) throws IOException {
        FileChannel inChannel = sourceStream.getChannel();
        FileChannel outChannel = destinationStream.getChannel();
        try {
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } catch (IOException e) {
            throw e;
        } finally {
            if (inChannel != null) {
                try {
                    inChannel.close();
                } catch (IOException ioe) {
                }
            }
            if (outChannel != null)
                outChannel.close();
        }
    }
    
    private static int countFilesInDir(File dir) {
        int count = 0;
        synchronized (ArchiveRepository.class) {
            if (dir.isDirectory()) {
                
                for (File file : dir.listFiles())
                    if (file.isFile())
                        count++;
            }
        }
        
        return count;
    }


}
