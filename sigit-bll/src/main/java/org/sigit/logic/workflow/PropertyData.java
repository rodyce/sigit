package org.sigit.logic.workflow;

import org.sigit.model.ladm.administrative.LA_BAUnitType;

import java.io.Serializable;

public class PropertyData implements Serializable {
    private static final long serialVersionUID = 8005440925799574243L;

    private long baUnitID;
    private String propertyCode;
    private String name;
    private LA_BAUnitType type;
    
    
    public long getBaUnitID() {
        return baUnitID;
    }
    public void setBaUnitID(long baUnitID) {
        this.baUnitID = baUnitID;
    }
    
    public String getPropertyCode() {
        return propertyCode;
    }
    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    public LA_BAUnitType getType() {
        return type;
    }
    public void setType(LA_BAUnitType type) {
        this.type = type;
    }
}
