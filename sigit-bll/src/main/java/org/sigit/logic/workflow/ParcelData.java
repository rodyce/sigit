package org.sigit.logic.workflow;

import java.io.Serializable;

public class ParcelData implements Serializable {
    private static final long serialVersionUID = -6283437529594893025L;

    private String suID;
    private String label;
    private Long fieldTab;
    private String municipalKey;
    private String cadastralKey;
    
    
    public String getSuID() {
        return suID;
    }
    public void setSuID(String suID) {
        this.suID = suID;
    }
    
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    
    public Long getFieldTab() {
        return fieldTab;
    }
    public void setFieldTab(Long fieldTab) {
        this.fieldTab = fieldTab;
    }
    
    public String getMunicipalKey() {
        return municipalKey;
    }
    public void setMunicipalKey(String municipalKey) {
        this.municipalKey = municipalKey;
    }
    
    public String getCadastralKey() {
        return cadastralKey;
    }
    public void setCadastralKey(String cadastralKey) {
        this.cadastralKey = cadastralKey;
    }
}
