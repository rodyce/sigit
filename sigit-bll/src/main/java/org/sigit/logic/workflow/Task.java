package org.sigit.logic.workflow;

import java.util.Date;
import java.util.UUID;

public class Task {
    private UUID id;
    private UUID transactionId;
    private Long presentationNo;
    private String name;
    private String description;
    private Date create;
    private Date start;
    private Date dueDate;
    private Date end;
    private boolean isCancelled;
    

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }
    
    public UUID getTransactionId() {
        return transactionId;
    }
    public void setTransactionId(UUID transactionId) {
        this.transactionId = transactionId;
    }
    
    public Long getPresentationNo() {
        return presentationNo;
    }
    public void setPresentationNo(Long presentationNo) {
        this.presentationNo = presentationNo;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDueDate() {
        return dueDate;
    }
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getEnd() {
        return end;
    }
    public void setEnd(Date end) {
        this.end = end;
    }

    public Date getCreate() {
        return create;
    }
    public void setCreate(Date create) {
        this.create = create;
    }

    public Date getStart() {
        return start;
    }
    public void setStart(Date start) {
        this.start = start;
    }

    public boolean isCancelled() {
        return isCancelled;
    }
}
