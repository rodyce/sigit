package org.sigit.logic.workflow;

public enum ActorType {
    RECEPTIONIST,
    
    ANALYST,
    
    EDITOR,
    
    APPROVER,
    
    ANALYST_OR_APPROVER
}
