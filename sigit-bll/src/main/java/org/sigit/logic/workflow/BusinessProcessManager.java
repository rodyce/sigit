package org.sigit.logic.workflow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.UUID;

import javax.transaction.Transactional;

import org.sigit.commons.di.CtxComponent;
import org.sigit.dao.SigitDAO;
import org.sigit.dao.hnd.administrative.HND_TransactionLogDAO;
import org.sigit.dao.hnd.administrative.HND_UserDAO;
import org.sigit.logic.workflow.ProcessConfig.Transition;
import org.sigit.logic.workflow.ProcessConfig.TransitionType;
import org.sigit.model.hnd.administrative.HND_ActivityType;
import org.sigit.model.hnd.administrative.HND_MunicipalTransaction;
import org.sigit.model.hnd.administrative.HND_Transaction;
import org.sigit.model.hnd.administrative.HND_TransactionLog;
import org.sigit.model.hnd.administrative.HND_TransactionType;
import org.sigit.model.hnd.administrative.HND_User;
import org.sigit.model.hnd.administrative.HND_UserRoleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;


@Service
@Scope("singleton")
public class BusinessProcessManager implements Serializable {
    private static final long serialVersionUID = 1L;

    @Autowired
    private ProcessConfig processConfig;
    
    public static BusinessProcessManager instance() {
        return CtxComponent.getInstance(BusinessProcessManager.class);
    }
    
    @Transactional
    public void resumeProcess(String processDefinition, UUID trxLogKey, String userName) {
        if (userName == null) {
            throw new IllegalArgumentException("Username is required to resume a process");
        }
        
        HND_User hndUser = HND_UserDAO.loadUser(userName);
        if (hndUser == null) {
            throw new IllegalArgumentException(String.format("Invalid username '%s'", userName));
        }

        HND_TransactionLog trxLog = HND_TransactionLogDAO.loadTransactionLogByID(trxLogKey);
        if (trxLog == null) {
            throw new NoSuchElementException(
                    String.format("No current activity for process %s with key %s and username %s",
                            processDefinition, trxLogKey, userName));
        }

        //if activity is not claimed then make it be
        if (trxLog.getDateClaimed() == null) {
            trxLog.setDateClaimed(new Date());
            trxLog.setClaimingUser(hndUser);
            HND_TransactionLogDAO.save(trxLog);
        }
    }
    
    @Transactional
    public void createProcess(final String processDefinition, final HND_Transaction transaction) {
        Date date = new Date();
        for (HND_ActivityType activity : processConfig.getStartStates(processDefinition)) {
            HND_TransactionLog trxLog = newTrxLog(activity, date, transaction,
                    processConfig.getTransactionType(processDefinition));
            HND_TransactionLogDAO.save(trxLog);
        }
    }
    
    @Transactional
    public void endTask(String processDefinition, UUID currentActivityId) {
        endTask(processDefinition, currentActivityId, "");
    }
    @Transactional
    public void endTask(String processDefinition, UUID currentActivityId, String transition) {
        //STEP 1: Finish current activity
        HND_TransactionLog trxLog = HND_TransactionLogDAO.loadTransactionLogByID(currentActivityId);
        if (trxLog == null) {
            throw new IllegalStateException("No current activity");
        }
        
        Date today = new Date();
        trxLog.setDateFinished(today);
        HND_TransactionLogDAO.save(trxLog);
        
        //STEP 2: Start all successor activities that has all its joined-member predecessors completed
        Set<Transition> allTransitions = processConfig.getSuccessors(processDefinition, trxLog.getActivity());
        TransitionType trType = TransitionType.NORMAL;
        for (Transition tr : allTransitions) {
            if (transition.equals(tr.getName())) {
                trType = tr.getTransitionType();
                break;
            }
        }
        switch (trType) {
        case NORMAL:
            for (Transition tr : allTransitions) {
                if (tr.getName().equals(transition) && tr.getTransitionType() == TransitionType.NORMAL) {
                    HND_Transaction trx = trxLog.getTransaction();
                    //If the transaction is a Municipal Transaction, then apply the current activity type.
                    //This only applies to Municipal Transaction.
                    if (trx instanceof HND_MunicipalTransaction) {
                        //Apply the state if the transaction's end
                        ((HND_MunicipalTransaction)trx).setCurrentActivity(tr.getEnd());
                        SigitDAO.save(trx);
                    }
                    HND_TransactionLogDAO.save(newTrxLog(tr.getEnd(), today, trxLog.getTransaction(),
                            processConfig.getTransactionType(processDefinition)));
                    break;
                }
            }
            break;
        case FORK_MEMBER:
            //Start all activities reached from all forked transitions
            //Here we do not check if target activity has multiple joined predecessors
            for (Transition tr : allTransitions) {
                if (tr.getName().equals(transition) && tr.getTransitionType() == TransitionType.FORK_MEMBER) {
                    HND_TransactionLog forkedTrxLog = newTrxLog(tr.getEnd(), today, trxLog.getTransaction(),
                            processConfig.getTransactionType(processDefinition));
                    HND_TransactionLogDAO.save(forkedTrxLog);
                }
            }
            break;
        case JOIN_MEMBER:
            //Start activity only if all activities reachable backwards
            //from joined transitions are complete
            //step 1: collect all transitions whose end is at the target activity
            Transition joinedTransition = null;
            for (Transition tr : allTransitions) {
                if (tr.getName().equals(transition) && tr.getTransitionType() == TransitionType.JOIN_MEMBER) {
                    joinedTransition = tr;
                }
            }
            if (joinedTransition != null) {
                Set<Transition> allPredecessors = processConfig.getPredecessors(
                        processDefinition, joinedTransition.getEnd());
                boolean doCreate = true;
                HND_ActivityType[] predActivities = new HND_ActivityType[allPredecessors.size()];
                int i = 0;
                for (Transition t : allPredecessors) {
                    predActivities[i++] = t.getStart();
                }
                
                //check all predecessors are completed
                List<HND_TransactionLog> predTrxLogs = HND_TransactionLogDAO
                        .loadTransactionLogsByTransactionAndType(trxLog.getTransaction(), predActivities);
                for (HND_TransactionLog trLog : predTrxLogs) {
                    if (trLog.getDateFinished() == null) {
                        //unfinished activity
                        doCreate = false;
                        break;
                    }
                }
                
                if (doCreate) {
                    HND_TransactionLogDAO.save(newTrxLog(
                            joinedTransition.getEnd(), today, trxLog.getTransaction(),
                            processConfig.getTransactionType(processDefinition)));
                }
            }
            break;
        }
    }
    
    @Transactional
    public List<Task> getTaskList(String processName, String userName) {
        List<Task> taskList = new ArrayList<>();
        for (HND_TransactionLog trxLog : HND_TransactionLogDAO
                .loadClaimedTransactionLogsByUsernameAndTransactionType(userName,
                processConfig.getTransactionType(processName))) {
            taskList.add(trxLog2Task(trxLog));
        }
        
        return taskList;
    }
    
    @Transactional
    public List<Task> getPooledTaskList(String processDefinition, Set<HND_UserRoleType> userRoleTypes) {
        List<Task> taskList = new ArrayList<>();
        Set<HND_ActivityType> userActivitySet = new HashSet<>();
        for (HND_UserRoleType role : userRoleTypes) {
            userActivitySet.addAll(processConfig.getActivitiesByUserRole(processDefinition, role));
        }
        
        for (HND_TransactionLog trxLog : HND_TransactionLogDAO.loadUnclaimedTransactionLogsByTransactionType(
                processConfig.getTransactionType(processDefinition))) {
            if (userActivitySet.contains(trxLog.getActivity())) {
                taskList.add(trxLog2Task(trxLog));
            }
        }

        return taskList;
    }
    
    @Transactional
    public void assignTaskToUser(Task task, String userName) {
        Date today = new Date();
        HND_TransactionLog trxLog = HND_TransactionLogDAO.loadTransactionLogByID(task.getId());
        trxLog.setDateClaimed(today);
        HND_User user = HND_UserDAO.loadUser(userName);
        trxLog.setClaimingUser(user);
        
        HND_TransactionLogDAO.save(trxLog);
    }
    
    private HND_TransactionLog newTrxLog(HND_ActivityType activity, Date date,
            HND_Transaction transaction, HND_TransactionType transactionType) {
        HND_TransactionLog trxLog = new HND_TransactionLog();
        trxLog.setActivity(activity); //initial activity for this type
        trxLog.setDateActivity(date);
        trxLog.setTransactionType(transactionType);
        trxLog.setTransaction(transaction);
        
        return trxLog;
    }
    
    
    private Task trxLog2Task(HND_TransactionLog trxLog) {
        Task task = new Task();
        task.setId(trxLog.getId());
        task.setTransactionId(trxLog.getTransaction().getId());
        task.setPresentationNo(trxLog.getTransaction().getPresentationNo());
        task.setName(trxLog.getActivity().toString());
        task.setDescription(trxLog.getActivity().toString());
        task.setCreate(trxLog.getDateActivity());
        
        return task;
    }
}
