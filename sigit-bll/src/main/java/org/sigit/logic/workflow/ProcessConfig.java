package org.sigit.logic.workflow;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.sigit.commons.di.CtxComponent;
import org.sigit.model.hnd.administrative.HND_ActivityType;
import org.sigit.model.hnd.administrative.HND_TransactionType;
import org.sigit.model.hnd.administrative.HND_UserRoleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

@Component("processConfig")
@Scope("singleton")
public class ProcessConfig implements Serializable {
    private static final long serialVersionUID = 1L;
    
    public static final String TRANSACTION_WORKFLOW = "transactionWorkflow";
    public static final String OPERATION_PERMIT_PROCESS = "operationPermitWorkflow";
    public static final String BUILDING_PERMIT_PROCESS = "buildingPermitWorkflow";
    public static final String TOPOGRAPHIC_MAINTENANCE_PROCESS = "topographicMaintenanceWorkflow";

    
    @Autowired
    private ServletContext servletContext;
    
    private File configFile;
    DocumentBuilderFactory dbf;
    DocumentBuilder db;
    Document doc;
    String appName;
    
    private Map<String, String> taskToView;
    private Map<String, HND_TransactionType> processDefToTrxType;
    private Map<String, HND_ActivityType[]> processDefStartSates;
    private Map<String, Set<Transition>> transitionSets;
    private Map<String, Map<HND_UserRoleType, Set<HND_ActivityType>>> processUserRoleActivities;
    
    
    private static String createKey(String appName, String processName, String taskName) {
        return appName + "$$$" + processName + "$$$" + taskName;
    }
    
    public static ProcessConfig instance() {
        return CtxComponent.getInstance(ProcessConfig.class);
    }
    
    @PostConstruct
    public void init() {
        configFile = new File(servletContext.getRealPath("/WEB-INF/process-view-map.xml"));
        taskToView = Collections.synchronizedMap(new HashMap<String, String>());
        processDefToTrxType = Collections.synchronizedMap(new HashMap<String, HND_TransactionType>());
        appName = servletContext.getServletContextName();
        String processName;
        
        dbf = DocumentBuilderFactory.newInstance();
        
        try {
            db = dbf.newDocumentBuilder();
            doc = db.parse(configFile);
            doc.getDocumentElement().normalize();
            NodeList processList = doc.getElementsByTagName("process");
            for (int p = 0; p < processList.getLength(); p++) {
                Node processNode = processList.item(p);
                if (processNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element processElem = (Element) processNode;
                    
                    Node nameNode = processElem.getElementsByTagName("name").item(0);
                    Node trxTypeNode = processElem.getElementsByTagName("type").item(0);
                    Node mappingsNode = processElem.getElementsByTagName("mappings").item(0);
                    
                    processName = nameNode.getFirstChild().getNodeValue();
                    processDefToTrxType.put(processName, HND_TransactionType.valueOf(
                            trxTypeNode.getFirstChild().getNodeValue()));
                    
                    NodeList mappingsList = mappingsNode.getChildNodes();
                    for (int m = 0; m < mappingsList.getLength(); m++) {
                        Node mappingNode = mappingsList.item(m);
                        if (mappingNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element mappingElem = (Element) mappingNode;
                            
                            Node taskNameNode = mappingElem.getElementsByTagName("task-name").item(0);
                            String taskName = taskNameNode.getFirstChild().getNodeValue();
                            
                            Node viewIdNode = mappingElem.getElementsByTagName("view-id").item(0);
                            String viewId = viewIdNode.getFirstChild().getNodeValue();
                            
                            taskToView.put(createKey(appName, processName, taskName), viewId);
                        }
                    }
                }
            }
            
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
            System.err.println("ERROR de configuracion del parser XML");
        } catch (SAXException e) {
            e.printStackTrace();
            System.err.println("ERROR al parsear documento XML");
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("ERROR al abrir documento XML, process-view-map.xml");
        }
        
        //hardcoded process definitions for the 3 workflows 
        //configure start states for each workflow
        processDefStartSates = new HashMap<>();
        processDefStartSates.put(TRANSACTION_WORKFLOW, new HND_ActivityType[]
                {HND_ActivityType.PRESENTATION});
        processDefStartSates.put(BUILDING_PERMIT_PROCESS, new HND_ActivityType[]
                {HND_ActivityType.DATAENTRY, HND_ActivityType.ASSESSMENTS});
        processDefStartSates.put(OPERATION_PERMIT_PROCESS, new HND_ActivityType[]
                {HND_ActivityType.ASSESSMENTS});
        
        Set<Transition> opTransitions = new HashSet<>();
        opTransitions.add(new Transition(HND_ActivityType.ASSESSMENTS, HND_ActivityType.APPROVAL));
        opTransitions.add(new Transition(HND_ActivityType.APPROVAL, HND_ActivityType.END));
        
        Set<Transition> bpTransitions = new HashSet<>();
        bpTransitions.add(new Transition(HND_ActivityType.DATAENTRY, HND_ActivityType.APPROVAL, TransitionType.JOIN_MEMBER));
        bpTransitions.add(new Transition(HND_ActivityType.ASSESSMENTS, HND_ActivityType.APPROVAL, TransitionType.JOIN_MEMBER));
        bpTransitions.add(new Transition(HND_ActivityType.APPROVAL, HND_ActivityType.END));
        
        Set<Transition> tmTransitions = new HashSet<>();
        tmTransitions.add(new Transition(HND_ActivityType.DATAENTRY, HND_ActivityType.END));

        Set<Transition> trTransitions = new HashSet<>();
        trTransitions.add(new Transition(HND_ActivityType.PRESENTATION, HND_ActivityType.ANALYSIS));
        trTransitions.add(new Transition(HND_ActivityType.ANALYSIS, HND_ActivityType.END, "analysis_not_ok"));
        trTransitions.add(new Transition(HND_ActivityType.ANALYSIS, HND_ActivityType.DATAENTRY, "analysis_ok"));
        trTransitions.add(new Transition(HND_ActivityType.DATAENTRY, HND_ActivityType.ANALYSIS, "dataentry_back2analysis"));
        trTransitions.add(new Transition(HND_ActivityType.DATAENTRY, HND_ActivityType.APPROVAL, "dataentry_ok"));
        trTransitions.add(new Transition(HND_ActivityType.APPROVAL, HND_ActivityType.DATAENTRY, "approval_back2dataentry"));
        trTransitions.add(new Transition(HND_ActivityType.APPROVAL, HND_ActivityType.ANALYSIS, "approval_back2analysis"));
        trTransitions.add(new Transition(HND_ActivityType.APPROVAL, HND_ActivityType.END, "approval_denied"));
        trTransitions.add(new Transition(HND_ActivityType.APPROVAL, HND_ActivityType.DELIVER_FOR_EXTERNAL_APPROVAL, "approval_ok"));
        trTransitions.add(new Transition(HND_ActivityType.DELIVER_FOR_EXTERNAL_APPROVAL, HND_ActivityType.EXTERNAL_APPROVAL));
        trTransitions.add(new Transition(HND_ActivityType.EXTERNAL_APPROVAL, HND_ActivityType.END));
        
        
        transitionSets = new HashMap<>();
        transitionSets.put(BUILDING_PERMIT_PROCESS, bpTransitions);
        transitionSets.put(OPERATION_PERMIT_PROCESS, opTransitions);
        transitionSets.put(TOPOGRAPHIC_MAINTENANCE_PROCESS, tmTransitions);
        transitionSets.put(TRANSACTION_WORKFLOW, trTransitions);
        
        configureUserRoleActivities();
    }
    
    
    public HND_TransactionType getTransactionType(String processDefinition) {
        return processDefToTrxType.get(processDefinition);
    }
    
    public String getViewId(String processName, String taskName) {
        return taskToView.get(createKey(appName, processName, taskName));
    }
    
    
    public HND_ActivityType[] getStartStates(String processDefinition) {
        return processDefStartSates.get(processDefinition);
    }
    
    public Set<Transition> getSuccessors(String processDefinition, HND_ActivityType activity) {
        return getAdjacent(processDefinition, activity, true);
    }
    public Set<Transition> getPredecessors(String processDefinition, HND_ActivityType activity) {
        return getAdjacent(processDefinition, activity, false);
    }
    
    public Set<HND_ActivityType> getActivitiesByUserRole(String processDefinition, HND_UserRoleType... userRoleTypes) {
        Set<HND_ActivityType> activitySet = new HashSet<>();
        for (HND_UserRoleType role : userRoleTypes) {
            Map<HND_UserRoleType, Set<HND_ActivityType>> userRoleActivities;
            userRoleActivities = processUserRoleActivities.get(processDefinition);
            if (userRoleActivities == null) {
                continue;
            }
            Set<HND_ActivityType> activities = userRoleActivities.get(role);
            activitySet.addAll(activities);
        }
        return activitySet;
    }
    
    private Set<Transition> getAdjacent(String processDefinition, HND_ActivityType activity, boolean successor) {
        Set<Transition> resultSet = new HashSet<>();
        Set<Transition> trSet = transitionSets.get(processDefinition);
        if (trSet == null) throw new IllegalStateException("Invalid process definition: " + processDefinition);
        for (Transition t : trSet) {
            if (successor) {
                if (t.getStart() == activity) {
                    resultSet.add(t);
                }
            } else {
                if (t.getEnd() == activity) {
                    resultSet.add(t);
                }
            }
        }
        return resultSet;
    }
    
    private void configureUserRoleActivities() {
        processUserRoleActivities = new HashMap<>();
        
        Map<HND_UserRoleType, Set<HND_ActivityType>> userRoleActivities;
        
        //========= Add role permissions for Municipal Transactions process
        userRoleActivities = new HashMap<>();
        processUserRoleActivities.put(TRANSACTION_WORKFLOW, userRoleActivities);
        
        Set<HND_ActivityType> receptionistActivities = new HashSet<>();
        receptionistActivities.add(HND_ActivityType.PRESENTATION);
        userRoleActivities.put(HND_UserRoleType.RECEPTIONIST, receptionistActivities);
        
        Set<HND_ActivityType> analystActivities = new HashSet<>();
        analystActivities.add(HND_ActivityType.ANALYSIS);
        userRoleActivities.put(HND_UserRoleType.ANALYST, analystActivities);
        
        Set<HND_ActivityType> editorActivities = new HashSet<>();
        editorActivities.add(HND_ActivityType.DATAENTRY);
        userRoleActivities.put(HND_UserRoleType.EDITOR, editorActivities);
        
        Set<HND_ActivityType> managerActivities = new HashSet<>();
        userRoleActivities.put(HND_UserRoleType.MANAGER, managerActivities);
        
        Set<HND_ActivityType> approverActivities = new HashSet<>();
        approverActivities.add(HND_ActivityType.APPROVAL);
        userRoleActivities.put(HND_UserRoleType.APPROVER, approverActivities);

        Set<HND_ActivityType> surveyingTechnicianActivities = new HashSet<>();
        surveyingTechnicianActivities.add(HND_ActivityType.DATAENTRY);
        userRoleActivities.put(HND_UserRoleType.SURVEYING_TECHNICIAN, surveyingTechnicianActivities);
        
        Set<HND_ActivityType> externalQuerierActivities = new HashSet<>();
        userRoleActivities.put(HND_UserRoleType.EXTERNAL_QUERIER, externalQuerierActivities);
        
        Set<HND_ActivityType> internalQuerierActivities = new HashSet<>();
        userRoleActivities.put(HND_UserRoleType.INTERNAL_QUERIER, internalQuerierActivities);
        
        Set<HND_ActivityType> systemAdministratorActivities = new HashSet<>();
        userRoleActivities.put(HND_UserRoleType.SYSTEM_ADMINISTRATOR, systemAdministratorActivities);
        
        //========= Add role permissions for Operation Permits
        userRoleActivities = new HashMap<>();
        processUserRoleActivities.put(OPERATION_PERMIT_PROCESS, userRoleActivities);
        
        analystActivities = new HashSet<>();
        analystActivities.add(HND_ActivityType.ASSESSMENTS);
        userRoleActivities.put(HND_UserRoleType.ANALYST, analystActivities);
        
        approverActivities = new HashSet<>();
        approverActivities.add(HND_ActivityType.APPROVAL);
        userRoleActivities.put(HND_UserRoleType.APPROVER, approverActivities);

        //========= Add role permissions for Building Permits
        userRoleActivities = new HashMap<>();
        processUserRoleActivities.put(BUILDING_PERMIT_PROCESS, userRoleActivities);

        analystActivities = new HashSet<>();
        analystActivities.add(HND_ActivityType.ASSESSMENTS);
        userRoleActivities.put(HND_UserRoleType.ANALYST, analystActivities);

        approverActivities = new HashSet<>();
        approverActivities.add(HND_ActivityType.APPROVAL);
        userRoleActivities.put(HND_UserRoleType.APPROVER, approverActivities);

        editorActivities = new HashSet<>();
        editorActivities.add(HND_ActivityType.DATAENTRY);
        userRoleActivities.put(HND_UserRoleType.EDITOR, editorActivities);

        //========= Add role permissions for Topographic Maintenance
        userRoleActivities = new HashMap<>();
        processUserRoleActivities.put(TOPOGRAPHIC_MAINTENANCE_PROCESS, userRoleActivities);

        editorActivities = new HashSet<>();
        editorActivities.add(HND_ActivityType.DATAENTRY);
        userRoleActivities.put(HND_UserRoleType.EDITOR, editorActivities);
    }
    
    public static class Transition {
        private HND_ActivityType start;
        private HND_ActivityType end;
        private String name;
        private TransitionType transitionType;
        
        private Transition(HND_ActivityType start, HND_ActivityType end) {
            this(start, end, "", TransitionType.NORMAL);
        }
        private Transition(HND_ActivityType start, HND_ActivityType end, TransitionType transitionType) {
            this(start, end, "", transitionType);
        }
        private Transition(HND_ActivityType start, HND_ActivityType end, String name) {
            this(start, end, name, TransitionType.NORMAL);
        }
        private Transition(HND_ActivityType start, HND_ActivityType end, String name, TransitionType transitionType) {
            this.start = start;
            this.end = end;
            this.name = name;
            this.transitionType = transitionType;
        }

        public HND_ActivityType getStart() {
            return start;
        }
        public HND_ActivityType getEnd() {
            return end;
        }
        public String getName() {
            return name;
        }
        public TransitionType getTransitionType() {
            return transitionType;
        }
    }
    
    public static enum TransitionType {
        FORK_MEMBER, JOIN_MEMBER, NORMAL
    }
}
