package org.sigit.logic.ladmshadow;

import org.sigit.dao.hnd.administrative.HND_PropertyDAO;
import org.sigit.dao.hnd.cadastre.HND_ParcelDAO;
import org.sigit.dao.hnd.ladmshadow.ParcelDAO;
import org.sigit.dao.hnd.ladmshadow.PartyDAO;
import org.sigit.dao.hnd.ladmshadow.PropertyDAO;
import org.sigit.dao.hnd.ladmshadow.RRRDAO;
import org.sigit.dao.hnd.ladmshadow.SpatialUnitDAO;
import org.sigit.model.hnd.administrative.HND_Property;
import org.sigit.model.hnd.cadastre.HND_Easement;
import org.sigit.model.hnd.cadastre.HND_Improvement;
import org.sigit.model.hnd.cadastre.HND_Parcel;
import org.sigit.model.hnd.ladmshadow.Easement;
import org.sigit.model.hnd.ladmshadow.Improvement;
import org.sigit.model.hnd.ladmshadow.LAPoint;
import org.sigit.model.hnd.ladmshadow.Parcel;
import org.sigit.model.hnd.ladmshadow.Party;
import org.sigit.model.hnd.ladmshadow.Property;
import org.sigit.model.hnd.ladmshadow.RRR;
import org.sigit.model.hnd.ladmshadow.Responsibility;
import org.sigit.model.hnd.ladmshadow.Restriction;
import org.sigit.model.hnd.ladmshadow.Right;
import org.sigit.model.hnd.ladmshadow.SpatialUnit;
import org.sigit.model.ladm.administrative.LA_BAUnit;
import org.sigit.model.ladm.administrative.LA_RRR;
import org.sigit.model.ladm.administrative.LA_Responsibility;
import org.sigit.model.ladm.administrative.LA_Restriction;
import org.sigit.model.ladm.administrative.LA_Right;
import org.sigit.model.ladm.party.LA_Party;
import org.sigit.model.ladm.spatialunit.LA_SpatialUnit;
import org.sigit.model.ladm.spatialunit.surveyingandrepresentation.LA_Point;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class Util {
    public static void copyMemberValues(Object src, Object dest, boolean onlyWithColumnAnnotation) {
        Method[] targetMethods = dest.getClass().getMethods();
        
        String methodName;
        try    {
            for (Method m : targetMethods) {
                methodName = m.getName();
                if (/*m.getAnnotation(javax.persistence.Column.class) != null
                        &&*/ methodName.startsWith("set")) {
                    
                    Method getterMethod;
                    try {
                        getterMethod = src.getClass().getMethod("get" + methodName.substring(3), (Class[])null);
                        
                        if (getterMethod.getAnnotation(javax.persistence.Id.class) != null)
                            getterMethod = null;
                        else if (onlyWithColumnAnnotation && getterMethod.getAnnotation(javax.persistence.Column.class) == null
                                && getterMethod.getAnnotation(org.hibernate.annotations.Columns.class) == null
                                && getterMethod.getAnnotation(javax.persistence.OneToOne.class) == null)
                            getterMethod = null;
                    }
                    catch (NoSuchMethodException nse) {
                        getterMethod = null;
                    }
                    
                    if (getterMethod != null) {
                        Class getterReturnType = getterMethod.getReturnType();
                        Class[] setterParams = m.getParameterTypes();
                        
                        if (setterParams != null && setterParams.length == 1
                                && setterParams[0] == getterReturnType) {
                            
                            Object res = getterMethod.invoke(src, new Object[] {});
                            m.invoke(dest, res);
                        }
                    }
                }
            }
        }
        catch (InvocationTargetException ite) {
            System.err.println(ite.getLocalizedMessage());
        }
        catch (IllegalAccessException iae) {
            System.err.println(iae.getLocalizedMessage());
        }
    }
    
    public static Party LA_PartyToParty(LA_Party laParty, long presentationNo, boolean isReadOnly, boolean isSnapshot) {
        if (laParty == null) return null;
        
        Party party = new Party();
        copyMemberValues(laParty, party, true);
        party.setLadmId(laParty.getpID());
        party.setPresentationNo(presentationNo);
        party.setExtParty(laParty.getExtParty());
        
        return party;
    }
    
    public static RRR LA_RRRToRRR(LA_RRR laRrr, long presentationNo, boolean isReadOnly, boolean isSnapshot) {
        if (laRrr == null) return null;
        
        RRR rrr = null;
        
        if (laRrr instanceof LA_Responsibility)
            rrr = Responsibility.newResponsibility(laRrr.getrID(), presentationNo, false, isSnapshot);
        else if (laRrr instanceof LA_Right)
            rrr = Right.newRight(laRrr.getrID(), presentationNo, false, isSnapshot);
        else if (laRrr instanceof LA_Restriction)
            rrr = Restriction.newRestriction(laRrr.getrID(), presentationNo, false, isSnapshot);

        if (rrr != null)
            copyMemberValues(laRrr, rrr, true);
        
        return rrr;
    }
    public static LA_RRR RRRToLA_RRR(RRR rrr, LA_BAUnit laBAUnit) {
        if (rrr == null) return null;
        
        LA_RRR laRrr = null;

        laRrr = null;
        if (rrr instanceof Right)
            laRrr = new LA_Right();
        else if (rrr instanceof Restriction)
            laRrr = new LA_Restriction();
        else if (rrr instanceof Responsibility)
            laRrr = new LA_Responsibility();
        
        if (laRrr != null) {
            copyMemberValues(rrr, laRrr, true);
            laRrr.setBaunit(laBAUnit);
        }
        
        return laRrr;
    }
    
    public static Property HND_PropertyToPropery(HND_Property hndProperty, long presentationNo, boolean isReadOnly, boolean isSnapshot) {
        Set<LA_SpatialUnit> laSpatialUnitSet = hndProperty.getSpatialUnits();
        
        return HND_PropertyToPropery(hndProperty, laSpatialUnitSet, presentationNo, isReadOnly, isSnapshot);
    }
    
    public static Property HND_PropertyToPropery(HND_Property hndProperty, HND_Parcel hndParcel, long presentationNo, boolean isReadOnly, boolean isSnapshot) {
        Set<LA_SpatialUnit> laSpatialUnitSet = new HashSet<LA_SpatialUnit>();
        laSpatialUnitSet.add(hndParcel);
        
        return HND_PropertyToPropery(hndProperty, laSpatialUnitSet, presentationNo, isReadOnly, isSnapshot); 
    }
    
    public static Property HND_PropertyToPropery(HND_Property hndProperty, Set<LA_SpatialUnit> laSpatialUnitSet, long presentationNo, boolean isReadOnly, boolean isSnapshot) {
        //TODO: Validar cuando no se encuentra
        Property property = Property.newProperty(hndProperty.getuID(), presentationNo, isReadOnly, isSnapshot);
        copyMemberValues(hndProperty, property, true);
        property.setRegistration(hndProperty.getRegistration());

        Parcel parcel;
        if (laSpatialUnitSet != null) {
            Set<SpatialUnit> spatialUnitSet = new HashSet<SpatialUnit>();
            for (LA_SpatialUnit laSpatialUnit : laSpatialUnitSet) {
                if (laSpatialUnit instanceof HND_Parcel) {
                    parcel = HND_ParcelToParcel((HND_Parcel)laSpatialUnit, presentationNo, isReadOnly, isSnapshot);
                    
                    //TODO: corregir esta situacion con los puntos de referencia!
                    parcel.setReferencePoint(laSpatialUnit.getReferencePoint());
                    
                    LA_Point laPointOfReference = laSpatialUnit.getPointOfReference();
                    if (laPointOfReference != null) {
                        LAPoint pointOfReference = LA_PointToLAPoint(laPointOfReference, presentationNo, isReadOnly, isSnapshot);
                        pointOfReference.setSpatialUnit(parcel);
                        
                        parcel.setPointOfReference(pointOfReference);
                    }
                    SpatialUnitDAO.save(parcel);
                    
                    spatialUnitSet.add(parcel);
                }
            }
            
            property.setSpatialUnits(spatialUnitSet);
        }
        
        Set<LA_RRR> setLaRrr = hndProperty.getRrr();
        Set<RRR> setRrr = new HashSet<RRR>();
        if (setLaRrr != null && setLaRrr.size() > 0) {
            Map<UUID, LA_Party> mapLaParty = new HashMap<>();
            for (LA_RRR laRrr : setLaRrr) {
                RRR rrr;
    
                rrr = LA_RRRToRRR(laRrr, presentationNo, isReadOnly, isSnapshot);
                rrr.setBaunit(property);
                rrr.setPartyLadmId(laRrr.getParty().getpID());
                rrr.setType(laRrr.getType());
                rrr.setShare(laRrr.getShare());
                
                setRrr.add(rrr);
                mapLaParty.put(laRrr.getParty().getpID(), laRrr.getParty());
            }
            
            Map<UUID, Party> mapParty = new HashMap<>();
            Collection<LA_Party> collLAParty = mapLaParty.values();
            Party party;
            for (LA_Party laParty : collLAParty) {
                party = LA_PartyToParty(laParty, presentationNo, isReadOnly, isSnapshot);
                party.setRrr(setRrr);
                mapParty.put(laParty.getpID(), party);
            }
            
            //TODO: Ver lo de los parties que se copien todos los miembros
            for (RRR rrr : setRrr) {
                party = mapParty.get(rrr.getPartyLadmId());
                rrr.setParty(party);
            }
    
            Collection<Party> collParty = mapParty.values();
            for (Party p : collParty)
                PartyDAO.save(p);
        }
        property.setRrr(setRrr);
        property.setOriginal(true);
        
        return property;
    }
    
    
    public static LAPoint LA_PointToLAPoint(LA_Point laPoint, long presentationNo, boolean isReadOnly, boolean isSnapshot) {
        if (laPoint == null) return null;
        
        LAPoint point = LAPoint.newLAPoint(laPoint.getpID(), presentationNo, isReadOnly, isSnapshot);
        copyMemberValues(laPoint, point, true);
        
        return point;
    }
    public static Parcel HND_ParcelToParcel(HND_Parcel hndParcel, long presentationNo, boolean isReadOnly, boolean isSnapshot) {
        if (hndParcel == null) return null;
        
        Parcel parcel = Parcel.newParcel(hndParcel.getSuID(), presentationNo, isReadOnly, isSnapshot);
        copyMemberValues(hndParcel, parcel, true);
        
        //copy rest of the complex attributes
        //TODO: Future version should use a framework like Dozer
        parcel.setOriginal(true);
        parcel.setLevel(hndParcel.getLevel());
        parcel.setShape(hndParcel.getShape());
        
        parcel.setAvailableServices(hndParcel.getAvailableServices());
        
        parcel.getEasements().clear();
        for (HND_Easement he : hndParcel.getEasements())
            parcel.getEasements().add(HND_EasementToEasement(he));
        
        parcel.getImprovements().clear();
        for (HND_Improvement hi : hndParcel.getImprovements())
            parcel.getImprovements().add(HND_ImprovementToImprovement(hi));
        
        //TODO: Incorporar los usos
        
        return parcel;
    }
    
    public static Improvement HND_ImprovementToImprovement(HND_Improvement hndImprovement) {
        Improvement improvement = new Improvement();
        copyMemberValues(hndImprovement, improvement, true);
        improvement.setRegistration(hndImprovement.getRegistration());
        
        return improvement;
    }
    
    public static Easement HND_EasementToEasement(HND_Easement hndEasement) {
        Easement easement = new Easement();
        copyMemberValues(hndEasement, easement, true);
        easement.setRegistration(hndEasement.getRegistration());
        
        return easement;
    }
    
    public static void applyRequestChanges(long presentationNo, UUID baUnitId) {
        Property property = PropertyDAO.loadPropertyByPresentationIdAndLadmId(presentationNo, baUnitId);
        Date todaysDate = new Date();
        
        //TODO: Ver primero si se crearon predios nuevos, por ahora no
        RRR rrr;
        RRRDAO rrrDao;
    }
    
    public static UUID copyParcelData(long presentationNo, UUID parcelId, boolean includeNeighbors, boolean isSnapshot) {
        HND_Parcel hndParcel = HND_ParcelDAO.loadParcelByID(parcelId);
        HND_Property hndProperty = hndParcel.getProperty();
        
        Set<LA_SpatialUnit> laSpatialUnitSet = new HashSet<>();
        laSpatialUnitSet.add(hndParcel);
        Property property = HND_PropertyToPropery(hndProperty, laSpatialUnitSet, presentationNo, false, isSnapshot);
        
        //TODO: hacer lo de RRR en HND_PropertyToPropery
        
        PropertyDAO.save(property);

        if (includeNeighbors) {
            List<HND_Parcel> hndNeighborParcelSet = HND_ParcelDAO.loadNeighborsByHNDParcel(hndParcel);
            if (hndNeighborParcelSet != null) {
                for (HND_Parcel hndNeighborParcel : hndNeighborParcelSet) {
                    HND_Property hndNeighborProperty = hndNeighborParcel.getProperty();
                    
                    Property neighborProp = HND_PropertyToPropery(hndNeighborProperty, hndNeighborParcel, presentationNo, true, isSnapshot);
                    PropertyDAO.save(neighborProp);
                }
            }
        }
        
        return property.getORMID();
    }
    
    public static List<UUID> copyParcelDataWithRelatedParcels(long presentationNo, List<UUID> parcelIdList, boolean includeNeighbors, boolean createSnapshot) {
        List<UUID> baUnitIdList = new ArrayList<>();
        Set<HND_Property> hndRequestedPropertiesSet = new HashSet<HND_Property>();
        Set<HND_Property> hndPropertiesToSaveSet = new HashSet<HND_Property>();

        for (UUID parcelId : parcelIdList) {
            HND_Parcel hndParcel = HND_ParcelDAO.loadParcelByID(parcelId);
            
            //TODO: Revisar caso en que un Parcel no tenga Property!!
            HND_Property hndProperty = hndParcel.getProperty();
            if (hndProperty == null) {
                hndProperty = new HND_Property();
                hndProperty.getSpatialUnits().add(hndParcel);
                hndParcel.getBaunits().add(hndProperty);
                
                HND_PropertyDAO.save(hndProperty);
                HND_ParcelDAO.save(hndParcel);
            }
            
            hndRequestedPropertiesSet.add(hndProperty);
            hndPropertiesToSaveSet.add(hndProperty);
        }
        
        for (HND_Property hndProp : hndRequestedPropertiesSet) {
            Set<HND_Property> hndNeighborPropSet = HND_PropertyDAO.loadNeighborsByHNDProperty(hndProp);
            hndPropertiesToSaveSet.addAll(hndNeighborPropSet);
        }
        
        for (HND_Property hndProp : hndPropertiesToSaveSet) {
            Property prop;
            if (hndRequestedPropertiesSet.contains(hndProp)) {
                //If it is a requested property...
                prop = HND_PropertyToPropery(hndProp, presentationNo, false, false);
                PropertyDAO.save(prop);
                baUnitIdList.add(prop.getORMID());
                if (createSnapshot) {
                    prop = HND_PropertyToPropery(hndProp, presentationNo, false, true);
                    PropertyDAO.save(prop);
                }
            }
            else {
                //If it is just a neighbor property, it is readonly...
                prop = HND_PropertyToPropery(hndProp, presentationNo, true, false);
                PropertyDAO.save(prop);
                if (createSnapshot) {
                    prop = HND_PropertyToPropery(hndProp, presentationNo, true, true);
                    PropertyDAO.save(prop);
                }
            }
        }
        
        return baUnitIdList;
    }
    
    public static void replaceParcel(Parcel oldParcel, Parcel newParcel, Date endLifespanVersion, boolean replaceProperty) {
        oldParcel.setEndLifespanVersion(endLifespanVersion);
        oldParcel.setModified(true);
        newParcel.setBeginLifespanVersion(endLifespanVersion);
        newParcel.setSuID(null);
        
        Property theProperty = oldParcel.getProperty();
        
        theProperty.getSpatialUnits().remove(oldParcel);
        theProperty.getSpatialUnits().add(newParcel);
        
        if (replaceProperty) {
            Property theNewProperty = theProperty.clone();
            
            theProperty.setEndLifespanVersion(endLifespanVersion);
            
            theNewProperty.setuID(null);
            theNewProperty.setBeginLifespanVersion(endLifespanVersion);
            theNewProperty.getSpatialUnits().clear();
            theNewProperty.getSpatialUnits().add(newParcel);
            
            ParcelDAO.save(oldParcel);
            ParcelDAO.save(newParcel);
            PropertyDAO.save(theProperty);
            PropertyDAO.save(theNewProperty);
        }
        else {
            ParcelDAO.save(oldParcel);
            ParcelDAO.save(newParcel);
            PropertyDAO.save(theProperty);
        }
    }
    
    public static void undoLastOperation(long presentationNo) {
        //TODO: Por ahora, solo hace UNDO de parcelas
        //agregar soporte para propiedades y derechos
        Date parcelTS = ParcelDAO.loadMaxTimeStamp(presentationNo);
        System.out.println(parcelTS);
    }
    
    public static void deletePresentationShadow(long presentationNo, boolean includeSnapshot) {
        //delete associations
        RRRDAO.clearRRRsAssociationsByPresentationNo(presentationNo, includeSnapshot);
        PropertyDAO.clearPropertiesAssociationsByPresentationNo(presentationNo, includeSnapshot);
        ParcelDAO.clearParcelsAssociationsByPresentationId(presentationNo, includeSnapshot);
        
        //delete entities
        RRRDAO.deleteRRRsByPresentationNo(presentationNo, includeSnapshot);
        PropertyDAO.deletePropertiesByPresentationNo(presentationNo, includeSnapshot);
        ParcelDAO.deleteParcelsByPresentationNo(presentationNo, includeSnapshot);
    }
    /*
    private static void copySnapshot(long presentationId) {
        
        List<RRR> rrrList = RRRDAO.loadRRRsByPresentationId(presentationId, true);
        for (RRR rrr : rrrList) {
            RRR clonedRrr = rrr
        }
    }
    */
    public static void undoAll(long presentationNo, List<UUID> parcelIdList) {
        //TODO: validar que si no se ha hecho ningun cambio entonces no ejecutar nada!!
        RRRDAO.clear();
        deletePresentationShadow(presentationNo, true);
        
        Util.copyParcelDataWithRelatedParcels( presentationNo, parcelIdList, true, true );
    }
}
