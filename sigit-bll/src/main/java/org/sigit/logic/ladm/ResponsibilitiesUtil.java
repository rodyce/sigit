package org.sigit.logic.ladm;

import org.sigit.model.commons.IProperty;
import org.sigit.model.commons.IResponsibility;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResponsibilitiesUtil {
    public static boolean propertyResponsibilitiesEqual(IProperty prc1, IProperty prc2) {
        if (prc1.responsibilitiesHash() == prc2.responsibilitiesHash()) {
            Map<Integer, IResponsibility<?,?>> responsibilitiesMap = new HashMap<>();
            for (IResponsibility<?,?> rc : prc1.getResponsibilities())
                responsibilitiesMap.put(rc.responsibilitiesHash(), rc);
            
            Map<Integer, IResponsibility<?,?>> otherResponsibilitiesMap = new HashMap<>();
            for (IResponsibility<?,?> rc : prc2.getResponsibilities())
                otherResponsibilitiesMap.put(rc.responsibilitiesHash(), rc);
            
            if ( responsibilitiesMap.keySet().equals( otherResponsibilitiesMap.keySet() ) )
                for (IResponsibility<?,?> rc : responsibilitiesMap.values())                    
                    if (!responsibilitiesEqual(rc, otherResponsibilitiesMap.get(rc.responsibilitiesHash())))
                        return false;
            
            return true;
        }
        return false;
    }
    
    public static boolean responsibilitiesEqual(IResponsibility<?,?> rc1, IResponsibility<?,?> rc2) {
        if (rc1.responsibilitiesHash() == rc2.responsibilitiesHash()) {
            return rc1.getShare().hashCode() == rc2.getShare().hashCode()
                    && rc1.getExtPID() == rc2.getExtPID()
                    && rc1.getType().equals(rc2.getType());
        }
        return false;
    }
    
    public static boolean propertyHasResponsibility(IProperty theProperty, IResponsibility<?,?> responsibility) {
        List<IResponsibility<?,?>> responsibilityList = theProperty.getResponsibilities();
        for (IResponsibility<?,?> rc : responsibilityList) {
            if (responsibilitiesEqual(rc, responsibility))
                return true;
        }
        
        return false;
    }

}
