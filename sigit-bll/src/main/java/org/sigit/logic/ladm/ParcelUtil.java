package org.sigit.logic.ladm;

import com.vividsolutions.jts.geom.Geometry;

import org.sigit.model.commons.IParcel;

public class ParcelUtil {
    private static boolean objectsEqual(Object obj1, Object obj2) {
        return (obj1 == obj2) || (obj1 != null && obj1.equals(obj2)) || (obj2 != null && obj2.equals(obj1));
    }
    
    public static boolean geomEquals(Geometry g1, Geometry g2) {
        return (g1 == g2) || (g1 != null && g1.equals(g2)) || (g2 != null && g2.equals(g2));
    }
    
    public static boolean parcelsEqual(IParcel pc1, IParcel pc2) {
        return geomEquals( pc1.getShape(),                    pc2.getShape() )
            && objectsEqual( pc1.getFieldTab(),                pc2.getFieldTab() )
            && objectsEqual( pc1.getCadastralKey(),            pc2.getCadastralKey())
            && objectsEqual( pc1.getMunicipalKey(),            pc2.getMunicipalKey())
            && objectsEqual( pc1.getGeometryArea(),            pc2.getGeometryArea())
            && objectsEqual( pc1.getDocumentedArea(),        pc2.getDocumentedArea())
            && objectsEqual( pc1.getMeasuredArea(),            pc2.getMeasuredArea())
            && objectsEqual( pc1.getDocumentedBuiltArea(),    pc2.getDocumentedBuiltArea())
            && objectsEqual( pc1.getGroundBuiltArea(),        pc2.getGroundBuiltArea())
            && objectsEqual( pc1.getZoneName(),                pc2.getZoneName())
            && objectsEqual( pc1.getNeighborhood(),            pc2.getNeighborhood())
            && objectsEqual( pc1.getAccessWay1(),            pc2.getAccessWay1())
            && objectsEqual( pc1.getAccessWay2(),            pc2.getAccessWay2())
            && objectsEqual( pc1.getHouseNumber(),            pc2.getHouseNumber())
            && objectsEqual( pc1.getLocationInCountry(),    pc2.getLocationInCountry())
            && objectsEqual( pc1.getCommercialAppraisal(),    pc2.getCommercialAppraisal())
            && objectsEqual( pc1.getFiscalAppraisal(),        pc2.getFiscalAppraisal())
            && objectsEqual( pc1.getTaxationBalanceDue(),    pc2.getTaxationBalanceDue())
            && objectsEqual( pc1.getTaxationStatus(),        pc2.getTaxationStatus())
            && objectsEqual( pc1.getLandUse(),                pc2.getLandUse() );
    }
}
