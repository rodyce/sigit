package org.sigit.logic.ladm;

import org.sigit.model.commons.IProperty;
import org.sigit.model.commons.IRight;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RightsUtil {
    public static boolean propertyRightsEqual(IProperty prc1, IProperty prc2) {
        if (prc1.rightsHash() == prc2.rightsHash()) {
            Map<Integer, IRight<?,?>> rightsMap = new HashMap<Integer, IRight<?,?>>();
            for (IRight<?,?> rc : prc1.getRights())
                rightsMap.put(rc.rightHash(), rc);
            
            Map<Integer, IRight<?,?>> otherRightsMap = new HashMap<>();
            for (IRight<?,?> rc : prc2.getRights())
                otherRightsMap.put(rc.rightHash(), rc);
            
            if ( rightsMap.keySet().equals( otherRightsMap.keySet() ) )
                for (IRight<?,?> rc : rightsMap.values())
                    if (!rightsEqual(rc, otherRightsMap.get(rc.rightHash())))
                        return false;
            
            return true;
        }
        return false;
    }
    
    public static boolean rightsEqual(IRight<?,?> rc1, IRight<?,?> rc2) {
        if (rc1.rightHash() == rc2.rightHash()) {
            return rc1.getShare().hashCode() == rc2.getShare().hashCode()
                    && rc1.getExtPID() == rc2.getExtPID()
                    && rc1.getType().equals(rc2.getType());
        }
        return false;
    }
    
    public static boolean propertyHasRight(IProperty theProperty, IRight<?,?> right) {
        List<IRight<?,?>> rightList = theProperty.getRights();
        for (IRight<?,?> rc : rightList) {
            if (rightsEqual(rc, right))
                return true;
        }
        
        return false;
    }
}
