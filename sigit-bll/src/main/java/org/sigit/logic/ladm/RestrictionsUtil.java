package org.sigit.logic.ladm;

import org.sigit.model.commons.IProperty;
import org.sigit.model.commons.IRestriction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RestrictionsUtil {
    public static boolean propertyRestrictionsEqual(IProperty prc1, IProperty prc2) {
        if (prc1.restrictionsHash() == prc2.restrictionsHash()) {
            Map<Integer, IRestriction<?,?>> restrictionsMap = new HashMap<>();
            for (IRestriction<?,?> rc : prc1.getRestrictions())
                restrictionsMap.put(rc.restrictionsHash(), rc);
            
            Map<Integer, IRestriction<?,?>> otherRestrictionsMap = new HashMap<>();
            for (IRestriction<?,?> rc : prc2.getRestrictions())
                otherRestrictionsMap.put(rc.restrictionsHash(), rc);
            
            if ( restrictionsMap.keySet().equals( otherRestrictionsMap.keySet() ) )
                for (IRestriction<?,?> rc : restrictionsMap.values())
                    if (!restrictionsEqual(rc, otherRestrictionsMap.get(rc.restrictionsHash())))
                        return false;
            
            return true;
        }
        return false;
    }
    
    public static boolean restrictionsEqual(IRestriction<?,?> rc1, IRestriction<?,?> rc2) {
        if (rc1.restrictionsHash() == rc2.restrictionsHash()) {
            return rc1.getShare().hashCode() == rc2.getShare().hashCode()
                    && rc1.getExtPID() == rc2.getExtPID()
                    && rc1.getType().equals(rc2.getType());
        }
        return false;
    }
    
    public static boolean propertyHasRestriction(IProperty theProperty, IRestriction<?,?> restriction) {
        List<IRestriction<?,?>> restrictionList = theProperty.getRestrictions();
        for (IRestriction<?,?> rc : restrictionList) {
            if (restrictionsEqual(rc, restriction))
                return true;
        }
        
        return false;
    }

}
