package org.sigit.logic.ladm;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.sigit.commons.geometry.GeometryOperations;
import org.sigit.model.commons.IParcel;
import org.sigit.model.commons.IProperty;
import org.sigit.model.commons.ISpatialZone;

import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;

public class PropertyUtil {
    public static MultiPolygon parcelsAsMultiPolygon(IProperty property) {
        Polygon[] polygons = null;
        
        if (property != null) {
            Set<IParcel> parcels = property.getParcels();
            
            if (parcels != null && parcels.size() > 0) {
                polygons = new Polygon[parcels.size()];
                int i = 0;
                for (IParcel hndP : parcels)
                    polygons[i++] = hndP.getShape();
            }
            return GeometryOperations.geomFactory.createMultiPolygon(polygons);
        }
        
        return null;
    }
    
    //TODO: Normalizar
    public static MultiPolygon parcelsAsMultiPolygon(ISpatialZone spatialZone) {
        Polygon[] polygons = null;
        IProperty property = spatialZone.getProperty();
        
        if (property != null) {
            Set<IParcel> parcels = property.getParcels();
            
            if (parcels != null && parcels.size() > 0) {
                polygons = new Polygon[parcels.size()];
                int i = 0;
                for (IParcel hndP : parcels)
                    polygons[i++] = hndP.getShape();
            }
        }
        else if (spatialZone != null) {
            if (spatialZone.getShape() != null) {
                polygons = new Polygon[1];
                polygons[0] = (Polygon) spatialZone.getShape();
                
            }
        }
        
        if (polygons != null)
            return GeometryOperations.geomFactory.createMultiPolygon(polygons);
        
        return null;
    }
    
    public static List<Polygon> polygonsFromProperty(IProperty property) {
        List<Polygon> polygonList = new ArrayList<Polygon>();
        
        if (property != null) {
            Set<IParcel> parcels = property.getParcels();
            
            if (parcels != null && parcels.size() > 0) {
                for (IParcel hndP : parcels)
                    polygonList.add((Polygon) hndP.getShape());
            }
        }
        
        return polygonList;
    }
    

}
