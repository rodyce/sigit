package org.sigit.logic.ladm;

import org.sigit.model.commons.IProperty;
import org.sigit.model.commons.IRRR;
import org.sigit.model.commons.IResponsibility;
import org.sigit.model.commons.IRestriction;
import org.sigit.model.commons.IRight;


public class RRRsUtil {
    public static boolean propertyRRRsEqual(IProperty prc1, IProperty prc2) {
        if (!RightsUtil.propertyRightsEqual(prc1, prc2)) return false;
        if (!RestrictionsUtil.propertyRestrictionsEqual(prc1, prc1)) return false;
        if (!ResponsibilitiesUtil.propertyResponsibilitiesEqual(prc1, prc2)) return false;
        
        return true;
    }
    
    public static boolean rrrsEqual(IRRR<?,?> rc1, IRRR<?,?> rc2) {
        if (rc1 instanceof IRight && rc2 instanceof IRight)
            return RightsUtil.rightsEqual((IRight<?,?>) rc1, (IRight<?,?>) rc2);
        if (rc1 instanceof IRestriction && rc2 instanceof IRestriction)
            return RestrictionsUtil.restrictionsEqual((IRestriction<?,?>) rc1, (IRestriction<?,?>) rc2);
        if (rc1 instanceof IResponsibility && rc2 instanceof IResponsibility)
            return ResponsibilitiesUtil.responsibilitiesEqual((IResponsibility<?,?>) rc1, (IResponsibility<?,?>) rc2);

        return false;
    }
    
    public static boolean propertyHasRRR(IProperty theProperty, IRRR<?,?> rrr) {
        if (rrr instanceof IRight)
            return RightsUtil.propertyHasRight(theProperty, (IRight<?,?>) rrr); 
        if (rrr instanceof IRestriction)
            return RestrictionsUtil.propertyHasRestriction(theProperty, (IRestriction<?,?>) rrr);
        if (rrr instanceof IResponsibility)
            return ResponsibilitiesUtil.propertyHasResponsibility(theProperty, (IResponsibility<?,?>) rrr);
        
        return false;
    }

}
