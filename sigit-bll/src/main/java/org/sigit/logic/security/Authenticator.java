package org.sigit.logic.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Set;

import org.sigit.dao.hnd.administrative.HND_UserDAO;
import org.sigit.model.hnd.administrative.HND_User;
import org.sigit.model.hnd.administrative.HND_UserRoleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component("authenticator")
@Scope("session")
public class Authenticator implements IAuthenticator {
    private Authentication authentication;
    
    @Autowired
    Credentials credentials;
    
    public static String MD5(String md5) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; i++)
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
                        .substring(1, 3));

            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return md5;
    }

    @Transactional
    public boolean authenticate() {
        if (!(authentication instanceof UsernamePasswordAuthenticationToken))
            return false;

        //UsernamePasswordAuthenticationToken credentials = (UsernamePasswordAuthenticationToken) authentication;
        
        String userName = credentials.getUsername();
        String password = MD5(credentials.getPassword());

        HND_User user = HND_UserDAO.loadUser(userName, password);

        if (user != null && user.isActive()) {
            credentials.setLoggedUserFullName(user.getParty() != null ? user.getParty().getName() : "-");
            Set<GrantedAuthority> roleSet = new HashSet<>();
            
            if (user.getReceptionist()) {
                roleSet.add(new SimpleGrantedAuthority(HND_UserRoleType.RECEPTIONIST.toString()));
            }

            if (user.getAnalyst()) {
                roleSet.add(new SimpleGrantedAuthority(HND_UserRoleType.ANALYST.toString()));
            }
            
            if (user.getEditor()) {
                roleSet.add(new SimpleGrantedAuthority(HND_UserRoleType.EDITOR.toString()));
            }
            
            if (user.getApprover()) {
                roleSet.add(new SimpleGrantedAuthority(HND_UserRoleType.APPROVER.toString()));
            }

            if (user.getSurveyingTechnician()) {
                roleSet.add(new SimpleGrantedAuthority(HND_UserRoleType.SURVEYING_TECHNICIAN.toString()));
            }
            
            if (user.getExternalQuerier()) {
                roleSet.add(new SimpleGrantedAuthority(HND_UserRoleType.EXTERNAL_QUERIER.toString()));
            }
            
            if (user.getInternalQuerier()) {
                roleSet.add(new SimpleGrantedAuthority(HND_UserRoleType.INTERNAL_QUERIER.toString()));
            }
            
            if (user.getManager()) {
                roleSet.add(new SimpleGrantedAuthority(HND_UserRoleType.MANAGER.toString()));
            }
            
            if (user.getSystemAdministrator()) {
                roleSet.add(new SimpleGrantedAuthority(HND_UserRoleType.SYSTEM_ADMINISTRATOR.toString()));
            }
            
            //TODO: See how to use this class
            authentication = new UsernamePasswordAuthenticationToken(null, null, roleSet);
            
            return true;
        }

        return false;
    }

    public boolean checkIfLoogedIn() {
        return authentication != null && authentication.isAuthenticated();
    }

    public Authentication getAuthentication() {
        return authentication;
    }
    public void setAuthentication(Authentication authentication) {
        this.authentication = authentication;
    }

    @Override
    public Credentials getCredentials() {
        return credentials;
    }
    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }
}
