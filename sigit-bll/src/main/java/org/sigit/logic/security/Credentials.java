package org.sigit.logic.security;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.sigit.model.hnd.administrative.HND_UserRoleType;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("credentials")
@Scope("session")
public class Credentials implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private String username;
    private String password;
    private String loggedUserFullName;
    private Set<HND_UserRoleType> userRoleTypeSet = new HashSet<>();
    
    
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getLoggedUserFullName() {
        return loggedUserFullName;
    }
    public void setLoggedUserFullName(String loggedUserFullName) {
        this.loggedUserFullName = loggedUserFullName;
    }
    
    public void addRoleType(HND_UserRoleType role) {
        userRoleTypeSet.add(role);
    }
    
    public Set<HND_UserRoleType> getUserRoleTypeSet() {
        return Collections.unmodifiableSet(userRoleTypeSet);
    }
}
