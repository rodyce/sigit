package org.sigit.logic.security;

public interface IAuthenticator {
    public boolean authenticate();
    public boolean checkIfLoogedIn();
    public Credentials getCredentials();
}
