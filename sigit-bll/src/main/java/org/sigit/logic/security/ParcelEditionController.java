package org.sigit.logic.security;

import java.io.Serializable;

import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

@Component("parcelEdition")
@Scope(value="sigit-conversation")
public class ParcelEditionController implements Serializable {
    
    /*
     *                     maxExtent: bounds,
                    maxResolution: 26.280468750000182,
                    projection: "EPSG:32616",
                    units: 'm',

     */
    
    private static final long serialVersionUID = 1L;
    
    private final float x1 =  387650.1f;
    private final float y1 = 1743525.2f;
    private final float x2 =  392000.9f;
    private final float y2 = 1745600.9f;
    
    
    final String parcelEditorOptions = String.format("{ controls:[], " +
        "maxExtent: new OpenLayers.Bounds(%f, %f, %f, %f), " +
        "maxResolution: 26.280468750000182, " +
        "projection: new OpenLayers.Projection('EPSG:32616'), " +
        "displayProjection: new OpenLayers.Projection('EPSG:32616'), " +
        "units: 'm' }", x1, y1, x2, y2);
    
    public String getParcelEditorOptions() {
        return parcelEditorOptions;
    }
}
