package org.sigit.logic.security;

import java.util.ArrayList;
import java.util.Collection;

import org.sigit.commons.di.CtxComponent;
import org.sigit.dao.hnd.administrative.HND_UserDAO;
import org.sigit.model.hnd.administrative.HND_User;
import org.sigit.model.hnd.administrative.HND_UserRoleType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userDetailsService")
public class SigitUserDetailsService implements UserDetailsService {

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        HND_User userEntity = HND_UserDAO.loadUser(username);
        if (userEntity == null) {
            throw new UsernameNotFoundException("user not found");
        }
        return buildUserFromEntity(userEntity);
    }

    private User buildUserFromEntity(HND_User userEntity) {
        String username = userEntity.getUserName();
        String password = userEntity.getPassword();
        boolean enabled = userEntity.isActive();
        boolean accountNonExpired = userEntity.isActive();
        boolean credentialsNonExpired = userEntity.isActive();
        boolean accountNonLocked = userEntity.isActive();

        Credentials credentials = CtxComponent.getInstance(Credentials.class);
        credentials
                .setLoggedUserFullName(userEntity.getParty() != null ? userEntity
                        .getParty().getName() : "-");

        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        if (userEntity.getAnalyst()) {
            authorities.add(new SimpleGrantedAuthority(
                    getRole(HND_UserRoleType.ANALYST)));
            credentials.addRoleType(HND_UserRoleType.ANALYST);
        }
        if (userEntity.getApprover()) {
            authorities.add(new SimpleGrantedAuthority(
                    getRole(HND_UserRoleType.APPROVER)));
            credentials.addRoleType(HND_UserRoleType.APPROVER);
        }
        if (userEntity.getEditor()) {
            authorities.add(new SimpleGrantedAuthority(
                    getRole(HND_UserRoleType.EDITOR)));
            credentials.addRoleType(HND_UserRoleType.EDITOR);
        }
        if (userEntity.getExternalQuerier()) {
            authorities.add(new SimpleGrantedAuthority(
                    getRole(HND_UserRoleType.EXTERNAL_QUERIER)));
            credentials.addRoleType(HND_UserRoleType.EXTERNAL_QUERIER);
        }
        if (userEntity.getInternalQuerier()) {
            authorities.add(new SimpleGrantedAuthority(
                    getRole(HND_UserRoleType.INTERNAL_QUERIER)));
            credentials.addRoleType(HND_UserRoleType.INTERNAL_QUERIER);
        }
        if (userEntity.getManager()) {
            authorities.add(new SimpleGrantedAuthority(
                    getRole(HND_UserRoleType.MANAGER)));
            credentials.addRoleType(HND_UserRoleType.MANAGER);
        }
        if (userEntity.getReceptionist()) {
            authorities.add(new SimpleGrantedAuthority(
                    getRole(HND_UserRoleType.RECEPTIONIST)));
            credentials.addRoleType(HND_UserRoleType.RECEPTIONIST);
        }
        if (userEntity.getSurveyingTechnician()) {
            authorities.add(new SimpleGrantedAuthority(
                    getRole(HND_UserRoleType.SURVEYING_TECHNICIAN)));
            credentials.addRoleType(HND_UserRoleType.SURVEYING_TECHNICIAN);
        }
        if (userEntity.getSystemAdministrator()) {
            authorities.add(new SimpleGrantedAuthority(
                    getRole(HND_UserRoleType.SYSTEM_ADMINISTRATOR)));
            credentials.addRoleType(HND_UserRoleType.SYSTEM_ADMINISTRATOR);
        }

        User user = new User(username, password, enabled, accountNonExpired,
                credentialsNonExpired, accountNonLocked, authorities);
        return user;
    }

    private static String getRole(HND_UserRoleType userRoleType) {
        return "ROLE_" + userRoleType.toString();
    }
}
