package org.sigit.interop.ws.taxation.types.v1;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "parcelNationalIdentifier", "commercialAppraisal",
        "fiscalAppraisal", "taxationBalanceDue", "taxationInfoLastUpdate",
        "taxationInfoSource" })
@XmlRootElement(name = "TaxationInfo")
public class TaxationInfo {
    
    @XmlElement(required = true, nillable=false)
    protected String parcelNationalIdentifier;
    @XmlElement(required = false, nillable=false)
    protected BigDecimal commercialAppraisal;
    @XmlElement(required = false, nillable=false)
    protected BigDecimal fiscalAppraisal;
    @XmlElement(required = true, nillable=false)
    protected BigDecimal taxationBalanceDue;
    @XmlElement(required = true, nillable=false)
    protected Date taxationInfoLastUpdate;
    @XmlElement(required = false, nillable=false)
    protected String taxationInfoSource;


    public String getParcelNationalIdentifier() {
        return parcelNationalIdentifier;
    }
    public void setParcelNationalIdentifier(String parcelNationalIdentifier) {
        this.parcelNationalIdentifier = parcelNationalIdentifier;
    }

    public BigDecimal getCommercialAppraisal() {
        return commercialAppraisal;
    }
    public void setCommercialAppraisal(BigDecimal commercialAppraisal) {
        this.commercialAppraisal = commercialAppraisal;
    }

    public BigDecimal getFiscalAppraisal() {
        return fiscalAppraisal;
    }
    public void setFiscalAppraisal(BigDecimal fiscalAppraisal) {
        this.fiscalAppraisal = fiscalAppraisal;
    }
    
    public BigDecimal getTaxationBalanceDue() {
        return taxationBalanceDue;
    }
    public void setTaxationBalanceDue(BigDecimal taxationBalanceDue) {
        this.taxationBalanceDue = taxationBalanceDue;
    }
    
    public Date getTaxationInfoLastUpdate() {
        return taxationInfoLastUpdate;
    }
    public void setTaxationInfoLastUpdate(Date taxationInfoLastUpdate) {
        this.taxationInfoLastUpdate = taxationInfoLastUpdate;
    }
    
    public String getTaxationInfoSource() {
        return taxationInfoSource;
    }
    public void setTaxationInfoSource(String taxationInfoSource) {
        this.taxationInfoSource = taxationInfoSource;
    }
}
