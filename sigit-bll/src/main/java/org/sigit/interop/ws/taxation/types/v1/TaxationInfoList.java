package org.sigit.interop.ws.taxation.types.v1;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "taxation"
})
@XmlRootElement(name = "TaxationInfoList")
public class TaxationInfoList {

    @XmlElement(name = "Taxation")
    protected List<TaxationInfo> taxation;


    public List<TaxationInfo> getTaxation() {
        if (taxation == null) {
            taxation = new ArrayList<TaxationInfo>();
        }
        return taxation;
    }
}
