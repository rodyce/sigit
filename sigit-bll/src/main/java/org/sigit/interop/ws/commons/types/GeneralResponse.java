package org.sigit.interop.ws.commons.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeneralResponse", propOrder = { "code", "message" })
public class GeneralResponse {

    @XmlElement(name = "code", type = String.class)
    private String code;
    @XmlElement(name = "message", type = String.class)
    private String message;
    
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}
