package org.sigit.interop.ws.transaction.v1;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.sigit.interop.ws.commons.GeneralResponseMsg;
import org.sigit.interop.ws.transaction.types.v1.TransactionList;
import org.sigit.interop.ws.transaction.types.v1.TransactionResponse;
import org.sigit.services.MunicipalTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Service("v1TransactionEndpoint")
@SOAPBinding(style=SOAPBinding.Style.DOCUMENT)
@WebService(targetNamespace="http://ws.sigit.org/transaction/v1")
public class SigitTransactionEndpoint extends SpringBeanAutowiringSupport {

    @Autowired
    private MunicipalTransactionService trxService;
    
    @WebMethod
    public String getTransactionData(
            @WebParam(name="presentationId") String presentationId) throws GeneralResponseMsg {
        return trxService.getTransactionData(presentationId);
    }
    
    @WebMethod
    public String receiveTransaction(
            @WebParam(name="presentationId") String presentationId,
            @WebParam(name="entityName") String entityName) throws GeneralResponseMsg {
        return trxService.receiveTransaction(presentationId, entityName);
    }

    @WebMethod
    public String getNextTransaction(
            @WebParam(name="idOnly") boolean idOnly) throws GeneralResponseMsg {
        return trxService.getNextTransaction(idOnly);
    }
    
    @WebMethod
    public TransactionList getPendingTransactions(
            @WebParam(name="deliverForExternalApprovalOnly") boolean deliverForExternalApprovalOnly) throws GeneralResponseMsg {
        return trxService.getPendingTransactions(deliverForExternalApprovalOnly);
    }
    
    @WebMethod
    public void processTransactionResponse(
            @WebParam(name="transactionResponse") TransactionResponse transactionResponse) throws GeneralResponseMsg {
        trxService.processTransactionResponse(transactionResponse);
    }
    
    @WebMethod
    public void processTransactionResponseXML(
            @WebParam(name="transactionResponseXML") String transactionResponseXML) throws GeneralResponseMsg {
        trxService.processTransactionResponseXML(transactionResponseXML);
    }
}
