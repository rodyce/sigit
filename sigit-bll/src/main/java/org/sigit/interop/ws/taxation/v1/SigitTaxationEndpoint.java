package org.sigit.interop.ws.taxation.v1;

import java.math.BigDecimal;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.sigit.interop.ws.commons.GeneralResponseMsg;
import org.sigit.interop.ws.taxation.types.v1.TaxationInfo;
import org.sigit.interop.ws.taxation.types.v1.TaxationInfoList;
import org.sigit.services.TaxationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Service("v1TaxationEndpoint")
@SOAPBinding(style=SOAPBinding.Style.DOCUMENT)
@WebService(targetNamespace="http://ws.sigit.org/taxation/v1")
public class SigitTaxationEndpoint extends SpringBeanAutowiringSupport {
    @Autowired
    private TaxationService taxationService;
    
    @PostConstruct
    private void init() {
    }
    
    @WebMethod
    public TaxationInfo getTaxationInfo(
            @WebParam(name="parcelNationalIdentifier") String parcelNationalIdentifier) throws GeneralResponseMsg {
        return taxationService.getTaxationInfo(parcelNationalIdentifier);
    }    

    @WebMethod
    public TaxationInfoList getTaxationInfoUpdatedAfter(
            @WebParam(name="date") Date date) throws GeneralResponseMsg {
        return taxationService.getTaxationInfoUpdatedAfter(date);
    }
    
    @WebMethod
    public TaxationInfo updateParcelTaxationInfo(
            @WebParam(name="parcelNationalIdentifier") String parcelNationalIdentifier,
            @WebParam(name="commercialAppraisal") BigDecimal commercialAppraisal,
            @WebParam(name="fiscalAppraisal") BigDecimal fiscalAppraisal,
            @WebParam(name="taxationBalanceDue") BigDecimal taxationBalanceDue) throws GeneralResponseMsg {
        
        return taxationService.updateParcelTaxationInfo(parcelNationalIdentifier,
                commercialAppraisal, fiscalAppraisal, taxationBalanceDue);
    }
}
