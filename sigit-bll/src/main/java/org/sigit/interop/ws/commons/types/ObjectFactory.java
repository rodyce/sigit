package org.sigit.interop.ws.commons.types;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {
    
    public ObjectFactory() {
    }

    public GeneralResponse createGeneralResponse(String code, String message) {
        GeneralResponse gr = new GeneralResponse();
        
        gr.setCode(code);
        gr.setMessage(message);
        
        return gr;
    }
}
