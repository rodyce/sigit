package org.sigit.interop.ws.commons;

import org.sigit.interop.ws.commons.types.GeneralResponse;

import javax.xml.ws.WebFault;

@WebFault(name = "GeneralResponseMsg", targetNamespace = "http://ws.sigit.org/commons")
public class GeneralResponseMsg extends Exception {
    private static final long serialVersionUID = 1L;
    
    private GeneralResponse generalResponse;
    

    public GeneralResponseMsg() {
        super();
    }
    
    public GeneralResponseMsg(String message) {
        super(message);
    }

    public GeneralResponseMsg(String message, Throwable cause) {
        super(message, cause);
    }

    public GeneralResponseMsg(String message, GeneralResponse generalResponse) {
        super(message);
        this.generalResponse = generalResponse;
    }

    public GeneralResponseMsg(String message, GeneralResponse generalResponse, Throwable cause) {
        super(message, cause);
        this.generalResponse = generalResponse;
    }

    public GeneralResponse getFaultInfo() {
        return this.generalResponse;
    }
}
