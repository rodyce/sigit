package org.sigit.interop.dbf;

import java.io.File;
import java.io.IOException;

import org.geotools.data.shapefile.dbf.DbaseFileHeader;

public class ResponsibilitiesDBF extends DBF {
    public ResponsibilitiesDBF(File file) throws Exception {
        super(file);

        DbaseFileHeader dbfHeader = getDbfHeader();
        dbfHeader.addColumn(DBFFieldNames.RRR.RESPONSIBILITY.Type, 'C', 30, 0);
    }
    
    public void addRecord(long id, long propertyId, long extPartyId,
            String description, long shareNumerator, long shareDenominator,
            String type) throws IOException {
        addRecord(new Object[] {id, propertyId, extPartyId, description, shareNumerator,
                shareDenominator, type});
    }
}
