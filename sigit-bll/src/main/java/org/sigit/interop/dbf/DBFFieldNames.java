package org.sigit.interop.dbf;

public class DBFFieldNames {
    public class SPATIAL_ZONE {
        public static final String ZoneId = "ZONE_ID";
        public static final String Shape = "SHAPE";
        public static final String ZoneName = "ZONE_NAME";
        public static final String LocationInCountry = "LOCCOUNTRY";
        public static final String GeometryPerimeter = "GEOM_PER";
        public static final String MeasuredPerimeter = "MEASD_PER";
        public static final String DocumentedPerimeter = "DOCTED_PER";
        public static final String GeometryArea = "GEOM_AREA";
        public static final String MeasuredArea = "MEASD_AREA";
        public static final String DocumentedArea = "DOCTEDAREA";
        public static final String ProposedLandUse = "PROP_LU";
        public static final String CurrentLandUse = "CURR_LU";
        public static final String GeomHash = "GEOM_HASH";

        public class PARCEL {
            public static final String FieldTab = "FIELD_TAB";
            public static final String MunicipalKey = "MUNI_KEY";
            public static final String CadastralKey = "CADTRALKEY";
            public static final String NeighborhoodName = "NEIGH_NAME";
            public static final String AccessWay1 = "AW1";
            public static final String AccessWay2 = "AW2";
            public static final String HouseNumber = "HOUSE_NO";
            public static final String DocumentedBuiltArea = "DOC_B_AREA";
            public static final String GroundBuiltArea = "GRNDB_AREA";
            public static final String CommercialAppraisal = "COMM_APPR";
            public static final String FiscalAppraisal = "FISCALAPPR";
            public static final String BalanceDue = "BLNCE_DUE";
            public static final String TaxationStatus = "TAX_STATUS";
        }
    }
    
    public class PROPERTY_ZONE {
        public static final String ZoneId = "ZONE_ID";
        public static final String PropertyId = "PROPERTYID";
    }
    
    public class PROPERTY {
        public static final String Id = "PROPERTYID";
        public static final String Registration = "REGISTRATI";
        public static final String Tome = "TOME";
        public static final String Folio = "FOLIO";
        public static final String AnnotationNumber = "ANN_NO";
    }
    
    public class RRR {
        public static final String Id = "RRR_ID";
        public static final String PropertyId = "PROPERTYID";
        public static final String ExtPartyId = "PARTYID";
        public static final String Description = "DESCRIPTIO";
        public static final String ShareNumerator = "NUMERATOR";
        public static final String ShareDenominator = "DENOMINATO";
        
        public class RIGHT {
            public static final String Type = "TYPE";
        }
        
        public class RESTRICTION {
            public static final String Type = "TYPE";
            public static final String PartyRequired = "PARTY_REQ";
        }
        
        public class RESPONSIBILITY {
            public static final String Type = "TYPE";
        }
    }
    
    public class PARTY {
        public static final String Id = "ID";
        public static final String Identity = "IDENTITY";
        public static final String Rtn = "RTN";
        public static final String Type = "TYPE";
        public static final String Name = "NAME";
        public static final String Address = "ADDRESS";
        public static final String Phones = "PHONES";
        public static final String Emails = "EMAILS";
        public static final String FirstName1 = "FIRST_NME1";
        public static final String FirstName2 = "FIRST_NME2";
        public static final String LastName1 = "LAST_NAME1";
        public static final String LastName2 = "LAST_NAME2";
        public static final String Gender = "GENDER";
        public static final String Nationality = "NATIONALI";
        public static final String Dob = "DOB";
        public static final String MaritalStatus = "MARIT_STAT";
    }
    
}


