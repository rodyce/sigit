package org.sigit.interop.dbf;

import java.io.File;

import org.geotools.data.shapefile.dbf.DbaseFileHeader;

public abstract class RRRDBF extends DBF {
    public RRRDBF(File file) throws Exception {
        super(file);
        
        DbaseFileHeader dbfHeader = getDbfHeader();
        dbfHeader.addColumn(DBFFieldNames.RRR.Id, 'N', DBF.LONG_LENGTH, 0);
        dbfHeader.addColumn(DBFFieldNames.RRR.PropertyId, 'N', DBF.LONG_LENGTH, 0);
        dbfHeader.addColumn(DBFFieldNames.RRR.ExtPartyId, 'N', DBF.LONG_LENGTH, 0);
        dbfHeader.addColumn(DBFFieldNames.RRR.Description, 'C', 100, 0);
        dbfHeader.addColumn(DBFFieldNames.RRR.ShareNumerator, 'N', DBF.INT_LENGTH, 0);
        dbfHeader.addColumn(DBFFieldNames.RRR.ShareDenominator, 'N', DBF.INT_LENGTH, 0);
    }
}
