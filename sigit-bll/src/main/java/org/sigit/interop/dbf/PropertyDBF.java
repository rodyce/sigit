package org.sigit.interop.dbf;

import java.io.File;
import java.io.IOException;

import org.geotools.data.shapefile.dbf.DbaseFileHeader;


public class PropertyDBF extends DBF {
    public PropertyDBF(File file) throws Exception {
        super(file);
        
        DbaseFileHeader dbfHeader = getDbfHeader();
        dbfHeader.addColumn(DBFFieldNames.PROPERTY.Id, 'N', 9, 0);
        dbfHeader.addColumn(DBFFieldNames.PROPERTY.Registration, 'C', 30, 0);
        dbfHeader.addColumn(DBFFieldNames.PROPERTY.Tome, 'N', DBF.INT_LENGTH, 0);
        dbfHeader.addColumn(DBFFieldNames.PROPERTY.Folio, 'N', DBF.INT_LENGTH, 0);
        dbfHeader.addColumn(DBFFieldNames.PROPERTY.AnnotationNumber, 'N', DBF.INT_LENGTH, 0);
    }
    
    public void addRecord(long id, String registration, Integer tome,
            Integer folio, Integer annotationNumber) throws IOException {
        addRecord(new Object[] {id, registration, tome, folio, annotationNumber});
    }
}
