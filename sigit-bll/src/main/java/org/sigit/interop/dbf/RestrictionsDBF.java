package org.sigit.interop.dbf;

import java.io.File;
import java.io.IOException;

import org.geotools.data.shapefile.dbf.DbaseFileHeader;

public class RestrictionsDBF extends RRRDBF {
    public RestrictionsDBF(File file) throws Exception {
        super(file);

        DbaseFileHeader dbfHeader = getDbfHeader();
        dbfHeader.addColumn(DBFFieldNames.RRR.RESTRICTION.Type, 'C', 30, 0);
        dbfHeader.addColumn(DBFFieldNames.RRR.RESTRICTION.PartyRequired, 'L', DBF.LOGICAL_LENGTH, 0);
    }
    
    public void addRecord(long id, long propertyId, long extPartyId,
            String description,    long shareNumerator, long shareDenominator,
            String type, boolean partyRequired) throws IOException {
        addRecord(new Object[] {id, propertyId, extPartyId, description, shareNumerator,
                shareDenominator, type, partyRequired ? "T" : "F"});
    }
}
