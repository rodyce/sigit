package org.sigit.interop.dbf;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.geotools.data.shapefile.dbf.DbaseFileHeader;


public class PartyDBF extends DBF {

    public PartyDBF(File file) throws Exception {
        super(file);

        DbaseFileHeader dbfHeader = getDbfHeader();
        dbfHeader.addColumn(DBFFieldNames.PARTY.Id, 'N', DBF.LONG_LENGTH, 0);
        dbfHeader.addColumn(DBFFieldNames.PARTY.Identity, 'C', 20, 0);
        dbfHeader.addColumn(DBFFieldNames.PARTY.Rtn, 'C', 20, 0);
        dbfHeader.addColumn(DBFFieldNames.PARTY.Type, 'C', 1, 0);
        dbfHeader.addColumn(DBFFieldNames.PARTY.Name, 'C', 150, 0);
        dbfHeader.addColumn(DBFFieldNames.PARTY.Address, 'C', 150, 0);
        dbfHeader.addColumn(DBFFieldNames.PARTY.Phones, 'C', 150, 0);
        dbfHeader.addColumn(DBFFieldNames.PARTY.Emails, 'C', 150, 0);
        dbfHeader.addColumn(DBFFieldNames.PARTY.FirstName1, 'C', 30, 0);
        dbfHeader.addColumn(DBFFieldNames.PARTY.FirstName2, 'C', 30, 0);
        dbfHeader.addColumn(DBFFieldNames.PARTY.LastName1, 'C', 30, 0);
        dbfHeader.addColumn(DBFFieldNames.PARTY.LastName2, 'C', 30, 0);
        dbfHeader.addColumn(DBFFieldNames.PARTY.Gender, 'C', 1, 0);
        dbfHeader.addColumn(DBFFieldNames.PARTY.Nationality, 'C', 50, 0);
        dbfHeader.addColumn(DBFFieldNames.PARTY.Dob, 'D', DBF.DATE_LENGTH, 0);
        dbfHeader.addColumn(DBFFieldNames.PARTY.MaritalStatus, 'C', 1, 0);
    }
    
    private void addUniqueRecord(Long id, String identity, String rtn, Character type, String name,
            String address, String phones, String emails,
            String firstName1, String firstName2, String lastName1, String lastName2, char gender,
            String nationality, Date dob, Character maritalStatus) throws IOException {
        addUniqueRecord(id, new Object[] {id, identity, rtn, type, name, address, phones, emails,
                firstName1, firstName2, lastName1, lastName2, gender, nationality, dob, maritalStatus});
    }
    
    public void addNaturalPerson(Long id, String identity, String rtn, char type, String name,
            String address, String phones, String emails,
            String firstName1, String firstName2, String lastName1, String lastName2, char gender,
            String nationality, Date dob, char maritalStatus) throws IOException {
        addUniqueRecord(id, identity, rtn, type, name, address, phones, emails,
                firstName1, firstName2, lastName1, lastName2, gender,
                nationality, dob, maritalStatus);
    }

    public void addLegalPerson(Long id, String identity, String rtn, char type, String name,
            String address, String phones, String emails) throws IOException {
        addUniqueRecord(id, identity, rtn, type, name, address, phones, emails,
                null, null, null, null, (Character) null, null, null, null);
    }

}
