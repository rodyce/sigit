package org.sigit.interop.dbf;

import java.io.File;
import java.io.IOException;


public class Property_SpatialZoneDBF extends DBF {
    public Property_SpatialZoneDBF(File file) throws Exception {
        super(file);
        
        getDbfHeader().addColumn(DBFFieldNames.PROPERTY_ZONE.ZoneId, 'N', 9, 0);
        getDbfHeader().addColumn(DBFFieldNames.PROPERTY_ZONE.PropertyId, 'N', 9, 0);
    }
    
    public void addRecord(long zoneId, long propertyId) throws IOException {
        Object[] newRec = new Object[] {zoneId, propertyId};
        addRecord(newRec);
    }
}
