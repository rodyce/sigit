package org.sigit.interop.dbf;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.geotools.data.shapefile.dbf.DbaseFileHeader;

public class RightsDBF extends RRRDBF {
    public RightsDBF(File file) throws Exception {
        super(file);

        DbaseFileHeader dbfHeader = getDbfHeader();
        dbfHeader.addColumn(DBFFieldNames.RRR.RIGHT.Type, 'C', 30, 0);
    }
    
    public void addRecord(UUID id, UUID propertyId, UUID extPartyId,
            String description, long shareNumerator, long shareDenominator,
            String type) throws IOException {
        addRecord(new Object[] {id, propertyId, extPartyId, description,
                shareNumerator, shareDenominator, type});
    }
}
