package org.sigit.interop.gmlproducers;

import org.sigit.model.commons.IParcel;
import org.sigit.model.commons.IProperty;
import org.sigit.model.commons.IResponsibility;
import org.sigit.model.commons.IRestriction;
import org.sigit.model.commons.IRight;

import java.util.ArrayList;
import java.util.List;

import org.deegree.commons.tom.TypedObjectNode;
import org.deegree.commons.tom.genericxml.GenericXMLElement;
import org.deegree.commons.tom.genericxml.GenericXMLElementContent;
import org.deegree.feature.GenericFeature;
import org.deegree.feature.property.GenericProperty;
import org.deegree.feature.property.Property;
import org.deegree.feature.types.GenericFeatureType;
import org.deegree.feature.types.property.ArrayPropertyType;
import org.deegree.feature.types.property.PropertyType;
import org.deegree.gml.GMLVersion;

public class PropertyGMLProducer extends GMLProducer<IProperty> {
    private static final ArrayPropertyType rrrPropertyType = new ArrayPropertyType(newQName("RRR"), 1, 1, false, false, null);
    private static final ArrayPropertyType zonesPropertyType = new ArrayPropertyType(newQName("Parcels"), 1, 1, false, false, null);

    private static GenericFeatureType propertyFeatureType;

    
    static {
        propertyFeatureType = buildFeatureType();
    }
    
    private static GenericFeatureType buildFeatureType() {
        List<PropertyType> propertyTypeList = new ArrayList<PropertyType>();
        propertyTypeList.add(rrrPropertyType);
        propertyTypeList.add(zonesPropertyType);
        
        return new GenericFeatureType(newQName("Property"), propertyTypeList, false);
    }
    
    public static GenericProperty buildRRRProperty(String jurisdictionCode, IProperty zoneProperty, ArrayPropertyType rrrPropertyType) {
        List<TypedObjectNode> rightsChildren = new ArrayList<TypedObjectNode>();
        for (IRight<?,?> right : zoneProperty.getRights())
            rightsChildren.add(new RRRGMLProducer(jurisdictionCode, right).buildFeature());
        GenericXMLElement rightsElements = new GenericXMLElement(newQName("Rights"), new GenericXMLElementContent(null, null, rightsChildren));
        
        List<TypedObjectNode> restrictionsChildren = new ArrayList<TypedObjectNode>();
        for (IRestriction<?,?> restriction : zoneProperty.getRestrictions())
            restrictionsChildren.add(new RRRGMLProducer(jurisdictionCode, restriction).buildFeature());
        GenericXMLElement restrictionsElements = new GenericXMLElement(newQName("Restrictions"), new GenericXMLElementContent(null, null, restrictionsChildren));

        List<TypedObjectNode> responsibilitiesChildren = new ArrayList<TypedObjectNode>();
        for (IResponsibility<?,?> responsibility : zoneProperty.getResponsibilities())
            responsibilitiesChildren.add(new RRRGMLProducer(jurisdictionCode, responsibility).buildFeature());
        GenericXMLElement responsibilitiesElements = new GenericXMLElement(newQName("Responsibilities"), new GenericXMLElementContent(null, null, responsibilitiesChildren));
        
        List<TypedObjectNode> rrrChildren = new ArrayList<TypedObjectNode>();
        rrrChildren.add(rightsElements);
        rrrChildren.add(restrictionsElements);
        rrrChildren.add(responsibilitiesElements);
        
        return new GenericProperty(rrrPropertyType, new GenericXMLElementContent(null, null, rrrChildren));
    }
    
    
    public PropertyGMLProducer(String jurisdictionCode, IProperty property) {
        super(jurisdictionCode, property);
    }
    
    @Override
    public GenericFeature buildFeature() {
        GenericProperty rrrProperty = buildRRRProperty(getJurisdictionCode(), getObjectFeature(), rrrPropertyType);
        
        List<TypedObjectNode> zoneList = new ArrayList<TypedObjectNode>();
        for (IParcel parcel : getObjectFeature().getParcels())
            zoneList.add(new ParcelGMLProducer(getJurisdictionCode(), parcel).buildFeature(false));
        GenericProperty zonesProperty = new GenericProperty(zonesPropertyType, new GenericXMLElementContent(null, null, zoneList));
        
        
        List<Property> propertyList = new ArrayList<Property>();
        propertyList.add(rrrProperty);
        propertyList.add(zonesProperty);
        
        return new GenericFeature(propertyFeatureType, String.valueOf(getObjectFeature().getuID()), propertyList, GMLVersion.GML_31);
    }


    public IProperty getProperty() {
        return getObjectFeature();
    }
}
