package org.sigit.interop.gmlproducers;

import org.sigit.model.commons.IAdministrativeSource;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.deegree.commons.tom.primitive.PrimitiveType;
import org.deegree.commons.utils.time.DateUtils;
import org.deegree.feature.GenericFeature;
import org.deegree.feature.property.Property;
import org.deegree.feature.property.SimpleProperty;
import org.deegree.feature.types.GenericFeatureType;
import org.deegree.feature.types.property.PropertyType;
import org.deegree.feature.types.property.SimplePropertyType;
import org.deegree.gml.GMLVersion;

public class AdministrativeSourceGMLProducer extends GMLProducer<IAdministrativeSource> {
    private static final SimplePropertyType sidPropertyType = new SimplePropertyType(newQName("sID"), 1, 1, PrimitiveType.INTEGER, false, false, null);
    private static final SimplePropertyType availabilityStatusPropertyType = new SimplePropertyType(newQName("availabilityStatus"), 1, 1, PrimitiveType.STRING, false, false, null);
    private static final SimplePropertyType typePropertyType = new SimplePropertyType(newQName("type"), 1, 1, PrimitiveType.STRING, false, false, null);
    private static final SimplePropertyType acceptancePropertyType = new SimplePropertyType(newQName("acceptance"), 1, 1, PrimitiveType.DATE_TIME, false, false, null);
    private static final SimplePropertyType recordationPropertyType = new SimplePropertyType(newQName("recordation"), 1, 1, PrimitiveType.DATE_TIME, false, false, null);
    private static final SimplePropertyType submissionPropertyType = new SimplePropertyType(newQName("submission"), 1, 1, PrimitiveType.DATE_TIME, false, false, null);
    //private static final ArrayPropertyType archivePropertyType;
    
    private static GenericFeatureType administrativeSourceFeatureType;
    
    
    static {
        administrativeSourceFeatureType = buildFeatureType();
    }
    
    private static GenericFeatureType buildFeatureType() {
        List<PropertyType> propertyTypeList = new ArrayList<PropertyType>();
        propertyTypeList.add(sidPropertyType);
        propertyTypeList.add(availabilityStatusPropertyType);
        propertyTypeList.add(typePropertyType);
        propertyTypeList.add(acceptancePropertyType);
        propertyTypeList.add(recordationPropertyType);
        propertyTypeList.add(submissionPropertyType);
        
        return new GenericFeatureType(newQName("AdministrativeSource"), propertyTypeList, false);
    }
    
    public AdministrativeSourceGMLProducer(String jurisdictionCode, IAdministrativeSource administrativeSource) {
        super(jurisdictionCode, administrativeSource);
    }

    @Override
    public GenericFeature buildFeature() {
        SimpleProperty sidProperty = new SimpleProperty(sidPropertyType, String.valueOf(getObjectFeature().getsID()), PrimitiveType.STRING);
        SimpleProperty availabilityStatusProperty = new SimpleProperty(availabilityStatusPropertyType, String.valueOf(getObjectFeature().getAvailabilityStatus().toString()), PrimitiveType.STRING);
        SimpleProperty typeProperty = new SimpleProperty(typePropertyType, String.valueOf(getObjectFeature().getType().toString()), PrimitiveType.STRING);

        SimpleProperty acceptanceProperty = null;
        if (getObjectFeature().getAcceptance() != null) {
            acceptanceProperty = new SimpleProperty(acceptancePropertyType,
                    DateUtils.formatISO8601Date(getObjectFeature().getAcceptance()),
                    PrimitiveType.DATE_TIME);
        }
        SimpleProperty recordationProperty = null;
        if (getObjectFeature().getRecordation() != null) {
            recordationProperty = new SimpleProperty(recordationPropertyType,
                    DateUtils.formatISO8601Date(getObjectFeature().getRecordation()),
                    PrimitiveType.DATE_TIME);
        }
        SimpleProperty submissionProperty = null;
        if (getObjectFeature().getSubmission() != null) {
            submissionProperty = new SimpleProperty(submissionPropertyType,
                    DateUtils.formatISO8601Date(getObjectFeature().getSubmission()),
                    PrimitiveType.DATE_TIME);
        }
        
        List<Property> propertyList = new ArrayList<Property>();
        propertyList.add(sidProperty);
        propertyList.add(availabilityStatusProperty);
        propertyList.add(typeProperty);
        if (acceptanceProperty != null) {
            propertyList.add(acceptanceProperty);
        }
        if (recordationProperty != null) {
            propertyList.add(recordationProperty);
        }
        if (submissionProperty != null) {
            propertyList.add(submissionProperty);
        }
        
        return new GenericFeature(administrativeSourceFeatureType, String.valueOf(getObjectFeature().getsID()), propertyList, GMLVersion.GML_31);
    }
}
