package org.sigit.util;

public enum EditMode {
    VIEWING,
    
    ADDING,
    
    EDITING,
    
    DELETING
}
