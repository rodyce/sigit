package org.sigit.util;

import org.sigit.model.ladm.special.Rational;

public class ShareValue implements Cloneable {
    private int numerator;
    private int denominator;
    
    public ShareValue() {
        setDefault();
    }
    public ShareValue(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
        
        this.simplify();
    }
    public ShareValue(Rational r) {
        if (r != null) {
            this.numerator = r.getNumerator();
            this.denominator = r.getDenominator();
            
            this.simplify();
        }
        else
            setDefault();
    }
    
    private void setDefault() {
        this.numerator = 0;
        this.denominator = 1;        
    }
    
    public int getNumerator() {
        return numerator;
    }
    public void setNumerator(int numerator) {
        this.numerator = numerator;
        
        if (numerator != 0)
            this.simplify();
        else
            this.denominator = 1;
    }
    
    public int getDenominator() {
        return denominator;
    }
    public void setDenominator(int denominator) {
        if (denominator == 0)
            throw new IllegalArgumentException("Denominator can not be zero");
        
        this.denominator = denominator;
        this.simplify();
    }
    
    public ShareValue add(ShareValue other) {
        ShareValue r = new ShareValue();
        r.setNumerator(this.numerator * other.denominator + other.numerator * this.denominator);
        r.setDenominator(this.denominator * other.denominator);
        r.simplify();
        
        return r;
    }
    
    public void addOther(ShareValue other) {
        numerator = this.numerator * other.denominator + other.numerator * this.denominator;
        denominator = this.denominator * other.denominator;
        
        simplify();
    }
    
    public ShareValue opposite() {
        return new ShareValue(numerator * -1, denominator);
    }
    
    @Override
    public String toString() {
        return String.format("%d/%d", numerator, denominator);
    }
    
    public void setToZero() {
        setNumerator(0);
    }
    
    public Rational asLadmRational() {
        return new Rational(numerator, denominator);
    }
    
    @Override
    public boolean equals(Object other) {
        if (other instanceof ShareValue) {
            ShareValue otherSV = (ShareValue) other;
            
            simplify();
            otherSV.simplify();
            
            return numerator == otherSV.getNumerator()
                && denominator == otherSV.getDenominator();
        }
        
        return false;
    }
    
    public boolean isZero() {
        return numerator == 0;
    }
    public boolean isOne() {
        return numerator == 1 && denominator == 1;
    }
    public boolean isOneHalf() {
        return numerator == 1 && denominator == 2;
    }
    public boolean isOneThird() {
        return numerator == 1 && denominator == 3;
    }
    

    @Override
    public ShareValue clone() {
        return new ShareValue(numerator, denominator);
    }
    
    private void simplify() {
        int gcd = _gcd(numerator, denominator);
        
        numerator /= gcd;
        denominator /= gcd;
    }
    
    private int _gcd(int u, int v) {
        int r, res;
        
        while (true) {
            if (v == 0) {
                res = u;
                break;
            }
            else {
                r = u % v;
                u = v;
                v = r;
            }
        }
        
        return res;
    }
}
