package org.sigit.util;

import java.io.Serializable;

import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

@Component("appOptions")
@Scope("session")
public class AppOptions implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private String uploadFileSavePath;

    public String getUploadFileSavePath() {
        if (uploadFileSavePath != null) {
            if (!uploadFileSavePath.startsWith("/"))
                uploadFileSavePath = "/" + uploadFileSavePath;
        }
        return System.getProperty("user.home") + uploadFileSavePath;
    }
    public void setUploadFileSavePath(String uploadFileSavePath) {
        this.uploadFileSavePath = uploadFileSavePath;
    }
}
