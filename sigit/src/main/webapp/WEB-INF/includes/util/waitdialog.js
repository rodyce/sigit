function alertar() {
    alert('funciona');
}

function applyModalPanelEffect(panelId, effectFunc, params) {
    alert('dia');
    if (panelId && effectFunc) {
        
        var modalPanel = $(panelId);
        
        if (modalPanel && modalPanel.component) {
            var component = modalPanel.component;
            var div = component.getSizedElement(); 
            
            Element.hide(div);
                        
            effectFunc.call(this, Object.extend({targetId: div.id}, params || {}));
        }
                
    }
}


var waitDialogShown = false;
        var useTimerBeforeShowWaitDialog = true;
        var waitDialogTimeout = 50;
        var waitDialogTimer;

function showWaitDialog() {
    //avoid attempt to show it if it is already shown
    if (!waitDialogShown) {
        Richfaces.showModalPanel('wait-dialog');
        waitDialogShown = true;
    }
}

function onRequestStart() {
    if (useTimerBeforeShowWaitDialog) {
        waitDialogTimer = setTimeout("showWaitDialog();", waitDialogTimeout);
    } else {
        showWaitDialog();
    }
}
function onRequestEnd() {
    if (waitDialogShown) {
        Richfaces.hideModalPanel('wait-dialog');
        waitDialogShown = false;
    } else if (useTimerBeforeShowWaitDialog && waitDialogTimer) {
        clearTimeout(waitDialogTimer);
    }
}
