var pickPointControl;
var pickSinglePointControl;

function setOffsetControls(geom, ipLat, ipLon, dir, dist) {
    var p1 = geom.components[0];
    var p2 = geom.components[1]; 

    safeSetValue(ipLat, p1.y);
    safeSetValue(ipLon, p1.x);
    safeSetValue(dir, cardinalDirection(p1, p2));
    safeSetValue(dist, geom.getLength());
}

function safeSetValue(obj, valueToSet) {
    if (obj != undefined)
        if (obj != null)
            obj.value = valueToSet;
}

function getSelectControl() {
    var selectControl = null;
    for (var i = 0; i < mapEditor.controls.length; i++)
        if (mapEditor.controls[i].id == 'parcelSelectControl') {
            selectControl = mapEditor.controls[i];
            break;
        }
    return selectControl;
}
function getPickPointControl() {
    if (pickPointControl == null) {
        for (var i = 0; i < mapEditor.controls.length; i++)
            if (mapEditor.controls[i].id == 'pickPointControl') {
                pickPointControl = mapEditor.controls[i];
                break;
            }
        if (pickPointControl == null) {
            pickPointControl = new OpenLayers.Control.DrawFeature(
                    polyLayer, OpenLayers.Handler.Path, {handlerOptions: {maxVertices: 2}});
            pickPointControl.id = 'pickPointControl';
            mapEditor.addControl(pickPointControl);
        }
    }
    
    return pickPointControl;
}
function getPickSinglePointControl() {
    if (pickSinglePointControl == null) {
        for (var i = 0; i < mapEditor.controls.length; i++)
            if (mapEditor.controls[i].id == 'pickSinglePointControl') {
                pickSinglePointControl = mapEditor.controls[i];
                break;
            }
        if (pickSinglePointControl == null) {
            pickSinglePointControl = new OpenLayers.Control.DrawFeature(
                    polyLayer, OpenLayers.Handler.Point);
            pickSinglePointControl.id = 'pickSinglePointControl';
            mapEditor.addControl(pickSinglePointControl);
        }
    }

    return pickSinglePointControl;
}


function resetInputs() {
    resetMethod1Inputs();
    resetMethod2Inputs();
    resetMethod3Inputs();
    vlayer.destroyFeatures();

    var selectControl = getSelectControl();
    if (selectControl != null)
        getSelectControl().unselectAll();
}

function clearMap() {
    vlayer.destroyFeatures();
}


function addOffsetFeatures(lineFeature, previousFeatures, removeAll) {
    var layer = lineFeature.layer;

    if (removeAll) {
        var i = 0;
        while (i < layer.features.size()) {
            if (lineFeature != layer.features[i])
                layer.destroyFeatures(layer.features[i]);
            else
                i++;
        }
    }
    else {
        if (previousFeatures != undefined)
            if (previousFeatures != null)
                layer.destroyFeatures(previousFeatures);
    }

    var fps = getLinePointsFeature(lineFeature);

    var featuresToAdd = [lineFeature, fps[0], fps[1]]; 
    layer.addFeatures(featuresToAdd);

    return featuresToAdd;
}

function setLineLength(lineFeature, distValue) {
    var newLine = newLineFromPoints(lineFeature.geometry, false, distValue);
    var attributes = {name: "my name", bar: "foo"};

    var newLineFeature = new OpenLayers.Feature.Vector(newLine, attributes);
    var fps = getLinePointsFeature(newLineFeature);

    return [newLineFeature, fps[0], fps[1]]; 
}

function setLineBearing(lineFeature, radAngle) {
    var newLine = newLineFromBearing(lineFeature.geometry, radAngle);
    var attributes = {name: "my name", bar: "foo"};

    var newLineFeature = new OpenLayers.Feature.Vector(newLine, attributes);
    var fps = getLinePointsFeature(newLineFeature);

    return [newLineFeature, fps[0], fps[1]]; 
}

function getLinePointsFeature(lineFeature) {
    var fp1 = getPointFeature(lineFeature.geometry.components[0].x,
            lineFeature.geometry.components[0].y); 
    var fp2 = getPointFeature(lineFeature.geometry.components[1].x,
            lineFeature.geometry.components[1].y);

    return [fp1, fp2]; 
}

function getPointFeature(x, y) {
    var fp1 = new OpenLayers.Feature.Vector(
            new OpenLayers.Geometry.Point(x, y));

    return fp1;
}
