<?xml version="1.0" encoding="UTF-8"?>
<sld:StyledLayerDescriptor xmlns="http://www.opengis.net/sld"
    xmlns:sld="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
    xmlns:gml="http://www.opengis.net/gml" version="1.0.0">
    <sld:UserLayer>
        <sld:LayerFeatureConstraints>
            <sld:FeatureTypeConstraint />
        </sld:LayerFeatureConstraints>
        <sld:UserStyle>
            <sld:Name>SIGIT Style for Land Uses</sld:Name>
            <sld:Title />
            <sld:FeatureTypeStyle>
                <sld:Name>group 0</sld:Name>
                <sld:FeatureTypeName>Feature</sld:FeatureTypeName>
                <sld:SemanticTypeIdentifier>generic:geometry
                </sld:SemanticTypeIdentifier>
                <sld:SemanticTypeIdentifier>simple</sld:SemanticTypeIdentifier>
                <sld:Rule>
                    <sld:Name>R01</sld:Name>
                    <sld:Title>Land use theme</sld:Title>

                    <ogc:Filter>
                        <ogc:PropertyIsEqualTo>
                            <ogc:Function name="dimension">
                                <ogc:PropertyName>shape</ogc:PropertyName>
                            </ogc:Function>
                            <ogc:Literal>2</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>

                    <sld:PolygonSymbolizer>
                        <sld:Fill>
                            <sld:CssParameter name="fill">
                                <ogc:PropertyName>fillcolor</ogc:PropertyName>
                            </sld:CssParameter>
                            <sld:CssParameter name="fill-opacity">
                                <ogc:PropertyName>fillopacity</ogc:PropertyName>
                            </sld:CssParameter>
                        </sld:Fill>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">
                                <ogc:PropertyName>strokecolor</ogc:PropertyName>
                            </sld:CssParameter>
                            <sld:CssParameter name="stroke-width">
                                <ogc:PropertyName>strokewidth</ogc:PropertyName>
                            </sld:CssParameter>
                        </sld:Stroke>
                    </sld:PolygonSymbolizer>
                </sld:Rule>

                <sld:Rule>
                    <sld:Name>R02</sld:Name>
                    <sld:Title>Land use theme</sld:Title>

                    <ogc:Filter>
                        <ogc:PropertyIsEqualTo>
                            <ogc:Function name="dimension">
                                <ogc:PropertyName>shape</ogc:PropertyName>
                            </ogc:Function>
                            <ogc:Literal>1</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>

                    <sld:LineSymbolizer>
                        <sld:Stroke>
                            <sld:CssParameter name="stroke">
                                <ogc:PropertyName>strokecolor</ogc:PropertyName>
                            </sld:CssParameter>
                            <sld:CssParameter name="stroke-width">
                                <ogc:PropertyName>strokewidth</ogc:PropertyName>
                            </sld:CssParameter>
                        </sld:Stroke>
                    </sld:LineSymbolizer>
                </sld:Rule>

                <sld:Rule>
                    <sld:Name>R03</sld:Name>
                    <sld:Title>Land use theme</sld:Title>

                    <ogc:Filter>
                        <ogc:PropertyIsEqualTo>
                            <ogc:Function name="dimension">
                                <ogc:PropertyName>shape</ogc:PropertyName>
                            </ogc:Function>
                            <ogc:Literal>0</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>

                    <sld:PointSymbolizer>
                        <sld:Graphic>
                            <sld:Mark>
                                <sld:WellKnownName>square</sld:WellKnownName>
                                <sld:Fill>
                                    <sld:CssParameter name="fill">
                                        <ogc:PropertyName>fillcolor</ogc:PropertyName>
                                    </sld:CssParameter>
                                    <sld:CssParameter name="fill-opacity">
                                        <ogc:PropertyName>fillopacity</ogc:PropertyName>
                                    </sld:CssParameter>
                                </sld:Fill>
                            </sld:Mark>
                            <sld:Size>6</sld:Size>
                        </sld:Graphic>
                    </sld:PointSymbolizer>
                </sld:Rule>

                <sld:Rule>
                    <sld:Name>zoomedIn</sld:Name>
                    <sld:MaxScaleDenominator>6500.0</sld:MaxScaleDenominator>
                    <sld:TextSymbolizer>
                        <sld:Label>
                            <ogc:PropertyName>zonename</ogc:PropertyName>
                        </sld:Label>
                        <sld:Font>
                            <sld:CssParameter name="font-family">Arial</sld:CssParameter>
                            <sld:CssParameter name="font-size">12.0</sld:CssParameter>
                            <sld:CssParameter name="font-style">normal</sld:CssParameter>
                            <sld:CssParameter name="font-weight">bold</sld:CssParameter>
                        </sld:Font>
                        <sld:LabelPlacement>
                            <sld:PointPlacement>
                                <sld:AnchorPoint>
                                    <sld:AnchorPointX>
                                        <ogc:Literal>0.5</ogc:Literal>
                                    </sld:AnchorPointX>
                                    <sld:AnchorPointY>
                                        <ogc:Literal>0.5</ogc:Literal>
                                    </sld:AnchorPointY>
                                </sld:AnchorPoint>
                                <sld:Displacement>
                                    <sld:DisplacementX>
                                        <ogc:Literal>0.0</ogc:Literal>
                                    </sld:DisplacementX>
                                    <sld:DisplacementY>
                                        <ogc:Literal>0.0</ogc:Literal>
                                    </sld:DisplacementY>
                                </sld:Displacement>
                                <sld:Rotation>
                                    <ogc:Literal>0.0</ogc:Literal>
                                </sld:Rotation>
                            </sld:PointPlacement>
                        </sld:LabelPlacement>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#000000
                            </sld:CssParameter>
                        </sld:Fill>
                    </sld:TextSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
    </sld:UserLayer>
</sld:StyledLayerDescriptor>