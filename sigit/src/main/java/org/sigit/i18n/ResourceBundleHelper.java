package org.sigit.i18n;

import java.io.Serializable;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Component;


@Component("resBundle")
@Scope(value="session")
public class ResourceBundleHelper implements Map<Object, Object>, Serializable {
    private static final long serialVersionUID = 1L;
    
    //Register this message source in static context
    private static ResourceBundleMessageSource resourceBundleMessageSource;
    
    static {
        resourceBundleMessageSource = new ResourceBundleMessageSource();
        resourceBundleMessageSource.setBasename("messages");
    }
    
    @Autowired
    private LocaleSelectorHelper localeSelector;
    
    
    public String loadMessage(String str) {
        String retval = "";
        
        try {
            retval = resourceBundleMessageSource.getMessage(str, null, getCurrentLocale());
        }
        catch (MissingResourceException | NoSuchMessageException ex) {
            retval = String.format("??? %s ???", str);
        }
        catch (NullPointerException npe) {
            retval = String.format("!!! %s !!!", str);
        }
        
        return retval;
    }

    @Override
    public Object get(Object key) {
        if (key instanceof String)
            return loadMessage((String) key);
        else if (key instanceof Enum<?>)
            return loadMessage(key.toString());
        
        return String.format("!!! %s !!!", key.toString());
    }
    
    
    public Locale getCurrentLocale() {
        return localeSelector.getSelectedLocale();
    }
    

    @Override
    public boolean containsKey(Object key) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException(getClass().getEnclosingMethod().getName());
    }


    
    @Override
    public int size() {
        throw new UnsupportedOperationException(getClass().getEnclosingMethod().getName());
    }
    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException(getClass().getEnclosingMethod().getName());
    }
    @Override
    public boolean containsValue(Object value) {
        throw new UnsupportedOperationException(getClass().getEnclosingMethod().getName());
    }
    @Override
    public Object put(Object key, Object value) {
        throw new UnsupportedOperationException(getClass().getEnclosingMethod().getName());
    }
    @Override
    public Object remove(Object key) {
        throw new UnsupportedOperationException(getClass().getEnclosingMethod().getName());
    }
    @Override
    public void putAll(Map<?,?> m) {
        throw new UnsupportedOperationException(getClass().getEnclosingMethod().getName());
    }
    @Override
    public void clear() {
        throw new UnsupportedOperationException(getClass().getEnclosingMethod().getName());
    }
    @Override
    public Set<Object> keySet() {
        throw new UnsupportedOperationException(getClass().getEnclosingMethod().getName());
    }
    @Override
    public Collection<Object> values() {
        throw new UnsupportedOperationException(getClass().getEnclosingMethod().getName());
    }
    @Override
    public Set<Map.Entry<Object, Object>> entrySet() {
        throw new UnsupportedOperationException(getClass().getEnclosingMethod().getName());
    }
}
