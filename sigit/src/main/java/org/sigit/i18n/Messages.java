package org.sigit.i18n;

import java.io.IOException;
import java.util.PropertyResourceBundle;

public class Messages extends PropertyResourceBundle {
    private static final String DEFAULT_PROP_FILE = "messages_es.properties";
    
    public Messages() throws IOException {
        this(DEFAULT_PROP_FILE);
    }

    public Messages(String propFile) throws IOException {
        super(Messages_es.class.getClassLoader().getResourceAsStream(propFile));
    }
}
