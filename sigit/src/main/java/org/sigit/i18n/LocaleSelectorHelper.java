package org.sigit.i18n;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("localeSelector")
@Scope(value="session")
public class LocaleSelectorHelper implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private static Collection<Locale> supportedLocales;
    private String localeString;
    private Locale selectedLocale;
    

    @PostConstruct
    void init() {
        Locale locale;
        
        if (FacesContext.getCurrentInstance() != null) {
            if (FacesContext.getCurrentInstance().getViewRoot() != null) {
                locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
            } else {
                locale = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
            }
        } else {
            locale = Locale.getDefault();
        }
        
        localeString = locale.getLanguage();
        selectedLocale = locale;
    }
    
    public Collection<Locale> getSupportedLocales() {
        if (supportedLocales == null) {
            synchronized(LocaleSelectorHelper.class) {
                if (supportedLocales == null) {
                    supportedLocales = new ArrayList<>();
                    Iterator<Locale> locales = FacesContext.getCurrentInstance()
                            .getApplication().getSupportedLocales();
                    while (locales.hasNext()) {
                        supportedLocales.add(locales.next());
                    }
                }
            }
        }
        return supportedLocales;
    }
    
    public void select() {
        if (localeString == null) return;
        
        for (Locale locale : getSupportedLocales()) {
            if (locale.getLanguage().equals(localeString)) {
                FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
                selectedLocale = locale;
                break;
            }
        }
    }
    
    public Locale getSelectedLocale() {
        return selectedLocale;
    }

    public String getLocaleString() {
        return localeString;
    }
    public void setLocaleString(String localeString) {
        this.localeString = localeString;
    }
}
