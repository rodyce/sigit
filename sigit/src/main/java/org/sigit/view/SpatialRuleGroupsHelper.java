package org.sigit.view;

import org.sigit.dao.hnd.administrative.HND_PermitRuleGroupDAO;
import org.sigit.dao.hnd.administrative.HND_SpatialRuleDAO;
import org.sigit.logic.general.GeneralHelper;
import org.sigit.model.hnd.administrative.HND_PermitRuleGroup;
import org.sigit.model.hnd.administrative.HND_SpatialRule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Scope;

@Component("spatialRuleGroupsHelper")
@Scope(value="sigit-conversation")
public class SpatialRuleGroupsHelper implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<HND_PermitRuleGroup> spatialRuleGroups;
    private HND_PermitRuleGroup selected;
    private boolean adding;
    
    private List<HND_SpatialRule> selectedSpatialRules;
    
    private List<SelectItem> spatialRuleSelectList;
    private Map<UUID, HND_SpatialRule> mapIdToRule;
    private List<String> spatialRulesSelected;
    
    @Autowired
    private GeneralHelper generalHelper;

    
    @Transactional
    public List<HND_PermitRuleGroup> getSpatialRuleGroups() {
        if (spatialRuleGroups == null) {
            spatialRuleGroups = HND_PermitRuleGroupDAO.loadPermitRuleGroups();
        }
        return spatialRuleGroups;
    }
    public void setSpatialRuleGroups(List<HND_PermitRuleGroup> spatialRuleGroups) {
        this.spatialRuleGroups = spatialRuleGroups;
    }
    
    public HND_PermitRuleGroup getSelected() {
        return selected;
    }
    public void setSelected(HND_PermitRuleGroup selected) {
        if (this.selected != selected) {
            this.selected = selected;
            selectedSpatialRules = null;
        }
    }
    
    public boolean isAdding() {
        return adding;
    }
    public void setAdding(boolean adding) {
        this.adding = adding;
    }
    
    public List<HND_SpatialRule> getSelectedSpatialRules() {
        if (selectedSpatialRules == null || selectedSpatialRules.size() == 0) {
            if (getSelected() != null) {
                selectedSpatialRules = new ArrayList<HND_SpatialRule>(getSelected().getSpatialRules());
            }
        }
        return selectedSpatialRules;
    }
    
    @Transactional
    public List<SelectItem> getSpatialRuleSelectList() {
        if (spatialRuleSelectList == null) {
            spatialRuleSelectList = new ArrayList<SelectItem>();
            Set<HND_SpatialRule> selectedSpatialRuleSet = getSelected().getSpatialRules();
            
            mapIdToRule = new HashMap<>(); 
            
            for (HND_SpatialRule sr : HND_SpatialRuleDAO.loadSpatialRules()) {
                if (selectedSpatialRuleSet == null || !selectedSpatialRuleSet.contains(sr)) {
                    mapIdToRule.put(sr.getId(), sr);
                    spatialRuleSelectList.add(new SelectItem(
                        sr.getId(),
                        generalHelper.spatialRuleDetailsText(sr)));
                }
            }
        }
        return spatialRuleSelectList;
    }
    
    public List<String> getSpatialRulesSelected() {
        return spatialRulesSelected;
    }
    public void setSpatialRulesSelected(List<String> spatialRulesSelected) {
        this.spatialRulesSelected = spatialRulesSelected;
    }
    
    public void acceptSpatialRules() {
        if (getSelected() == null || getSpatialRulesSelected() == null) return;
        
        for (String srId : getSpatialRulesSelected()) {
            getSelected().getSpatialRules().add(mapIdToRule.get(UUID.fromString(srId)));
        }
        
        spatialRuleSelectList = null;
        selectedSpatialRules = null;
    }
    
    public void deleteSpatialRule(HND_SpatialRule sr) {
        if (getSelected() == null || getSelected().getSpatialRules() == null) return;
        
        getSelected().getSpatialRules().remove(sr);
        mapIdToRule.remove(sr.getId());
        
        spatialRuleSelectList = null;
        selectedSpatialRules = null;
    }
    
    public void newPermitRuleGroup() {
        setSelected(new HND_PermitRuleGroup());
        adding = true;
    }
    
    public void editPermitRuleGroup(HND_PermitRuleGroup prg) {
        setSelected(prg);
        adding = false;
    }
    
    @Transactional
    public void deletePermitRuleGroup(HND_PermitRuleGroup sr) {
        HND_PermitRuleGroupDAO.delete(sr);
        spatialRuleGroups = null;
    }
    
    @Transactional
    public String applyChanges() {
        if (selected != null) {
            if (adding)
                spatialRuleGroups.add(selected);
            
            HND_PermitRuleGroupDAO.save(selected);
        }
            
        return null;
    }
}
