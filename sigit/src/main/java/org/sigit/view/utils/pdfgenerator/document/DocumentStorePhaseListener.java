package org.sigit.view.utils.pdfgenerator.document;

import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DocumentStorePhaseListener implements PhaseListener {
    private static final long serialVersionUID = 7308251684939658978L;

    private static final Logger log = LoggerFactory
            .getLogger(DocumentStorePhaseListener.class);

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RENDER_RESPONSE;
    }

    @Override
    public void afterPhase(PhaseEvent phaseEvent) {
        // ...
    }

    @Override
    public void beforePhase(PhaseEvent phaseEvent) {
        FacesContext facesContext = phaseEvent.getFacesContext();
        String rootId = facesContext.getViewRoot().getViewId();

        HttpServletRequest request = (HttpServletRequest) facesContext
                .getExternalContext().getRequest();

        String id = request.getParameter("docId");
        if (rootId.contains(DocumentStore.DOCSTORE_BASE_URL)) {
            sendContent(phaseEvent.getFacesContext(), id);
        }
    }

    public void sendContent(FacesContext context, String contentId) {
        try {
            DocumentData documentData = DocumentStore.instance()
                    .getDocumentData(contentId);

            if (documentData != null) {
                HttpServletResponse response = (HttpServletResponse) context
                        .getExternalContext().getResponse();
                response.setContentType(documentData.getDocumentType()
                        .getMimeType());

                response.setHeader("Content-Disposition",
                        documentData.getDisposition() + "; filename=\""
                                + documentData.getFileName() + "\"");

                documentData.writeDataToStream(response.getOutputStream());
                context.responseComplete();
            }
        } catch (IOException e) {
            log.warn(e.getLocalizedMessage(), e);
        }
    }

}
