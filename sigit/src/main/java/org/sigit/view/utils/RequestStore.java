package org.sigit.view.utils;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("request")
public class RequestStore {
    private final Map<String, Object> map = new HashMap<>();
    
    public Object get(String key) {
        return map.get(key);
    }
    public void set(String key, Object value) {
        map.put(key, value);
    }
}
