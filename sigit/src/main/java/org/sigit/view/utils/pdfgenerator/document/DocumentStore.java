package org.sigit.view.utils.pdfgenerator.document;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;

import org.sigit.commons.di.CtxComponent;
import org.sigit.view.utils.JSF;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("org.sigit.view.utils.pdfgenerator.document.documentStore")
@Scope("session")
public class DocumentStore implements Serializable {
    public static final String DOCSTORE_BASE_URL = "/services/documentstore";

    private static final long serialVersionUID = -357154201942127711L;

    Map<String, DocumentData> dataStore = new HashMap<String, DocumentData>();

    long nextId = 1;

    boolean useExtensions = false;
    String errorPage = null;

    public void setUseExtensions(boolean useExtensions) {
        this.useExtensions = useExtensions;
    }

    public void setErrorPage(String errorPage) {
        this.errorPage = errorPage;
    }

    public String getErrorPage() {
        return errorPage;
    }

    public String newId() {
        return String.valueOf(nextId++);
    }

    public void saveData(String id, DocumentData documentData) {
        dataStore.put(id, documentData);
    }

    public boolean idIsValid(String id) {
        return dataStore.get(id) != null;
    }

    public DocumentData getDocumentData(String id) {
        return dataStore.get(id);
    }

    public static DocumentStore instance() {
        return (DocumentStore) CtxComponent
                .getInstance("org.sigit.view.utils.pdfgenerator.document.documentStore");
    }

    public String preferredUrlForContent(String baseName, String extension,
            String contentId) {
        StringBuffer url = new StringBuffer(baseUrlForContent(baseName,
                extension));
        if (url.toString().indexOf('?') >= 0) {
            url.append("&docId=").append(contentId);
        } else {
            url.append("?docId=").append(contentId);
        }
        return url.toString();
    }

    protected String baseUrlForContent(String baseName, String extension) {
        if (useExtensions) {
            return baseName + "." + extension;
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            ViewHandler handler = context.getApplication().getViewHandler();
            String url = handler.getActionURL(
                    context,
                    DOCSTORE_BASE_URL
                            + JSF.getDefaultSuffix(context, "." + extension));
            return context.getExternalContext().encodeActionURL(url);
        }
    }

}
