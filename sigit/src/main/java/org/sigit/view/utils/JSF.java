/*
 * Copyright 2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sigit.view.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.el.ValueExpression;
import javax.faces.FacesException;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Constant declarations for JSF tags
 * 
 * @author Anton Koinov 
 * @author Manfred Geiler
 * 
 */
public class JSF
{

   // Common Attributes
   public static final String ID_ATTR = "id";

   public static final String VALUE_ATTR = "value";

   public static final String BINDING_ATTR = "binding";

   public static final String STYLE_CLASS_ATTR = "styleClass";

   public static final String ESCAPE_ATTR = "escape";

   // Common Output Attributes
   public static final String FOR_ATTR = "for";

   public static final String CONVERTER_ATTR = "converter";

   // Ouput_Time Attributes
   public static final String TIME_STYLE_ATTR = "timeStyle";

   public static final String TIMEZONE_ATTR = "timezone";

   // Common Input Attributes
   public static final String REQUIRED_ATTR = "required";

   public static final String VALIDATOR_ATTR = "validator";

   // Input_Secret Attributes
   public static final String REDISPLAY_ATTR = "redisplay";

   // Input_Checkbox Attributes
   public static final String LAYOUT_ATTR = "layout";

   // Select_Menu Attributes
   public static final String SIZE_ATTR = "size";

   // SelectMany Checkbox List/ Select One Radio Attributes
   public static final String BORDER_ATTR = "border";

   public static final String DISABLED_CLASS_ATTR = "disabledClass";

   public static final String ENABLED_CLASS_ATTR = "enabledClass";

   // Common Command Attributes
   /** @deprecated */
   public static final String COMMAND_CLASS_ATTR = "commandClass";

   public static final String LABEL_ATTR = "label";

   public static final String IMAGE_ATTR = "image";

   public static final String ACTION_ATTR = "action";

   public static final String IMMEDIATE_ATTR = "immediate";

   // Command_Button Attributes
   public static final String TYPE_ATTR = "type";

   // Common Panel Attributes
   /** @deprecated */
   public static final String PANEL_CLASS_ATTR = "panelClass";

   public static final String FOOTER_CLASS_ATTR = "footerClass";

   public static final String HEADER_CLASS_ATTR = "headerClass";

   public static final String COLUMN_CLASSES_ATTR = "columnClasses";

   public static final String ROW_CLASSES_ATTR = "rowClasses";

   // Panel_Grid Attributes
   public static final String COLUMNS_ATTR = "columns";

   public static final String COLSPAN_ATTR = "colspan"; // extension

   // UIMessage and UIMessages attributes
   public static final String SHOW_SUMMARY_ATTR = "showSummary";

   public static final String SHOW_DETAIL_ATTR = "showDetail";

   public static final String GLOBAL_ONLY_ATTR = "globalOnly";

   // HtmlOutputMessage attributes
   public static final String ERROR_CLASS_ATTR = "errorClass";

   public static final String ERROR_STYLE_ATTR = "errorStyle";

   public static final String FATAL_CLASS_ATTR = "fatalClass";

   public static final String FATAL_STYLE_ATTR = "fatalStyle";

   public static final String INFO_CLASS_ATTR = "infoClass";

   public static final String INFO_STYLE_ATTR = "infoStyle";

   public static final String WARN_CLASS_ATTR = "warnClass";

   public static final String WARN_STYLE_ATTR = "warnStyle";

   public static final String TITLE_ATTR = "title";

   public static final String TOOLTIP_ATTR = "tooltip";

   // GraphicImage attributes
   public static final String URL_ATTR = "url";

   // UISelectItem attributes
   public static final String ITEM_DISABLED_ATTR = "itemDisabled";

   public static final String ITEM_DESCRIPTION_ATTR = "itemDescription";

   public static final String ITEM_LABEL_ATTR = "itemLabel";

   public static final String ITEM_VALUE_ATTR = "itemValue";

   // UIData attributes
   public static final String ROWS_ATTR = "rows";

   public static final String VAR_ATTR = "var";

   public static final String FIRST_ATTR = "first";
   
   
   public static final Logger log = LoggerFactory.getLogger(JSF.class);

   public static void renderChildren(FacesContext facesContext,
         UIComponent component) throws IOException
   {
      List<UIComponent> children = component.getChildren();
      for (int j=0, size = component.getChildCount(); j<size; j++)
      {
         UIComponent child = (UIComponent) children.get(j);
         renderChild(facesContext, child);
      }
   }

   public static void renderChild(FacesContext facesContext, UIComponent child)
         throws IOException
   {
      if ( child.isRendered() )
      {
         child.encodeBegin(facesContext);
         if ( child.getRendersChildren() )
         {
            child.encodeChildren(facesContext);
         } 
         else
         {
            renderChildren(facesContext, child);
         }
         child.encodeEnd(facesContext);
      }
   }

   public static String getStringValue(FacesContext context, ValueExpression vb)
   {
       Object value = vb.getValue(context.getELContext());
       if (value == null)
       {
           return null;
       }
       return value.toString();
   }
   
   public static Integer getIntegerValue(FacesContext context, ValueExpression vb)
   {
       String value = getStringValue(context, vb);
       if (value == null)
       {
           return null;
       }
       return new Integer(value);
   }
   
   public static Double getDoubleValue(FacesContext context, ValueExpression vb)
   {
       String value = getStringValue(context, vb);
       if (value == null)
       {
           return null;
       }
       return new Double(value);
   }
   
   public static Boolean getBooleanValue(FacesContext context, ValueExpression vb)
   {
       Object value = vb.getValue(context.getELContext());
       if (value == null)
       {
          return null;
       }
       if (value instanceof Boolean) 
       {
          return (Boolean) value;
       }
       else
       {
          return Boolean.valueOf(value.toString());
       }
   }

   public static String getDefaultSuffix(FacesContext context, String userSuffix) throws FacesException 
   {
      String viewSuffix = context.getExternalContext().getInitParameter(ViewHandler.DEFAULT_SUFFIX_PARAM_NAME);
      return (viewSuffix != null) ? viewSuffix : userSuffix;
   }

   public static String getCurrentBaseName()
   {
      String viewId = getViewId(FacesContext.getCurrentInstance());

      int pos = viewId.lastIndexOf("/");
      if (pos != -1)
      {
         viewId = viewId.substring(pos + 1);
      }

      pos = viewId.lastIndexOf(".");
      if (pos != -1)
      {
         viewId = viewId.substring(0, pos);
      }

      return viewId;      
   }   
   
   public static String getViewId(FacesContext facesContext)
   {
      if (facesContext!=null)
      {
         UIViewRoot viewRoot = facesContext.getViewRoot();
         if (viewRoot!=null) return viewRoot.getViewId();
      }
      return null;
   }

   public static InputStream getResourceAsStream(String resource, ServletContext servletContext)
   {
      String stripped = resource.startsWith("/") ? 
            resource.substring(1) : resource;
   
      InputStream stream = null; 

      if (servletContext!=null) {
         try {
            stream = servletContext.getResourceAsStream(resource);
            if (stream!=null) {
                log.debug("Loaded resource from servlet context: " + resource);
            }
         } catch (Exception e) {       
             //
         }
      }
      
      if (stream==null) {
         stream = getResourceAsStream(resource, stripped);
      }
      
      return stream;
   }

   public static URL getResource(String resource, ServletContext servletContext) 
   {
      if (!resource.startsWith("/"))
      {
         resource = "/" + resource;
      }
      
      String stripped = resource.startsWith("/") ? 
            resource.substring(1) : resource;
   
      URL url  = null; 

      if (servletContext!=null)
      {
         try {
            url = servletContext.getResource(resource);
            log.debug("Loaded resource from servlet context: " + url);
         } catch (Exception e) {
             //
         }
      }
      
      if (url==null)
      {
        url = getResource(resource, stripped);
      }
      
      return url;
   }
   
   static InputStream getResourceAsStream(String resource, String stripped)
   {
      ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
      InputStream stream = null;
      if (classLoader!=null) {
         stream = classLoader.getResourceAsStream(stripped);
         if (stream !=null) {
             log.debug("Loaded resource from context classloader: " + stripped);
         }
      }
      
      return stream;
   }
   
   static URL getResource(String resource, String stripped)
   {
       ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
       URL url = null;
       if (classLoader!=null) {
           url = classLoader.getResource(stripped);
           if (url!=null) {
               log.debug("Loaded resource from context classloader: " + url);
           }
       }

       return url;
   }

   public static void closeStream(InputStream inputStream) {
       if (inputStream == null) {
           return;
       }
       
       try {
           inputStream.close();
       } catch (IOException e) {
          // 
       }       
   }
   
   public static void closeReader(java.io.Reader reader) {
      if (reader == null) {
          return;
      }
      
      try {
          reader.close();
      } catch (IOException e) {
         // 
      }       
  }
   
   public static File getRealFile(ServletContext servletContext, String path)
   {
      String realPath = servletContext.getRealPath(path);
      if (realPath==null) //WebLogic!
      {
         try 
         {
            URL resourcePath = servletContext.getResource(path);
            if ((resourcePath != null) && (resourcePath.getProtocol().equals("file"))) 
            {
               realPath = resourcePath.getPath();
            }
            else
            {
               log.warn("Unable to determine real path from servlet context for \"" + path + "\" path does not exist.");
            }
         }
         catch (MalformedURLException e) 
         {
            log.warn("Unable to determine real path from servlet context for : " + path);
            log.debug("Caused by MalformedURLException", e);
         }

      }
      
      if (realPath != null)
      {
         File file = new File(realPath);
         if (file.exists())
         {
            return file;
         }
      }
      return null;
   }

}
