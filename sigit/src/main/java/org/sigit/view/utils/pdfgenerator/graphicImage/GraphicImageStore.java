package org.sigit.view.utils.pdfgenerator.graphicImage;

import java.io.Serializable;
import java.rmi.server.UID;
import java.util.HashMap;
import java.util.Map;

import org.sigit.commons.di.CtxComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component("org.sigit.view.utils.pdfgenerator.graphicImage.graphicImageStore")
@Scope("session")
public class GraphicImageStore implements Serializable
{
    private static final long serialVersionUID = 1L;
    private Logger log = LoggerFactory.getLogger(GraphicImageStore.class);

   public static class ImageWrapper implements Serializable
   {
    private static final long serialVersionUID = 1L;

    private byte[] image;

      private Image.Type contentType;

      public ImageWrapper(byte[] image, Image.Type contentType)
      {
         this.image = image;
         this.contentType = contentType;
      }

      public Image.Type getContentType()
      {
         return contentType;
      }

      public byte[] getImage()
      {
         return image;
      }
   }

   private Map<String, ImageWrapper> store = new HashMap<String, ImageWrapper>();

   /**
    * Put a image into the store
    * @param image
    * @return the key of the image
    */
   public String put(ImageWrapper image)
   {
      return put(image, null);
   }
   
   /**
    * Put an image into the store.
    * @param image
    * @param key The key to use, if null, a key will be generated
    * @return The key of the image
    */
   public String put(ImageWrapper image, String key) 
   {
      if (key == null)
      {
         key = "org.jboss.seam.ui.GraphicImageStore." + new UID().toString().replace(":", "-");
      }
      store.put(key, image);
      log.debug("Put image into to session with key #0", key);
      return key;
   }

   public ImageWrapper get(String key)
   {
      log.debug("Get image into to session with key #0", key);
      ImageWrapper image = store.get(key);
      return image;
   }
   
   public ImageWrapper remove(String key)
   {
      log.debug("Get image from session with key #0", key);
      ImageWrapper imageWrapper = store.remove(key);
      if ( imageWrapper != null )
      {
      }
      return imageWrapper;
   }
   
   public boolean contains(String key) 
   {
      return store.containsKey(key);
   }

   public static GraphicImageStore instance()
   {
      return (GraphicImageStore) CtxComponent.getInstance(GraphicImageStore.class);
   }

}