package org.sigit.view.utils.pdfgenerator;

import org.sigit.commons.di.CtxComponent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component("org.jboss.seam.pdf.pdfKeyStore")
@Scope("singleton")
public class KeyStoreConfig
{
   String keyStore = null;
   String keyStorePassword = null;
   String keyPassword = null;
   String keyAlias = null;

   public String getKeyStore()
   {
      return keyStore;
   }

   public void setKeyStore(String keyStore)
   {
      this.keyStore = keyStore;
   }

   public String getKeyAlias()
   {
      return keyAlias;
   }

   public void setKeyAlias(String keyAlias)
   {
      this.keyAlias = keyAlias;
   }

   public String getKeyStorePassword()
   {
      return keyStorePassword;
   }

   public void setKeyStorePassword(String keyStorePassword)
   {
      this.keyStorePassword = keyStorePassword;
   }

   public String getKeyPassword()
   {
      return keyPassword;
   }

   public void setKeyPassword(String keyPassword)
   {
      this.keyPassword = keyPassword;
   }

   public static KeyStoreConfig instance()
   {
      return (KeyStoreConfig) CtxComponent.getInstance(KeyStoreConfig.class);
   }
}
