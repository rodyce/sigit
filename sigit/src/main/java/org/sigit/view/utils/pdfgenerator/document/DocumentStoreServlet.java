package org.sigit.view.utils.pdfgenerator.document;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Scope("request")
public class DocumentStoreServlet {
    public static final String CONTROLLER_PATH = "/documentstore";

    @Autowired
    private HttpServletResponse response;

    @RequestMapping(value=CONTROLLER_PATH, method=RequestMethod.GET)
    public void dispatchDocument(
            @RequestParam(required=true)    String docId) throws Exception {
        
        DocumentStore store = DocumentStore.instance();
        
        if (store.idIsValid(docId)) {
            DocumentData documentData = store.getDocumentData(docId);
            
            response.setContentType(documentData.getDocumentType().getMimeType());
            response.setHeader("Content-Disposition", documentData.getDisposition() + "; filename=\"" + documentData.getFileName() + "\"");
            
            documentData.writeDataToStream(response.getOutputStream());
            
        } else {
            response.sendError(404);
        }
    }
}
