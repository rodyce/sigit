package org.sigit.view.utils.pdfgenerator.graphicImage;

import org.sigit.view.utils.pdfgenerator.graphicImage.GraphicImageStore.ImageWrapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Serves images from the image store
 */
@Controller
public class GraphicImageResource {

    public static final String RESOURCE_PATH = "/graphicImage";

    @RequestMapping(value=RESOURCE_PATH+"/{pathInfo}", method=RequestMethod.GET)
    public @ResponseBody ResponseEntity<byte[]> getGraphicImage(
            @PathVariable String pathInfo) {
        ImageWrapper image = GraphicImageStore.instance().remove(pathInfo);
        
        if (image != null) {
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(image.getContentType().getMimeType()))
                    .contentLength(image.getImage().length)
                    .body(image.getImage());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(new byte[] {});
        }
    }
}
