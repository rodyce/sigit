package org.sigit.view;

import org.sigit.dao.hnd.administrative.HND_PermitRuleGroupDAO;
import org.sigit.model.hnd.administrative.HND_PermitRuleGroup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Scope;

@Component("optionsHelper")
@Scope(value="sigit-conversation")
public class OptionsHelper implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private List<HND_PermitRuleGroup> spatialRuleGroups;
    private List<SelectItem> spatialRuleGroupsSelectList;
    private HND_PermitRuleGroupConverter permitRuleGroupConverter;

    @Transactional
    public List<HND_PermitRuleGroup> getSpatialRuleGroups() {
        if (spatialRuleGroups == null) {
            spatialRuleGroups = HND_PermitRuleGroupDAO.loadPermitRuleGroups();
        }
        return spatialRuleGroups;
    }

    public List<SelectItem> getSpatialRuleGroupsSelectList() {
        if (spatialRuleGroupsSelectList == null) {
            spatialRuleGroupsSelectList = new ArrayList<SelectItem>();
            spatialRuleGroupsSelectList.add(new SelectItem(null, "(seleccionar)"));
            for (HND_PermitRuleGroup prg : getSpatialRuleGroups()) {
                spatialRuleGroupsSelectList.add(new SelectItem(prg, prg.getName()));
            }
        }
        return spatialRuleGroupsSelectList;
    }

    public HND_PermitRuleGroupConverter getPermitRuleGroupConverter() {
        if (permitRuleGroupConverter == null) {
            permitRuleGroupConverter = new HND_PermitRuleGroupConverter();
        }
        return permitRuleGroupConverter;
    }



    public class HND_PermitRuleGroupConverter implements Converter {
        @Override
        @Transactional
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
            UUID id = UUID.fromString(value);
            
            for (HND_PermitRuleGroup prg : getSpatialRuleGroups())
                if (prg.getId().equals(id))
                    return prg;
            
            return null;
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            if (value == null) return "";
            
            return String.valueOf(((HND_PermitRuleGroup) value).getId());
        }
        
    }
}
