package org.sigit.view;

import java.io.Serializable;

import org.sigit.model.commons.IParcel;
import org.sigit.model.commons.ISpatialZone;

import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

@Component("zoneInfoHelper")
@Scope(value="session")
public class ZoneInfoHelper implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private ISpatialZone spatialZone;

    
    public ISpatialZone getSpatialZone() {
        return spatialZone;
    }
    public void setSpatialZone(ISpatialZone spatialZone) {
        this.spatialZone = spatialZone;
    }
    
    public IParcel getParcel() {
        if (isParcelInstance())
            return (IParcel) spatialZone;
        return null;
    }
    
    public boolean isParcelInstance() {
        return spatialZone instanceof IParcel;
    }
}
