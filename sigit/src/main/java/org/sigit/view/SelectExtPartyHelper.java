package org.sigit.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.ajax4jsf.model.DataVisitor;
import org.ajax4jsf.model.ExtendedDataModel;
import org.ajax4jsf.model.Range;
import org.richfaces.model.ArrangeableModel;
import org.sigit.model.ladm.external.ExtParty;


public abstract class SelectExtPartyHelper<T extends ExtParty> {
    protected List<T> partyList;
    protected ArrangeableModel dataModel;
    protected ExtParty selectedParty;
    protected Collection<Object> partySelection;
    protected String partyFullName;
    protected List<ExtParty> selectedParties = new ArrayList<ExtParty>();
    
    protected boolean addToList = false;
    protected String addToListMethod;

    protected String partyIdField;
    protected String identityField;
    protected String partyNameField;
    protected String selectedPartyField;
    protected String reRenderComponents;

    

    public abstract List<T> getPartyList();
    
    public void setPartyList(List<T> partyList) {
        this.partyList = partyList;
    }
    
    public ExtParty getSelectedParty() {
        return selectedParty;
    }
    public void setSelectedParty(ExtParty selectedParty) {
        this.selectedParty = selectedParty;
    }
    
    public Collection<Object> getPartySelection() {
        return partySelection;
    }
    public void setPartySelection(Collection<Object> partySelection) {
        this.partySelection = partySelection;
    }
    
    public List<ExtParty> getSelectedParties() {
        return selectedParties;
    }
    public void setSelectedParties(List<ExtParty> selectedParties) {
        this.selectedParties = selectedParties;
    }
    
    public boolean isAddToList() {
        return addToList;
    }
    public void setAddToList(boolean addToList) {
        this.addToList = addToList;
    }
    
    public String getAddToListMethod() {
        return addToListMethod;
    }
    public void setAddToListMethod(String addToListMethod) {
        this.addToListMethod = addToListMethod;
    }

    //TODO: Finish implementation
    public ArrangeableModel getDataModel() {
        if (dataModel == null) {
            ExtendedDataModel<T> t = new ExtendedDataModel<T>() {
                @Override
                public void setRowKey(Object key) {
                    // TODO Auto-generated method stub
                    
                }

                @Override
                public Object getRowKey() {
                    // TODO Auto-generated method stub
                    return null;
                }

                @Override
                public void walk(FacesContext context, DataVisitor visitor,
                        Range range, Object argument) {
                    // TODO Auto-generated method stub
                    
                }

                @Override
                public int getRowCount() {
                    return getPartyList().size();
                }

                @Override
                public T getRowData() {
                    // TODO Auto-generated method stub
                    return null;
                }

                @Override
                public int getRowIndex() {
                    // TODO Auto-generated method stub
                    return 0;
                }

                @Override
                public Object getWrappedData() {
                    // TODO Auto-generated method stub
                    return null;
                }

                @Override
                public boolean isRowAvailable() {
                    // TODO Auto-generated method stub
                    return false;
                }

                @Override
                public void setRowIndex(int arg0) {
                    // TODO Auto-generated method stub
                    
                }

                @Override
                public void setWrappedData(Object arg0) {
                    // TODO Auto-generated method stub
                    
                }
            };
            dataModel = new ArrangeableModel(t, null, null);
            /*
            dataModel = new ExtendedTableDataModel<T>(new DataProvider<T>() {
 
                private static final long serialVersionUID = 5054087821033164847L;

                public int getRowCount() {
                    return getPartyList().size();
                }
                public List<T> getItemsByRange(int firstRow, int endRow) {
                    return getPartyList().subList(firstRow, endRow);
                }

                public T getItemByKey(Object key) {
                    //TODO: Eliminar busqueda lineal
                    for (T ep : getPartyList())
                        if (key.equals(getKey(ep)))
                            return ep;

                    return null;
                }
 
                 public Object getKey(ExtParty item) {
                    return item.getExtPID();
                }
            });
            */
         }
        return dataModel;
    }
    
    public abstract void takeSelection(ActionEvent ae);
    
    public String getPartyIdField() {
        return partyIdField;
    }
    public void setPartyIdField(String partyIdField) {
        this.partyIdField = partyIdField;
    }
    
    public String getIdentityField() {
        return identityField;
    }
    public void setIdentityField(String identityField) {
        this.identityField = identityField;
    }
    
    public String getPartyNameField() {
        return partyNameField;
    }
    public void setPartyNameField(String partyNameField) {
        this.partyNameField = partyNameField;
    }
    
    public String getPartyFullName() {
        return partyFullName;
    }
    public void setPartyFullName(String partyFullName) {
        this.partyFullName = partyFullName;
    }
    
    public String getSelectedPartyField() {
        return selectedPartyField;
    }
    public void setSelectedPartyField(String selectedPartyField) {
        this.selectedPartyField = selectedPartyField;
    }
    
    public String getReRenderComponents() {
        return reRenderComponents;
    }
    public void setReRenderComponents(String reRenderComponents) {
        this.reRenderComponents = reRenderComponents;
    }
}
