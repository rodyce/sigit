package org.sigit.view;

import org.sigit.dao.SigitDAO;
import org.sigit.model.hnd.cadastre.HND_LandUse;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Scope;

@Component("selectLandUseHelper")
@Scope(value="sigit-conversation")
public class SelectLandUseHelper extends SimpleLandUseHelper implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private HND_LandUse pickedLandUse;
    
    public HND_LandUse getPickedLandUse() {
        return pickedLandUse;
    }
    public void setPickedLandUse(HND_LandUse pickedLandUse) {
        this.pickedLandUse = pickedLandUse;
    }

    @Transactional
    public String addCompatibility() {
        if (getSelected() != null && pickedLandUse != null && !getSelected().equals(pickedLandUse) ) {
            if (!addToListIfNotExists(getSelected().getCompatibleLandUses(), pickedLandUse)) {
                //Bail out early if nothing changed
                return null;
            }
            addToListIfNotExists(pickedLandUse.getCompatibleLandUses(), getSelected());
            
            SigitDAO.save(getSelected());
            SigitDAO.save(pickedLandUse);
        }
        return null;
    }
    
    @Transactional
    public String deleteCompatibility() {
        if (getSelected() != null && pickedLandUse != null) {
            if (!removeAllFromList(getSelected().getCompatibleLandUses(), pickedLandUse)) {
                //Bail out early if nothing changed
                return null;
            }
            removeAllFromList(pickedLandUse.getCompatibleLandUses(), getSelected());
            
            SigitDAO.save(getSelected());
            SigitDAO.save(pickedLandUse);
        }
        return null;
    }
    
    private boolean addToListIfNotExists(List<HND_LandUse> landUseList, HND_LandUse lu) {
        if (!landUseList.contains(lu)) {
            landUseList.add(lu);
            return true;
        }
        return false;
    }
    private boolean removeAllFromList(List<HND_LandUse> landUseList, HND_LandUse lu) {
        boolean val = false;
        while (landUseList.remove(lu)) {
            val = true;
        }
        return val;
    }
}
