package org.sigit.view;

import org.sigit.dao.hnd.ladmshadow.ParcelDAO;
import org.sigit.dao.hnd.ladmshadow.PropertyDAO;
import org.sigit.i18n.ResourceBundleHelper;
import org.sigit.model.hnd.ladmshadow.Parcel;
import org.sigit.model.hnd.ladmshadow.Property;
import org.sigit.model.hnd.ladmshadow.RRR;
import org.sigit.model.hnd.ladmshadow.Responsibility;
import org.sigit.model.hnd.ladmshadow.Restriction;
import org.sigit.model.hnd.ladmshadow.Right;
import org.sigit.model.ladm.external.ExtParty;
import org.sigit.util.ShareValue;
import org.sigit.logic.viewer.toolbox.ParcelResponsibility;
import org.sigit.logic.viewer.toolbox.ParcelRestriction;
import org.sigit.logic.viewer.toolbox.ParcelRight;
import org.sigit.logic.viewer.toolbox.RRRHelperLogic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Scope;

@Component("viewTransactionHelper")
@Scope(value="sigit-conversation")
public class ViewTransactionHelper implements Serializable {
    private static final long serialVersionUID = 1L;

    private boolean renderTransactionData = false;
    
    private UUID baUnitId;
    private UUID suID;
    
    private Property property;
    private Set<RRR> rrrSet;
    private Parcel parcel;
    
    private List<ParcelRight> parcelRightsList;
    private List<ParcelRestriction> parcelRestrictionsList;
    private List<ParcelResponsibility> parcelResponsibilitiesList;
    
    @Autowired
    private ResourceBundleHelper resBundle;


    public boolean isRenderTransactionData() {
        return renderTransactionData;
    }
    public void setRenderTransactionData(boolean renderTransactionData) {
        this.renderTransactionData = renderTransactionData;
    }

    public UUID getBaUnitId() {
        return this.baUnitId;
    }
    public void setBaUnitId(UUID baUnitId) {
        if (baUnitId != this.baUnitId) {
            invalidateRRRsList();
            this.baUnitId = baUnitId;
        }
    }
    
    public UUID getSuID() {
        return suID;
    }
    public void setSuID(UUID suID) {
        if (suID != this.suID) {
            this.parcel = null; 
            this.suID = suID;
        }
    }
    
    @Transactional
    private Property getProperty() {
        if (property == null) {
            property = PropertyDAO.loadPropertyByID(baUnitId);
        }
        return property;
    }
    private Set<RRR> getRrrSet() {
        if (rrrSet == null) {
            if (getProperty() != null)
                rrrSet = getProperty().getRrr();
        }
        return rrrSet;
    }
    
    @Transactional
    public Parcel getParcel(String suID) {
        setSuID(UUID.fromString(suID));
        
        if (parcel == null) {
            parcel = ParcelDAO.loadParcelByID(this.suID);
        }
        return parcel;
    }
    
    public Parcel getParcel() {
        return getParcel(String.valueOf(suID));
    }
    
    public List<ParcelRight> getParcelRightsList(String baUnitId) {
        setBaUnitId(UUID.fromString(baUnitId));
        
        if (parcelRightsList == null) {
            parcelRightsList = new ArrayList<ParcelRight>();
            
            if (getRrrSet() != null) {
                ParcelRight pr;
                for (RRR rrr : getRrrSet()) {
                    pr = null;
                    if (rrr instanceof Right) {
                        Right right = (Right)rrr;
                        ExtParty extParty = right.getParty().getExtParty();
                        pr = new ParcelRight(null, 0, extParty, right.getType(),
                                new ShareValue(right.getShare()), right);
                    }
                    
                    if (pr != null)
                        parcelRightsList.add(pr);
                }
            }
        }
        return parcelRightsList;
    }
    public ShareValue getRightsSumShares() {
        return RRRHelperLogic.getSumShares(parcelRightsList);
    }

    public List<ParcelRestriction> getParcelRestrictionsList(String baUnitId) {
        setBaUnitId(UUID.fromString(baUnitId));
        
        if (parcelRestrictionsList == null) {
            parcelRestrictionsList = new ArrayList<ParcelRestriction>();
            
            if (getRrrSet() != null) {
                ParcelRestriction pr;
                for (RRR rrr : getRrrSet()) {
                    pr = null;
                    if (rrr instanceof Restriction) {
                        Restriction restriction = (Restriction)rrr;
                        ExtParty extParty = restriction.getParty().getExtParty();
                        pr = new ParcelRestriction(null, 0, extParty, restriction.getType(),
                                new ShareValue(restriction.getShare()), restriction);
                    }
                    
                    if (pr != null)
                        parcelRestrictionsList.add(pr);
                }
            }
        }
        return parcelRestrictionsList;
    }
    public ShareValue getRestrictionsSumShares() {
        return RRRHelperLogic.getSumShares(parcelRestrictionsList);
    }

    public List<ParcelResponsibility> getParcelResponsibilitiesList(String baUnitId) {
        setBaUnitId(UUID.fromString(baUnitId));

        if (parcelResponsibilitiesList == null) {
            parcelResponsibilitiesList = new ArrayList<ParcelResponsibility>();

            if (getRrrSet() != null) {
                ParcelResponsibility pr;
                for (RRR rrr : getRrrSet()) {
                    pr = null;
                    if (rrr instanceof Responsibility) {
                        Responsibility responsibility = (Responsibility)rrr;
                        ExtParty extParty = responsibility.getParty().getExtParty();
                        pr = new ParcelResponsibility(null, 0, extParty, responsibility.getType(),
                                new ShareValue(responsibility.getShare()), responsibility);
                    }
                    
                    if (pr != null)
                        parcelResponsibilitiesList.add(pr);
                }
            }
        }
        return parcelResponsibilitiesList;
    }
    public ShareValue getResponsibilitiesSumShares() {
        return RRRHelperLogic.getSumShares(parcelResponsibilitiesList);
    }

    private void invalidateRRRsList() {
        property = null;
        rrrSet = null;
        parcelRightsList = null;
        parcelRestrictionsList = null;
        parcelResponsibilitiesList = null;
    }
    
    public String formatEmptyText(Object value) {
        if (value == null || value.equals(""))
            return resBundle.loadMessage("global.empty");
        
        return value.toString();
    }
    
    public String getLandUseText() {
        if (parcel == null || parcel.getLandUse() == null)
            return resBundle.loadMessage("global.empty");

        return parcel.getLandUse().getFullName();
    }
    
    public String getProposedLandUseText() {
        if (parcel == null || parcel.getProposedLandUse() == null)
            return resBundle.loadMessage("global.empty");
        
        return parcel.getProposedLandUse().getFullName();
    }

}
