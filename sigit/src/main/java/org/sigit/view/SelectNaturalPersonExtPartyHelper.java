package org.sigit.view;

import org.sigit.model.hnd.administrative.HND_NaturalPerson;
import org.sigit.dao.hnd.administrative.HND_NaturalPersonDAO;
import org.sigit.model.ladm.external.ExtParty;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.el.ValueExpression;
import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Scope;

@Component("selectNaturalPersonExtPartyHelper")
@Scope(value="sigit-conversation")
public class SelectNaturalPersonExtPartyHelper extends SelectExtPartyHelper<HND_NaturalPerson> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Override
    @Transactional
    public List<HND_NaturalPerson> getPartyList() {
        if (partyList == null) {
            partyList = HND_NaturalPersonDAO.loadNaturalPersonParties();
        }
        return partyList;
    }
    
    @Override
    public void takeSelection(ActionEvent ae) {
        getSelectedParties().clear();
        Iterator<Object> iterator = getPartySelection().iterator();
        while (iterator.hasNext()){
            Object key = iterator.next();
            getDataModel().setRowKey(key);
            selectedParty = (ExtParty) getDataModel().getRowData();
            //getSelectedNaturalPersonParties().add(getNaturalPersonPartiesDataModel().getObjectByKey(key));
            if (selectedParty != null) {
                partyFullName = selectedParty.getName();
                
                Application app = FacesContext.getCurrentInstance().getApplication();
                ExpressionFactory exprFactory = app.getExpressionFactory();
                ELContext elCtx = FacesContext.getCurrentInstance().getELContext();
                ValueExpression ve;
                
                if (!addToList) {
                    if (partyIdField != null) {
                        ve = exprFactory.createValueExpression(elCtx, "#{" + partyIdField + "}", Long.class);
                        ve.setValue(elCtx, selectedParty.getExtPID());
                    }
                    if (identityField != null) {
                        ve = exprFactory.createValueExpression(elCtx, "#{" + identityField + "}", String.class);
                        ve.setValue(elCtx, ((HND_NaturalPerson)selectedParty).getIdentity());
                    }
                    if (partyNameField != null) {
                        ve = exprFactory.createValueExpression(elCtx, "#{" + partyNameField + "}", String.class);
                        ve.setValue(elCtx, partyFullName);
                    }
                    if (selectedPartyField != null) {
                        ve = exprFactory.createValueExpression(elCtx, "#{" + selectedPartyField + "}", ExtParty.class);
                        ve.setValue(elCtx, selectedParty);
                    }
                }
                else {
                    //adding to a list
                    MethodExpression me;
                    
                    if (selectedPartyField != null && addToListMethod != null) {
                        ve = exprFactory.createValueExpression(elCtx, "#{" + selectedPartyField + "}", ExtParty.class);
                        ve.setValue(elCtx, selectedParty);
                        
                        //third parameter is set to null meaning that we do not care for the
                        //result type... and no expected parameters for fourth parameter,
                        //however we can not use null for this argument
                        me = exprFactory.createMethodExpression(elCtx, "#{" + addToListMethod + "}", null, new Class[] {});
                        me.invoke(elCtx, null);
                    }
                }
            }
        }
    }


}
