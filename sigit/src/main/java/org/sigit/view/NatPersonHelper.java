package org.sigit.view;

import org.sigit.dao.hnd.administrative.HND_NaturalPersonDAO;
import org.sigit.model.hnd.administrative.HND_NaturalPerson;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Scope;

@Component("personHelper")
@Scope(value="sigit-conversation")
public class NatPersonHelper extends MantHelper<HND_NaturalPerson> {
    private static final long serialVersionUID = 1L;
    
    protected final String BY_NATIONAL_ID = "0";
    protected final String BY_RTN = "1";
    protected final String BY_LASTNAMES = "2";
    protected final String BY_FIRSTNAMES = "3";
    

    @Override
    public String addNew() {
        setSelected(new HND_NaturalPerson());
        editing = true;
        adding = true;
        
        return null;
    }
    
    @Override
    @Transactional
    public String delete(HND_NaturalPerson np) {
        HND_NaturalPersonDAO.delete(np);
        entityList.remove(np);
        
        return null;
    }

    @Override
    public String select(HND_NaturalPerson np) {
        setSelected(np);
        super.doSelection();
        
        return null;
    }

    @Override
    @Transactional
    public String saveSelected() {
        getSelected().setIdentity(getSelected().getIdentity().replace("-", ""));
        HND_NaturalPersonDAO.save(getSelected());
        if (adding) {
            getEntityList().clear();
            getEntityList().add(getSelected());
        }
        
        return null;
    }
    
    
    @Override
    @Transactional
    public String quickSearch() {
        searchText = searchText.trim();
        List<HND_NaturalPerson> searchResultList = new ArrayList<HND_NaturalPerson>();
        
        if (searchSelector.equals(BY_NATIONAL_ID)) {
            searchText = searchText.replaceAll("-", "");
            searchResultList = wrapSingle(HND_NaturalPersonDAO.loadNatPersonPartyByIDentity(searchText)); 
        }
        else if (searchSelector.equals(BY_RTN)) {
            searchText = searchText.replaceAll("-", "");
            searchResultList = wrapSingle(HND_NaturalPersonDAO.loadNatPersonPartyByRTN(searchText)); 
        }
        else if (searchSelector.equals(BY_LASTNAMES))
            searchResultList = HND_NaturalPersonDAO.loadNatPersonPartiesByLastNames(searchText);
        else if (searchSelector.equals(BY_FIRSTNAMES))
            searchResultList = HND_NaturalPersonDAO.loadNatPersonPartiesByFirstNames(searchText);
        
        if (searchResultList.size() == 0) {
            FacesContext.getCurrentInstance().addMessage("",
                    new FacesMessage(
                            FacesMessage.SEVERITY_INFO,
                            resBundle.loadMessage("txt.no_search_results"),
                            ""
                    )
            );
        }
        
        setEntityList(searchResultList);
        
        return null;
    }
    
    
    public String names(HND_NaturalPerson natPerson) {
        StringBuffer sb = new StringBuffer();
        if (natPerson.getFirstName1() != null && natPerson.getFirstName1().trim().length() > 0)
            sb.append(natPerson.getFirstName1());
        
        if (natPerson.getFirstName2() != null && natPerson.getFirstName2().trim().length() > 0) {
            if (sb.length() > 0) sb.append(", ");
            sb.append(natPerson.getFirstName2());
        }
        
        return sb.toString();
    }

    public String lastNames(HND_NaturalPerson natPerson) {
        StringBuffer sb = new StringBuffer();
        if (natPerson.getLastName1() != null && natPerson.getLastName1().trim().length() > 0)
            sb.append(natPerson.getLastName1());
        
        if (natPerson.getLastName2() != null && natPerson.getLastName2().trim().length() > 0) {
            if (sb.length() > 0) sb.append(", ");
            sb.append(natPerson.getLastName2());
        }

        return sb.toString();
    }

    public String gender(HND_NaturalPerson natPerson) {
        if (natPerson != null && natPerson.getGender() != null) {
            switch (natPerson.getGender()) {
            case 'M':
                return resBundle.loadMessage("txt.gender_male");
            case 'F':
                return resBundle.loadMessage("txt.gender_female");
            case 'O':
                return resBundle.loadMessage("txt.gender_other");
            }
        }
        return "--";
    }

    public String maritalStatus(HND_NaturalPerson natPerson) {
        if (natPerson != null && natPerson.getMaritalStatus() != null) {
            switch (natPerson.getMaritalStatus()) {
            case 'S':
                return resBundle.loadMessage("txt.marital_status_single");
            case 'C':
                return resBundle.loadMessage("txt.marital_status_married");
            case 'W':
                return resBundle.loadMessage("txt.marital_status_widowhood");
            case 'F':
                return resBundle.loadMessage("txt.marital_status_free_union");
            case 'O':
                return resBundle.loadMessage("txt.marital_status_other");
            }
        }
        return "--";
    }
    
    public String getBY_NATIONAL_ID() {
        return BY_NATIONAL_ID;
    }
    public String getBY_RTN() {
        return BY_RTN;
    }
    public String getBY_LASTNAMES() {
        return BY_LASTNAMES;
    }
    public String getBY_FIRSTNAMES() {
        return BY_FIRSTNAMES;
    }
}
