package org.sigit.view;

import java.io.Serializable;
import java.util.List;

import org.sigit.dao.hnd.administrative.HND_TopographicTransactionDAO;
import org.sigit.logic.security.Credentials;
import org.sigit.logic.viewer.InteractiveViewerHelper;
import org.sigit.model.commons.ISpatialZone;
import org.sigit.model.hnd.administrative.HND_TopographicTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component("topographyHelper")
@Scope(value="sigit-conversation")

public class TopographyHelper implements Serializable {
    private static final long serialVersionUID = 1L;

    private HND_TopographicTransaction selected;
    private ISpatialZone selectedSpatialZone;
    private List<HND_TopographicTransaction> pendingTopographicTransactions;
    
    @Autowired
    private InteractiveViewerHelper interactiveViewerHelper;

    @Autowired
    private Credentials credentials;
    

    public HND_TopographicTransaction getSelected() {
        return selected;
    }
    public void setSelected(HND_TopographicTransaction selected) {
        this.selected = selected;
    }
    
    public ISpatialZone getSelectedSpatialZone() {
        return selectedSpatialZone;
    }
    public void setSelectedSpatialZone(ISpatialZone selectedSpatialZone) {
        this.selectedSpatialZone = selectedSpatialZone;
    }
    
    
    @Transactional
    public List<HND_TopographicTransaction> getPendingTopographicTransactions() {
        if (pendingTopographicTransactions == null)
            pendingTopographicTransactions = HND_TopographicTransactionDAO.loadInitiatedTopographicTransactionsByUserName(credentials.getUsername());
        return pendingTopographicTransactions;
    }
    
    
    public String selectZoneFromMap() {
        setSelectedSpatialZone(interactiveViewerHelper.getSelectedZone());
        return null;
    }
}
