package org.sigit.view;

import org.sigit.model.hnd.administrative.HND_MunicipalTransactionType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.model.SelectItem;

import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

@Component("requestTypeSelector")
@Scope(value="sigit-conversation")
public class RequestTypeSelectorHelper implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private ResourceBundle res = ResourceBundle.getBundle("messages");
    private List<SelectItem> requestTypes;
    private List<SelectItem> requestSubTypes;
    private HND_MunicipalTransactionType selectedRequestType;

    public RequestTypeSelectorHelper() {
        requestTypes = new ArrayList<SelectItem>();
        HND_MunicipalTransactionType[] hndRequestTypes = HND_MunicipalTransactionType.values();
        requestTypes.add(new SelectItem("", "(seleccione clasificacion)"));
        for (HND_MunicipalTransactionType rt : hndRequestTypes)
            requestTypes.add(new SelectItem(rt, (String)res.getObject(rt.toString())));
    }
    
    public List<SelectItem> getRequestTypes() {
        return requestTypes;
    }
    public List<SelectItem> getRequestSubTypes() {
        //TODO: Ver si eliminar este metodo
        
        //List<HND_TransactionMetaData> rmdList = HND_TransactionMetaDataDAO.loadRequestMetaDataByRequestType(selectedRequestType);

        requestSubTypes = new ArrayList<SelectItem>();
        //for (HND_TransactionMetaData rmd : rmdList)
        //    requestSubTypes.add(new SelectItem(rmd.getName(), (String)res.getObject(rmd.getName().toString())));

        return requestSubTypes;
    }

    public HND_MunicipalTransactionType getSelectedRequestType() {
        return selectedRequestType;
    }
    public void setSelectedRequestType(HND_MunicipalTransactionType selectedRequestType) {
        this.selectedRequestType = selectedRequestType;
    }
}
