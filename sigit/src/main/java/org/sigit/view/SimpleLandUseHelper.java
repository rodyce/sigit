package org.sigit.view;

import org.sigit.dao.hnd.cadastre.HND_LandUseDAO;
import org.sigit.dao.hnd.cadastre.HND_LandUseDomainDAO;
import org.sigit.model.hnd.cadastre.HND_LandUse;
import org.sigit.model.hnd.cadastre.HND_LandUseDomain;
import org.sigit.viewer.LandUseStyleResource;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import org.sigit.commons.di.CtxComponent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Scope;

@Component("simpleLandUseHelper")
@Scope(value="sigit-conversation")
public class SimpleLandUseHelper extends MantHelper<HND_LandUse> implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private int currentLevel = 0;
    private List<HND_LandUseDomain> landUseDomainList;
    private List<SelectItem> landUseDomainItems;
    private List<HND_LandUse> landUseList;
    private HND_LandUseDomain selectedDomain;
    private HND_LandUse selected;
    
    private String landUseIdInputFieldName = "";
    private String landUseNameInputFieldName = "";
    private String landUseField;

    
    public int getCurrentLevel() {
        return currentLevel;
    }

    
    @Transactional
    public List<HND_LandUseDomain> getLandUseDomainList() {
        if (landUseDomainList == null) {
            landUseDomainList = HND_LandUseDomainDAO.loadLandUseDomains();
        }
        return landUseDomainList;
    }
    public void setLandUseDomainList(List<HND_LandUseDomain> landUseDomainList) {
        this.landUseDomainList = landUseDomainList;
    }
    
    public List<SelectItem> getLandUseDomainItems() {
        if (landUseDomainItems == null) {
            landUseDomainItems = new ArrayList<SelectItem>();
            for (HND_LandUseDomain luDomain : getLandUseDomainList())
                landUseDomainItems.add(new SelectItem(luDomain, luDomain.getName()));
        }
        return landUseDomainItems;
    }
    public void setLandUseDomainItems(List<SelectItem> landUseDomainItems) {
        this.landUseDomainItems = landUseDomainItems;
    }


    @Transactional
    public List<HND_LandUse> getLandUseList() {
        if (landUseList == null) {
            if (currentLevel == 0)
                landUseList = HND_LandUseDAO.loadLandUsesByLevel(getSelectedDomain(), 0, true);
            else if (selected != null)
                landUseList = HND_LandUseDAO.loadLandUsesByParent(selected, true);
        }
        return landUseList;
    }
    public void setLandUseList(List<HND_LandUse> landUseList) {
        this.landUseList = landUseList;
    }
    
    public HND_LandUseDomain getSelectedDomain() {
        if (selectedDomain == null) {
            if (getLandUseDomainList().size() > 0)
                selectedDomain = getLandUseDomainList().get(0);
        }
        return selectedDomain;
    }
    public void setSelectedDomain(HND_LandUseDomain selectedDomain) {
        if (this.selectedDomain != selectedDomain) {
            this.selectedDomain = selectedDomain;
            
            setLandUseList(null);
            setSelected(null);
            currentLevel = 0;
        }
    }

    public HND_LandUse getSelected() {
        return selected;
    }
    public void setSelected(HND_LandUse selected) {
        this.selected = selected;
    }

    public String getLandUseIdInputFieldName() {
        return landUseIdInputFieldName;
    }
    public void setLandUseIdInputFieldName(String landUseIdInputFieldName) {
        this.landUseIdInputFieldName = landUseIdInputFieldName;
    }

    public String getLandUseNameInputFieldName() {
        return landUseNameInputFieldName;
    }
    public void setLandUseNameInputFieldName(String landUseNameInputFieldName) {
        this.landUseNameInputFieldName = landUseNameInputFieldName;
    }
    
    public String getLandUseField() {
        return landUseField;
    }
    public void setLandUseField(String landUseField) {
        this.landUseField = landUseField;
    }

    public boolean isInputFieldSpecified() {
        return !landUseIdInputFieldName.equals("") && !landUseNameInputFieldName.equals("");
    }
    
    public void setInputFields(String idField, String nameField) {
        landUseIdInputFieldName = idField;
        landUseNameInputFieldName = nameField;
    }
    
    public String getSelectedIdStr() {
        if (selected == null) return "";
        
        return selected.getId().toString().replaceAll("'", "\\'");
    }
    
    public String getSelectedNameStr() {
        if (selected == null) return "";
        
        return String.format("%s:%s - %s",
                selected.getDomain().getName(),
                selected.getCompleteCode(),
                selected.getName()).replaceAll("'", "\\'");
    }


    public void drillDown(HND_LandUse landUse) {
        selected = landUse;
        currentLevel++;
        landUseList = null;
    }
    
    public void rollUp() {
        if (currentLevel == 0) return;
        
        if (selected.getParent() != null)
            selected = selected.getParent();
        currentLevel--;
        landUseList = null;
    }
    

    private boolean styleInfoEdited;
    private String styleInfoFillColor;
    private Integer styleInfoOpacity;
    private String styleInfoStrokeColor;
    private Integer styleInfoStrokeWidth;
    
    public boolean isStyleInfoEdited() {
        return styleInfoEdited;
    }

    public String getStyleInfoFillColor() {
        if (styleInfoFillColor == null)
            styleInfoFillColor = getFillColor(getSelected());
        return styleInfoFillColor;
    }
    public void setStyleInfoFillColor(String styleInfoFillColor) {
        if ( this.styleInfoFillColor != styleInfoFillColor && !styleInfoFillColor.equalsIgnoreCase(getSelected().getStyleInfo().getFillColor()) ) {
            this.styleInfoFillColor = styleInfoFillColor;
            getSelected().getStyleInfo().setFillColor(this.styleInfoFillColor);
            
            styleInfoEdited = true;
        }
    }

    public Integer getStyleInfoOpacity() {
        if (styleInfoOpacity == null)
            styleInfoOpacity = getOpacity(getSelected(), 100);
        return styleInfoOpacity;
    }
    public void setStyleInfoOpacity(Integer styleInfoOpacity) {
        if (this.styleInfoOpacity != styleInfoOpacity) {
            this.styleInfoOpacity = styleInfoOpacity;
            getSelected().getStyleInfo().setFillOpacity( (float) this.styleInfoOpacity / 100.0f );
            
            styleInfoEdited = true;
        }
    }

    public String getStyleInfoStrokeColor() {
        if (styleInfoStrokeColor == null)
            styleInfoStrokeColor = getStrokeColor(getSelected());
        return styleInfoStrokeColor;
    }
    public void setStyleInfoStrokeColor(String styleInfoStrokeColor) {
        if (this.styleInfoStrokeColor != styleInfoStrokeColor && !styleInfoStrokeColor.equalsIgnoreCase(getSelected().getStyleInfo().getStrokeColor())) {
            this.styleInfoStrokeColor = styleInfoStrokeColor;
            getSelected().getStyleInfo().setStrokeColor(this.styleInfoStrokeColor);
            
            styleInfoEdited = true;
        }
    }

    public Integer getStyleInfoStrokeWidth() {
        if (styleInfoStrokeWidth == null)
            styleInfoStrokeWidth = getStrokeWidth(getSelected(), 100);
        return styleInfoStrokeWidth;
    }
    public void setStyleInfoStrokeWidth(Integer styleInfoStrokeWidth) {
        if (this.styleInfoStrokeWidth != styleInfoStrokeWidth && this.styleInfoStrokeWidth.intValue() != styleInfoStrokeWidth.intValue()) {
            this.styleInfoStrokeWidth = styleInfoStrokeWidth;
            getSelected().getStyleInfo().setStrokeWidth( (float) this.styleInfoStrokeWidth / 100.0f );
            
            styleInfoEdited = true;
        }
    }
    
    public String editLegend(HND_LandUse lu) {
        setSelected(lu);
        
        styleInfoEdited = false;
        styleInfoFillColor = null;
        styleInfoOpacity = null;
        styleInfoStrokeColor = null;
        styleInfoStrokeWidth = null;

        return null;
    }

    @Transactional
    public String applyLegendChange() {
        if (styleInfoEdited) {
            if (getSelected() != null) {
                HND_LandUseDAO.save(getSelected());
                styleInfoEdited = false;
                
                //refrescar cache de tipos de uso
                LandUseStyleResource landUseStyleResource = (LandUseStyleResource) CtxComponent.getInstance("landUseStyleResource");
                landUseStyleResource.doRefresh();
            }
        }
        return null;
    }


    private String getFillColor(HND_LandUse lu) {
        String color = "#000000";
        if (lu != null) {
            color = lu.getStyleInfo().getFillColor() != null ? lu.getStyleInfo().getFillColor().toUpperCase() : color;
            color = checkColor(color);
        }
        return color;
    }
    private int getOpacity(HND_LandUse lu, int scale) {
        int _int = 0;
        if (lu != null) {
            _int = lu.getStyleInfo().getFillOpacity() != null ? (int) (lu.getStyleInfo().getFillOpacity() * scale) : _int;
        }
        return _int;
    }
    private String getStrokeColor(HND_LandUse lu) {
        String color = "#000000";
        if (lu != null) {
            color = lu.getStyleInfo().getStrokeColor() != null ? lu.getStyleInfo().getStrokeColor().toUpperCase() : color;
            color = checkColor(color);
        }
        return color;
    }
    private int getStrokeWidth(HND_LandUse lu, int scale) {
        int _int = 0;
        if (lu != null) {
            _int = lu.getStyleInfo().getStrokeWidth() != null ? (int) (lu.getStyleInfo().getStrokeWidth() * scale) : _int;
        }
        return _int;
    }
    private String checkColor(String color) {
        if (color.lastIndexOf('#') > 0 || color.length() < 7) {
            StringBuilder sb = new StringBuilder(color);
            sb.setCharAt(0, '#');
            for (int i = 1; i < sb.length(); i++)
                if (sb.charAt(i) == '#')
                    sb.setCharAt(i, '0');
            for (int i = color.length(); i < 7; i++)
                sb.append("0");
            color = sb.toString();
        }
        return color;
    }
    /*
    public String takeSelection() {
        if (landUseField != null) {
            Application app = FacesContext.getCurrentInstance().getApplication();
            ExpressionFactory exprFactory = app.getExpressionFactory();
            ELContext elCtx = FacesContext.getCurrentInstance().getELContext();
            ValueExpression ve;
            
            ve = exprFactory.createValueExpression(elCtx, "#{" + landUseField + "}", HND_LandUse.class);
            ve.setValue(elCtx, selected);
        }
        return null;
    }
    */

    @Override
    public String quickSearch() {
        // TODO Auto-generated method stub
        return null;
    }

}
