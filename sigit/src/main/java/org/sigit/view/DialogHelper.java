package org.sigit.view;

import java.io.Serializable;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.faces.application.Application;
import javax.faces.context.FacesContext;

import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;


@Component("dialogHelper")
@Scope(value="sigit-conversation")
public class DialogHelper implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private String confirmAction = "";
    private String confirmMsg = "";
    private String reRenderList = "";
    private String jsFunction = "";
    private String width = "600";
    private String height = "180";
    private String zIndex = "20000";
    private boolean moveable = true;
    private boolean resizeable = false;
    private boolean actionConfirmed = false;
    private boolean strictConfirmationDialog = false;
    private Object tagObject;


    public String doConfirmAction() {
        if (strictConfirmationDialog && !actionConfirmed) return null;
        
        Application app = FacesContext.getCurrentInstance().getApplication();
        ExpressionFactory exprFactory = app.getExpressionFactory();
        ELContext elCtx = FacesContext.getCurrentInstance().getELContext();
        MethodExpression me;
        
        //third parameter is set to null meaning that we do not care for the
        //result type... and no expected parameters for fourth parameter,
        //however we can not use null for this argument
        me = exprFactory.createMethodExpression(elCtx, "#{" + confirmAction + "}", null, new Class[] {});
        Object invokeResult = me.invoke(elCtx, null);
        
        return invokeResult != null ? invokeResult.toString() : null;
    }
    
    public String getConfirmAction() {
        return confirmAction;
    }
    public void setConfirmAction(String confirmAction) {
        this.confirmAction = confirmAction;
        
        //confirm-actions and javascript function calls
        //are mutually exclusive
        this.jsFunction = "";
    }
    
    public String getConfirmMsg() {
        return confirmMsg;
    }
    public void setConfirmMsg(String confirmMsg) {
        this.confirmMsg = confirmMsg;
    }
    
    public String getReRenderList() {
        //if (strictConfirmationDialog && !actionConfirmed) return "";
        return reRenderList;
    }
    public void setReRenderList(String reRenderList) {
        this.reRenderList = reRenderList;
    }

    public String getJsFunction() {
        return jsFunction;
    }
    public void setJsFunction(String jsFunction) {
        this.jsFunction = jsFunction;
        
        //confirm-actions and javascript function calls
        //are mutually exclusive
        this.confirmAction = "";
    }

    public String getWidth() {
        return width;
    }
    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }
    public void setHeight(String height) {
        this.height = height;
    }
    
    public String getzIndex() {
        return zIndex;
    }
    public void setzIndex(String zIndex) {
        this.zIndex = zIndex;
    }

    public boolean isMoveable() {
        return moveable;
    }
    public void setMoveable(boolean moveable) {
        this.moveable = moveable;
    }

    public boolean isResizeable() {
        return resizeable;
    }
    public void setResizeable(boolean resizeable) {
        this.resizeable = resizeable;
    }
    
    public boolean isActionConfirmed() {
        return actionConfirmed;
    }
    public void setActionConfirmed(boolean actionConfirmed) {
        this.actionConfirmed = actionConfirmed;
    }

    public boolean isStrictConfirmationDialog() {
        return strictConfirmationDialog;
    }
    public void setStrictConfirmationDialog(boolean strictConfirmationDialog) {
        this.strictConfirmationDialog = strictConfirmationDialog;
    }

    public Object getTagObject() {
        return tagObject;
    }
    public void setTagObject(Object tagObject) {
        this.tagObject = tagObject;
    }

    public boolean isServerAction() {
        return jsFunction == null || jsFunction.trim().equals("");
    }
}
