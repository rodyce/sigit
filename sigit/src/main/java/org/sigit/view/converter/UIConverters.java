package org.sigit.view.converter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;

import org.sigit.i18n.ResourceBundleHelper;
import org.sigit.logic.viewer.InteractiveViewerHelper;
import org.sigit.model.hnd.administrative.HND_ActivityType;
import org.sigit.model.hnd.administrative.HND_BuildingPermitType;
import org.sigit.model.hnd.administrative.HND_MunicipalTransactionType;
import org.sigit.model.hnd.administrative.HND_OperationPermitType;
import org.sigit.model.hnd.administrative.HND_PermitType;
import org.sigit.model.hnd.cadastre.HND_DevelopmentStatusType;
import org.sigit.model.hnd.cadastre.HND_DocumentType;
import org.sigit.model.hnd.cadastre.HND_EasementType;
import org.sigit.model.hnd.cadastre.HND_TaxationStatusType;
import org.sigit.model.ladm.administrative.LA_AdministrativeSourceType;
import org.sigit.model.ladm.administrative.LA_ResponsibilityType;
import org.sigit.model.ladm.administrative.LA_RestrictionType;
import org.sigit.model.ladm.administrative.LA_RightType;
import org.sigit.model.ladm.spatialunit.LA_BuildingUnitType;
import org.sigit.view.output.ViewerPrintHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component("uiConverters")
@Scope(value="sigit-conversation")
public class UIConverters implements Serializable {
    private final static long serialVersionUID = 1L;
    
    private String DEFAULT_OPTION;

    //enum converters
    private EnumTypeConverter<LA_AdministrativeSourceType> administrativeSourceType;
    private EnumTypeConverter<HND_DocumentType> documentType;
    private EnumTypeConverter<HND_EasementType> easementType;
    private EnumTypeConverter<HND_TaxationStatusType> taxStatusType;
    private EnumTypeConverter<HND_TaxationStatusType> taxStatusTypeNoDefault;
    private EnumTypeConverter<HND_DevelopmentStatusType> developmentStatusType;
    private EnumTypeConverter<LA_RightType> rightType;
    private EnumTypeConverter<LA_RestrictionType> restrictionType;
    private EnumTypeConverter<LA_ResponsibilityType> responsibilityType;
    private EnumTypeConverter<HND_MunicipalTransactionType> municipalTransactionType;
    private EnumTypeConverter<HND_ActivityType> activityType;
    private EnumTypeConverter<HND_PermitType> permitType;
    private EnumTypeConverter<HND_BuildingPermitType> buildingPermitType;
    private EnumTypeConverter<HND_OperationPermitType> operationPermitType;
    private EnumTypeConverter<LA_BuildingUnitType> buildingUnitType;
    private EnumTypeConverter<YesNoType> yesNoType;
    
    //viewer
    private EnumTypeConverter<InteractiveViewerHelper.LayerTheme> viewerLayerThemeType;
    private EnumTypeConverter<ViewerPrintHelper.PrintTarget> viewerPrintTargetType;
    
    //entity converters
    private HND_LayerConverter layer;
    private HND_LandUseDomainConverter landUseDomain;
    private HND_SpatialRuleConverter spatialRule;
    private HND_PermitRuleGroupConverter buildingPermitRuleGroup;
    private HND_PermitRuleGroupConverter operationPermitRuleGroup;
    
    
    @Autowired
    private ResourceBundleHelper resBundle;
    
    
    @PostConstruct
    void _init() {
        DEFAULT_OPTION = resBundle.loadMessage("txt.select_item");

        //entity converters initialization
        layer = new HND_LayerConverter(DEFAULT_OPTION);
        landUseDomain = new HND_LandUseDomainConverter();
        spatialRule = new HND_SpatialRuleConverter(DEFAULT_OPTION);
        buildingPermitRuleGroup = new HND_PermitRuleGroupConverter(DEFAULT_OPTION);
        operationPermitRuleGroup = new HND_PermitRuleGroupConverter(DEFAULT_OPTION);

        //enum converters initialization
        administrativeSourceType = new EnumTypeConverter<>(LA_AdministrativeSourceType.class);
        documentType = new EnumTypeConverter<>(HND_DocumentType.class);
        easementType = new EnumTypeConverter<>(HND_EasementType.class);
        taxStatusType = new EnumTypeConverter<>(HND_TaxationStatusType.class);
        taxStatusTypeNoDefault = new EnumTypeConverter<>(HND_TaxationStatusType.class);
        developmentStatusType = new EnumTypeConverter<>(HND_DevelopmentStatusType.class);
        rightType = new EnumTypeConverter<>(LA_RightType.class);
        restrictionType = new EnumTypeConverter<>(LA_RestrictionType.class);
        responsibilityType = new EnumTypeConverter<>(LA_ResponsibilityType.class);
        municipalTransactionType = new EnumTypeConverter<>(HND_MunicipalTransactionType.class);
        activityType = new EnumTypeConverter<>(HND_ActivityType.class);
        permitType = new EnumTypeConverter<>(HND_PermitType.class);
        buildingPermitType = new EnumTypeConverter<>(HND_BuildingPermitType.class);
        operationPermitType = new EnumTypeConverter<>(HND_OperationPermitType.class);
        buildingUnitType = new EnumTypeConverter<>(LA_BuildingUnitType.class);
        yesNoType = new EnumTypeConverter<>(YesNoType.class);
        
        //viewer
        viewerLayerThemeType = new EnumTypeConverter<InteractiveViewerHelper.LayerTheme>(InteractiveViewerHelper.LayerTheme.class);
        viewerPrintTargetType = new EnumTypeConverter<ViewerPrintHelper.PrintTarget>(ViewerPrintHelper.PrintTarget.class, false);
    }

    //enum converters getters
    public EnumTypeConverter<LA_AdministrativeSourceType> getAdministrativeSourceType() {
        return administrativeSourceType;
    }
    public EnumTypeConverter<HND_DocumentType> getDocumentType() {
        return documentType;
    }
    public EnumTypeConverter<HND_EasementType> getEasementType() {
        return easementType;
    }
    public EnumTypeConverter<HND_TaxationStatusType> getTaxStatusType() {
        return taxStatusType;
    }
    public EnumTypeConverter<HND_TaxationStatusType> getTaxStatusTypeNoDefault() {
        return taxStatusTypeNoDefault;
    }
    public EnumTypeConverter<HND_DevelopmentStatusType> getDevelopmentStatusType() {
        return developmentStatusType;
    }
    public EnumTypeConverter<LA_RightType> getRightType() {
        return rightType;
    }
    public EnumTypeConverter<LA_RestrictionType> getRestrictionType() {
        return restrictionType;
    }
    public EnumTypeConverter<LA_ResponsibilityType> getResponsibilityType() {
        return responsibilityType;
    }
    public EnumTypeConverter<HND_MunicipalTransactionType> getMunicipalTransactionType() {
        return municipalTransactionType;
    }
    public EnumTypeConverter<HND_ActivityType> getActivityType() {
        return activityType;
    }
    public EnumTypeConverter<HND_PermitType> getPermitType() {
        return permitType;
    }
    public EnumTypeConverter<HND_BuildingPermitType> getBuildingPermitType() {
        return buildingPermitType;
    }
    public EnumTypeConverter<HND_OperationPermitType> getOperationPermitType() {
        return operationPermitType;
    }
    public EnumTypeConverter<YesNoType> getYesNoType() {
        return yesNoType;
    }
    public EnumTypeConverter<LA_BuildingUnitType> getBuildingUnitType() {
        return buildingUnitType;
    }
    
    //viewer
    public EnumTypeConverter<InteractiveViewerHelper.LayerTheme> getViewerLayerThemeType() {
        return viewerLayerThemeType;
    }
    public EnumTypeConverter<ViewerPrintHelper.PrintTarget> getViewerPrintTargetType() {
        return viewerPrintTargetType;
    }

    //entity converter getters
    public HND_LayerConverter getLayer() {
        return layer;
    }
    public HND_LandUseDomainConverter getLandUseDomain() {
        return landUseDomain;
    }
    public HND_SpatialRuleConverter getSpatialRule() {
        return spatialRule;
    }
    public HND_PermitRuleGroupConverter getBuildingPermitRuleGroup() {
        return buildingPermitRuleGroup;
    }
    public HND_PermitRuleGroupConverter getOperationPermitRuleGroup() {
        return operationPermitRuleGroup;
    }

    public static enum YesNoType {
        YES,
        NO
    }

    public class EnumTypeConverter<E extends Enum<E>> implements Converter {
        private Class<E> clazz;
        private List<SelectItem> valueList;
        
        public EnumTypeConverter(Class<E> clazz) {
            this(clazz, true);
        }
        public EnumTypeConverter(Class<E> clazz, boolean includeDefault) {
            this.clazz = clazz;
            
            this.valueList = new ArrayList<SelectItem>();
            if (includeDefault)
                valueList.add(new SelectItem("", DEFAULT_OPTION));
            for (E e : clazz.getEnumConstants())
                valueList.add(new SelectItem(e, resBundle.loadMessage(e.name())));
        }

        @Override
        public Object getAsObject(FacesContext context, UIComponent component,
                String value) {
            value = value.trim();
            if (value.equals("") || value.equals(DEFAULT_OPTION))
                return null;
            return Enum.valueOf(clazz, value);
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component,
                Object value) {
            if (value instanceof String) {
                if (((String) value).trim().equals(""))
                    return DEFAULT_OPTION;
                else
                    return value.toString();
            }
            return ((Enum<?>) value).name();
        }
        
        public List<SelectItem> getValueList() {
            return Collections.unmodifiableList(valueList);
        }
    }
}
