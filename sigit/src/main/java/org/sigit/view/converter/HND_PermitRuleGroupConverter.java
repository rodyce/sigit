package org.sigit.view.converter;

import java.util.UUID;

import org.sigit.model.hnd.administrative.HND_PermitRuleGroup;

public class HND_PermitRuleGroupConverter extends EntityConverter<HND_PermitRuleGroup> {

    public HND_PermitRuleGroupConverter() {
    }
    public HND_PermitRuleGroupConverter(String DEFAULT_OPTION) {
        super(DEFAULT_OPTION);
    }

    @Override
    protected UUID getItemId(Object object) {
        if (object instanceof HND_PermitRuleGroup)
            return ((HND_PermitRuleGroup) object).getId();
        return null;
    }

    @Override
    protected String getItemIdAsString(Object object) {
        if (getItemId(object) != null)
            return String.valueOf(getItemId(object));
        return null;
    }

    @Override
    protected String getItemNameAsString(Object object) {
        if (object instanceof HND_PermitRuleGroup)
            return ((HND_PermitRuleGroup) object).getName();
        return null;
    }

}
