package org.sigit.view.converter;

import java.util.UUID;

import org.sigit.model.hnd.cadastre.HND_Layer;

public class HND_LayerConverter extends EntityConverter<HND_Layer> {

    public HND_LayerConverter(String DEFAULT_OPTION) {
        super(DEFAULT_OPTION);
    }
    

    @Override
    protected UUID getItemId(Object object) {
        if (object instanceof HND_Layer)
            return ((HND_Layer) object).getIID();
        return null;
    }

    @Override
    protected String getItemIdAsString(Object object) {
        if (getItemId(object) != null)
            return String.valueOf(getItemId(object));
        return null;
    }

    @Override
    protected String getItemNameAsString(Object object) {
        if (object instanceof HND_Layer)
            return ((HND_Layer) object).getName();
        return null;
    }
}
