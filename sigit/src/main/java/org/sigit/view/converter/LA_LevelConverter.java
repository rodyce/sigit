package org.sigit.view.converter;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;

import org.sigit.model.ladm.spatialunit.LA_Level;
import org.springframework.transaction.annotation.Transactional;

public class LA_LevelConverter implements Converter {
    
    private List<SelectItem> laLevelList;
    
    public LA_LevelConverter(List<SelectItem> laLevelList) {
        this.laLevelList = laLevelList;
    }

    @Override
    @Transactional
    public Object getAsObject(FacesContext context, UIComponent component,
            String value) {
        for (SelectItem item : laLevelList) {
            LA_Level objValue = (LA_Level) item.getValue();
            if (objValue != null && objValue.getIID().toString().equals(value)) {
                return item.getValue();
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component,
            Object value) {
        if (value == null) return "";
        
        return String.valueOf(((LA_Level) value).getIID());
    }

}
