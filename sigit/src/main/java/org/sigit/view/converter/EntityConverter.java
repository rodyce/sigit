package org.sigit.view.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;

public abstract class EntityConverter<T> implements Converter {
    private final String DEFAULT_OPTION;

    private Map<List<T>, List<SelectItem>> itemListMap = new HashMap<>();
    private Map<UUID, T> itemMap = new HashMap<>();

    public EntityConverter() {
        DEFAULT_OPTION = null;
    }
    public EntityConverter(String DEFAULT_OPTION) {
        this.DEFAULT_OPTION = DEFAULT_OPTION;
    }
    

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        value = value.trim();
        if (value.equals("") || value.equals(DEFAULT_OPTION))
            return null;
        return itemMap.get(UUID.fromString(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object object) {
        if (object == null) return "";
        
        if (object instanceof String) {
            if (((String) object).trim().equals(""))
                return DEFAULT_OPTION;
            else
                return object.toString();
        }
        return getItemIdAsString(object);//String.valueOf( ((HND_Layer) object).getIID() );
    }
    
    public List<SelectItem> selectItems(List<T> itemList) {
        List<SelectItem> listWrapper = itemListMap.get(itemList);
        if (listWrapper == null) {
            listWrapper = new ArrayList<SelectItem>();
            if (DEFAULT_OPTION != null)
                listWrapper.add(new SelectItem("", DEFAULT_OPTION));
            for (T l : itemList) {
                listWrapper.add(new SelectItem(getItemIdAsString(l), getItemNameAsString(l)));
                itemMap.put(getItemId(l), l);
            }
            listWrapper = Collections.unmodifiableList(listWrapper);
            itemListMap.put(itemList, listWrapper);
        }
        
        return listWrapper;
    }

    protected abstract UUID getItemId(Object object);
    protected abstract String getItemIdAsString(Object object);
    protected abstract String getItemNameAsString(Object object);
}
