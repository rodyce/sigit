package org.sigit.view.converter;

import java.util.UUID;

import org.sigit.model.hnd.cadastre.HND_LandUseDomain;

public class HND_LandUseDomainConverter extends EntityConverter<HND_LandUseDomain> {

    public HND_LandUseDomainConverter() {
    }
    public HND_LandUseDomainConverter(String DEFAULT_OPTION) {
        super(DEFAULT_OPTION);
    }

    @Override
    protected UUID getItemId(Object object) {
        if (object instanceof HND_LandUseDomain)
            return ((HND_LandUseDomain) object).getId();
        return null;
    }

    @Override
    protected String getItemIdAsString(Object object) {
        if (getItemId(object) != null)
            return String.valueOf(getItemId(object));
        return null;
    }

    @Override
    protected String getItemNameAsString(Object object) {
        if (object instanceof HND_LandUseDomain)
            return ((HND_LandUseDomain) object).getName();
        return null;
    }

}
