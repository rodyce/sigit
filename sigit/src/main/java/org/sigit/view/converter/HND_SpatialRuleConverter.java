package org.sigit.view.converter;

import java.util.UUID;

import org.sigit.model.hnd.administrative.HND_SpatialRule;

public class HND_SpatialRuleConverter extends EntityConverter<HND_SpatialRule> {

    public HND_SpatialRuleConverter(String DEFAULT_OPTION) {
        super(DEFAULT_OPTION);
    }
    

    @Override
    protected UUID getItemId(Object object) {
        if (object instanceof HND_SpatialRule)
            return ((HND_SpatialRule) object).getId();
        return null;
    }

    @Override
    protected String getItemIdAsString(Object object) {
        if (getItemId(object) != null)
            return String.valueOf(getItemId(object));
        return null;
    }

    @Override
    protected String getItemNameAsString(Object object) {
        if (object instanceof HND_SpatialRule)
            return ((HND_SpatialRule) object).getCode();
        return null;
    }
}
