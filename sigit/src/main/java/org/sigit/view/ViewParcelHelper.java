package org.sigit.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.sigit.dao.hnd.cadastre.HND_ParcelDAO;
import org.sigit.i18n.ResourceBundleHelper;
import org.sigit.model.commons.IParcel;
import org.sigit.model.commons.IParty;
import org.sigit.model.commons.IProperty;
import org.sigit.model.commons.IRRR;
import org.sigit.model.commons.IResponsibility;
import org.sigit.model.commons.IRestriction;
import org.sigit.model.commons.IRight;
import org.sigit.model.hnd.cadastre.HND_Parcel;
import org.sigit.util.ShareValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Scope;

@Component("viewParcelHelper")
@Scope(value="sigit-conversation")
public class ViewParcelHelper implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private IParcel lastPredParcel;
    private IParcel lastSuccParcel;
    private IProperty lastProperty;
    private List<IRight<?,?>> propertyRightsList;
    private List<IRestriction<?,?>> propertyRestrictionsList;
    private List<IResponsibility<?,?>> propertyResponsibilitiesList;
    private List<HND_Parcel> parcelPredecessors;
    private List<HND_Parcel> parcelSuccessors;
    
    @Autowired
    private ResourceBundleHelper resBundle;


    public String formatEmptyText(Object value) {
        if (value == null || value.equals(""))
            return resBundle.loadMessage("global.empty");
        
        return value.toString();
    }
    
    public String formatBooleanText(Boolean value) {
        if (value == null)
            return resBundle.loadMessage("txt.no_value");

        return value ? resBundle.loadMessage("txt.YES") : resBundle.loadMessage("txt.NO");
    }
    
    public List<IRight<?,?>> getPropertyRightsList(IProperty property) {
        if (propertyRightsList == null || property != lastProperty) {
            if (property != null) {
                propertyRightsList = property.getRights();
                lastProperty = property;
            }
            else
                propertyRightsList = new ArrayList<IRight<?,?>>();
        }
        return propertyRightsList;
    }

    public List<IRestriction<?,?>> getPropertyRestrictionsList(IProperty property) {
        if (propertyRestrictionsList == null || property != lastProperty) {
            if (property != null) {
                propertyRestrictionsList = property.getRestrictions();
                lastProperty = property;
            }
            else
                propertyRestrictionsList = new ArrayList<IRestriction<?,?>>();
        }
        return propertyRestrictionsList;
    }
    
    public List<IResponsibility<?,?>> getPropertyResponsibilitiesList(IProperty property) {
        if (propertyResponsibilitiesList == null || property != lastProperty) {
            if (property != null) {
                propertyResponsibilitiesList = property.getResponsibilities();
                lastProperty = property;
            }
            else
                propertyResponsibilitiesList = new ArrayList<IResponsibility<?,?>>();
        }
        return propertyResponsibilitiesList;
    }
    
    public String getOwner(IRRR<?,?> rrr) {
        IParty party = rrr.getParty();
        
        if (party == null) return "";
        if (party.getExtParty() == null) return "";
        
        return party.getExtParty().getName();
    }
    
    public String getTypeString(IRRR<?,?> rrr) {
        String typeStr = "";

        if (rrr instanceof IRight) {
            if ( ((IRight<?,?>) rrr).getType() != null )
                typeStr = ((IRight<?,?>) rrr).getType().name();
        }
        else if (rrr instanceof IRestriction) {
            if ( ((IRestriction<?,?>) rrr).getType() != null )
                typeStr = ((IRestriction<?,?>) rrr).getType().name();
        }
        else if (rrr instanceof IResponsibility) {
            if ( ((IResponsibility<?,?>) rrr).getType() != null )
                typeStr = ((IResponsibility<?,?>) rrr).getType().name();
        }
        
        return typeStr;
    }
    
    public String getShareString(IRRR<?,?> rrr) {
        String shareStr = "";
        if (rrr.getShare() != null)
            shareStr = rrr.getShare().toString();
        return shareStr;
    }
    
    public ShareValue getRightsSumShares(IProperty property) {
        ShareValue result = new ShareValue();
        
        if (getPropertyRightsList(property) != null) {
            for (IRight<?,?> rrr : propertyRightsList)
                result.addOther(new ShareValue(((IRRR<?,?>)rrr).getShare()));
            
        }
        
        return result;
    }
    
    public ShareValue getRestrictionsSumShares(IProperty property) {
        ShareValue result = new ShareValue();

        if (getPropertyRestrictionsList(property) != null)
            for (IRestriction<?,?> rrr : propertyRestrictionsList)
                result.addOther(new ShareValue(((IRRR<?,?>)rrr).getShare()));
        
        return result;
    }

    public ShareValue getResponsibilitiesSumShares(IProperty property) {
        ShareValue result = new ShareValue();

        if (getPropertyResponsibilitiesList(property) != null)
            for (IResponsibility<?,?> rrr : propertyResponsibilitiesList)
                result.addOther(new ShareValue(((IRRR<?,?>)rrr).getShare()));
        
        return result;
    }
    
    @Transactional
    public List<HND_Parcel> getParcelPredecessors(HND_Parcel hndParcel) {
        if (parcelPredecessors == null || hndParcel != lastPredParcel) {
            parcelPredecessors = HND_ParcelDAO.loadParcelPredecessors(hndParcel);
            lastPredParcel = hndParcel;
        }
        return parcelPredecessors;
    }
    
    @Transactional
    public List<HND_Parcel> getParcelSuccessors(HND_Parcel hndParcel) {
        if (parcelSuccessors == null || hndParcel != lastSuccParcel) {
            parcelSuccessors = HND_ParcelDAO.loadParcelSuccessors(hndParcel);
            lastSuccParcel = hndParcel;
        }
        return parcelSuccessors;
    }
}
