package org.sigit.view;

import java.io.Serializable;

import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

@Component("mantUISettingsHelper")
@Scope(value="sigit-conversation")
public class MantUISettingsHelper implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Boolean showTitle = true;
    private Integer numElements = 10;
    
    
    public Boolean getShowTitle() {
        return showTitle;
    }
    public void setShowTitle(Boolean showTitle) {
        this.showTitle = showTitle;
    }
    public Integer getNumElements() {
        return numElements;
    }
    public void setNumElements(Integer numElements) {
        this.numElements = numElements;
    }
}
