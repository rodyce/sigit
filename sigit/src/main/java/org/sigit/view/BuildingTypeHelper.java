package org.sigit.view;

import org.sigit.dao.hnd.cadastre.HND_BuildingTypeDAO;
import org.sigit.model.hnd.cadastre.HND_BuildingType;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Scope;

@Component("buildingTypeHelper")
@Scope(value="sigit-conversation")
public class BuildingTypeHelper extends MantHelper<HND_BuildingType> {
    private static final long serialVersionUID = 1L;
    
    protected final String BY_NAME = "0";
    

    @Override
    public String addNew() {
        setSelected(new HND_BuildingType());
        return super.addNew();
    }


    @Override
    @Transactional
    public String quickSearch() {
        searchText = searchText.trim();
        List<HND_BuildingType> searchResultList = new ArrayList<HND_BuildingType>();
        
        if (searchSelector.equals(BY_NAME))
            searchResultList = HND_BuildingTypeDAO.loadBuildingTypesByName(searchText);
        
        if (searchResultList.size() == 0) {
            FacesContext.getCurrentInstance().addMessage("",
                    new FacesMessage(
                            FacesMessage.SEVERITY_INFO,
                            resBundle.loadMessage("txt.no_search_results"),
                            ""
                    )
            );
        }

        setEntityList(searchResultList);
        
        return null;
    }


    public String getBY_NAME() {
        return BY_NAME;
    }
}
