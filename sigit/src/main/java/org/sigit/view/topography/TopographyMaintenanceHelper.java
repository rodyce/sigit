package org.sigit.view.topography;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.geotools.geometry.jts.WKTReader2;
import org.sigit.commons.geometry.GeometryOperations;
import org.sigit.dao.SigitDAO;
import org.sigit.dao.hnd.administrative.HND_PropertyDAO;
import org.sigit.dao.hnd.administrative.HND_TopographicTransactionDAO;
import org.sigit.dao.hnd.cadastre.HND_LayerDAO;
import org.sigit.dao.hnd.cadastre.HND_SpatialZoneDAO;
import org.sigit.i18n.ResourceBundleHelper;
import org.sigit.logic.security.Credentials;
import org.sigit.logic.viewer.InteractiveViewerHelper;
import org.sigit.model.commons.IParcel;
import org.sigit.model.hnd.administrative.HND_Property;
import org.sigit.model.hnd.administrative.HND_RuleOperatorType;
import org.sigit.model.hnd.administrative.HND_TopographicTransaction;
import org.sigit.model.hnd.administrative.HND_TopographicTransactionStateType;
import org.sigit.model.hnd.cadastre.HND_BuildingUnit;
import org.sigit.model.hnd.cadastre.HND_Layer;
import org.sigit.model.hnd.cadastre.HND_Parcel;
import org.sigit.model.hnd.cadastre.HND_SpatialZone;
import org.sigit.model.hnd.special.HND_LockLevelType;
import org.sigit.view.NavigationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.io.ParseException;

@Component("topographyMaintenanceHelper")
@Scope(value="sigit-conversation")
public class TopographyMaintenanceHelper implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private static final double PICK_BOX_MINIMUM_AREA = 20.0;
    private static final int PICK_BOX_MAX_ELEMENT_COUNT = 50;
    
    private String zoneWkts;
    
    private List<HND_Layer> layers;
    private HND_TopographicTransaction transaction;
    
    private Geometry envelopeBox;
    
    private double x1;
    private double y1;
    private double x2;
    private double y2;
    
    private Date endLifespan;
    private String endLifespanReason;
    
    
    @Autowired
    private InteractiveViewerHelper interactiveViewerHelper;
    
    @Autowired
    private ResourceBundleHelper resBundle;
    
    @Autowired
    private Credentials credentials;
    
    @Autowired
    private NavigationHelper navigationHelper;
    

    private String loggedUserFullName;


    public String getZoneWkts() {
        return zoneWkts;
    }
    public void setZoneWkts(String zoneWkts) {
        this.zoneWkts = zoneWkts.trim();
    }
    
    @Transactional
    public List<HND_Layer> getLayers() {
        if (layers == null)
            layers = HND_LayerDAO.loadLayers(true);
        return layers;
    }
    
    public HND_TopographicTransaction getTransaction() {
        if (transaction == null)
            transaction = new HND_TopographicTransaction();
        return transaction;
    }
    public void setTransaction(HND_TopographicTransaction transaction) {
        if (this.transaction != transaction) {
            this.transaction = transaction;
            if (transaction.getExtents() != null) {
                Envelope env = transaction.getExtents().getEnvelopeInternal();
                x1 = env.getMinX();
                y1 = env.getMinY();
                x2 = env.getMaxX();
                y2 = env.getMaxY();
            }
        }
    }
    
    public double getX1() {
        return x1;
    }
    public double getY1() {
        return y1;
    }

    public double getX2() {
        return x2;
    }
    public double getY2() {
        return y2;
    }
    
    public void pickExtentsFromMap() {
        envelopeBox = interactiveViewerHelper.getExtentsBox();
        if (validateEnvelopeBox(true, true)) {
            Coordinate[] coords = envelopeBox.getCoordinates();
            if (coords.length > 0) {
                x1 = x2 = coords[0].x;
                y1 = y2 = coords[0].y;
                for (Coordinate coord : coords) {
                    if (coord.x < x1) x1 = coord.x;
                    if (coord.x > x2) x2 = coord.x;
                    if (coord.y < y1) y1 = coord.y;
                    if (coord.y > y2) y2 = coord.y;
                }
            }
        }
    }
    
    @Transactional
    public String endDataIntro() {
        if (validateEndDataIntro()) {
            getTransaction().setStartDate(new Date());
            getTransaction().setEditorUserName(credentials.getUsername());
            getTransaction().setEditorFullName(loggedUserFullName);
            getTransaction().setState(HND_TopographicTransactionStateType.INITIATED);
            getTransaction().setExtents(envelopeBox);
            
            List<HND_SpatialZone> list = HND_SpatialZoneDAO.loadSpatialZonesByLayerAndGeom(
                    getTransaction().getWorkingLayer(), envelopeBox, HND_RuleOperatorType.WITHIN);

            for (HND_SpatialZone sz : list) {
                sz.getLock().setLockingTransaction(getTransaction());
                if (sz.getShape().within(envelopeBox)) {
                    //Explicitly lock the spatial zone only if it is completely within the envelope box
                    sz.getLock().setLockLevel(HND_LockLevelType.LOCKED_EXPLICIT);
                } else {
                    //Otherwise, just make an implicit lock
                    sz.getLock().setLockLevel(HND_LockLevelType.LOCKED_IMPLICIT);
                }
                
                SigitDAO.save(sz);
            }
            
            SigitDAO.save(getTransaction());
            
            return navigationHelper.navigateToHome();
        }
        return null;
    }

    @Transactional
    public String endTransaction() {
        getTransaction().setCompletionDate(new Date());
        getTransaction().setState(HND_TopographicTransactionStateType.FINISHED);
        
        SigitDAO.save(getTransaction());
        
        return navigationHelper.navigateToHome();
    }
    
    public boolean validateEndDataIntro() {
        return validateEnvelopeBox(true, true);
    }
    
    public String finishDataIntro() {
        return navigationHelper.navigateToHome();
    }
    
    private boolean validateEnvelopeBox(boolean setMsg, boolean allowEmptyBox) {
        //Check that the envelope box does not overlap an existing one from another transaction
        //in the same working layer
        if (HND_TopographicTransactionDAO.isTopographicTransactionInProgressExtentOverlappingGeom(
                envelopeBox, getTransaction().getWorkingLayer())) {
            if (setMsg) {
                setError("", resBundle.loadMessage("topography.pickbox_another_transaction_intercepting_err"), "");
            }
            return false;
        }

        //Check that the pick box is at least as large as the minimum area
        boolean val = envelopeBox != null && envelopeBox instanceof Polygon && envelopeBox.getArea() > PICK_BOX_MINIMUM_AREA;
        if ( !val ) {
            if (setMsg) {
                setError("", resBundle.loadMessage("topography.pickbox_too_small_err"), "");
            }
            return false;
        }
        
        //Retrieve a list of spatial zones that are within the envelope box 
        List<HND_SpatialZone> list = HND_SpatialZoneDAO.loadSpatialZonesByLayerAndGeom(
                getTransaction().getWorkingLayer(), envelopeBox, HND_RuleOperatorType.WITHIN);
        
        //Check that at least one object in the selected layer is within the box...
        if (!allowEmptyBox && list.size() == 0) {
            if (setMsg) {
                setError("", resBundle.loadMessage("topography.pickbox_nothing_within_err"), errorDetail());
            }
            return false;
        }
        
        //Check that no more than the maximum number elements are selected in this transaction
        if (list.size() > PICK_BOX_MAX_ELEMENT_COUNT) {
            if (setMsg) {
                setError("", resBundle.loadMessage("topography.pickbox_nothing_too_many_elements_within_err"), errorDetail());
            }
            return false;
        }
        
        //Check that no spatial zone is locked
        for (HND_SpatialZone sz : list) {
            if (sz.getLock() != null && sz.getLock().isLocked()) {
                if (setMsg) {
                    setError("", resBundle.loadMessage("topography.pickbox_zones_already_locked"), "");
                }
                return false;
            }
        }

        return true;
    }
    
    private String errorDetail() {
        HND_Layer wl = getTransaction().getWorkingLayer();
        if (wl != null)
            return String.format("%s: %s", resBundle.loadMessage("topography.layer"), wl.getName());
        
        return resBundle.loadMessage("topography.no_layer");    
    }
    
    private void setError(String clientId, String message, String detail) {
        FacesContext.getCurrentInstance().addMessage(clientId,
                new FacesMessage(
                        FacesMessage.SEVERITY_ERROR,
                        message,
                        detail
                )
        );
    }

    
    
    public Date getEndLifespan() {
        if (endLifespan == null)
            endLifespan = new Date();
        return endLifespan;
    }
    public void setEndLifespan(Date endLifespan) {
        this.endLifespan = endLifespan;
    }
    
    public String getEndLifespanReason() {
        return endLifespanReason;
    }
    public void setEndLifespanReason(String endLifespanReason) {
        this.endLifespanReason = endLifespanReason;
    }
    
    
    @Transactional
    public String deleteSelectedSZ() {
        if (interactiveViewerHelper != null && interactiveViewerHelper.getSelectedZone() instanceof HND_SpatialZone) {
            HND_SpatialZone sz = (HND_SpatialZone) interactiveViewerHelper.getSelectedZone();
            sz.setEndLifespanVersion(getEndLifespan());
            sz.setEndLifespanReason(getEndLifespanReason());

            SigitDAO.save(sz);

            setEndLifespan(null);
            setEndLifespanReason(null);
            interactiveViewerHelper.setSelectedZone(null);
            interactiveViewerHelper.setHighlightedSpatialUnitIds(null);
        }
        return null;
    }
    
    public boolean isSelectedSZLockedInCurrentTransaction() {
        if (getTransaction().getId() != null && interactiveViewerHelper != null && interactiveViewerHelper.getSelectedZone() instanceof HND_SpatialZone) {
            HND_SpatialZone sz = (HND_SpatialZone) interactiveViewerHelper.getSelectedZone();
            if (sz.getLock().isLocked())
                return sz.getLock().getLockingTransaction().getId().equals(getTransaction().getId());
        }
        return false;
    }



    @Transactional
    public void saveZones() {
        if (zoneWkts == null || zoneWkts.trim().equals("")) return;
        
        HND_SpatialZone newSpatialZone;
        Geometry geom;
        GeometryCollection geomCollection;
        
        WKTReader2 wktReader = new WKTReader2(GeometryOperations.geomFactory);
        try {
            geom = wktReader.read(zoneWkts);
        }
        catch (ParseException pe) {
            pe.printStackTrace();
            return;
        }
        
        if ( !(geom instanceof GeometryCollection) ) return;
        
        geomCollection = (GeometryCollection) geom;
        
        //Validate that every geometry is simple and valid
        for (int i = 0; i < geomCollection.getNumGeometries(); i++) {
            geom = geomCollection.getGeometryN(i);
            
            if (!geom.isSimple() || !geom.isValid()) {
                FacesContext.getCurrentInstance().addMessage(
                        "",
                        new FacesMessage(
                                FacesMessage.SEVERITY_ERROR,
                                "ERROR: Uno o mas elementos geometricos no son validos",
                                ""
                        )
                );
                return;
            }
        }
        
        //Store each geometry in its corresponding layer
        for (int i = 0; i < geomCollection.getNumGeometries(); i++) {
            geom = geomCollection.getGeometryN(i);
            
            
            //Instantiate the correct SpatialZone class depending
            //on the current layer type.
            HND_Layer workingLayer = getTransaction().getWorkingLayer();
            switch (workingLayer.getLayerType()) {
            case BUILDING:
                newSpatialZone = new HND_BuildingUnit();
                ((HND_BuildingUnit) newSpatialZone).setNumberOfFloors(1);
                break;
            case STREET:
            case SPATIAL_ZONE:
                newSpatialZone = new HND_SpatialZone();
                break;
            case PARCEL:
            default:
                newSpatialZone = new HND_Parcel();
                ((IParcel) newSpatialZone).setFieldTab( interactiveViewerHelper.getNextFieldTab() );
                break;
            }

            
            newSpatialZone.setShape(geom);
            newSpatialZone.setOriginatingTransaction(getTransaction());
            getTransaction().getOriginatedSpatialZones().add(newSpatialZone);
            
            //Create the corresponding Property. Logically, SIGIT only supports
            //One-to-One mapping between spatial units and baunits.
            HND_Property property = new HND_Property();
            newSpatialZone.getBaunits().add(property);
            property.getSpatialUnits().add(newSpatialZone);
            
            
            //Set the layer
            newSpatialZone.setLevel(workingLayer);
            
            //lock new spatial zone with current transaction
            newSpatialZone.getLock().setLockingTransaction(getTransaction());
            newSpatialZone.getLock().setLockLevel(HND_LockLevelType.LOCKED_IMPLICIT);
            
            HND_SpatialZoneDAO.save(newSpatialZone);
            HND_PropertyDAO.save(property);
            HND_TopographicTransactionDAO.save(getTransaction());
        }
    }
    public void cancel() {
        setZoneWkts(null);
    }
}
