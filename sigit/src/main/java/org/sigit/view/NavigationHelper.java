package org.sigit.view;

import java.io.Serializable;

import org.sigit.commons.conversation.Conversation;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("navigationHelper")
@Scope("singleton")
public class NavigationHelper implements Serializable {
    private static final long serialVersionUID = 1L;

    public String navigateTo(String view) {
        return navigateTo(view, false);
    }
    
    public String navigateToNewConv(String view) {
        return navigateTo(view, true);
    }
    
    public String navigateToHome() {
        return navigateTo("/home", true);
    }

    public String navigateTo(String view, boolean beginNewConv) {
        resetConversation(beginNewConv);
        return view + "?faces-redirect=true";
    }
    
    private void resetConversation(boolean beginNewConv) {
        Conversation.instance().end(); //Flag this conversation for ending
        Conversation.instance().leave(); //Leave this conversation (forces new temporary converation to start
        
        if (beginNewConv)
            Conversation.instance().begin(); //Make our new temporary conversation long running
    }

}
