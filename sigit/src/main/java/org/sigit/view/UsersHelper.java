package org.sigit.view;

import org.sigit.logic.general.GeneralHelper;
import org.sigit.logic.security.Authenticator;
import org.sigit.model.hnd.administrative.HND_NaturalPerson;
import org.sigit.model.hnd.administrative.HND_Person;
import org.sigit.model.hnd.administrative.HND_User;
import org.sigit.dao.hnd.administrative.HND_UserDAO;
import org.sigit.i18n.ResourceBundleHelper;

import java.util.List;
import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Scope;

@Component("usersHelper")
@Scope(value="sigit-conversation")
public class UsersHelper implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<HND_User> users;
    private String password;
    private String confPassword;
    private HND_User selected;
    private int currentRow;
    
    
    @Autowired
    private ResourceBundleHelper resBundle;
    
    @Autowired
    private GeneralHelper generalHelper;

    
    @Transactional
    public List<HND_User> getUsers() {
        if (users == null)
            users = HND_UserDAO.loadUsers();
        
        return users;
    }
    public void setUsers(List<HND_User> users) {
        this.users = users;
    }
    
    public String partyType(HND_User user) {
        String txt = "-";
        
        if (user != null) {
            if (user.getParty() instanceof HND_NaturalPerson)
                txt = "txt.natural_person";
            else if (user.getParty() instanceof HND_Person)
                txt = "txt.legal_person";
        }
        
        return getResBundle().loadMessage(txt);
    }
    
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getConfPassword() {
        return confPassword;
    }
    public void setConfPassword(String confPassword) {
        this.confPassword = confPassword;
    }
    public HND_User getSelected() {
        return selected;
    }
    public void setSelected(HND_User selected) {
        this.selected = selected;
    }
    
    public int getCurrentRow() {
        return currentRow;
    }
    public void setCurrentRow(int currentRow) {
        this.currentRow = currentRow;
    }
    
    public String getNewUserPartyName() {
        if (getSelected() == null || getSelected().getParty() == null)
            return "";
        
        return getSelected().getParty().getName();
    }
    
    public String newUser() {
        setSelected(new HND_User());
        
        return null;
    }
    
    public String editUser(HND_User user) {
        setSelected(user);
        
        return null;
    }
    
    @Transactional
    public String toggleActivation(HND_User user) {
        user.setActive(!user.isActive());
        HND_UserDAO.save(user);
        
        return null;
    }
    
    @Transactional
    public String applyUserDataChange() {
        if (selected != null)
            HND_UserDAO.save(selected);

        return null;
    }
    
    @Transactional
    public String saveSelected() {
        if (selected != null) {
            if (selected.getParty() != null) {
                if (password.equals(confPassword)) {
                    selected.setPassword(Authenticator.MD5(password));
                    if (selected.getORMID() == null)
                        setUsers(null);
                    HND_UserDAO.save(selected);
                }
                else {
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            getResBundle().loadMessage("user.reset_password_err"),
                            getResBundle().loadMessage("user.reset_password_err_detail"));
                    FacesContext.getCurrentInstance().addMessage(null, message);
                }
            }
            else {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        getResBundle().loadMessage("user.no_party_selected_err"),
                        getResBundle().loadMessage("user.no_party_selected_err_detail"));
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
        }
        
        return null;
    }
    

    protected ResourceBundleHelper getResBundle() {
        if (resBundle == null) {
            resBundle = generalHelper.getResBundle();
        }
        return resBundle;
    }
}
