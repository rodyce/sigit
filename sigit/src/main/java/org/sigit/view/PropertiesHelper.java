package org.sigit.view;

import java.io.IOException;
import java.util.Properties;

import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

@Component("propertiesHelper")
@Scope(value="application")
public class PropertiesHelper {
    private static final String PROPERTIES_FILE = "sigit.properties";
    
    private final Properties properties;
    private final String version;
    
    
    public PropertiesHelper() {
        properties = new Properties();

        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            properties.load(classLoader.getResourceAsStream(PROPERTIES_FILE));
        }
        catch (IOException e) {
            System.err.println("ERROR: " + PROPERTIES_FILE + " IS MISSING");
        }
            
        version = properties.get("version").toString();
    }
    public String getVersion() {
        return version;
    }
}
