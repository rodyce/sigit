package org.sigit.view;

import org.sigit.dao.hnd.administrative.HND_SpatialRuleDAO;
import org.sigit.dao.ladm.spatialunit.LA_LevelDAO;
import org.sigit.i18n.ResourceBundleHelper;
import org.sigit.logic.general.GeneralHelper;
import org.sigit.model.hnd.administrative.HND_ComparisonOperatorType;
import org.sigit.model.hnd.administrative.HND_RuleActionType;
import org.sigit.model.hnd.administrative.HND_RuleOperatorType;
import org.sigit.model.hnd.administrative.HND_SpatialRule;
import org.sigit.model.ladm.spatialunit.LA_Level;
import org.sigit.view.converter.LA_LevelConverter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Scope;

@Component("spatialRulesHelper")
@Scope(value="sigit-conversation")
public class SpatialRulesHelper implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<HND_SpatialRule> spatialRules;
    private HND_SpatialRule selected;
    private boolean adding;
    
    private List<SelectItem> levelList;
    private List<SelectItem> comparisonOperatorValueList;
    private List<SelectItem> ruleActionValueList;
    private List<SelectItem> ruleOperatorValueList;
    
    private Converter levelConverter;
    
    @Autowired
    private ResourceBundleHelper resBundle;
    
    @Autowired
    private GeneralHelper generalHelper;
    

    @Transactional
    public List<HND_SpatialRule> getSpatialRules() {
        if (spatialRules == null) {
            spatialRules = HND_SpatialRuleDAO.loadSpatialRules();
        }
        return spatialRules;
    }
    public void setSpatialRules(List<HND_SpatialRule> spatialRules) {
        this.spatialRules = spatialRules;
    }
    
    
    public HND_SpatialRule getSelected() {
        return selected;
    }
    public void setSelected(HND_SpatialRule selected) {
        this.selected = selected;
    }
    
    @Transactional
    public List<SelectItem> getLevelList() {
        if (levelList == null) {
            levelList = new ArrayList<SelectItem>();
            levelList.add(new SelectItem(null, "(no capa)"));
            for (LA_Level level : LA_LevelDAO.loadLevels())
                levelList.add(new SelectItem(level, level.getName()));
        }
        return levelList;
    }
    public void setLevelList(List<SelectItem> levelList) {
        this.levelList = levelList;
    }
    
    

    public List<SelectItem> getComparisonOperatorValueList() {
        if (comparisonOperatorValueList == null) {
            comparisonOperatorValueList = new ArrayList<SelectItem>();
            comparisonOperatorValueList.add(new SelectItem(null, "(seleccionar)"));
            for (HND_ComparisonOperatorType cot : HND_ComparisonOperatorType.values())
                comparisonOperatorValueList.add(new SelectItem(cot, resBundle.loadMessage(cot.name())));
        }
        return comparisonOperatorValueList;
    }
    
    public List<SelectItem> getRuleActionValueList() {
        if (ruleActionValueList == null) {
            ruleActionValueList = new ArrayList<SelectItem>();
            ruleActionValueList.add(new SelectItem(null, "(seleccionar)"));
            for (HND_RuleActionType rat : HND_RuleActionType.values())
                ruleActionValueList.add(new SelectItem(rat, resBundle.loadMessage(rat.name())));
        }
        return ruleActionValueList;
    }

    public List<SelectItem> getRuleOperatorValueList() {
        if (ruleOperatorValueList == null) {
            ruleOperatorValueList = new ArrayList<SelectItem>();
            ruleOperatorValueList.add(new SelectItem(null, "(seleccionar)"));
            for (HND_RuleOperatorType rot : HND_RuleOperatorType.values())
                ruleOperatorValueList.add(new SelectItem(rot, resBundle.loadMessage(rot.name())));
        }
        return ruleOperatorValueList;
    }
    
    public String getLandUseName1() {
        if (selected == null || selected.getLandUseOperand1() == null) return "";
        return generalHelper.landUseName(selected.getLandUseOperand1());
    }
    public void setLandUseName1(String lun) {
    }

    public String getLandUseName2() {
        if (selected == null || selected.getLandUseOperand2() == null) return "";
        return generalHelper.landUseName(selected.getLandUseOperand2());
    }
    public void setLandUseName2(String lun) {
    }

    public boolean isAdding() {
        return adding;
    }
    public void setAdding(boolean adding) {
        this.adding = adding;
    }
    
    public Converter getLevelConverter() {
        if (levelConverter == null) {
            levelConverter = new LA_LevelConverter(getLevelList());
        }
        return levelConverter;
    }
    
    public void newSpatialRule() {
        selected = new HND_SpatialRule();
        adding = true;
    }
    
    public void editSpatialRule(HND_SpatialRule sr) {
        selected = sr;
        adding = false;
    }
    
    @Transactional
    public void deleteSpatialRule(HND_SpatialRule sr) {
        HND_SpatialRuleDAO.delete(sr);
        spatialRules = null;
    }
    
    @Transactional
    public String applyChanges() {
        if (selected != null) {

            if (adding) {
                spatialRules.add(selected);
            }
            
            //For the distance operator, if these 2 parameters came blank,
            //then use default values.
            if (selected.getRuleOperator() == HND_RuleOperatorType.DISTANCE) {
                if (selected.getComparisonOperator() == null) {
                    selected.setComparisonOperator(HND_ComparisonOperatorType.EQ);
                }
                if (selected.getComparisonParameterValue() == null) {
                    selected.setComparisonParameterValue(0.0);
                }
            }
            
            HND_SpatialRuleDAO.save(selected);
        }
            
        return null;
    }
}
