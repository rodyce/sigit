package org.sigit.view;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.sigit.dao.hnd.administrative.HND_BusinessTypeDAO;
import org.sigit.model.hnd.administrative.HND_BusinessType;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Scope;

@Component("businessTypeHelper")
@Scope(value="sigit-conversation")
public class BusinessTypeHelper extends MantHelper<HND_BusinessType> {
    private static final long serialVersionUID = 1L;

    protected final String BY_NAME = "0";
    

    @Override
    public String addNew() {
        setSelected(new HND_BusinessType());
        return super.addNew();
    }
    
    @Override
    @Transactional
    public String quickSearch() {
        searchText = searchText.trim();
        List<HND_BusinessType> searchResultList = new ArrayList<HND_BusinessType>();
        
        if (searchSelector.equals(BY_NAME))
            searchResultList = HND_BusinessTypeDAO.loadBusinessTypesByName(searchText);
        
        if (searchResultList.size() == 0) {
            FacesContext.getCurrentInstance().addMessage("",
                    new FacesMessage(
                            FacesMessage.SEVERITY_INFO,
                            resBundle.loadMessage("txt.no_search_results"),
                            ""
                    )
            );
        }

        setEntityList(searchResultList);
        
        return null;
    }


    public String getBY_NAME() {
        return BY_NAME;
    }
}
