package org.sigit.view.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;

import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

@Component("uiValidators")
@Scope("singleton")
public class UIValidators {
    public void validateNotNull(FacesContext context, UIComponent toValidate, Object value) {
        String message = "Valor requerido";
        
        if (value == null) {
            ((UIInput) toValidate).setValid(false);
            context.addMessage(toValidate.getClientId(context), new FacesMessage(message));
        }
    }
}
