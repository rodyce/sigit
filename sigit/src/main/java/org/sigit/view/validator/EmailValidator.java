package org.sigit.view.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("org.sigit.EmailValidator")
public class EmailValidator implements Validator {

    private static final String EMAIL_PATTERN = "^([a-zA-Z0-9_\\.\\-])" +
            "+\\@(([a-zA-Z0-9\\-])" +
            "+\\.)+([a-zA-Z0-9]{2,4})+$";
    
    private final Pattern pattern;
    
    public EmailValidator() {
        pattern = Pattern.compile(EMAIL_PATTERN);
    }
    
    @Override
    public void validate(FacesContext context, UIComponent component,
            Object value) throws ValidatorException {

        Matcher matcher = pattern.matcher(value.toString());
        if (!matcher.matches()) {
            FacesMessage msg = new FacesMessage("Email validation failed",
                    "Invalid Email format");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }        
    }
}
