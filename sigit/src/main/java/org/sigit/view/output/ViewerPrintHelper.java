package org.sigit.view.output;

import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;

import org.geotools.data.ows.CRSEnvelope;
import org.geotools.data.wms.request.GetMapRequest;
import org.sigit.logic.general.ConfigurationHelper;
import org.sigit.logic.general.GeneralHelper;
import org.sigit.logic.viewer.InteractiveViewerHelper;
import org.sigit.logic.viewer.toolbox.LayerHelper.ThemeLayerGroup;
import org.sigit.model.hnd.cadastre.HND_Layer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("viewerPrintHelper")
@Scope(value="sigit-conversation")
public class ViewerPrintHelper extends OutputHelperBase implements Serializable {
    private static final long serialVersionUID = 7614591876224510164L;
    
    private boolean printTitle = true;
    private boolean printScale = true;
    private boolean printLegend = true;
    
    private String title;
    private String defaultTitle;
    
    private PrintMeasure printExtent = new PrintMeasure();
    private PrintMeasure clientExtent = new PrintMeasure();
    private PrintMeasure pageMargins = new PrintMeasure(1.5, 1.5, 1.5, 1.5);
    private PrintTarget printTarget = PrintTarget.VIEWER_CENTER;
    
    private double scale;
    private long scaleNum;
    private long scaleDen;
    private String customScaleText;
    private ScaleOption scaleOption = ScaleOption.AUTOMATIC;


    @Autowired
    private InteractiveViewerHelper interactiveViewerHelper;
    
    @Autowired
    private GeneralHelper generalHelper;
    
    @Autowired
    private ConfigurationHelper configurationHelper;
    

    public String getWmsMapRequestUrl() {
        return getGMR().getFinalURL().toString();
    }
    
    public InputStream getImageStream() {
        return imageStream(getWmsMapRequestUrl());
    }
    
    public InputStream imageStream(String urlText) {
        URL url;
        try {
            url = new URL(urlText);
            return url.openStream();
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return null;
    }
    
    public Image getImage() {
        try {
            return ImageIO.read(getImageStream());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return null;
    }
    
    public Image image(String urlText) {
        try {
            Image img = ImageIO.read(imageStream(urlText));
            return img;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return null;
    }
    
    public boolean isPrintTitle() {
        return printTitle;
    }
    public void setPrintTitle(boolean printTitle) {
        this.printTitle = printTitle;
    }

    public boolean isPrintScale() {
        return printScale;
    }
    public void setPrintScale(boolean printScale) {
        this.printScale = printScale;
    }

    public boolean isPrintLegend() {
        return printLegend;
    }
    public void setPrintLegend(boolean printLegend) {
        this.printLegend = printLegend;
    }
    
    public boolean isDrawLegendSidebar() {
        return printLegend && interactiveViewerHelper.getLayerHelper().getVisibleLayerSmallThemeGroups().size() > 0;
    }

    public String getTitle() {
        if (title == null || title.trim().equals(""))
            return getDefaultTitle();
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getDefaultTitle() {
        if (defaultTitle == null) {
            defaultTitle = String.format("%s-%s",
                    configurationHelper.getJurisdictionCode(),
                    configurationHelper.getJurisdictionName());
        }
        return defaultTitle;
    }



    private PageSize pageSize = new LetterPageSize();
    public PageSize getPageSize() {
        return pageSize;
    }

    public PrintMeasure getPrintExtent() {
        return printExtent;
    }
    public PrintMeasure getClientExtent() {
        return clientExtent;
    }
    public PrintMeasure getPageMargins() {
        return pageMargins;
    }
    
    public PrintTarget getPrintTarget() {
        return printTarget;
    }
    public void setPrintTarget(PrintTarget printTarget) {
        this.printTarget = printTarget;
    }
    
        
    public String getCustomScaleText() {
        return customScaleText;
    }
    public void setCustomScaleText(String customScaleText) {
        this.customScaleText = customScaleText;
    }

    public ScaleOption getScaleOption() {
        return scaleOption;
    }
    public void setScaleOption(ScaleOption scaleOption) {
        this.scaleOption = scaleOption;
    }

    public String doPrint() {
        printExtent.setBottom(clientExtent.getBottom());
        printExtent.setLeft(clientExtent.getLeft());
        printExtent.setTop(clientExtent.getTop());
        printExtent.setRight(clientExtent.getRight());
        
        adjustExtentToImage(printExtent);
        
        return null;
    }
    public String showPrint() {
        //this method assumes that the function doPrint()
        //was called before
        return "/queries/reports/viewerMapPrintout.xhtml";
    }

    //////// LAYOUT UTILITY FUNCTIONS (in PTs) ///////
    public int getLayoutWidth() {
        return pageSize.getPtWidth() - cm2pt(pageMargins.left + pageMargins.right);
    }
    public int getLayoutHeight() {
        return pageSize.getPtHeight() - cm2pt(pageMargins.top + pageMargins.bottom);
    }
    
    public int getTitleWidth() {
        return printTitle ? getLayoutWidth() : 0;
    }
    public int getTitleHeight() {
        return printTitle ? (int) (72 * 1.0) : 0;
    }
    
    public int getImageWidth() {
        return getLayoutWidth() - getLegendWidth();
    }
    public int getImageHeight() {
        return getLayoutHeight() - getTitleHeight() - getNotesHeight();
    }
    
    public int getLegendWidth() {
        return isDrawLegendSidebar() ? (int) (72 * 2.5) : 0;
    }
    public int getLegendHeight() {
        return isDrawLegendSidebar() ? getImageHeight() : 0;
    }
    
    public int getNotesHeight() {
        return printScale ?  (int) (72 * 0.25) : 0;
    }
    public int getNotesWidth() {
        return printScale ? getLayoutWidth() : 0;
    }
    
    public int getLayoutNumColumns() {
        return isDrawLegendSidebar() ? 2 : 1;
    }
    public String getColumnWidths() {
        return "" + getImageWidth() + (isDrawLegendSidebar() ? " " + getLegendWidth() : "");
    }
    

    ////////UTILITY METHODS//////////////////
    private GetMapRequest getGMR() {
        GetMapRequest gmr = getWms().createGetMapRequest();
        
        for (ThemeLayerGroup tlg : interactiveViewerHelper.getLayerHelper().getVisibleLayerThemeGroups())
            for (HND_Layer layer : tlg.getLayers())
                gmr.addLayer(generalHelper.layerFullQualifiedName(layer), tlg.getStyleName());
        
        int width = 500; //asumiendo que son 10 pulgadas el ancho de papel...
        int height = (int) (width * printExtent.getHeight() / printExtent.getWidth());
        
        gmr.setDimensions(width, height);
        gmr.setTransparent(true);
        gmr.setBBox(new CRSEnvelope(generalHelper.getWorkingSRS(), printExtent.left,
                printExtent.bottom, printExtent.right, printExtent.top));
        gmr.setFormat("image/png");
        gmr.setSRS( generalHelper.getWorkingSRS() );
        
        return gmr;
    }
    
    public double getScaleFactor() {
        return ( pt2cm(getPageSize().getPtWidth()) / 100 ) / printExtent.getWidth();
    }
    
    private static final DecimalFormat df = new DecimalFormat("###,###.####");
    public String getScaleRatioText() {
        //return String.format("1:%s", df.format( 1 / scale ));
        return String.format("%s:%s", df.format(scaleNum), df.format(scaleDen));
    }
    
    private static int cm2pt(double cm) {
        return (int) (cm * (72.0 / 2.54));
    }
    private static double pt2cm(int pt) {
        return (double) (pt / (72.0 / 2.54));
    }
    
    
    private boolean adjustExtentToImage(PrintMeasure extent) {
        //This assumes image width is greater than height.
        //That is true in landscape orientation.
        double targetLength;
        if (extent.getWidth() > extent.getHeight()) {
            scale = (pt2cm(getImageHeight()) / 100) / extent.getHeight();
            targetLength = extent.getHeight() * getImageWidth() / getImageHeight();
            
            adjustWidth(extent, targetLength);
        } else {
            scale = (pt2cm(getImageWidth()) / 100) / extent.getWidth();
            targetLength = extent.getWidth() * getImageHeight() / getImageWidth();
            
            adjustHeight(extent, targetLength);
        }
        
        if (scaleOption == ScaleOption.CUSTOM) {
            if (customScaleText != null && customScaleText.trim().length() > 0) {
                customScaleText = customScaleText.replace(" ", "");
                
                int colonIdx = customScaleText.indexOf(':');
                try {
                    if (colonIdx != -1) {
                        String[] parts = customScaleText.split(":");
                        scaleNum = Integer.parseInt(parts[0]);
                        scaleDen = Integer.parseInt(parts[1]);
                    } else {
                        scaleNum = 1;
                        scaleDen = Integer.parseInt(customScaleText);
                    }
                } catch (NumberFormatException ex) {
                    FacesContext.getCurrentInstance().addMessage(
                            "",
                            new FacesMessage(
                                    FacesMessage.SEVERITY_WARN,
                                    "Valor de la escala personalizada invalido",
                                    ""
                            )
                    );
                    return false;
                }
                
                double targetScale = ((double) scaleNum) / scaleDen;
                double targetWidth = extent.getWidth() * scale / targetScale;
                double targetHeight = extent.getHeight() * scale / targetScale;
                
                adjustWidth(extent, targetWidth);
                adjustHeight(extent, targetHeight);
                
                scale = targetScale;
            } else {
                FacesContext.getCurrentInstance().addMessage(
                        "",
                        new FacesMessage(
                                FacesMessage.SEVERITY_WARN,
                                "Debe especificar la escala personalizada",
                                ""
                        )
                );
                
                return false;
            }
        } else {
            //automatic scale logic
            scaleNum = 1;
            scaleDen = Math.round(1 / scale);
            
            double targetScale = ((double) scaleNum) / scaleDen;
            double targetWidth = extent.getWidth() * scale / targetScale;
            double targetHeight = extent.getHeight() * scale / targetScale;
            
            adjustWidth(extent, targetWidth);
            adjustHeight(extent, targetHeight);
            
            scale = targetScale;
        }
        
        return true;
    }
    
    private static void adjustWidth(PrintMeasure extent, double targetWidth) {
        double centerX = extent.getCenterX();
        extent.setLeft(centerX - targetWidth / 2.0);
        extent.setRight(centerX + targetWidth / 2.0);
    }
    private static void adjustHeight(PrintMeasure extent, double targetHeight) {
        double centerY = extent.getCenterY();
        extent.setBottom(centerY - targetHeight / 2.0);
        extent.setTop(centerY + targetHeight / 2.0);
    }
    
    
    //////////// Utility classes /////////////
    public static enum PrintTarget {
        VIEWER_CENTER,
        SELECTED_ZONE
    }
    
    public static class PrintMeasure {
        private double left;
        private double bottom;
        private double right;
        private double top;
        
        
        public PrintMeasure() {
        }
        public PrintMeasure(double left, double bottom, double right, double top) {
            this.left = left;
            this.bottom = bottom;
            this.right = right;
            this.top = top;
        }
        
        public double getLeft() {
            return left;
        }
        public void setLeft(double left) {
            this.left = left;
        }
        
        public double getBottom() {
            return bottom;
        }
        public void setBottom(double bottom) {
            this.bottom = bottom;
        }
        
        public double getRight() {
            return right;
        }
        public void setRight(double right) {
            this.right = right;
        }
        
        public double getTop() {
            return top;
        }
        public void setTop(double top) {
            this.top = top;
        }
        
        public double getCenterX() {
            return (left + right) / 2.0;
        }
        public double getCenterY() {
            return (top + bottom) / 2.0;
        }
        
        public double getWidth() {
            return Math.abs( right - left );
        }
        public double getHeight() {
            return Math.abs( top - bottom );
        }
        
        public String getSpaceValues() {
            return String.format("%d %d %d %d",
                    cm2pt(left), cm2pt(bottom), cm2pt(right), cm2pt(top));
        }
    }
    
    public abstract static class PageSize {
        protected int ptWidth;
        protected int ptHeight;
        
        public PageSize(int ptWidth, int ptHeight) {
            this.ptWidth = ptWidth;
            this.ptHeight = ptHeight;
        }
        
        public int getPtWidth() {
            return ptWidth;
        }
        public int getPtHeight() {
            return ptHeight;
        }
        
        public String getSize() {
            return String.format("%d %d", (int) ptWidth, (int) ptHeight);
        }
        
        @Override
        public String toString() {
            return getSize();
        }
    }
    
    public static class LetterPageSize extends PageSize {
        private static final int WIDTH = 792;
        private static final int HEIGHT = 612;
        private static final String SIZE = "LETTER";
        
        public LetterPageSize() {
            super(WIDTH, HEIGHT);
        }
        public String getSize() {
            return SIZE;
        }
    }
    
    public static class CustomPageSize extends PageSize {
        public CustomPageSize() {
            super(LetterPageSize.WIDTH, LetterPageSize.HEIGHT);
        }
        public void setPtWidth(int ptWidth) {
            this.ptWidth = ptWidth;
        }
        public void setPtHeight(int ptHeight) {
            this.ptHeight = ptHeight;
        }
    }
    
    public static enum ScaleOption {
        AUTOMATIC,
        
        CUSTOM
    }
    
}
