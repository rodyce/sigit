package org.sigit.view.output;

import org.sigit.model.hnd.cadastre.HND_Parcel;

import java.io.Serializable;

import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

@Component("cadastralCertificateHelper")
@Scope(value="sigit-conversation")
public class CadastralCertificateHelper extends OutputHelperBase implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private HND_Parcel parcel;

    public HND_Parcel getParcel() {
        return parcel;
    }
    public void setParcel(HND_Parcel parcel) {
        this.parcel = parcel;
    }
}
