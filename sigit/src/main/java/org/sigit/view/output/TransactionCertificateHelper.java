package org.sigit.view.output;

import org.sigit.model.hnd.administrative.HND_Transaction;

import java.io.Serializable;

public abstract class TransactionCertificateHelper extends OutputHelperBase implements Serializable {
    private static final long serialVersionUID = 1L;

    public abstract HND_Transaction getTransaction();
    
    public String getTransactionCode() {
        if (generalHelper == null || getTransaction() == null) return "";
        
        return String.format("%s - %06d", configurationHelper.getJurisdictionCode(), getTransaction().getPresentationNo());
    }
}
