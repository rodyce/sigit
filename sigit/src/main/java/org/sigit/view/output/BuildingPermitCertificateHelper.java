package org.sigit.view.output;

import org.sigit.logic.queries.BuildingPermitQueryHelper;
import org.sigit.model.hnd.administrative.HND_BuildingPermit;
import org.sigit.model.hnd.administrative.HND_Transaction;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

@Component("buildingPermitCertificateHelper")
@Scope(value="sigit-conversation")
public class BuildingPermitCertificateHelper extends TransactionCertificateHelper implements Serializable {
    private static final long serialVersionUID = 1L;

    @Autowired
    private BuildingPermitQueryHelper buildingPermitQueryHelper;


    public HND_BuildingPermit getPermit() {
        return buildingPermitQueryHelper.getSelected();
    }
    public void setPermit(HND_BuildingPermit permit) {
        buildingPermitQueryHelper.setSelected(permit);
    }
    
    
    @Override
    public HND_Transaction getTransaction() {
        return getPermit();
    }
    
}
