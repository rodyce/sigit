package org.sigit.view.output;

import org.sigit.i18n.ResourceBundleHelper;
import org.sigit.logic.general.ConfigurationHelper;
import org.sigit.logic.general.GeneralHelper;
import org.sigit.model.commons.IParcel;
import org.sigit.model.commons.IProperty;
import org.sigit.model.commons.IRight;
import org.sigit.model.hnd.cadastre.HND_LandUse;
import org.sigit.util.GeneralUtils;
import org.sigit.view.output.CadastralRecordHelper.RRRData;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.geotools.data.wms.WebMapServer;
import org.geotools.ows.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class OutputHelperBase {
    private Calendar today;
    
    private final String[] months = new DateFormatSymbols().getMonths();

    @Autowired
    protected GeneralHelper generalHelper;
    
    @Autowired
    protected ConfigurationHelper configurationHelper;
    
    @Autowired
    protected ResourceBundleHelper resBundle;

    
    private String     capsUrl;
    private WebMapServer wms;
    
    
    
    public String spanishConvertNumberToWords(long number) {
        return GeneralUtils.spanishConvertNumberToWords(number);
    }
    public String spanishConvertNumberToWords(BigDecimal bd) {
        return GeneralUtils.spanishConvertNumberToWords(bd);
    }
    public String spanishConvertNumberToWords(BigDecimal bd, String intSuffix, String fractPrefix, String fractSuffix) {
        return GeneralUtils.spanishConvertNumberToWords(bd, intSuffix, fractPrefix, fractSuffix);
    }
    
    public Calendar getToday() {
        if (today == null) {
            today = Calendar.getInstance();
            today.setTime(new Date());
        }
        return today;
    }
    
    public int getTodaysDay() {
        return getToday().get(Calendar.DAY_OF_MONTH);
    }
    public String getTodaysMonth() {
        return months[getToday().get(Calendar.MONTH)];
    }
    public int getTodaysYear() {
        return getToday().get(Calendar.YEAR);
    }
    
    
    public String cadastralKey(IParcel parcel) {
        String ckey = parcel.getCadastralKey();
        return ckey != null && !ckey.trim().equals("") ? ckey : resBundle.loadMessage("txt.no_cadastral_key");
    }
    
    public String fiscalAppraisal(IParcel parcel) {
        BigDecimal bd = parcel.getFiscalAppraisal();
        return bd != null ? bd.toString() : resBundle.loadMessage("txt.no_value");
    }
    public String fiscalAppraisalWords(IParcel parcel) {
        BigDecimal bd = parcel.getFiscalAppraisal();
        return bd != null ? spanishConvertNumberToWords(bd, "lempira(s)", "con", "centavo(s)") : resBundle.loadMessage("txt.no_value");
    }
    public String documentedArea(IParcel parcel) {
        BigDecimal bd = parcel.getDocumentedArea();
        return bd != null ? bd.toString() : resBundle.loadMessage("txt.no_value");
    }
    
    public String rightsTextStr(IProperty property) {
        StringBuilder sb = new StringBuilder();
        for (RRRData rd : rightsTextList(property)) {
            sb.append(String.format("\"%s\" a favor de %s con una participación de %s",
                    rd.getRrrName(),
                    rd.getPartyName(),
                    rd.getShare() + (!rd.isLast() ? ", " : "")));
        }
        
        if (sb.length() == 0)
            sb.append(resBundle.loadMessage("txt.no_rights"));
        
        return sb.toString();
    }
    public String rightsTextStr(IParcel parcel) {
        return rightsTextStr(parcel.getProperty());
    }
    
    public String neighborHood(IParcel parcel) {
        String nhood = parcel.getNeighborhood();
        return nhood != null && !nhood.trim().equals("") ? nhood : resBundle.loadMessage("txt.no_neighborhood");
    }


    public List<RRRData> rightsTextList(IProperty property) {
        List<RRRData> rrrDataList = new ArrayList<RRRData>();
        for (IRight<?,?> right : property.getRights()) {
            rrrDataList.add(new RRRData(
                    resBundle.loadMessage(right.getType().name()),
                    right.getParty().getExtParty().getName(),
                    right.getShare().toString()));
            
        }
        if (rrrDataList.size() > 0)
            rrrDataList.get(rrrDataList.size() - 1).setLast();
        return rrrDataList;
    }
    public List<RRRData> rightsTextList(IParcel parcel) {
        return rightsTextList(parcel.getProperty());
    }
    
    public int min(int a, int b) {
        return a < b ? a : b;
    }
    public int max(int a, int b) {
        return a > b ? a : b;
    }
    
    
    public String landUsesText(HND_LandUse landUse, Collection<HND_LandUse> otherLandUses) {
        StringBuilder sb = new StringBuilder();
        List<HND_LandUse> lus = new ArrayList<HND_LandUse>();
        
        if (landUse != null)
            lus.add(landUse);
        if (otherLandUses != null)
            lus.addAll(otherLandUses);
        
        for (HND_LandUse lu : lus) {
            sb.append(lu.getName());
            sb.append(", ");
        }
        if (sb.length() >= 2)
            sb.setLength(sb.length() - 2);
        
        return sb.toString();
    }
    
    public String nullText(String text) {
        if (text == null || text.trim().equals(""))
            return resBundle.loadMessage("txt.unspecified");
        return text;
    }


    
    protected String getCapsUrl() {
        if (capsUrl == null) {
            capsUrl = generalHelper.getWmsCapabilitiesUrl111();
        }
        return capsUrl;
    }
    
    protected WebMapServer getWms() {
        if (wms == null) {
            try {
                wms = new WebMapServer(new URL(getCapsUrl()));
            } catch (ServiceException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return wms;
    }
}
