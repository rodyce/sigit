package org.sigit.view.output;

import org.sigit.model.hnd.administrative.HND_OperationPermit;
import org.sigit.model.hnd.administrative.HND_Transaction;
import org.sigit.logic.queries.OperationPermitQueryHelper;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

@Component("operationPermitCertificateHelper")
@Scope(value="sigit-conversation")
public class OperationPermitCertificateHelper extends TransactionCertificateHelper implements Serializable {
    private static final long serialVersionUID = 1L;

    @Autowired
    private OperationPermitQueryHelper operationPermitQueryHelper;

    
    public HND_OperationPermit getPermit() {
        return operationPermitQueryHelper.getSelected();
    }
    public void setPermit(HND_OperationPermit permit) {
        operationPermitQueryHelper.setSelected(permit);
    }
    
    public String getLandUsesText() {
        if (getPermit() != null)
            return landUsesText(getPermit().getLandUse(), getPermit().getOtherLandUses());
        
        return "-";
    }
    
    
    @Override
    public HND_Transaction getTransaction() {
        return getPermit();
    }
}
