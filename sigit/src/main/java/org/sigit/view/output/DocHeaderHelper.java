package org.sigit.view.output;

import org.sigit.logic.general.ConfigurationHelper;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

@Component("docHeaderHelper")
@Scope(value="sigit-conversation")
public class DocHeaderHelper extends OutputHelperBase implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Autowired
    private ConfigurationHelper configurationHelper;
    
    private Boolean validLogoUrl;
    private URL url;
    private java.awt.Image image;
    
    public boolean isValidLogoUrl() {
        if (validLogoUrl == null) {
            try {
                if (getUrl() != null) {
                    getUrl().getContent();
                    InputStream is = getUrl().openStream();
                    image = ImageIO.read( is );
                    is.close();
                    
                    validLogoUrl = true;
                }
            } catch (IOException e) {
                e.printStackTrace();
                validLogoUrl = false;
            }
        }
        return validLogoUrl != null ? validLogoUrl : false;
    }
    
    public String getMunicipalityLogoUrl() {
        return configurationHelper.getMunicipalityLogoUrl();
    }
    
    public java.awt.Image getImage() {
        if (image == null) {
            try {
                InputStream is = getUrl().openStream();
                image = ImageIO.read( is );
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return image;
    }
    
    private URL getUrl() {
        if (url == null){
            try {
                String strUrl = configurationHelper.getMunicipalityLogoUrl();
                if (strUrl != null) url = new URL(strUrl);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return url;
    }
}
