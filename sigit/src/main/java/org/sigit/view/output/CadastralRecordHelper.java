package org.sigit.view.output;

import org.sigit.dao.hnd.cadastre.HND_ParcelDAO;
import org.sigit.logic.queries.TransactionQueryHelper;
import org.sigit.model.commons.IParcel;
import org.sigit.model.hnd.administrative.HND_MunicipalTransaction;
import org.sigit.model.hnd.administrative.HND_SpatialZoneInTransaction;
import org.sigit.model.hnd.cadastre.HND_Parcel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Scope;

@Component("cadastralRecordHelper")
@Scope(value="sigit-conversation")
public class CadastralRecordHelper extends TransactionCertificateHelper implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Autowired
    private TransactionQueryHelper transactionQueryHelper;
    
    private List<IParcel> requestedParcels;

    
    @Override
    public HND_MunicipalTransaction getTransaction() {
        return transactionQueryHelper.getSelected();
    }
    
    public List<IParcel> getRequestedParcels() {
        if (requestedParcels == null) {
            requestedParcels = new ArrayList<IParcel>();
            if (getTransaction() != null)
                for (HND_SpatialZoneInTransaction szit : getTransaction().getSpatialZoneInTransactions())
                    if (szit.getNeighborLevel() == 0 && szit.getSpatialZone() instanceof IParcel) //level 0 is requested
                        requestedParcels.add((IParcel) szit.getSpatialZone());
        }
        return requestedParcels;
    }
    
    @Transactional
    public List<IParcel> neighborParcels(IParcel parcel) {
        List<IParcel> parcelList = new ArrayList<IParcel>();
        
        if (parcel instanceof HND_Parcel)
            for (IParcel p : HND_ParcelDAO.loadNeighborsByHNDParcel((HND_Parcel) parcel)) {
                parcelList.add(p);
            }
        
        return parcelList;
    }
    
    
    public static class RRRData {
        private String rrrName;
        private String partyName;
        private String share;
        private boolean last = false;
        
        public RRRData(String rrrName, String partyName, String share) {
            this.rrrName = rrrName;
            this.partyName = partyName;
            this.share = share;
        }

        public String getRrrName() {
            return rrrName;
        }
        public String getPartyName() {
            return partyName;
        }
        public String getShare() {
            return share;
        }
        public boolean isLast() {
            return last;
        }

        public void setLast() {
            this.last = true;
        }
    }
}
