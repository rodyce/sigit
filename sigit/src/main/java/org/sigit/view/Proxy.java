package org.sigit.view;

import org.sigit.logic.security.IAuthenticator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("proxy")
@Scope("session")
public class Proxy {
    @Autowired
    private IAuthenticator authenticator;
    
    public void testWait(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    public String getHelloWorld() {
        return "Hello World from Spring!!!";
    }
}
