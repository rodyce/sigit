package org.sigit.services.rest;

import java.io.IOException;
import java.util.UUID;

import org.sigit.interop.ws.commons.GeneralResponseMsg;
import org.sigit.interop.ws.transaction.types.v1.AdministrativeSourceList;
import org.sigit.interop.ws.transaction.types.v1.TransactionList;
import org.sigit.interop.ws.transaction.types.v1.TransactionResponse;
import org.sigit.services.MunicipalTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController("municipalTransactionController")
@ResponseBody
@RequestMapping("/v1/municipalTransaction")
public class MunicipalTransactionController extends AbstractControllerBase {
    
    public MunicipalTransactionController() {
        txnCodeToHttpStatusMap.put("SIGIT-TXN-01", HttpStatus.BAD_REQUEST);
        txnCodeToHttpStatusMap.put("SIGIT-TXN-02", HttpStatus.UNPROCESSABLE_ENTITY);
        txnCodeToHttpStatusMap.put("SIGIT-TXN-03", HttpStatus.NOT_FOUND);
        txnCodeToHttpStatusMap.put("SIGIT-TXN-04", HttpStatus.NOT_FOUND);
        txnCodeToHttpStatusMap.put("SIGIT-TXN-05", HttpStatus.UNPROCESSABLE_ENTITY);
        txnCodeToHttpStatusMap.put("SIGIT-TXN-06", HttpStatus.UNPROCESSABLE_ENTITY);
        txnCodeToHttpStatusMap.put("SIGIT-TXN-07", HttpStatus.UNPROCESSABLE_ENTITY);
        txnCodeToHttpStatusMap.put("SIGIT-TXN-08", HttpStatus.BAD_REQUEST);
    }

    @Autowired
    private MunicipalTransactionService trxService;
    
    @Autowired
    private GeneralDocsController docsController;

    @RequestMapping(method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody TransactionList getPendingTransactions(
            @RequestParam(required=false) boolean externalApprovalOnly) throws GeneralResponseMsg {
        return trxService.getPendingTransactions(externalApprovalOnly);
    }

    @RequestMapping(value="/{presentationId}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody JsonNode getTransactionData(
            @PathVariable String presentationId) throws GeneralResponseMsg, JsonProcessingException, IOException {
        String jsonInString = xmlToJson(trxService.getTransactionData(presentationId));
        JsonNode node = new ObjectMapper().readTree(jsonInString);
        return node;
    }

    @RequestMapping(value="/{presentationId}/administrativeSources", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody AdministrativeSourceList getTransactionSources(
            @PathVariable String presentationId) throws GeneralResponseMsg {
        return trxService.getTransactionSources(presentationId);
    }

    @RequestMapping(value="/{presentationId}/administrativeSources/{sourceId}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody AdministrativeSourceList.AdministrativeSource getTransactionSource(
            @PathVariable String presentationId,
            @PathVariable String sourceId) throws GeneralResponseMsg {
        return trxService.getTransactionSource(presentationId, sourceId);
    }

    @RequestMapping(value="/{presentationId}/administrativeSources/{sourceId}/data", method=RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> getTransactionSourceData(
            @PathVariable String presentationId,
            @PathVariable String sourceId) throws GeneralResponseMsg {
        UUID extArchiveId = trxService.getTransactionSourceArchiveId(presentationId, sourceId);
        return docsController.getDocument(extArchiveId.toString(), false);
    }

    @RequestMapping(value="/{presentationId}/entityName/{entityName}", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody JsonNode receiveTransaction(
            @PathVariable String presentationId,
            @PathVariable String entityName) throws GeneralResponseMsg, JsonProcessingException, IOException {
        String txnXml = trxService.receiveTransaction(presentationId, entityName);
        String jsonInString = xmlToJson(txnXml);
        JsonNode node = new ObjectMapper().readTree(jsonInString);
        return node;
    }

    @RequestMapping(value="/next", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody JsonNode getNextTransaction(
            @RequestParam(required=false) boolean idOnly) throws GeneralResponseMsg, JsonProcessingException, IOException {
        String txnXml = trxService.getNextTransaction(idOnly);
        String jsonInString = xmlToJson(txnXml);
        JsonNode node = new ObjectMapper().readTree(jsonInString);
        return node;
    }

    @RequestMapping(value="/response", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    public void processTransactionResponse(@RequestBody TransactionResponse transactionResponse) throws GeneralResponseMsg {
        trxService.processTransactionResponse(transactionResponse);
    }

    @RequestMapping(value="/xmlresponse", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    public void processTransactionResponseXML(@RequestBody String transactionResponseXML) throws GeneralResponseMsg {
        trxService.processTransactionResponseXML(transactionResponseXML);
    }    
}
