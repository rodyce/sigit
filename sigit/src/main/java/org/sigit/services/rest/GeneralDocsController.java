package org.sigit.services.rest;

import java.util.List;
import java.util.UUID;

import org.sigit.logic.workflow.ArchiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController("generalDocsController")
@ResponseBody
@RequestMapping("/docs")
public class GeneralDocsController extends AbstractControllerBase {

    @Autowired
    private ArchiveRepository archiveRepository;


    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<String> getDocuments() {
        return archiveRepository.getArchiveIds();
    }

    @RequestMapping(value="/{uuidHex}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> getDocument(@PathVariable String uuidHex,
            @RequestParam(required=false) boolean forceDownload) {
        BodyBuilder bb = ResponseEntity.ok();
        
        UUID uuid = UUID.fromString(uuidHex);
        if (forceDownload) {
            bb.header("Content-Disposition", "attachment;filename=\"" + archiveRepository.getArchiveName(uuid) + "\"");
        }
        
        return bb.contentType(MediaType.parseMediaType(
                archiveRepository.getAppropriateMIME(
                        archiveRepository.getArchiveName(uuid), forceDownload)))
                .body(archiveRepository.getArchiveData(uuid));
    }

}
