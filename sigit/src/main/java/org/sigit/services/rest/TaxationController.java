package org.sigit.services.rest;

import java.util.Date;

import org.sigit.interop.ws.commons.GeneralResponseMsg;
import org.sigit.interop.ws.taxation.types.v1.TaxationInfo;
import org.sigit.interop.ws.taxation.types.v1.TaxationInfoList;
import org.sigit.services.TaxationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController("taxationController")
@ResponseBody
@RequestMapping("/v1/taxationInfo")
public class TaxationController extends AbstractControllerBase {
    @Autowired
    private TaxationService taxationService;
    
    public TaxationController() {
        txnCodeToHttpStatusMap.put("SIGIT-TAX-01", HttpStatus.NOT_FOUND);
    }
    
    @RequestMapping(method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody TaxationInfoList getTaxationInfoUpdatedAfter(
            @RequestParam(required=false) Date beginDate) throws GeneralResponseMsg {
        return taxationService.getTaxationInfoUpdatedAfter(beginDate);
    }

    @RequestMapping(value="/{parcelNationalIdentifier}",
            method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody TaxationInfo getTaxationInfo(@PathVariable String parcelNationalIdentifier) throws GeneralResponseMsg {
        return taxationService.getTaxationInfo(parcelNationalIdentifier);
    }

    @RequestMapping(value="/{parcelNationalIdentifier}",
            method=RequestMethod.PUT, produces=MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody TaxationInfo updateParcelTaxationInfo(
            @PathVariable String parcelNationalIdentifier,
            @RequestBody TaxationInfo taxationInfo) throws GeneralResponseMsg {
        return taxationService.updateParcelTaxationInfo(parcelNationalIdentifier,
                taxationInfo.getCommercialAppraisal(),
                taxationInfo.getFiscalAppraisal(),
                taxationInfo.getTaxationBalanceDue());
    }
}
