package org.sigit.services.rest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.sigit.interop.ws.commons.GeneralResponseMsg;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class AbstractControllerBase extends HandlerInterceptorAdapter {
    protected final Map<String, HttpStatus> txnCodeToHttpStatusMap = new HashMap<>();

    public HttpStatus getHttpStatusFromCode(String code) {
        HttpStatus httpStatus = txnCodeToHttpStatusMap.get(code);
        return httpStatus != null ? httpStatus : HttpStatus.INTERNAL_SERVER_ERROR;
    }
    
    @ExceptionHandler(GeneralResponseMsg.class)
    public ResponseEntity<JsonNode> errorResponse(Exception ex) {
        GeneralResponseMsg respEx = (GeneralResponseMsg) ex;

        JSONObject jsonObj = new JSONObject(respEx);
        jsonObj.remove("localizedMessage");
        jsonObj.remove("stackTrace");
        jsonObj.remove("suppressed");

        ObjectMapper mapper = new ObjectMapper(); 
        String json = jsonObj.toString();
        try {
            JsonNode jsonNode = mapper.readTree(json);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            return new ResponseEntity<JsonNode>(jsonNode, headers,
                    getHttpStatusFromCode(respEx.getFaultInfo().getCode()));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return null;
    }

    @ResponseStatus(value=HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoSuchElementException.class)
    public void notFound() {
    }
    
    @ResponseStatus(value=HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value={NumberFormatException.class, IllegalArgumentException.class})
    public void invalidNumericFormat() {
    }

    @ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value={IllegalStateException.class})
    public void illegalState() {
    }

    protected String xmlToJson(String xml) throws GeneralResponseMsg {
        try {
            JSONObject jsonObj = XML.toJSONObject(xml);
            
            //output JSON with a indent factor of 4
            return jsonObj.toString(4);
        } catch (JSONException e) {
            throw new GeneralResponseMsg("Error processing data to JSON", e);
        }
    }
}
