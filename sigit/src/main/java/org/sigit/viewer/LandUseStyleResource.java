package org.sigit.viewer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.xml.transform.TransformerException;

import org.geotools.factory.CommonFactoryFinder;
import org.geotools.styling.FeatureTypeStyle;
import org.geotools.styling.PolygonSymbolizer;
import org.geotools.styling.Rule;
import org.geotools.styling.SLDTransformer;
import org.geotools.styling.Style;
import org.geotools.styling.StyleBuilder;
import org.geotools.styling.StyleFactory;
import org.geotools.styling.StyledLayerDescriptor;
import org.geotools.styling.UserLayer;
import org.opengis.filter.expression.Expression;
import org.sigit.dao.hnd.cadastre.HND_LandUseDAO;
import org.sigit.model.hnd.cadastre.HND_LandUse;
import org.sigit.model.hnd.special.HND_StyleInfo;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller("landUseStyleResource")
@Scope("singleton")
public class LandUseStyleResource implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final String RESOURCE_PATH = "/landusestyle";
    
    private String sldXml;
    private Random random = new Random();
    
    @RequestMapping(value=RESOURCE_PATH, produces=MediaType.TEXT_XML_VALUE)
    public @ResponseBody String getLandUseStyleResource(@RequestParam(required=false) boolean refresh) {
        return loadLandUseStyleResource(refresh);
    }
    
    private String loadLandUseStyleResource(boolean refresh) {
        if (refresh) doRefresh();
        
        return getSldXml();
    }

    @Transactional
    public String getSldXml() {
        if (sldXml == null) {
            synchronized (this) {
                if (sldXml == null) {
                    sldXml = generateSLDXml();
                }
            }
        }
        return sldXml;
    }
    

    public synchronized void doRefresh() {
        sldXml = null;
    }
    
    private String generateSLDXml() {
        String xml = "";
        
        StyleFactory styleFactory = CommonFactoryFinder.getStyleFactory(null);
        StyledLayerDescriptor sld = styleFactory.createStyledLayerDescriptor();
        sld.setName("SIGIT LandUse SLD");
        
        UserLayer layer = styleFactory.createUserLayer();
        layer.setName("Land Uses");
        
        Style style = styleFactory.createStyle();
        
        style.getDescription().setTitle("SIGIT LandUse Style");
        
        layer.userStyles().add(style);
        
        sld.layers().add(layer);

        
        addToStyle(style, styleFactory);
        
        
        SLDTransformer sldTransformer = new SLDTransformer();
        try {
            xml = sldTransformer.transform(sld);
        } catch (TransformerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return xml;
    }

    private void addToStyle(Style style, StyleFactory sf) {
        StyleBuilder sb = new StyleBuilder(sf);

        List<Rule> ruleList = new ArrayList<Rule>();
        for (HND_LandUse lu : HND_LandUseDAO.loadLandUses()) {
            HND_StyleInfo styleInfo = lu.getStyleInfo();
            PolygonSymbolizer polSym = sb.createPolygonSymbolizer(
                    sb.createStroke( getColorExpr(styleInfo.getStrokeColor(), sb), getFloatExpr(styleInfo.getStrokeWidth(), sb)),
                    sb.createFill( getColorExpr(styleInfo.getFillColor(), sb), getFloatExpr(styleInfo.getFillOpacity(), sb)));
            
            Rule rule = sb.createRule(polSym);
            rule.setName(lu.getCompleteCode());
            rule.setTitle(lu.getFullName());
            
            ruleList.add(rule);
        }
        Rule[] rules = new Rule[ruleList.size()];
        ruleList.toArray(rules);
        FeatureTypeStyle featureTypeStyle = sb.createFeatureTypeStyle("Feature", rules);
        style.featureTypeStyles().add(featureTypeStyle);
    }
    

    private Expression getColorExpr(String color, StyleBuilder sb) {
        return color != null ? sb.getFilterFactory().literal(color) : sb.colorExpression(new java.awt.Color(random.nextInt()));
    }
    private Expression getFloatExpr(Float value, StyleBuilder sb) {
        if (value == null) value = 0.5f;
        return sb.getFilterFactory().literal(value); 
    }
}
