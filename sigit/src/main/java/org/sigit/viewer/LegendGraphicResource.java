package org.sigit.viewer;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;

import org.geotools.data.wms.WebMapServer;
import org.geotools.data.wms.request.GetLegendGraphicRequest;
import org.geotools.data.wms.response.GetLegendGraphicResponse;
import org.geotools.ows.ServiceException;
import org.sigit.commons.di.CtxComponent;
import org.sigit.logic.general.GeneralHelper;
import org.sigit.logic.viewer.InteractiveViewerHelper.StyleNames;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@Scope("singleton")
public class LegendGraphicResource implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final String RESOURCE_PATH = "/legendgraphic";

    private transient WebMapServer wms;
    private String capsUrl;
    private GeneralHelper generalHelper;

    @RequestMapping(value=RESOURCE_PATH, method=RequestMethod.GET, produces=MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody ResponseEntity<InputStreamResource> getLegendGraphicResource(
            @RequestParam(required=true)    String layer,
            @RequestParam(required=true)    String styleName,
            @RequestParam(required=false)    String ruleName,
            @RequestParam(required=false)    boolean refresh) throws Exception {
        if (getWms() != null) {
            GetLegendGraphicRequest legendGraphicRequest = getWms().createGetLegendGraphicRequest();
            legendGraphicRequest.setLayer(layer);
            
            if ( styleName.equals( StyleNames.SIGIT_LANDUSE ) ) {
                String sldReq = getGeneralHelper().getRequestResourcePath() + LandUseStyleResource.RESOURCE_PATH;
                if (refresh)
                    sldReq += "?refresh=true";
                legendGraphicRequest.setSLD(sldReq);
            } else
                legendGraphicRequest.setStyle(styleName);
            
            if (ruleName != null)
                legendGraphicRequest.setRule(ruleName);
            
            legendGraphicRequest.setFormat(MediaType.IMAGE_JPEG_VALUE);
            try {
                GetLegendGraphicResponse legendGraphicResponse = getWms().issueRequest(legendGraphicRequest);
                
                InputStream is = legendGraphicResponse.getInputStream();
                
                return ResponseEntity.ok()
                        .contentType(MediaType.IMAGE_PNG)
                        .body(new InputStreamResource(is));
            } catch (ServiceException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                throw e;
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                throw e;
            }
        }
        
        //TODO: Handle exception case better
        throw new Exception("WMS is down");
    }


    private String getCapsUrl() {
        if (capsUrl == null) {
            synchronized(this) {
                if (capsUrl == null) {
                    capsUrl = getGeneralHelper().getWmsCapabilitiesUrl111();
                }
            }
        }
        return capsUrl;
    }
    
    private WebMapServer getWms() {
        if (wms == null) {
            synchronized(this) {
                if (wms == null) {
                    try {
                        wms = new WebMapServer(new URL(getCapsUrl()));
                    } catch (ServiceException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (MalformedURLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }
        return wms;
    }
    
    private GeneralHelper getGeneralHelper() {
        if (generalHelper == null) {
            synchronized(this) {
                if (generalHelper == null) {
                    generalHelper = (GeneralHelper) CtxComponent.getInstance("generalHelper");
                }
            }
        }
        return generalHelper;
    }
}
