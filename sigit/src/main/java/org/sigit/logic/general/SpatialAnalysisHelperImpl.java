package org.sigit.logic.general;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.sigit.dao.hnd.cadastre.HND_SpatialZoneDAO;
import org.sigit.model.hnd.administrative.HND_PermitRuleGroup;
import org.sigit.model.hnd.administrative.HND_RuleOperatorType;
import org.sigit.model.hnd.administrative.HND_SpatialRule;
import org.sigit.model.hnd.cadastre.HND_LandUse;
import org.sigit.model.hnd.cadastre.HND_SpatialZone;
import org.sigit.model.ladm.spatialunit.LA_Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.vividsolutions.jts.geom.Geometry;


@Component(SpatialAnalysisHelper.NAME)
@Scope(value="sigit-conversation")
public class SpatialAnalysisHelperImpl implements Serializable, SpatialAnalysisHelper {
    private static final long serialVersionUID = 1L;
    
    private HND_SpatialZone spatialZone;
    
    private Double distance;
    
    private boolean doDistance = true;
    private boolean doOverlaps = true;
    private boolean doWithin = true;
    private boolean doTouches = true;
    private boolean doCrosses = true;
    private boolean doContains = true;
    
    private boolean showDoSpatialAnalysisForm = true;
    private String spatialAnalysisPanelName = "spatialAnalysisPanel";
    
    
    private List<SpatialAnalysisResult> distanceResults;
    private List<SpatialAnalysisResult> overlapsResults;
    private List<SpatialAnalysisResult> withinResults;
    private List<SpatialAnalysisResult> touchesResults;
    private List<SpatialAnalysisResult> crossesResults;
    private List<SpatialAnalysisResult> containsResults;

    private HND_PermitRuleGroup ruleGroup;
    private LA_Level operand1Level;
    private HND_LandUse operand1LandUse;
    private LA_Level operand2Level;
    private HND_LandUse operand2LandUse;
    
    private Set<HND_LandUse> lhsAdditionalLUSet;
    private Set<HND_LandUse> rhsAdditionalLUSet;

    
    private Map<HND_SpatialRule, List<SpatialAnalysisResult>> spatialRuleToAnalysisMap; 
    private boolean analysisComplete = false;

    @Autowired
    private GeneralHelper generalHelper;// = (GeneralHelper) CtxComponent.getInstance("generalHelper");
    
    
    public HND_SpatialZone getSpatialZone() {
        return spatialZone;
    }
    @Override
    public void setSpatialZone(HND_SpatialZone spatialZone) {
        this.spatialZone = spatialZone;
    }
    
    @Override
    public Double getDistance() {
        if (distance == null && ruleGroup != null) {
            double maxDistance = 0.0;
            Double comparisonValue;
            for (HND_SpatialRule sr : ruleGroup.getSpatialRules()) {
                if (sr.getRuleOperator() == HND_RuleOperatorType.DISTANCE) {
                    comparisonValue = sr.getComparisonParameterValue(); 
                    if (comparisonValue != null && comparisonValue > maxDistance)
                        maxDistance = comparisonValue;
                }
            }
            distance = maxDistance;
        }
        return distance;
    }
    @Override
    public void setDistance(Double distance) {
        this.distance = distance;
    }

    @Override
    public HND_PermitRuleGroup getRuleGroup() {
        return ruleGroup;
    }
    @Override
    public void setRuleGroup(HND_PermitRuleGroup ruleGroup) {
        this.ruleGroup = ruleGroup;
    }
    
    @Override
    public boolean isDoDistance() {
        return doDistance;
    }
    @Override
    public void setDoDistance(boolean doDistance) {
        this.doDistance = doDistance;
    }
    
    @Override
    public boolean isDoOverlaps() {
        return doOverlaps;
    }
    @Override
    public void setDoOverlaps(boolean doOverlaps) {
        this.doOverlaps = doOverlaps;
    }
    
    @Override
    public boolean isDoWithin() {
        return doWithin;
    }
    @Override
    public void setDoWithin(boolean doWithin) {
        this.doWithin = doWithin;
    }
    
    @Override
    public boolean isDoTouches() {
        return doTouches;
    }
    @Override
    public void setDoTouches(boolean doTouches) {
        this.doTouches = doTouches;
    }
    
    @Override
    public boolean isDoCrosses() {
        return doCrosses;
    }
    @Override
    public void setDoCrosses(boolean doCrosses) {
        this.doCrosses = doCrosses;
    }
    
    @Override
    public boolean isDoContains() {
        return doContains;
    }
    @Override
    public void setDoContains(boolean doContains) {
        this.doContains = doContains;
    }
    
    @Override
    public boolean isShowDoSpatialAnalysisForm() {
        return showDoSpatialAnalysisForm;
    }
    @Override
    public void setShowDoSpatialAnalysisForm(boolean showDoSpatialAnalysisForm) {
        this.showDoSpatialAnalysisForm = showDoSpatialAnalysisForm;
    }

    @Override
    public String getSpatialAnalysisPanelName() {
        return spatialAnalysisPanelName;
    }
    @Override
    public void setSpatialAnalysisPanelName(String spatialAnalysisPanelName) {
        this.spatialAnalysisPanelName = spatialAnalysisPanelName;
    }
    
    @Override
    public List<SpatialAnalysisResult> getDistanceResults() {
        return distanceResults;
    }
    @Override
    public List<SpatialAnalysisResult> getOverlapsResults() {
        return overlapsResults;
    }
    
    @Override
    public List<SpatialAnalysisResult> getWithinResults() {
        return withinResults;
    }
    @Override
    public List<SpatialAnalysisResult> getTouchesResults() {
        return touchesResults;
    }
    
    @Override
    public List<SpatialAnalysisResult> getCrossesResults() {
        return crossesResults;
    }
    @Override
    public List<SpatialAnalysisResult> getContainsResults() {
        return containsResults;
    }
    
    @Transactional
    private void doOperation(Geometry shapeAnalysis, List<SpatialAnalysisResult> sarList, HND_RuleOperatorType rot) {
        for (HND_SpatialZone spatialZone : HND_SpatialZoneDAO.loadSpatialZonesByGeom(shapeAnalysis, rot))
            sarList.add(new SpatialAnalysisResultImpl(this.spatialZone, spatialZone));
        
        determineAffectingRules(sarList, rot);
    }

    private boolean ruleAffects(HND_SpatialRule spatialRule,
            SpatialAnalysisResult sar, HND_RuleOperatorType rot) {
        //Check operand type
        if (spatialRule.getRuleOperator() != rot)
            return false;
        
        HND_SpatialZone lhsSZ = sar.getLhsSpatialZone();
        Set<HND_LandUse> lhsLUSet = new HashSet<HND_LandUse>();
        if (lhsSZ.getLandUse() != null)
            lhsLUSet.add(lhsSZ.getLandUse());
        if (lhsSZ.getOtherLandUses() != null)
            lhsLUSet.addAll(lhsSZ.getOtherLandUses());
        if (lhsAdditionalLUSet != null)
            lhsLUSet.addAll(lhsAdditionalLUSet);
        
        HND_SpatialZone rhsSZ = sar.getRhsSpatialZone();
        Set<HND_LandUse> rhsLUSet = new HashSet<HND_LandUse>();
        if (rhsSZ.getLandUse() != null)
            rhsLUSet.add(rhsSZ.getLandUse());
        if (rhsSZ.getOtherLandUses() != null)
            rhsLUSet.addAll(rhsSZ.getOtherLandUses());
        if (rhsAdditionalLUSet != null)
            rhsLUSet.addAll(rhsAdditionalLUSet);
        
        //Check for First Operand (LHS)
        if (spatialRule.getLevelOperand1() != null && spatialRule.getLevelOperand1() != lhsSZ.getLevel())
            return false;
        if ( spatialRule.getLandUseOperand1() != null && !lhsLUSet.contains(spatialRule.getLandUseOperand1()) )
            return false;
        
        //Check for Second Operand (RHS) in the spatial analysis result list
        if (spatialRule.getLevelOperand2() != null && spatialRule.getLevelOperand2() != rhsSZ.getLevel())
            return false;
        if ( spatialRule.getLandUseOperand2() != null && !rhsLUSet.contains(spatialRule.getLandUseOperand2()) )
            return false;
        
        return true;
    }
    private void determineAffectingRules(List<SpatialAnalysisResult> sarList, HND_RuleOperatorType rot) {
        if (ruleGroup != null && spatialZone != null) {
            Set<HND_SpatialRule> spatialRuleSet = ruleGroup.getSpatialRules();
            
            for (SpatialAnalysisResult sar : sarList) {
                for (HND_SpatialRule spatialRule : spatialRuleSet) {
                    if (ruleAffects(spatialRule, sar, rot)) {
                        sar.getSpatialRuleSet().add(spatialRule);
                        
                        List<SpatialAnalysisResult> ruleSarList;
                        if (!getSpatialRuleToAnalysisMap().containsKey(spatialRule)) {
                            ruleSarList = new ArrayList<SpatialAnalysisResult>();
                            ruleSarList.add(sar);
                            getSpatialRuleToAnalysisMap().put(spatialRule, ruleSarList);
                        }
                        else {
                            ruleSarList = getSpatialRuleToAnalysisMap().get(spatialRule);
                            ruleSarList.add(sar);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void doNewSpatialAnalysis() {
        if (spatialZone == null || spatialZone.getShape() == null) return;
        
        Geometry shapeAnalysis = spatialZone.getShape();
        Geometry zoneAux;
        
        spatialRuleToAnalysisMap = null;
        
        if (doDistance) {
            distanceResults = new ArrayList<SpatialAnalysisResult>();
            zoneAux = shapeAnalysis;
            if (getDistance() != null) {
                zoneAux = shapeAnalysis.buffer(getDistance());
                zoneAux.setSRID(shapeAnalysis.getSRID());
            }
            doOperation(zoneAux, distanceResults, HND_RuleOperatorType.DISTANCE);
        }
        if (doOverlaps) {
            overlapsResults = new ArrayList<SpatialAnalysisResult>();
            doOperation(shapeAnalysis, overlapsResults, HND_RuleOperatorType.OVERLAP);
        }
        if (doWithin) {
            withinResults = new ArrayList<SpatialAnalysisResult>();
            doOperation(shapeAnalysis, withinResults, HND_RuleOperatorType.WITHIN);
        }
        if (doTouches) {
            touchesResults = new ArrayList<SpatialAnalysisResult>();
            doOperation(shapeAnalysis, touchesResults, HND_RuleOperatorType.TOUCH);
        }
        if (doCrosses) {
            crossesResults = new ArrayList<SpatialAnalysisResult>();
            doOperation(shapeAnalysis, crossesResults, HND_RuleOperatorType.CROSS);
        }
        if (doContains) {
            containsResults = new ArrayList<SpatialAnalysisResult>();
            doOperation(shapeAnalysis, containsResults, HND_RuleOperatorType.CONTAINS);
        }
        analysisComplete = true;
    }

    

    @Override
    public LA_Level getOperand1Level() {
        return operand1Level;
    }
    @Override
    public void setOperand1Level(LA_Level operand1Level) {
        this.operand1Level = operand1Level;
    }
    
    @Override
    public HND_LandUse getOperand1LandUse() {
        return operand1LandUse;
    }
    @Override
    public void setOperand1LandUse(HND_LandUse operand1LandUse) {
        this.operand1LandUse = operand1LandUse;
    }
    
    
    @Override
    public LA_Level getOperand2Level() {
        return operand2Level;
    }
    @Override
    public void setOperand2Level(LA_Level operand2Level) {
        this.operand2Level = operand2Level;
    }

    @Override
    public HND_LandUse getOperand2LandUse() {
        return operand2LandUse;
    }
    @Override
    public void setOperand2LandUse(HND_LandUse operand2LandUse) {
        this.operand2LandUse = operand2LandUse;
    }
    
    @Override
    public boolean isAnalysisComplete() {
        return analysisComplete;
    }
    @Override
    public void setAnalysisComplete(boolean analysisComplete) {
        this.analysisComplete = analysisComplete;
    }
    
    @Override
    public Map<HND_SpatialRule, List<SpatialAnalysisHelper.SpatialAnalysisResult>> getSpatialRuleToAnalysisMap() {
        if (spatialRuleToAnalysisMap == null) {
            spatialRuleToAnalysisMap = new HashMap<HND_SpatialRule, List<SpatialAnalysisResult>>();
        }
        return spatialRuleToAnalysisMap;
    }
    
    @Override
    public List<SpatialAnalysisHelper.SpatialAnalysisResult> sarListFromRule(HND_SpatialRule spatialRule) {
        List<SpatialAnalysisResult> sarList;
        if (getSpatialRuleToAnalysisMap().containsKey(spatialRule))
            sarList = getSpatialRuleToAnalysisMap().get(spatialRule);
        else
            sarList = new ArrayList<SpatialAnalysisResult>();
        return sarList;
    }

    @Override
    public List<HND_SpatialRule> getRuleGroupRules() {
        if (ruleGroup != null) {
            List<HND_SpatialRule> spatialRuleList = new ArrayList<HND_SpatialRule>();
            for (HND_SpatialRule rule : ruleGroup.getSpatialRules())
                spatialRuleList.add(rule);
            return spatialRuleList;
        }
        return null;
    }
    
    
    @Override
    public Set<HND_LandUse> getLhsAdditionalLUSet() {
        return lhsAdditionalLUSet;
    }
    @Override
    public void setLhsAdditionalLUSet(Set<HND_LandUse> lhsAdditionalLUSet) {
        this.lhsAdditionalLUSet = lhsAdditionalLUSet;
    }
    @Override
    public Set<HND_LandUse> getRhsAdditionalLUSet() {
        return rhsAdditionalLUSet;
    }
    @Override
    public void setRhsAdditionalLUSet(Set<HND_LandUse> rhsAdditionalLUSet) {
        this.rhsAdditionalLUSet = rhsAdditionalLUSet;
    }




    public class SpatialAnalysisResultImpl implements SpatialAnalysisHelper.SpatialAnalysisResult, Serializable {
        private static final long serialVersionUID = 1L;
        
        private HND_SpatialZone lhsSpatialZone;
        private HND_SpatialZone rhsSpatialZone;
        private Set<HND_SpatialRule> spatialRuleSet;
        private String analystObservation;
        private String zoneName;
        private String locationInCountry;
        private String landUse;
        private String proposedLandUse;
        private String levelName;


        public SpatialAnalysisResultImpl(HND_SpatialZone lhsSpatialZone, HND_SpatialZone rhsSpatialZone) {
            this.lhsSpatialZone = lhsSpatialZone;
            this.rhsSpatialZone = rhsSpatialZone;
            this.zoneName = generalHelper.formatZoneNameText(rhsSpatialZone);
            this.locationInCountry = generalHelper.formatNullText(rhsSpatialZone.getLocationInCountry());
            this.landUse = generalHelper.formatLandUseText(rhsSpatialZone.getLandUse());
            this.proposedLandUse = generalHelper.formatLandUseText(rhsSpatialZone.getProposedLandUse());
            this.levelName = generalHelper.formatLevelText(rhsSpatialZone.getLevel());
        }


        @Override
        public HND_SpatialZone getLhsSpatialZone() {
            return lhsSpatialZone;
        }

        @Override
        public HND_SpatialZone getRhsSpatialZone() {
            return rhsSpatialZone;
        }

        @Override
        public Set<HND_SpatialRule> getSpatialRuleSet() {
            if (spatialRuleSet == null) {
                spatialRuleSet = new HashSet<HND_SpatialRule>();
            }
            return spatialRuleSet;
        }

        @Override
        public HND_SpatialRule[] getSpatialRuleArray() {
            HND_SpatialRule[] spatialRules = new HND_SpatialRule[getSpatialRuleSet().size()];
            
            int i = 0;
            for (HND_SpatialRule sr : getSpatialRuleSet())
                spatialRules[i++] = sr;
            
            return spatialRules;
        }
        
        @Override
        public double getDistance() {
            if (lhsSpatialZone != null && rhsSpatialZone != null)
                return lhsSpatialZone.getShape().distance(rhsSpatialZone.getShape());
            
            return 0.0;
        }
        

        @Override
        public String getAnalystObservation() {
            return analystObservation;
        }

        @Override
        public void setAnalystObservation(String analystObservation) {
            this.analystObservation = analystObservation;
        }

        @Override
        public UUID getSuID() {
            return rhsSpatialZone.getSuID();
        }
        @Override
        public String getZoneName() {
            return zoneName;
        }
        @Override
        public String getLocationInCountry() {
            return locationInCountry;
        }
        @Override
        public String getLandUse() {
            return landUse;
        }
        @Override
        public String getProposedLandUse() {
            return proposedLandUse;
        }
        
        @Override
        public String getLevelName() {
            return levelName;
        }
    }
}
