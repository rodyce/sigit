package org.sigit.logic.general;

import org.sigit.model.hnd.administrative.HND_PermitRuleGroup;
import org.sigit.model.hnd.administrative.HND_SpatialRule;
import org.sigit.model.hnd.cadastre.HND_LandUse;
import org.sigit.model.hnd.cadastre.HND_SpatialZone;
import org.sigit.model.ladm.spatialunit.LA_Level;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;


public interface SpatialAnalysisHelper {
    String NAME = "spatialAnalysisHelper";
    

    HND_SpatialZone getSpatialZone();

    void setSpatialZone(HND_SpatialZone spatialZone);

    Double getDistance();

    void setDistance(Double distance);

    HND_PermitRuleGroup getRuleGroup();

    void setRuleGroup(HND_PermitRuleGroup ruleGroup);

    boolean isDoDistance();

    void setDoDistance(boolean doDistance);

    boolean isDoOverlaps();

    void setDoOverlaps(boolean doOverlaps);

    boolean isDoWithin();

    void setDoWithin(boolean doWithin);

    boolean isDoTouches();

    void setDoTouches(boolean doTouches);

    boolean isDoCrosses();

    void setDoCrosses(boolean doCrosses);

    boolean isDoContains();

    void setDoContains(boolean doContains);

    boolean isShowDoSpatialAnalysisForm();

    void setShowDoSpatialAnalysisForm(boolean showDoSpatialAnalysisForm);

    String getSpatialAnalysisPanelName();

    void setSpatialAnalysisPanelName(String spatialAnalysisPanelName);

    List<SpatialAnalysisResult> getDistanceResults();

    List<SpatialAnalysisResult> getOverlapsResults();

    List<SpatialAnalysisResult> getWithinResults();

    List<SpatialAnalysisResult> getTouchesResults();

    List<SpatialAnalysisResult> getCrossesResults();

    List<SpatialAnalysisResult> getContainsResults();

    void doNewSpatialAnalysis();

    LA_Level getOperand1Level();

    void setOperand1Level(LA_Level operand1Level);

    HND_LandUse getOperand1LandUse();

    void setOperand1LandUse(HND_LandUse operand1LandUse);

    LA_Level getOperand2Level();

    void setOperand2Level(LA_Level operand2Level);

    HND_LandUse getOperand2LandUse();

    void setOperand2LandUse(HND_LandUse operand2LandUse);

    boolean isAnalysisComplete();

    void setAnalysisComplete(boolean analysisComplete);

    Map<HND_SpatialRule, List<SpatialAnalysisResult>> getSpatialRuleToAnalysisMap();

    List<SpatialAnalysisResult> sarListFromRule(HND_SpatialRule spatialRule);

    List<HND_SpatialRule> getRuleGroupRules();
    
    Set<HND_LandUse> getLhsAdditionalLUSet();

    void setLhsAdditionalLUSet(Set<HND_LandUse> lhsAdditionalLUSet);

    Set<HND_LandUse> getRhsAdditionalLUSet();

    void setRhsAdditionalLUSet(Set<HND_LandUse> rhsAdditionalLUSet);

    public static interface SpatialAnalysisResult {

        HND_SpatialZone getLhsSpatialZone();

        HND_SpatialZone getRhsSpatialZone();

        Set<HND_SpatialRule> getSpatialRuleSet();

        HND_SpatialRule[] getSpatialRuleArray();

        double getDistance();

        String getAnalystObservation();

        void setAnalystObservation(String analystObservation);

        UUID getSuID();

        String getZoneName();

        String getLocationInCountry();

        String getLandUse();
        
        String getProposedLandUse();

        String getLevelName();

    }

}