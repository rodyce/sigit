package org.sigit.logic.general;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.el.ValueExpression;
import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringEscapeUtils;
import org.deegree.protocol.wms.client.WMSClient111;
import org.geotools.data.DataUtilities;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.feature.FeatureCollections;
import org.geotools.feature.SchemaException;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.gml.producer.FeatureTransformer;
import org.geotools.referencing.CRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.sigit.dao.SigitDAO;
import org.sigit.dao.hnd.cadastre.HND_LayerDAO;
import org.sigit.dao.hnd.cadastre.HND_ParcelDAO;
import org.sigit.dao.hnd.special.SystemConfigurationDAO;
import org.sigit.i18n.ResourceBundleHelper;
import org.sigit.logic.viewer.MapHelper;
import org.sigit.model.commons.IParcel;
import org.sigit.model.commons.ISpatialZone;
import org.sigit.model.hnd.administrative.HND_PermitRuleGroup;
import org.sigit.model.hnd.administrative.HND_SpatialRule;
import org.sigit.model.hnd.cadastre.HND_LandUse;
import org.sigit.model.hnd.cadastre.HND_Layer;
import org.sigit.model.hnd.special.SystemConfiguration;
import org.sigit.model.ladm.spatialunit.LA_Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.io.WKTWriter;

@Component("generalHelper")
@Scope(value="session")
public class GeneralHelper implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private static final Logger logger = LoggerFactory.getLogger(GeneralHelper.class);
    
    private List<HND_Layer> layerList;
    
    
    @Autowired
    private ResourceBundleHelper resBundle;
    
    @Autowired
    private HttpServletRequest request;
    
    
    private String requestRootPath;
    private String requestRootMapServer;
    private String contextPath;
    private String resourceRelativePath;
    private String requestResourcePath;
    private String openLayersPath;
    private String openLayersImgPath;
    private String openLayersThemeDefaultImgPath;
    private String openLayersJS;
    private String wmsUrl;
    private String wmsCapabilitiesUrl111;
    private boolean boolState;
    private boolean isStyleRefreshed = false;

    private String workingSRS;
    private String workingNamespace;
    private String primaryWMSLayer;

    private final int maxUploadedFileSize = 15_728_640; // 15MB

    // Internal requests to the REST API and WMS server are done through this URL.
    // TODO: Consider separating SIGIT from the WMS server.
    private final String INTERNAL_REQUEST_ROOT_PATH = "http://localhost:8080";


    @PostConstruct
    void init() {
        requestRootPath = String.format("http://%s:%d", request.getServerName(), request.getServerPort());
        requestRootMapServer = INTERNAL_REQUEST_ROOT_PATH;
        contextPath = request.getContextPath();
        resourceRelativePath = contextPath + "/services";
        // For querying the REST API internally within the application.
        requestResourcePath = String.format("%s%s", requestRootPath, getResourceRelativePath());

        // For external clients access (i.e.: From the browser).
        openLayersPath = String.format("%s%s/resources/openlayers", getRequestRootPath(), getContextPath());
        openLayersImgPath = String.format("%s/img", openLayersPath);
        openLayersThemeDefaultImgPath = String.format("%s/theme/default/img", openLayersPath);
        openLayersJS = "OpenLayers.js";
        wmsUrl = String.format("%s/geoserver/wms", getRequestRootPath());

        // For querying the WMS capabilities internally within the application.
        wmsCapabilitiesUrl111 = String.format("%s/geoserver/wms?service=wms&version=1.1.1&request=GetCapabilities", requestRootMapServer);
    }

    public boolean isMapServerAlive() {
        String urlText = getWmsCapabilitiesUrl111();
        try {
            URL url = new URL(urlText);
            try {
                WMSClient111 wmsClient = new WMSClient111(url);
                String systemId = wmsClient.getSystemId();
                if (systemId != null) return true;
            } catch (Exception e) {
                logger.debug("Exception thrown when calling map server at URL: " + url.toString(), e);
            }
        } catch (MalformedURLException e) {
            logger.debug("Malformed URL contacting map server: " + urlText);
        }
        
        return false;
    }

    
    @Transactional
    public String getWorkingSRS() {
        if (workingSRS == null) {
            SystemConfiguration sc = SystemConfigurationDAO.loadSystemConfiguration();
            workingSRS = String.format("%s:%d", "EPSG", sc.getSrid());
        }
        return workingSRS;
    }
    
    
    @Transactional
    public String getWorkingNamespace() {
        if (workingNamespace == null) {
            SystemConfiguration sc = SystemConfigurationDAO.loadSystemConfiguration();
            workingNamespace = sc.getWorkingNamespace();
        }
        return workingNamespace;
    }
    
    
    public String getRasterLayerName() {
        return String.format("%s:%s", getWorkingNamespace(), "orthophoto");
    }
    
    
    @Transactional
    public String getPrimaryWMSLayer() {
        if (primaryWMSLayer == null) {
            try {
                primaryWMSLayer = String.format("%s:%s", getWorkingNamespace(), "sigit_instance_extent");
                WMSClient111 wmsClient = new WMSClient111(new URL(getWmsCapabilitiesUrl111()));
                if ( !wmsClient.hasLayer(primaryWMSLayer) ) {
                    throw new IllegalStateException("Primary layer does not exist: " + primaryWMSLayer);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return primaryWMSLayer;
    }
    
    
    @Transactional
    public String getTopographicTxnsWMSLayerName() {
        return String.format("%s:%s", getWorkingNamespace(), "topographic_transaction");
    }
    
    
    public String getRequestRootPath() {
        return requestRootPath;
    }

    
    public String getRequestRootMapServer() {
        return requestRootMapServer;
    }
    
    
    public String getContextPath() {
        return contextPath;
    }
    
    
    public String getResourceRelativePath() {
        return resourceRelativePath;
    }

    
    public String getRequestResourcePath() {
        return requestResourcePath;
    }

    
    public String getOpenLayersPath() {
        return openLayersPath;
    }
    
    
    public String getOpenLayersImgPath() {
        return openLayersImgPath;
    }
    
    
    public String getOpenLayersThemeDefaultImgPath() {
        return openLayersThemeDefaultImgPath;
    }

    
    public String getOpenLayersJS() {
        return openLayersJS;
    }
    
    
    public String getWmsCapabilitiesUrl111() {
        return wmsCapabilitiesUrl111;
    }
    
    
    public String getWmsUrl() {
        return wmsUrl;
    }
    
    
    public String formatText(String fmt, Object... args) {
        for (int i = 0; i < args.length; i++) {
            Object obj = args[i];
            if (obj != null)
                fmt = fmt.replaceAll("\\{" + i + "\\}", obj.toString());
        }
        return fmt;
    }
    
    
    public String formatNullText(String text) {
        return text != null ? text : "-";
    }
    
    
    public String formatEmptyText(Object value) {
        if (value == null || value.equals(""))
            return resBundle.loadMessage("global.empty");
        
        return value.toString();
    }
    
    
    public boolean isParcelInstance(ISpatialZone spatialZone) {
        return spatialZone instanceof IParcel;
    }

    
    public String formatZoneNameText(ISpatialZone spatialZone) {
        return getZoneUsefulName(spatialZone);
    }

    
    public String formatLevelText(LA_Level level) {
        return level != null ? formatNullText(level.getName()) : "-";
    }

    
    public String formatLandUseText(HND_LandUse landUse) {
        return landUse != null ? formatNullText(landUse.getFullName()) : "-";
    }

    
    public String landUseName(HND_LandUse lu) {
        if (lu == null) return "";
        
        return String.format("%s:%s - %s",
                lu.getDomain().getName(),
                lu.getCompleteCode(),
                lu.getName()).replaceAll("'", "\\'");
    }

    
    public String getZoneUsefulName(ISpatialZone spatialZone) {
        String retval = "";
        
        if (spatialZone != null) {
            if (spatialZone instanceof IParcel) {
                IParcel parcel = (IParcel) spatialZone;
                String cadastralKey = parcel.getCadastralKey();
                String municipalKey = parcel.getMunicipalKey();
                String fieldTab = parcel.getFieldTab();
                
                if (cadastralKey != null) retval = cadastralKey;
                else if (municipalKey != null) retval = municipalKey;
                else if (fieldTab != null) retval = fieldTab;
                else retval = "id: " + String.valueOf(parcel.getSuID());
            } else {
                retval = spatialZone.getZoneName();
            }
        }
        return retval;
    }
    
    
    public String spatialRuleText(HND_SpatialRule sr) {
        return spatialRuleText(sr, false);
    }
    
    
    public String spatialRuleText(HND_SpatialRule sr, boolean includeDescription) {
        if (sr == null) return "-";
        
        String op = "";
        switch (sr.getAction()) {
        case ASTERISK:
            op = "*";
            break;
        case PLUS:
            op = "+";
            break;
        case MINUS:
            op = "-";
            break;
        }

        return String.format("%s (%s)", sr.getCode(), op)
                + (includeDescription ? ": " + sr.getDescription() : "");
    }
    
    
    
    public String spatialRuleDetailsText(HND_SpatialRule sr) {
        if (sr == null) return "-";
        
        ResourceBundleHelper resBundle = new ResourceBundleHelper();
        
        return String.format("%s - %s. {%s, %s} %s {%s, %s}: %s",
                sr.getCode(),
                sr.getDescription() != null ? sr.getDescription() : "", 
                sr.getLandUseOperand1() != null ? resBundle.loadMessage(sr.getLandUseOperand1().getName()) : "-",
                sr.getLevelOperand1() != null ? resBundle.loadMessage(sr.getLevelOperand1().getName()) : "-",
                resBundle.loadMessage(sr.getRuleOperator().name()),
                sr.getLandUseOperand2() != null ? resBundle.loadMessage(sr.getLandUseOperand2().getName()) : "-",
                sr.getLevelOperand2() != null ? resBundle.loadMessage(sr.getLevelOperand2().getName()) : "-",
                resBundle.loadMessage(sr.getAction().name()));
    }
    
    
    public String formatRuleGroupText(HND_PermitRuleGroup ruleGroup) {
        if (ruleGroup == null) return "-";
        
        StringBuilder sb = new StringBuilder(ruleGroup.getName());
        if (ruleGroup.getDescription() != null && !ruleGroup.getDescription().trim().equals(""))
            sb.append(String.format(" - %s", ruleGroup.getDescription()));
        
        return sb.toString();
    }

    //used to execute EL expression methods denoted by "elMethod" and do action
    
    public String executeELMethod(String elMethod, String action) {
        Application app = FacesContext.getCurrentInstance().getApplication();
        ExpressionFactory exprFactory = app.getExpressionFactory();
        ELContext elCtx = FacesContext.getCurrentInstance().getELContext();
        
        //third parameter is set to null meaning that we do not care for the
        //result type... and no expected parameters for fourth parameter,
        //however we can not use null for this argument
        //createMethodExpression(ELContext context, String expression, Class<?> expectedReturnType, Class<?>[] expectedParamTypes) 
        MethodExpression me = exprFactory.createMethodExpression(elCtx, "#{" + elMethod + "}", null, new Class[] {});
        me.invoke(elCtx, null);
        
        
        return action;
    }
    
    
    public String setValueExpr(String valueExpr, Object value, String action) {
        Application app = FacesContext.getCurrentInstance().getApplication();
        ExpressionFactory exprFactory = app.getExpressionFactory();
        ELContext elCtx = FacesContext.getCurrentInstance().getELContext();
        
        //createValueExpression(ELContext context, String expression, Class<?> expectedType) 
        ValueExpression ve = exprFactory.createValueExpression(elCtx, String.format("#{%s}", valueExpr), value.getClass());
        ve.setValue(elCtx, value);
        
        return action;
    }
    
    //used to retrieve parcel's WKT to render features in OpenLayers
    
    @Transactional
    public String getParcelWKT(UUID parcelId) {
        IParcel parcel = HND_ParcelDAO.loadParcelByID(parcelId);
        WKTWriter wktWriter = new WKTWriter();
        
        return wktWriter.write(parcel.getShape());
    }
    
    
    @Transactional
    public List<HND_Layer> getLayerList() {
        if (layerList == null) {
            layerList = HND_LayerDAO.loadLayers(true);
        }
        return layerList;
    }

    
    public String drawLayerJS(String mapControlName, HND_Layer layer, int layerIndex, boolean isLayerVisible, String styleName) {
        StringBuffer sb = new StringBuffer();
        sb.append(String.format(
                "var wms = new OpenLayers.Layer.WMS(" +
                "    '%s'," +
                "    '%s'," +
                "    {layers: '%s', transparent: true, format: 'image/png'}," +
                "    {projection: '%s'});" +
                "wms.id = '%s';" +
                "wms.setVisibility(%b);" +
                "%s.addLayer(wms);" +
                "%s.setLayerIndex(wms, %d);",
                
                layer.getName(),
                getWmsUrl(),
                layerFullQualifiedName(layer),
                getWorkingSRS(),
                layerFullQualifiedName(layer),
                
                isLayerVisible,
                
                mapControlName,
                mapControlName,
                layerIndex
        ));

        if (styleName != null)
            sb.append(String.format( "wms.params['STYLES'] = '%s';", styleName ));
        
        return sb.toString();
    }
    
    
    public String drawAllLayersJS(String mapControlName) {
        StringBuffer sb = new StringBuffer();
        for (HND_Layer layer : getLayerList()) {
            sb.append( drawLayerJS(mapControlName, layer, MapHelper.getLayerZOrder(layer.getWmsLayerName()), true, null) );
        }
        return sb.toString();
    }
    
    
    public double[] calcZoomEnvelope(List<Geometry> envelopeList) {
        double x1 = Double.MAX_VALUE;
        double y1 = Double.MAX_VALUE;
        double x2 = Double.MIN_VALUE;
        double y2 = Double.MIN_VALUE;
        
        for (Geometry envelope : envelopeList) {
            for (Coordinate coord : envelope.getCoordinates()) {
                if (coord.x < x1) x1 = coord.x;
                if (coord.y < y1) y1 = coord.y;
                if (coord.x > x2) x2 = coord.x;
                if (coord.y > y2) y2 = coord.y;
            }
        }
        
        double dx = Math.abs(x2 - x1);
        double dy = Math.abs(y2 - y1);
        double D = dx > dy ? dx : dy;
        
        x1 -= D * 0.15;
        y1 -= D * 0.15;
        x2 += D * 0.15;
        y2 += D * 0.15;
        
        
        return new double[] {x1, y1, x2, y2};
    }
    
    
    public <SZ extends ISpatialZone> double[] calcSZsZoomEnvelope(List<SZ> szList) {
        return calcZoomEnvelope(calcEnvelopeFromSZList(szList));
    }
    
    
    public <SZ extends ISpatialZone> List<Geometry> calcEnvelopeFromSZList(List<SZ> szList) {
        List<Geometry> geomList = new ArrayList<Geometry>();
        for (SZ sz : szList)
            geomList.add(sz.getShape());
        
        return geomList;
    }

    
    
    public <SZ extends ISpatialZone> String getSpatialZonesGML(List<SZ> spatialZoneList, int neighborLevel) {
        if (spatialZoneList == null) return "";
        
        String retval = "";

        FeatureTransformer transform = new FeatureTransformer();
        transform.setEncoding(Charset.defaultCharset());
        transform.setIndentation(4);
        transform.setGmlPrefixing(true);
        
        try {
            SimpleFeatureType TYPE;
            TYPE = DataUtilities.createType("Polygon", "geom:Polygon,label:String,detailed_label:String,name:String,neighborLevel:Integer");
            String prefix = "sigit";//(String) TYPE.getUserData().get("prefix");
            String namespace = "http://localhost";//TYPE.getName().getNamespaceURI();
            transform.getFeatureTypeNamespaces().declareDefaultNamespace(prefix, namespace);
            transform.addSchemaLocation(prefix, namespace);
            
            String srsName = CRS.toSRS(TYPE.getCoordinateReferenceSystem());
            if (srsName != null)
                transform.setSrsName(srsName);
            
            SimpleFeatureCollection collection = FeatureCollections.newCollection("internal");
            for (ISpatialZone spatialZone : spatialZoneList) {
                SimpleFeature sf;
                sf = SimpleFeatureBuilder.build(TYPE,
                        new Object[] { (Polygon) spatialZone.getShape(), "name1" },
                        String.valueOf(spatialZone.getSuID()));
                sf.setAttribute("label", getSZUsefulLabel(spatialZone, false));
                sf.setAttribute("detailed_label", getSZUsefulLabel(spatialZone, true));
                sf.setAttribute("name", getSZUsefulName(spatialZone));
                sf.setAttribute("neighborLevel", neighborLevel);
                collection.add(sf);
                ByteArrayOutputStream baos = new ByteArrayOutputStream(16834);
                
                transform.setCollectionBounding(true);
                transform.transform(collection, baos);
                
                retval = baos.toString();
            }
        }
        catch (SchemaException se) {
            se.printStackTrace();
            return null;
        }
        catch (TransformerException te) {
            te.printStackTrace();
            return null;
        }

        return retval;
    }
    
    
    
    public ResourceBundleHelper getResBundle() {
        if (resBundle == null) {
            resBundle = new ResourceBundleHelper();
        }
        return resBundle;
    }
    
    
    public String getDateTimePattern() {
        return "dd/MM/yyyy h:mm a";
    }
    
    
    public String getTimeZone() {
        return "GMT-06:00";
    }
    
    public String escapeJS(String txt) {
        return StringEscapeUtils.escapeJavaScript(txt);
    }
    
    public boolean areLandUsesCompatible(HND_LandUse landUse1, HND_LandUse landUse2) {
        HND_LandUse pLandUse1 = landUse1;
        HND_LandUse pLandUse2 = landUse2;
        
        if (pLandUse1 != null && pLandUse2 != null) {
            do {
                if (pLandUse1.getId().equals(pLandUse2.getId()) || pLandUse1.getCompatibleLandUses().contains(pLandUse2))
                    return true;
                else if (pLandUse2.getParent() != null)
                    pLandUse2 = pLandUse2.getParent();
                else if (pLandUse1.getParent() != null) {
                    pLandUse1 = pLandUse1.getParent();
                    pLandUse2 = landUse2;
                }
                else //if (pLandUse1.getParent() == null && pLandUse2.getParent() == null)
                    break;
            } while (true);
        }
        
        return false;
    }
    
    public String layerFullQualifiedName(HND_Layer layer) {
        String namespace = layer.getWmsNamespace();
        if (namespace == null || namespace.equals("") || namespace.equalsIgnoreCase("sigit"))
        {
            namespace = getWorkingNamespace();
        }
        
        return String.format("%s:%s", namespace, layer.getWmsLayerName());
    }
    
    
    @Transactional
    public SystemConfiguration getSystemConfiguration() {
        SystemConfiguration sc = SystemConfigurationDAO.loadSystemConfiguration();
        SigitDAO.evict(sc);
        return sc;
    }
    
    
    public boolean isBoolState() {
        return boolState;
    }
    public void setBoolState(boolean boolState) {
        this.boolState = boolState;
    }
    
    public boolean isStyleRefreshed() {
        return isStyleRefreshed;
    }
    public void setStyleRefreshed(boolean isStyleRefreshed) {
        this.isStyleRefreshed = isStyleRefreshed;
    }

    public int getMaxUploadedFileSize() {
        return maxUploadedFileSize;
    }

    public void simulateDelay() {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private String getSZUsefulLabel(ISpatialZone sz, boolean detailed) {
        if (sz == null) return "";
        
        String label = "x";
        
        if (sz instanceof IParcel) {
            IParcel p = (IParcel) sz;

            if (detailed)
                label = String.format("%s - %s",
                        p.getMunicipalKey() != null ? p.getMunicipalKey() : p.getFieldTab(),
                        p.getCadastralKey() != null ? p.getCadastralKey() : getResBundle().loadMessage("dataentry.no_cadastralkey"));
            else
                label = p.getCadastralKey() != null ? p.getCadastralKey()
                        : (p.getMunicipalKey() != null ? p.getMunicipalKey()
                                : (p.getFieldTab() != null ? p.getFieldTab().toString() : "x"));
        }
        else {
            label = getSZUsefulName(sz);
        }
        
        return label;
    }
    
    private String getSZUsefulName(ISpatialZone sz) {
        if (sz == null) return "";
        
        return sz.getZoneName() != null && !sz.getZoneName().trim().equals("") ? sz.getZoneName() : "(sin nombre)";
    }
}
