package org.sigit.logic.general;

import java.io.Serializable;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component("testHelper")
@Scope(value="session")
public class TestHelper implements Serializable {
    private static final long serialVersionUID = 1L;

    @Autowired
    private HttpSession session;

    public void waitInMillis(long millis) throws InterruptedException {
        Thread.sleep(millis);
    }
    
    public void raiseException(String msg) {
        throw new RuntimeException(msg);
    }
    
    public void expireSession() {
        SecurityContextHolder.getContext().setAuthentication(null);
        session.invalidate();
    }
}

