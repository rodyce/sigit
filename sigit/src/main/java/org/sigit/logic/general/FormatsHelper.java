package org.sigit.logic.general;

import java.io.Serializable;

import org.sigit.model.hnd.cadastre.HND_LandUse;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("formatsHelper")
@Scope(value="singleton")
public class FormatsHelper implements Serializable {
    private static final long serialVersionUID = 1L;

    public String getFormatLandUseText() {
        return "dummy";
    }
    
    public String formatLandUseText(HND_LandUse landUse) {
        return landUse != null ? formatNullText(landUse.getFullName()) : "-";
    }

    public String formatNullText(String text) {
        return text != null ? text : "-";
    }
}
