package org.sigit.logic.general;

import org.sigit.dao.hnd.administrative.HND_PermitRuleGroupDAO;
import org.sigit.dao.hnd.cadastre.HND_SpatialZoneDAO;
import org.sigit.dao.hnd.special.SystemConfigurationDAO;
import org.sigit.i18n.ResourceBundleHelper;
import org.sigit.model.hnd.administrative.HND_PermitRuleGroup;
import org.sigit.model.hnd.administrative.HND_PermitType;
import org.sigit.model.hnd.cadastre.HND_SpatialZone;
import org.sigit.model.hnd.special.SystemConfiguration;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Scope;

import com.vividsolutions.jts.geom.Geometry;

@Component("systemConfigurationHelper")
@Scope(value="sigit-conversation")
public class SystemConfigurationHelper implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final String UNKNOWN = "UNKNOWN";

    private SystemConfiguration systemConfig;
    private boolean ableToEditProjection;
    private List<HND_PermitRuleGroup> buildingPermitRuleGroups;
    private List<HND_PermitRuleGroup> operationPermitRuleGroups;
    private Boolean mapServerAlive;
    
    @Autowired
    private ResourceBundleHelper resBundle;
    
    @Autowired
    private GeneralHelper generalHelper;

    
    @PostConstruct
    void init() {
        HND_SpatialZone sz = HND_SpatialZoneDAO.loadSpatialZoneWithNonNullGeom();
        ableToEditProjection = sz == null;
        if (!ableToEditProjection) {
            Geometry geom = sz.getShape();
            int srid = geom.getSRID();
            
            SystemConfiguration sc = getSystemConfig();
            sc.setSrid(srid);
            if (sc.getMunicipalityName() == null)
                sc.setMunicipalityName(getResBundle().loadMessage(UNKNOWN).toUpperCase());
            if (sc.getWorkingNamespace() == null)
                sc.setWorkingNamespace(getResBundle().loadMessage(UNKNOWN).toUpperCase());
            
            SystemConfigurationDAO.save(sc);
        }
    }

    @Transactional
    public SystemConfiguration getSystemConfig() {
        if (systemConfig == null) {
            systemConfig = SystemConfigurationDAO.loadSystemConfiguration();
            if (systemConfig == null)
                systemConfig = new SystemConfiguration();
        }
        return systemConfig;
    }
    public void setSystemConfig(SystemConfiguration systemConfig) {
        this.systemConfig = systemConfig;
    }
    
    
    public boolean isAbleToEditProjection() {
        return ableToEditProjection;
    }
    

    @Transactional
    public List<HND_PermitRuleGroup> getBuildingPermitRuleGroups() {
        if (buildingPermitRuleGroups == null) {
            buildingPermitRuleGroups = HND_PermitRuleGroupDAO.loadPermitRuleGroupsByType(HND_PermitType.BUILDING_PERMIT);
        }
        return buildingPermitRuleGroups;
    }
    public void setBuildingPermitRuleGroups(
            List<HND_PermitRuleGroup> buildingPermitRuleGroups) {
        this.buildingPermitRuleGroups = buildingPermitRuleGroups;
    }

    @Transactional
    public List<HND_PermitRuleGroup> getOperationPermitRuleGroups() {
        if (operationPermitRuleGroups == null) {
            operationPermitRuleGroups = HND_PermitRuleGroupDAO.loadPermitRuleGroupsByType(HND_PermitType.OPERATION_PERMIT);
        }
        return operationPermitRuleGroups;
    }
    public void setOperationPermitRuleGroups(
            List<HND_PermitRuleGroup> operationPermitRuleGroups) {
        this.operationPermitRuleGroups = operationPermitRuleGroups;
    }


    public Boolean getMapServerAlive() {
        if (mapServerAlive == null) {
            mapServerAlive = generalHelper.isMapServerAlive();
        }
        return mapServerAlive;
    }
    
    
    public String refreshMapServerAliveFlag() {
        mapServerAlive = null;
        FacesContext.getCurrentInstance().addMessage(
                "",
                new FacesMessage(
                        FacesMessage.SEVERITY_INFO,
                        "Se ha refrescado el estado del servidor de mapas",
                        ""
                )
        );
        return null;
    }


    @Transactional
    public String save() {
        SystemConfigurationDAO.save(getSystemConfig());
        FacesContext.getCurrentInstance().addMessage(
                "",
                new FacesMessage(
                        FacesMessage.SEVERITY_INFO,
                        "Los cambios se han aplicado correctamente",
                        ""
                )
        );
        return null;
    }


    protected ResourceBundleHelper getResBundle() {
        if (resBundle == null) {
            resBundle = generalHelper.getResBundle();
        }
        return resBundle;
    }
}
