package org.sigit.logic.general;

import java.io.Serializable;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Scope;
import org.sigit.dao.hnd.special.SystemConfigurationDAO;
import org.sigit.model.hnd.special.SystemConfiguration;

@Component("configurationHelper")
@Scope(value="singleton")
public class ConfigurationHelper implements Serializable {
    private static final long serialVersionUID = 1L;

    private volatile String jurisdictionCode;
    private volatile String jurisdictionName;
    private volatile String municipalityLogoUrl;
    private volatile String webSiteURL;
    private volatile String userGuideURL;
    private volatile String supportURL;

    @Transactional
    public String getJurisdictionCode() {
        if (jurisdictionCode == null) {
            SystemConfiguration sc = SystemConfigurationDAO.loadSystemConfiguration();
            jurisdictionCode = sc.getWorkingNamespace();
        }
        return jurisdictionCode;
    }
    public void setJurisdictionCode(String jurisdictionCode) {
        this.jurisdictionCode = jurisdictionCode;
    }

    @Transactional
    public String getJurisdictionName() {
        if (jurisdictionName == null) {
            SystemConfiguration sc = SystemConfigurationDAO.loadSystemConfiguration();
            jurisdictionName = sc.getMunicipalityName();
        }
        return jurisdictionName;
    }
    public void setJurisdictionName(String jurisdictionName) {
        this.jurisdictionName = jurisdictionName;
    }
    
    @Transactional
    public String getMunicipalityLogoUrl() {
        if (municipalityLogoUrl == null) {
            SystemConfiguration sc = SystemConfigurationDAO.loadSystemConfiguration();
            municipalityLogoUrl = sc.getLogoURL();
        }
        return municipalityLogoUrl;
    }
    public void setMunicipalityLogoUrl(String municipalityLogoUrl) {
        this.municipalityLogoUrl = municipalityLogoUrl;
    }
    
    @Transactional
    public String getWebSiteURL() {
        if (webSiteURL == null) {
            SystemConfiguration sc = SystemConfigurationDAO.loadSystemConfiguration();
            webSiteURL = sc.getWebSiteURL();
            if (webSiteURL == null) webSiteURL = "";
        }
        return webSiteURL;
    }
    public void setWebSiteURL(String webSiteURL) {
        this.webSiteURL = webSiteURL;
    }
    
    @Transactional
    public String getUserGuideURL() {
        if (userGuideURL == null) {
            SystemConfiguration sc = SystemConfigurationDAO.loadSystemConfiguration();
            userGuideURL = sc.getUserGuideURL();
            if (userGuideURL == null) userGuideURL = "";
        }
        return userGuideURL;
    }
    public void setUserGuideURL(String userGuideURL) {
        this.userGuideURL = userGuideURL;
    }
    
    @Transactional
    public String getSupportURL() {
        if (supportURL == null) {
            SystemConfiguration sc = SystemConfigurationDAO.loadSystemConfiguration();
            supportURL = sc.getSupportURL();
            if (supportURL == null) supportURL = "";
        }
        return supportURL;
    }
    public void setSupportURL(String supportURL) {
        this.supportURL = supportURL;
    }
}
