package org.sigit.logic.viewer.toolbox;

import org.sigit.dao.hnd.cadastre.HND_ParcelDAO;
import org.sigit.dao.hnd.cadastre.HND_SpatialZoneDAO;
import org.sigit.dao.hnd.ladmshadow.ParcelDAO;
import org.sigit.dao.hnd.ladmshadow.SpatialZoneDAO;
import org.sigit.logic.viewer.InteractiveViewerHelper;
import org.sigit.model.commons.IEasement;
import org.sigit.model.commons.IImprovement;
import org.sigit.model.commons.IParcel;
import org.sigit.model.commons.ISpatialZone;
import org.sigit.model.hnd.cadastre.HND_BuildingMaterial;
import org.sigit.model.hnd.cadastre.HND_Easement;
import org.sigit.model.hnd.cadastre.HND_Improvement;
import org.sigit.model.hnd.cadastre.HND_Parcel;
import org.sigit.model.hnd.cadastre.HND_SpatialZone;
import org.sigit.model.hnd.ladmshadow.Easement;
import org.sigit.model.hnd.ladmshadow.Improvement;
import org.sigit.model.hnd.ladmshadow.Parcel;
import org.sigit.model.hnd.ladmshadow.SpatialZone;
import org.sigit.util.EditMode;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;


public class ParcelDataHelper implements Serializable {
    private static final long serialVersionUID = 1L;

    private InteractiveViewerHelper interactiveViewerHelper;
    
    private IImprovement improvement;
    private EditMode improvementEditMode = EditMode.VIEWING;
    
    private IEasement easement;
    private EditMode easementEditMode = EditMode.VIEWING;

    private boolean editing = false;
    private HND_BuildingMaterial newForbiddenBuildingMaterial;
    
    private String reRenderSaveBtns;
    

    public ParcelDataHelper(InteractiveViewerHelper interactiveViewerHelper) {
        this.interactiveViewerHelper = interactiveViewerHelper;
    }
    
    public ISpatialZone getSelectedZone() {
        return interactiveViewerHelper.getSelectedZone();
    }
    
    public IImprovement getImprovement() {
        return improvement;
    }
    public void setImprovement(IImprovement improvement) {
        this.improvement = improvement;
    }
    public EditMode getImprovementEditMode() {
        return improvementEditMode;
    }
    public boolean isImprovementReadOnly() {
        return improvementEditMode == EditMode.VIEWING || improvementEditMode == EditMode.DELETING;
    }


    public IEasement getEasement() {
        return easement;
    }
    public void setEasement(IEasement easement) {
        this.easement = easement;
    }
    public EditMode getEasementEditMode() {
        return easementEditMode;
    }
    public boolean isEasementReadOnly() {
        return easementEditMode == EditMode.VIEWING || easementEditMode == EditMode.DELETING;
    }


    public String newImprovement() {
        improvementEditMode = EditMode.ADDING;
        if (getSelectedZone() instanceof Parcel)
            improvement = new Improvement();
        else if (getSelectedZone() instanceof HND_Parcel)
            improvement = new HND_Improvement();
        return null;
    }
    public String editImprovement(IImprovement improvement) {
        improvementEditMode = EditMode.EDITING;
        this.improvement = improvement;
        return null;
    }
    public String viewImprovement(IImprovement improvement) {
        improvementEditMode = EditMode.VIEWING;
        this.improvement = improvement;
        return null;
    }
    public String deleteImprovement(IImprovement improvement) {
        improvementEditMode = EditMode.DELETING;
        this.improvement = improvement;
        return null;
    }
    public String acceptImprovement() {
        switch (improvementEditMode) {
        case ADDING:
            interactiveViewerHelper.getSelectedParcel().getImprovements().add(improvement);
            break;
        case DELETING:
            interactiveViewerHelper.getSelectedParcel().getImprovements().remove(improvement);
            break;
        default:
            break;
        }
        return null;
    }

    public String newEasement() {
        easementEditMode = EditMode.ADDING;
        if (getSelectedZone() instanceof Parcel)
            easement = new Easement();
        else if (getSelectedZone() instanceof HND_Parcel)
            easement = new HND_Easement();
        return null;
    }
    public String editEasement(IEasement easement) {
        easementEditMode = EditMode.EDITING;
        this.easement = easement;
        return null;
    }
    public String viewEasement(IEasement easement) {
        easementEditMode = EditMode.VIEWING;
        this.easement = easement;
        return null;
    }
    public String deleteEasement(IEasement easement) {
        easementEditMode = EditMode.DELETING;
        this.easement = easement;
        return null;
    }
    public String acceptEasement() {
        if (getSelectedZone() instanceof IParcel) {
            switch (easementEditMode) {
            case ADDING:
                ((IParcel) getSelectedZone()).getEasements().add(easement);
                break;
            case DELETING:
                ((IParcel) getSelectedZone()).getEasements().remove(easement);
                break;
            default:
                break;
            }
        }

        return null;
    }
    

    public String beginEditData() {
        editing = true;
        return null;
    }
    
    public String cancelEditData() {
        editing = false;
        return null;
    }
    
    @Transactional
    public String applyDataChange() {
        if (getSelectedZone() != null) {
            if (getSelectedZone() instanceof Parcel) {
                ((Parcel) getSelectedZone()).setModified(true);
                ((Parcel) getSelectedZone()).setOriginal(false);
                ParcelDAO.save(getSelectedZone());
            }
            else if (getSelectedZone() instanceof HND_Parcel) {
                HND_ParcelDAO.save(getSelectedZone());
            }
            else if (getSelectedZone() instanceof SpatialZone) {
                ((SpatialZone) getSelectedZone()).setModified(true);
                ((SpatialZone) getSelectedZone()).setOriginal(false);
                SpatialZoneDAO.save(getSelectedZone());
            }
            else if (getSelectedZone() instanceof HND_SpatialZone) {
                HND_SpatialZoneDAO.save(getSelectedZone());
            }
            editing = false;
        }
        return null;
    }

    public boolean isEditing() {
        return editing;
    }
    
    public String getLandUseName() {
        if (getSelectedZone() == null
                || getSelectedZone().getLandUse() == null)
            return "";
        
        return getSelectedZone().getLandUse().getFullName();
    }
    public void setLandUseName(String landUseName) {
    }
    
    
    public HND_BuildingMaterial getNewForbiddenBuildingMaterial() {
        return newForbiddenBuildingMaterial;
    }
    public void setNewForbiddenBuildingMaterial(
            HND_BuildingMaterial newForbiddenBuildingMaterial) {
        this.newForbiddenBuildingMaterial = newForbiddenBuildingMaterial;
    }

    public String addForbiddenBuildingMaterial() {
        if (!isFrontEndSchema() || newForbiddenBuildingMaterial == null) return null;
        
        if (getSelectedZone() instanceof HND_SpatialZone) {
            HND_SpatialZone sz = (HND_SpatialZone) getSelectedZone();
            if (!sz.getPermitNorm().getForbiddenBuildingMaterials().contains(newForbiddenBuildingMaterial))
                sz.getPermitNorm().getForbiddenBuildingMaterials().add(newForbiddenBuildingMaterial);
        }
        
        return null;
    }
    public String deleteForbiddenBuildingMaterial() {
        if (!isFrontEndSchema() || newForbiddenBuildingMaterial == null) return null;
        
        ((HND_SpatialZone) getSelectedZone()).getPermitNorm().getForbiddenBuildingMaterials().remove(newForbiddenBuildingMaterial);
        
        return null;
    }

    public boolean isFrontEndSchema() {
        return getSelectedZone() instanceof HND_SpatialZone;
    }
    
    public String getReRenderSaveBtns() {
        if (reRenderSaveBtns == null) {
            StringBuilder sb = new StringBuilder("transactionCnts,editDataCadastre1Tab,editDataCadastre2Tab,editDataRegistryTab,editDataTaxTab,editDataLandUseTab,editSaveBtns");
            if (isFrontEndSchema())
                sb.append(",cadastreNormPermitTab");
            
            reRenderSaveBtns = sb.toString();
        }
        return reRenderSaveBtns;
    }
}
