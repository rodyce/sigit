package org.sigit.logic.viewer.toolbox;

import org.sigit.dao.hnd.ladmshadow.PartyDAO;
import org.sigit.dao.ladm.party.LA_PartyDAO;
import org.sigit.logic.ladmshadow.Util;
import org.sigit.model.commons.IParty;
import org.sigit.model.commons.IRRR;
import org.sigit.model.hnd.ladmshadow.RRR;
import org.sigit.model.ladm.external.ExtParty;
import org.sigit.model.ladm.party.LA_Party;
import org.sigit.model.ladm.special.Rational;
import org.sigit.util.ShareValue;

import java.util.Date;
import java.util.UUID;

public abstract class ParcelRRR<RT extends Enum<?>> {
    private boolean selected;
    private boolean modified = false;
    private boolean newRRR = false;
    private ExtParty extParty;
    private RT rrrType;
    private ShareValue share;
    private long presentationNo;
    protected UUID transactionId;
    protected IRRR<?,?> rrr;
    
    private ShareValue backShare;
    
    
    public ParcelRRR(UUID presentationId, long presentationNo, ExtParty extParty, RT rrrType, IRRR<?,?> rrr) {
        this(presentationId, presentationNo, extParty, rrrType, new ShareValue(), rrr);
    }
    public ParcelRRR(UUID presentationId, long presentationNo, ExtParty extParty, RT rrrType, ShareValue share, IRRR<?,?> rrr) {
        this.transactionId = presentationId;
        this.presentationNo = presentationNo;
        this.extParty = extParty;
        this.rrrType = rrrType;
        this.share = share;
        this.rrr = rrr;
        
        this.backShare = this.share.clone();
    }
    
    public RT getRrrType() {
        return rrrType;
    }
    public void setRrrType(RT rrrType) {
        if (this.rrrType != rrrType) {
            this.rrrType = rrrType;
            setModified(true);
        }
    }
    //This has proven to make it work on Mac OS X servers
    @SuppressWarnings("unchecked")
    public void setRrrType(Object rrrType) {
        setRrrType((RT) rrrType);
    }
    
    public String getTypeString() {
        if (rrrType == null) return "";
        
        return rrrType.name();
    }
    public void setTypeString(String value) {
        System.out.println("tipo: " + value);
    }


    
    public boolean isSelected() {
        return selected;
    }
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    public boolean isModified() {
        return modified;
    }
    protected void setModified(boolean modified) {
        this.modified = modified;
    }
    
    public boolean isNewRRR() {
        return newRRR;
    }
    public void setNewRRR(boolean newRRR) {
        this.newRRR = newRRR;
    }
    
    public ExtParty getExtParty() {
        return extParty;
    }
    public void setExtParty(ExtParty extParty) {
        if (this.extParty != extParty) {
            this.extParty = extParty;
            setModified(true);
        }
    }
    
    
    public ShareValue getShare() {
        return share;
    }
    public void setShare(ShareValue share) {
        if (!this.share.equals(share)) {
            this.share = share;
            setModified(true);
        }
    }
    
    public UUID getTransactionId() {
        return transactionId;
    }
    
    public long getPresentationNo() {
        return presentationNo;
    }
    
    public String getOwner() {
        if (extParty != null)
            return extParty.getName();
        return "";
    }

    public String getShareString() {
        return share.toString();
    }
    
    public IRRR<?,?> getRrr() {
        return rrr;
    }
    

    @SuppressWarnings("unchecked")
    public void cancelChanges() {
        if (rrr == null || !modified) return;
        
        rrrType = (RT) rrr.getType();
        Rational rrrShare = rrr.getShare();
        if (rrrShare != null) {
            share.setNumerator(rrrShare.getNumerator());
            share.setDenominator(rrrShare.getDenominator());
        }
        else {
            share.setNumerator(0);
            share.setDenominator(1);
        }
        
        IParty rrrParty = rrr.getParty();
        if (rrrParty != null && rrrParty.getExtParty() != null)
            extParty = rrrParty.getExtParty();
    }
    
    public void applyChanges() {
        if (rrr == null) return;

        setModified(modified || !share.equals(backShare));
        if (!isModified()) return;
        
        rrr.setType(getRrrType());
        rrr.setShare(share.asLadmRational());
        
        if (rrr.getParty() == null || rrr.getParty().getExtParty() != extParty) {
            IParty party = LA_PartyDAO.loadPartyByExtParty(extParty);
            
            //when a new ExtParty is created, there is no corresponding LA_Party
            //therefore, we have to verify this and create a new Party if that is the case
            if (party == null) {
                party = new LA_Party();
                ((LA_Party) party).setBeginLifespanVersion(new Date());
                ((LA_Party) party).setExtParty(extParty);
                LA_PartyDAO.save(party);
            }
            
            //If presentationId != null we are in a municipal transaction
            //Therefore, use Shadow schema
            if (transactionId != null) {
                party = Util.LA_PartyToParty((LA_Party) party, presentationNo, false, false);
                PartyDAO.save(party);
            }
            rrr.setParty(party);
        }
        
        if (rrr instanceof RRR)
            ((RRR) rrr).setModified(true);
    }
}
