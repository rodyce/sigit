package org.sigit.logic.viewer.toolbox;

import java.util.UUID;

import org.sigit.model.commons.IRight;
import org.sigit.model.hnd.ladmshadow.Right;
import org.sigit.model.ladm.administrative.LA_Right;
import org.sigit.model.ladm.administrative.LA_RightType;
import org.sigit.model.ladm.external.ExtParty;
import org.sigit.util.ShareValue;

public class ParcelRight extends ParcelRRR<LA_RightType> {
    public ParcelRight(UUID transactionId, long presentationNo, ExtParty extParty) {
        //if presentationId is null then we use shadow schema because we are in
        //the context of a municipal transaction
        super(transactionId, presentationNo,
                extParty,
                null,
                transactionId != null ? Right.newRight(null, presentationNo, false, false) : new LA_Right());
    }
    public ParcelRight(UUID presentationId, long presentationNo, ExtParty extParty, LA_RightType rrrType, ShareValue share, IRight<?,?> right) {
        super(presentationId, presentationNo,
                extParty,
                rrrType,
                share,
                right);
    }
}
