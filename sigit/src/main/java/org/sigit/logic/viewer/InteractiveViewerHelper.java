package org.sigit.logic.viewer;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Resource;
import javax.faces.model.SelectItem;

import org.deegree.protocol.wms.client.WMSClient111;
import org.sigit.commons.di.CtxComponent;
import org.sigit.commons.geometry.GeometryOperations;
import org.sigit.dao.hnd.administrative.HND_PropertyDAO;
import org.sigit.dao.hnd.administrative.HND_TopographicTransactionDAO;
import org.sigit.dao.hnd.cadastre.HND_LayerDAO;
import org.sigit.dao.hnd.cadastre.HND_SpatialZoneDAO;
import org.sigit.dao.hnd.special.HND_CounterDAO;
import org.sigit.i18n.ResourceBundleHelper;
import org.sigit.logic.general.ConfigurationHelper;
import org.sigit.logic.general.GeneralHelper;
import org.sigit.logic.viewer.toolbox.BorderHelper;
import org.sigit.logic.viewer.toolbox.CreateZoneViewerCallable;
import org.sigit.logic.viewer.toolbox.LayerHelper;
import org.sigit.logic.viewer.toolbox.MergeHelper;
import org.sigit.logic.viewer.toolbox.ParcelDataHelper;
import org.sigit.logic.viewer.toolbox.RRRHelper;
import org.sigit.logic.viewer.toolbox.SearchHelper;
import org.sigit.logic.viewer.toolbox.SplitViewerCallable;
import org.sigit.logic.viewer.toolbox.ViewerCallable;
import org.sigit.model.commons.IParcel;
import org.sigit.model.commons.IProperty;
import org.sigit.model.commons.IRegistration;
import org.sigit.model.commons.ISpatialZone;
import org.sigit.model.hnd.administrative.HND_Property;
import org.sigit.model.hnd.administrative.HND_Registration;
import org.sigit.model.hnd.administrative.HND_TopographicTransaction;
import org.sigit.model.hnd.cadastre.HND_BuildingUnit;
import org.sigit.model.hnd.cadastre.HND_LandUse;
import org.sigit.model.hnd.cadastre.HND_Layer;
import org.sigit.model.hnd.cadastre.HND_LayerType;
import org.sigit.model.hnd.cadastre.HND_Parcel;
import org.sigit.model.hnd.cadastre.HND_SpatialZone;
import org.sigit.model.hnd.special.HND_CounterType;
import org.sigit.model.hnd.special.HND_Lock;
import org.sigit.model.hnd.special.HND_LockLevelType;
import org.sigit.view.topography.TopographyMaintenanceHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.io.WKTWriter;

@Component("interactiveViewerHelper")
@Scope(value="sigit-conversation", proxyMode=ScopedProxyMode.TARGET_CLASS)
public class InteractiveViewerHelper implements Serializable {
    private static final long serialVersionUID = -5485155887832623658L;
    
    public static final String NAME = "interactiveViewerHelper";
    
    private UUID transactionId;
    private long presentationNo;
    
    
    private ISpatialZone selectedZone;
    private IRegistration selectedZonePropertyRegistration;
    
    private HND_Layer selectedLayer;
    private LayerTheme selectedLayerTheme;
    private IProperty selectedProperty;
    private ISpatialZone zoomedInZone;
    private HND_LandUse selectedZoneLandUse;
    
    private Double envelopeLeft = 0.0;
    private Double envelopeBottom = 0.0;
    private Double envelopeRight = 0.0;
    private Double envelopeTop = 0.0;

    private SearchHelper searchHelper;
    private RRRHelper rrrHelper;
    private BorderHelper borderHelper;
    private MergeHelper mergeHelper;
    private ParcelDataHelper parcelDataHelper;
    private LayerHelper layerHelper;
    
    private ViewerCallable splitCallable = new SplitViewerCallable();
    private ViewerCallable createZoneCallable = new CreateZoneViewerCallable();
    
    private ResourceBundleHelper resBundle;
    
    private String highlightedSpatialUnitIds;
    private boolean immediateHighlight;
    
    private List<HND_SpatialZone> highlightedSpatialZoneList;
    private List<HND_Layer> layerList;
    private Map<HND_Layer, LayerTheme> selectedThemePerLayerMap;
    private List<LayerTheme> selectedLayerApplicableThemes;
    private List<SelectItem> layerItems;
    private List<SelectItem> applicableLayerThemeItems;
    
    
    private Boolean showSearchSpatialZoneToolbox = true;
    private Boolean showRightsToolbox = true;
    private Boolean showRestrictionsToolbox = true;
    private Boolean showResponsibilitiesToolbox = true;
    private Boolean showDataToolbox = true;
    private Boolean showSpatialAnalysisToolbox = true;
    private Boolean showSaveModificationButtons = false;
    private Boolean showHelpToolbox = true;
    private Boolean showTitle = true;
    private Boolean drawParcelLayerOnly = false;
    private Boolean showTopographicTxnLayer = false;
    private Boolean showBBoxOptions = false;
    
    private Set<HND_LayerType> layerTypesToShow = new HashSet<HND_LayerType>();
    
    private boolean allowEditing = false;
    private BorderEditMode borderEditMode = BorderEditMode.SPLIT_MERGE;
    private CreateZoneMode createZoneMode = CreateZoneMode.CREATE_NEW;
    private String borderOperationTitle;
    private String borderShapeExpr;
    
    private List<Geometry> createdZoneGeoms;
    private final WKTWriter wktWriter = new WKTWriter();
    
    private List<String> additionalReRenderIds;
    private String reRenderList;
    private String defaultLayerFQName;
    private final String defaultReRenderList = "zoomFun,grpSelectedZone,panelRights,panelRestrictions,panelResponsibilities,panelData,splitParcelPanel,mergeParcelPanel";
    
    private String extentsBoxWKT;
    private Geometry extentsBox;
    private WKTReader wktReader = new WKTReader(GeometryOperations.geomFactory);
    
    private Geometry customMapExtents;
    
    private WMSClient111 wmsClient;
    private List<String> namedLayers;
    private Boolean mapServerAlive;

    
    @Autowired
    private GeneralHelper generalHelper;
    
    @Autowired
    private ConfigurationHelper configurationHelper;
    
    @Resource
    private MapHelper mapHelper;
    
    //when creating new zones, we need to get the transaction
    @Autowired
    private TopographyMaintenanceHelper topographyMaintenanceHelper;
    
    

    public InteractiveViewerHelper() {
        this.additionalReRenderIds = new ArrayList<String>();
        
        if (this.generalHelper == null)
            this.generalHelper = (GeneralHelper) CtxComponent.getInstance("generalHelper");
        
        if (this.configurationHelper == null)
            this.configurationHelper = (ConfigurationHelper) CtxComponent.getInstance("configurationHelper");
        
        if (this.mapHelper == null)
            this.mapHelper = (MapHelper) CtxComponent.getInstance(MapHelper.NAME);
    }
    public GeneralHelper getGeneralHelper() {
        if (generalHelper == null)
            generalHelper = (GeneralHelper) CtxComponent.getInstance("generalHelper");
        return generalHelper;
    }
    public ConfigurationHelper getConfigurationHelper() {
        if (configurationHelper == null)
            configurationHelper = (ConfigurationHelper) CtxComponent.getInstance("configurationHelper");
        return configurationHelper;
    }
    public MapHelper getMapHelper() {
        if (mapHelper == null)
            mapHelper = (MapHelper) CtxComponent.getInstance(MapHelper.NAME);
        return mapHelper;
    }
    
    
    public boolean isSelectedZoneEditable() {
        if (selectedZone != null) {
            HND_TopographicTransaction trx = getTopographicTransaction();
            if (trx != null && trx.getId() != null) { //Are we in a topographic transaction?
                //if so, check that the selected spatial zone was created
                //in this transaction.
                for ( HND_SpatialZone sz : trx.getOriginatedSpatialZones() ) {
                    if ( selectedZone.getBeginLifespanVersion() == null && selectedZone.getSuID().equals(sz.getSuID()) )
                        return true;
                }
                
                //Otherwise, check if the selected spatial zone was explicitly locked by this transaction.
                HND_Lock lockInfo = ((HND_SpatialZone)selectedZone).getLock();
                if (lockInfo != null && lockInfo.getLockingTransaction() != null &&
                        lockInfo.getLockingTransaction().getId().equals(trx.getId()) &&
                        lockInfo.getLockLevel() == HND_LockLevelType.LOCKED_EXPLICIT) {
                    return true;
                }
            } else {
                return true;
            }
        }
        return false;
    }
    
    
    public String getDefaultLayerFQName() {
        return defaultLayerFQName;
    }
    public void setDefaultLayerFQName(String defaultLayerFQName) {
        this.defaultLayerFQName = defaultLayerFQName;
    }
    

    public String getExtentsBoxWKT() {
        return extentsBoxWKT;
    }
    public void setExtentsBoxWKT(String extentsBoxWKT) {
        if (this.extentsBoxWKT == null || !this.extentsBoxWKT.equals(extentsBoxWKT)) {
            this.extentsBoxWKT = extentsBoxWKT;
            try {
                extentsBox = wktReader.read(this.extentsBoxWKT).getEnvelope();
            } catch (ParseException e) {
                e.printStackTrace();
                extentsBox = null;
            }
        }
    }

    public Geometry getExtentsBox() {
        return extentsBox;
    }
    
    
    public Geometry getCustomMapExtents() {
        return customMapExtents;
    }
    public void setCustomMapExtents(Geometry customMapExtents) {
        this.customMapExtents = customMapExtents;
    }
    
    
    public ResourceBundleHelper getResBundle() {
        if (resBundle == null) {
            resBundle = getGeneralHelper().getResBundle();
        }
        return resBundle;
    }

    
    public UUID getTransactionId() {
        return transactionId;
    }
    public void setTransactionId(UUID transactionId) {
        this.transactionId = transactionId;
    }
    
    public long getPresentationNo() {
        return presentationNo;
    }
    public void setPresentationNo(long presentationNo) {
        this.presentationNo = presentationNo;
    }
    
    public SearchHelper getSearchHelper() {
        if (searchHelper == null) {
            searchHelper = new SearchHelper(this);
        }
        return searchHelper;
    }

    public RRRHelper getRrrHelper() {
        if (rrrHelper == null) {
            rrrHelper = new RRRHelper(this);
        }
        return rrrHelper;
    }

    public BorderHelper getBorderHelper() {
        if (borderHelper == null) {
            borderHelper = new BorderHelper(this);
        }
        return borderHelper;
    }

    public MergeHelper getMergeHelper() {
        if (mergeHelper == null)
            mergeHelper = new MergeHelper(this);
        return mergeHelper;
    }

    public ParcelDataHelper getParcelDataHelper() {
        if (parcelDataHelper == null)
            parcelDataHelper = new ParcelDataHelper(this);
        return parcelDataHelper;
    }

    public LayerHelper getLayerHelper() {
        if (layerHelper == null) {
            layerHelper = new LayerHelper(this, generalHelper);
        }
        return layerHelper;
    }
    
    public ViewerCallable getSplitCallable() {
        return splitCallable;
    }
    public void setSplitCallable(ViewerCallable splitCallable) {
        this.splitCallable = splitCallable;
    }
    
    public ViewerCallable getCreateZoneCallable() {
        return createZoneCallable;
    }
    public void setCreateZoneCallable(ViewerCallable createZoneCallable) {
        this.createZoneCallable = createZoneCallable;
    }

    public String getMapOptions() {
        if (customMapExtents != null)
            return getMapHelper().customExtentsMapOptions(drawParcelLayerOnly, customMapExtents);
        return getMapHelper().mapOptions(drawParcelLayerOnly);
    }
    
    public boolean isAllowEditing() {
        return allowEditing && isSelectedZoneEditable();
    }
    public void setAllowEditing(boolean allowEditing) {
        this.allowEditing = allowEditing;
    }

    public BorderEditMode getBorderEditMode() {
        return borderEditMode;
    }
    public void setBorderEditMode(BorderEditMode borderEditMode) {
        this.borderEditMode = borderEditMode;
    }
    
    public CreateZoneMode getCreateZoneMode() {
        return createZoneMode;
    }
    public void setCreateZoneMode(CreateZoneMode createZoneMode) {
        this.createZoneMode = createZoneMode;
    }
    
    public String getBorderOperationTitle() {
        if (borderOperationTitle == null) {
            switch (borderEditMode) {
            case SPLIT_MERGE:
                borderOperationTitle = resBundle.loadMessage("dataentry.split.title");
                break;
            case CREATE:
                borderOperationTitle = resBundle.loadMessage("dataentry.create_zone.title");
                break;
            case MIXED:
                borderOperationTitle = String.format("%s / %s",
                        resBundle.loadMessage("dataentry.split.title"),
                        resBundle.loadMessage("dataentry.create_zone.title"));
                break;
            case NONE:
                break;
            }
        }
        return borderOperationTitle;
    }
    public void setBorderOperationTitle(String borderOperationTitle) {
        this.borderOperationTitle = borderOperationTitle;
    }

    public String getBorderShapeExpr() {
        return borderShapeExpr;
    }
    public void setBorderShapeExpr(String borderShapeExpr) {
        this.borderShapeExpr = borderShapeExpr;
    }

    public ISpatialZone getSelectedZone() {
        return selectedZone;
    }
    public void setSelectedZone(ISpatialZone selectedZone) {
        if (this.selectedZone != selectedZone) {
            this.selectedZone = selectedZone;
            this.selectedProperty = null;
            setSelectedZonePropertyRegistration(null);
            setZoomedInZone(selectedZone);
            if (selectedZone != null && allowEditing) {
                if (selectedZone.getBeginLifespanVersion() == null)
                    getParcelDataHelper().beginEditData();
                else
                    getParcelDataHelper().cancelEditData();
            }
            //mark the RRR fields to be updated/refreshed
            if (rrrHelper != null) {
                rrrHelper.setParcelRightsList(null);
                rrrHelper.setParcelRestrictionsList(null);
                rrrHelper.setParcelResponsibilitiesList(null);
                rrrHelper.setRrrSet(null);
                rrrHelper.setProperty(null);
            }
        }
    }
    
    public IRegistration getSelectedZonePropertyRegistration() {
        if (selectedZonePropertyRegistration == null) {
            selectedZonePropertyRegistration = getSelectedProperty().getRegistration();
            if (selectedZonePropertyRegistration == null)
                selectedZonePropertyRegistration = new HND_Registration();
        }
        return selectedZonePropertyRegistration;
    }
    public void setSelectedZonePropertyRegistration(
            IRegistration selectedZonePropertyRegistration) {
        this.selectedZonePropertyRegistration = selectedZonePropertyRegistration;
    }

    public HND_LandUse getSelectedZoneLandUse() {
        return selectedZoneLandUse;
    }
    public void setSelectedZoneLandUse(HND_LandUse selectedZoneLandUse) {
        this.selectedZoneLandUse = selectedZoneLandUse;
    }
    public void addLandUseToSelectedZone() {
        if (selectedZoneLandUse == null || selectedZone == null) return;
        
        List<HND_LandUse> otherLandUses = selectedZone.getOtherLandUses();
        if (!otherLandUses.contains(selectedZoneLandUse))
            otherLandUses.add(selectedZoneLandUse);
    }
    public void deleteLandUseFromSelectedZone(HND_LandUse landUse) {
        if (landUse == null) return;
        
        List<HND_LandUse> otherLandUses = selectedZone.getOtherLandUses();
        otherLandUses.remove(landUse);
    }

    public IProperty getSelectedProperty() {
        if (selectedProperty == null) {
            if (getSelectedZone() != null)
                selectedProperty = getSelectedZone().getProperty();
        }
        return selectedProperty;
    }
    public void setSelectedProperty(IProperty selectedProperty) {
        this.selectedProperty = selectedProperty;
    }

    public ISpatialZone getZoomedInZone() {
        return zoomedInZone;
    }
    public void setZoomedInZone(ISpatialZone zoomedInZone) {
        if (zoomedInZone == null || this.zoomedInZone == zoomedInZone) return;
        
        this.zoomedInZone = zoomedInZone;
        Geometry shape = zoomedInZone.getShape();
        Polygon p = null;
        if (shape instanceof MultiPolygon)
            p = (Polygon) ((MultiPolygon) shape).getGeometryN(0);
        else if (shape instanceof Polygon)
            p = (Polygon) shape;
        if (p != null) {
            Polygon pEnvelope = (Polygon) p.getEnvelope();
            Coordinate[] coords = pEnvelope.getCoordinates();
            
            envelopeLeft = envelopeBottom = envelopeRight = envelopeTop = null;
            if (coords.length > 1) {
                Double minx = Double.MAX_VALUE;
                Double miny = Double.MAX_VALUE;
                Double maxx = Double.MIN_VALUE;
                Double maxy = Double.MIN_VALUE;
                for (Coordinate c : coords) {
                    if (c.x < minx) minx = c.x;
                    if (c.x > maxx) maxx = c.x;
                    if (c.y < miny) miny = c.y;
                    if (c.y > maxy) maxy = c.y;
                }
                envelopeLeft = minx;
                envelopeBottom = miny;
                envelopeRight = maxx;
                envelopeTop = maxy;
            }
        }
    }

    public IParcel getSelectedParcel() {
        if (selectedZone != null && selectedZone instanceof IParcel)
            return (IParcel) selectedZone;
        return null;
    }
    public boolean isParcelSelected() {
        return selectedZone != null && selectedZone instanceof IParcel;
    }
    public boolean isBuildingSelected() {
        return selectedZone != null && selectedZone instanceof HND_BuildingUnit;
    }

    public Double getEnvelopeLeft() {
        return envelopeLeft;
    }
    public Double getEnvelopeBottom() {
        return envelopeBottom;
    }
    public Double getEnvelopeRight() {
        return envelopeRight;
    }
    public Double getEnvelopeTop() {
        return envelopeTop;
    }


    public String getSelectedZoneText() {
        if (getSelectedZone() != null)
            return generalHelper.getZoneUsefulName(getSelectedZone());
        else
            return generalHelper.getResBundle().loadMessage("txt.no_zone_or_parcel_selected");
    }
    
        
    public String getLandUseName() {
        if (getSelectedZone() == null
                || getSelectedZone().getLandUse() == null)
            return "";
        
        return getSelectedZone().getLandUse().getFullName();
    }
    public void setLandUseName(String landUseName) {
    }
    
    public String getHighlightedSpatialUnitIds() {
        return highlightedSpatialUnitIds;
    }
    public void setHighlightedSpatialUnitIds(String highlightedSpatialUnitIds) {
        this.highlightedSpatialUnitIds = highlightedSpatialUnitIds;
        highlightedSpatialZoneList = null;
    }
    
    public boolean isImmediateHighlight() {
        return immediateHighlight;
    }
    public void setImmediateHighlight(boolean immediateHighlight) {
        this.immediateHighlight = immediateHighlight;
    }

    @Transactional
    public List<HND_SpatialZone> getHighlightedSpatialZoneList() {
        HND_SpatialZone spatialZone;
        if (highlightedSpatialZoneList == null) {
            highlightedSpatialZoneList = new ArrayList<HND_SpatialZone>();
            if (highlightedSpatialUnitIds != null) {
                String[] suIds = highlightedSpatialUnitIds.split(",");
                if (suIds.length == 0 || suIds[0].trim().equals("")) {
                    highlightedSpatialZoneList.clear();
                    setSelectedZone(null);
                } else if (immediateHighlight) {
                    String str = suIds[0];
                    spatialZone = HND_SpatialZoneDAO.loadSpatialZoneByID(UUID.fromString(str));
                    if (spatialZone != null) {
                        highlightedSpatialZoneList.add(spatialZone);
                        setSelectedZone(spatialZone);
                    }
                } else {
                    for (String str : suIds) {
                        if (!str.trim().equals("")) {
                            spatialZone = HND_SpatialZoneDAO.loadSpatialZoneByID(UUID.fromString(str));
                            if (spatialZone != null)
                                highlightedSpatialZoneList.add(spatialZone);
                        }
                    }
                }
            }
        }
        return highlightedSpatialZoneList;
    }
    

    @Transactional
    public List<HND_Layer> getLayerList() {
        if (layerList == null) {
            layerList = new ArrayList<HND_Layer>();
            if (layerTypesToShow != null && layerTypesToShow.size() > 0) {
                for (HND_Layer layer : HND_LayerDAO.loadLayers(true))
                    if ( layerTypesToShow.contains(layer.getLayerType()) )
                        addLayerToList(layerList, layer);
            }
            else
            {
                for (HND_Layer layer : HND_LayerDAO.loadLayers(true))
                {
                    addLayerToList(layerList, layer);
                }
            }
        }
        return layerList;
    }
    private void addLayerToList(List<HND_Layer> layerList, HND_Layer layer) {
        if (getNamedLayers().contains(generalHelper.layerFullQualifiedName(layer)))
        {
            layerList.add(layer);
        }
    }
    
    public List<SelectItem> getLayerItems() {
        if (layerItems == null) {
            layerItems = new ArrayList<SelectItem>();
            for (HND_Layer layer : getLayerList())
                if (layerTypesToShow != null && layerTypesToShow.size() > 0) {
                    if (layerTypesToShow.contains(layer.getLayerType()))
                        layerItems.add(new SelectItem(layer, layer.getName()));
                }
                else
                    layerItems.add(new SelectItem(layer, layer.getName()));
        }
        return layerItems;
    }
    public void setLayerItems(List<SelectItem> layerItems) {
        this.layerItems = layerItems;
    }
    
    public List<SelectItem> getApplicableLayerThemeItems() {
        if (applicableLayerThemeItems == null) {
            applicableLayerThemeItems = new ArrayList<SelectItem>();
            for (LayerTheme lt : getSelectedLayerApplicableThemes())
                applicableLayerThemeItems.add(new SelectItem(lt, getResBundle().loadMessage(lt.name())));
        }
        return applicableLayerThemeItems;
    }
    public void setApplicableLayerThemeItems(
            List<SelectItem> applicableLayerThemeItems) {
        this.applicableLayerThemeItems = applicableLayerThemeItems;
    }

    public HND_Layer getSelectedLayer() {
        if (selectedLayer == null) {
            if (defaultLayerFQName != null) {
                selectedLayer = findLayerByFullQualifiedName(defaultLayerFQName);
            } else if (getLayerList().size() > 0) {
                selectedLayer = getLayerList().get(0);
            }
        }
        return selectedLayer;
    }
    public void setSelectedLayer(HND_Layer selectedLayer) {
        if (this.selectedLayer != selectedLayer) {
            this.selectedLayer = selectedLayer;
            //if ( !getSelectedLayerApplicableThemes().contains(getSelectedLayerTheme()) )
            getSelectedThemePerLayerMap().get(selectedLayer);
            setSelectedLayerTheme(getSelectedThemePerLayerMap().get(selectedLayer));
            //else
                
            setApplicableLayerThemeItems(null);
            setSelectedLayerApplicableThemes(null);
        }
    }
    
    public Map<HND_Layer, LayerTheme> getSelectedThemePerLayerMap() {
        if (selectedThemePerLayerMap == null) {
            selectedThemePerLayerMap = new HashMap<HND_Layer, LayerTheme>();
            for (HND_Layer layer : getLayerList())
                selectedThemePerLayerMap.put(layer, LayerTheme.DEFAULT_THEME);
        }
        return selectedThemePerLayerMap;
    }
    public void setSelectedThemePerLayerMap(
            Map<HND_Layer, LayerTheme> selectedThemePerLayerMap) {
        this.selectedThemePerLayerMap = selectedThemePerLayerMap;
    }

    public LayerTheme getSelectedLayerTheme() {
        if (selectedLayerTheme == null)
            selectedLayerTheme = LayerTheme.DEFAULT_THEME;
        return selectedLayerTheme;
    }
    public void setSelectedLayerTheme(LayerTheme selectedLayerTheme) {
        if (this.selectedLayerTheme != selectedLayerTheme) {
            this.selectedLayerTheme = selectedLayerTheme;
            getSelectedThemePerLayerMap().put(getSelectedLayer(), this.selectedLayerTheme);
        }
    }

    public Boolean getShowSearchSpatialZoneToolbox() {
        return showSearchSpatialZoneToolbox;
    }
    public void setShowSearchSpatialZoneToolbox(Boolean showSearchSpatialZoneToolbox) {
        if (showSearchSpatialZoneToolbox == null) return;
        this.showSearchSpatialZoneToolbox = showSearchSpatialZoneToolbox;
    }

    public Boolean getShowRightsToolbox() {
        return showRightsToolbox;
    }
    public void setShowRightsToolbox(Boolean showRightsToolbox) {
        if (showRightsToolbox == null) return;
        this.showRightsToolbox = showRightsToolbox;
    }

    public Boolean getShowRestrictionsToolbox() {
        return showRestrictionsToolbox;
    }
    public void setShowRestrictionsToolbox(Boolean showRestrictionsToolbox) {
        if (showRestrictionsToolbox == null) return;
        this.showRestrictionsToolbox = showRestrictionsToolbox;
    }

    public Boolean getShowResponsibilitiesToolbox() {
        return showResponsibilitiesToolbox;
    }
    public void setShowResponsibilitiesToolbox(Boolean showResponsibilitiesToolbox) {
        if (showResponsibilitiesToolbox == null) return;
        this.showResponsibilitiesToolbox = showResponsibilitiesToolbox;
    }

    public Boolean getShowDataToolbox() {
        return showDataToolbox;
    }
    public void setShowDataToolbox(Boolean showDataToolbox) {
        if (showDataToolbox == null) return;
        this.showDataToolbox = showDataToolbox;
    }

    public Boolean getShowSpatialAnalysisToolbox() {
        return showSpatialAnalysisToolbox;
    }
    public void setShowSpatialAnalysisToolbox(Boolean showSpatialAnalysisToolbox) {
        if (showSpatialAnalysisToolbox == null) return;
        this.showSpatialAnalysisToolbox = showSpatialAnalysisToolbox;
    }

    public Boolean getShowSaveModificationButtons() {
        return showSaveModificationButtons;
    }
    public void setShowSaveModificationButtons(Boolean showSaveModificationButtons) {
        if (showSaveModificationButtons == null) return;
        this.showSaveModificationButtons = showSaveModificationButtons;
    }
    
    public Boolean getDrawParcelLayerOnly() {
        return drawParcelLayerOnly;
    }
    public void setDrawParcelLayerOnly(Boolean drawParcelLayerOnly) {
        if (drawParcelLayerOnly == null) return;
        this.drawParcelLayerOnly = drawParcelLayerOnly;
        if (drawParcelLayerOnly) {
            layerTypesToShow.clear();
            layerTypesToShow.add(HND_LayerType.PARCEL);
        }
    }
    
    public Boolean getShowTopographicTxnLayer() {
        return showTopographicTxnLayer;
    }
    public void setShowTopographicTxnLayer(Boolean showTopographicTxnLayer) {
        if (showTopographicTxnLayer == null) return;
        this.showTopographicTxnLayer = showTopographicTxnLayer;
    }

    public Boolean getShowHelpToolbox() {
        return showHelpToolbox;
    }
    public void setShowHelpToolbox(Boolean showHelpToolbox) {
        if (showHelpToolbox == null) return;
        this.showHelpToolbox = showHelpToolbox;
    }

    public Boolean getShowTitle() {
        return showTitle;
    }
    public void setShowTitle(Boolean showTitle) {
        if (showTitle == null) return;
        this.showTitle = showTitle;
    }
    
    public Boolean getShowBBoxOptions() {
        return showBBoxOptions;
    }
    public void setShowBBoxOptions(Boolean showBBoxOptions) {
        if (showBBoxOptions == null) return;
        this.showBBoxOptions = showBBoxOptions;
    }
    
    
    public Set<HND_LayerType> getLayerTypesToShow() {
        return layerTypesToShow;
    }
    public void setLayerTypes(String layerTypes) {
        if (layerTypes == null || layerTypes.trim().equals("")) return;
        
        String[] types = layerTypes.split("\\s*,\\s*");
        for (String type : types)
            layerTypesToShow.add( HND_LayerType.valueOf(type) );
    }
    

    public String getZoneSelectionRerenderString() {
        StringBuffer bs = new StringBuffer();
        
        if (showRightsToolbox) bs.append("panelRights,");
        if (showRestrictionsToolbox) bs.append("panelRestrictions,");
        if (showResponsibilitiesToolbox) bs.append("panelResponsibilities,");
        if (showDataToolbox) {
            bs.append("panelData,frmEditData,");
        }
        if (showSpatialAnalysisToolbox) bs.append("panelSpatialAnalysis,grpOperand1,");
        
        if (bs.length() > 1) {
            bs.deleteCharAt(bs.length()-1);
        }
        
        return bs.toString();
    }

    public boolean isEditing() {
        return getRrrHelper().isEditing() || getParcelDataHelper().isEditing();
    }

    private long getFieldTabCounter() {
        return HND_CounterDAO.nextValue(HND_CounterType.FIELD_TAB);
    }
    @Transactional
    public String getNextFieldTab() {
        return String.format("GIT-%s-%06d", getConfigurationHelper().getJurisdictionCode(), getFieldTabCounter());
    }
    
    public void addAditionalReRenderId(String id) {
        if (!additionalReRenderIds.contains(id)) {
            additionalReRenderIds.add(id);
            reRenderList = null;
        }
    }
    
    public String getReRenderList() {
        if (reRenderList == null) {
            StringBuilder sb = new StringBuilder(defaultReRenderList);
            for (String str : additionalReRenderIds) {
                sb.append(',');
                sb.append(str);
            }
            reRenderList = sb.toString();
        }
        return reRenderList;
    }


    public GeneralHelper getGeneralHelperInstance() {
        if (generalHelper == null) {
            generalHelper = (GeneralHelper) CtxComponent.getInstance("generalHelper");
            if (generalHelper == null)
                generalHelper = new GeneralHelper();
        }
        return generalHelper;
    }
    
    
    private HND_Layer findLayerByFullQualifiedName(String fullQualifiedName) {
        for (HND_Layer layer : getLayerList())
            if (generalHelper.layerFullQualifiedName(layer).equals(fullQualifiedName))
                return layer;
        return null;
    }
    
    public List<Geometry> getCreatedZoneGeoms() {
        if (createdZoneGeoms == null) {
            createdZoneGeoms = new ArrayList<Geometry>();
        }
        return createdZoneGeoms;
    }
    public void setCreatedZoneGeoms(List<Geometry> createdZoneGeoms) {
        this.createdZoneGeoms = createdZoneGeoms;
    }
    public String getLastGeomWKT() {
        if (getCreatedZoneGeoms().size() == 0) return "";
        
        Geometry lastGeom = getCreatedZoneGeoms().get(getCreatedZoneGeoms().size()-1);
        return wktWriter.write(lastGeom);
    }
    public String resetCreatedZones() {
        getCreatedZoneGeoms().clear();
        return null;
    }

    @Transactional
    public String saveCreatedZones() {
        ISpatialZone firstSZ = null;
        boolean first = true;
        for (Geometry geom : getCreatedZoneGeoms()) {
            if (geom instanceof Polygon) {
                HND_SpatialZone newSZ;
                
                switch (getSelectedLayer().getLayerType()) {
                case PARCEL:
                    newSZ = new HND_Parcel();
                    ((HND_Parcel) newSZ).setFieldTab( getNextFieldTab() );
                    break;
                case BUILDING:
                    newSZ = new HND_BuildingUnit();
                    ((HND_BuildingUnit) newSZ).setNumberOfFloors(1);
                    break;
                case SPATIAL_ZONE:
                default:
                    newSZ = new HND_SpatialZone();
                    break;
                }
                
                newSZ.setLevel(getSelectedLayer());
                
                
                if (first) {
                    firstSZ = newSZ;
                    first = false;
                }
                
                newSZ.setShape(geom);
                
                HND_Property newProperty = new HND_Property();
                newProperty.getSpatialUnits().add(newSZ);
                newSZ.getBaunits().add(newProperty);
                
                HND_TopographicTransaction topoTrx = getTopographicTransaction();
                if (topoTrx != null) { //if there is an active transaction...
                    //register originating transaction in sz
                    newSZ.setOriginatingTransaction(topoTrx);
                    topoTrx.getOriginatedSpatialZones().add(newSZ);
                    
                    //lock new spatial zone with current transaction
                    newSZ.getLock().setLockingTransaction(topoTrx);
                    newSZ.getLock().setLockLevel(HND_LockLevelType.LOCKED_IMPLICIT);
                }
                
                HND_SpatialZoneDAO.save(newSZ);
                HND_PropertyDAO.save(newProperty);
                if (topoTrx != null) HND_TopographicTransactionDAO.save(topoTrx);
            }
        }
        
        if (firstSZ != null)
            setSelectedZone(firstSZ);
        
        getCreatedZoneGeoms().clear();
        
        return null;
    }
    private HND_TopographicTransaction getTopographicTransaction() {
        if (topographyMaintenanceHelper != null && topographyMaintenanceHelper.getTransaction() != null)
            return topographyMaintenanceHelper.getTransaction();
        return null;
    }
    
    public void setShapeField(Geometry shape) {
        if (borderShapeExpr != null)
            getGeneralHelper().setValueExpr(borderShapeExpr, shape, null);
    }

    private static final List<LayerTheme> initialLayerThemes;
    static {
        initialLayerThemes = new ArrayList<LayerTheme>();
        initialLayerThemes.add(LayerTheme.DEFAULT_THEME);
        initialLayerThemes.add(LayerTheme.LAND_USE);
    }
    public List<LayerTheme> getSelectedLayerApplicableThemes() {
        if (selectedLayerApplicableThemes == null) {
            selectedLayerApplicableThemes = new ArrayList<LayerTheme>(initialLayerThemes);
            switch (getSelectedLayer().getLayerType()) {
            case SPATIAL_ZONE:
                selectedLayerApplicableThemes.add(LayerTheme.IN_TRANSACTION);
                break;
            case BUILDING:
                selectedLayerApplicableThemes.add(LayerTheme.IN_TRANSACTION);
                selectedLayerApplicableThemes.add(LayerTheme.PERMITS);
                break;
            case PARCEL:
                selectedLayerApplicableThemes.add(LayerTheme.IN_TRANSACTION);
                selectedLayerApplicableThemes.add(LayerTheme.TAXATION_STATUS);
                selectedLayerApplicableThemes.add(LayerTheme.PERMITS);
                break;
            case STREET:
                break;
            case RASTER:
                break;
            }
        }
        return selectedLayerApplicableThemes;
    }
    public void setSelectedLayerApplicableThemes(
            List<LayerTheme> selectedLayerApplicableThemes) {
        this.selectedLayerApplicableThemes = selectedLayerApplicableThemes;
    }
    
    
    public WMSClient111 getWMSClient() {
        if (wmsClient == null) {
            try {
                wmsClient = new WMSClient111(new URL(getGeneralHelper().getWmsCapabilitiesUrl111()));
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return wmsClient;
    }

    public List<String> getNamedLayers() {
        if (namedLayers == null) {
            namedLayers = getWMSClient().getNamedLayers();
        }
        return namedLayers;
    }
    
    public Boolean getMapServerAlive() {
        if (mapServerAlive == null) {
            mapServerAlive = generalHelper.isMapServerAlive();
        }
        return mapServerAlive;
    }
    
    public String refreshMapServerStatus() {
        mapServerAlive = null;
        return null;
    }




    public static enum BorderEditMode {
        NONE,
        SPLIT_MERGE,
        CREATE,
        MIXED
    }
    
    public static enum CreateZoneMode {
        CREATE_NEW,
        SET_SHAPE_FIELD
    }

    public static enum LayerTheme {
        DEFAULT_THEME,
        IN_TRANSACTION,
        LAND_USE,
        TAXATION_STATUS,
        PERMITS
    }
    
    private static final Map<LayerTheme, String> layerThemeStyleMap;
    private static final Map<HND_LayerType, String> layerThemeDefaultStyleMap;
    static {
        layerThemeStyleMap = new HashMap<LayerTheme, String>();
        
        layerThemeStyleMap.put(LayerTheme.DEFAULT_THEME, StyleNames.EMPTY);
        layerThemeStyleMap.put(LayerTheme.IN_TRANSACTION, StyleNames.SIGIT_TRANSACTION);
        layerThemeStyleMap.put(LayerTheme.LAND_USE, StyleNames.SIGIT_LANDUSE);
        layerThemeStyleMap.put(LayerTheme.PERMITS, StyleNames.SIGIT_PERMIT);
        layerThemeStyleMap.put(LayerTheme.TAXATION_STATUS, StyleNames.SIGIT_PARCEL_TAXATION);
        
        
        layerThemeDefaultStyleMap = new HashMap<HND_LayerType, String>();
        
        layerThemeDefaultStyleMap.put(HND_LayerType.BUILDING, StyleNames.SIGIT_BUILDINGUNIT);
        layerThemeDefaultStyleMap.put(HND_LayerType.PARCEL, StyleNames.SIGIT_PARCEL);
        layerThemeDefaultStyleMap.put(HND_LayerType.SPATIAL_ZONE, StyleNames.SIGIT_LANDUSE);
        layerThemeDefaultStyleMap.put(HND_LayerType.STREET, StyleNames.SIGIT_SPATIALZONESTREET);
    }
    
    public static Map<LayerTheme, String> getLayerThemeStyleMap() {
        return Collections.unmodifiableMap(layerThemeStyleMap);
    }
    public static String getStyleName(LayerTheme lt, HND_LayerType layerType) {
        if (lt == LayerTheme.DEFAULT_THEME)
            return layerThemeDefaultStyleMap.get(layerType);

        return layerThemeStyleMap.get(lt);
    }
    
    public String getSelectedLayerStyleName() {
        if (getSelectedLayerTheme() == LayerTheme.DEFAULT_THEME)
            return layerThemeDefaultStyleMap.get(getSelectedLayer().getLayerType());
        
        return layerThemeStyleMap.get(getSelectedLayerTheme());
    }
    
    public List<LegendStyleInfo> getLegendStyleInfo(String styleName) {
        List<LegendStyleInfo> retval = styleToLegendInfoMap.get(styleName);
        if (retval == null) retval = new ArrayList<LegendStyleInfo>();
        
        return Collections.unmodifiableList(retval);
    }
    public int legendStyleInfoCount(String styleName) {
        return getLegendStyleInfo(styleName).size();
    }
    
    private static Map<String, List<LegendStyleInfo>> styleToLegendInfoMap;
    static {
        styleToLegendInfoMap = new HashMap<String, List<LegendStyleInfo>>();
        
        List<LegendStyleInfo> lsiList;
        
        lsiList = new ArrayList<LegendStyleInfo>();
        lsiList.add(new LegendStyleInfo("R01", "style.buildingunit"));
        styleToLegendInfoMap.put(StyleNames.SIGIT_BUILDINGUNIT, lsiList);
        
        lsiList = new ArrayList<LegendStyleInfo>();
        lsiList.add( new LegendStyleInfo("R01", "style.taxation.solvent") );
        lsiList.add( new LegendStyleInfo("R02", "style.taxation.insolvent") );
        lsiList.add( new LegendStyleInfo("R03", "style.taxation.na") );
        styleToLegendInfoMap.put(StyleNames.SIGIT_PARCEL_TAXATION, lsiList);
        
        lsiList = new ArrayList<LegendStyleInfo>();
        lsiList.add( new LegendStyleInfo("R01", "style.parcel") );
        styleToLegendInfoMap.put(StyleNames.SIGIT_PARCEL, lsiList);
        
        lsiList = new ArrayList<LegendStyleInfo>();
        lsiList.add( new LegendStyleInfo("R01", "style.permit.building") );
        lsiList.add( new LegendStyleInfo("R02", "style.permit.operation") );
        lsiList.add( new LegendStyleInfo("R03", "style.permit.none") );
        styleToLegendInfoMap.put(StyleNames.SIGIT_PERMIT, lsiList);
        
        lsiList = new ArrayList<LegendStyleInfo>();
        lsiList.add( new LegendStyleInfo("R01", "style.street") );
        styleToLegendInfoMap.put(StyleNames.SIGIT_SPATIALZONESTREET, lsiList);
        
        lsiList = new ArrayList<LegendStyleInfo>();
        lsiList.add( new LegendStyleInfo("R01", "style.transaction.user_locked") );
        lsiList.add( new LegendStyleInfo("R02", "style.transaction.locked") );
        lsiList.add( new LegendStyleInfo("R03", "style.transaction.unlocked") );
        styleToLegendInfoMap.put(StyleNames.SIGIT_TRANSACTION, lsiList);
    }
    public static class LegendStyleInfo {
        private String ruleName;
        private String label;

        public LegendStyleInfo(String ruleName, String label) {
            this.ruleName = ruleName;
            this.label = label;
        }
        
        public String getRuleName() {
            return ruleName;
        }
        public String getLabel() {
            return label;
        }
    }
    
    public static class StyleNames {
        public static final String SIGIT_BUILDINGUNIT = "sigit_buildingunit";
        public static final String SIGIT_LANDUSE = "sigit_landuse";
        public static final String SIGIT_PARCEL = "sigit_parcel";
        public static final String SIGIT_PARCEL_TAXATION = "sigit_parcel_taxation";
        public static final String SIGIT_PERMIT = "sigit_permit";
        public static final String SIGIT_SPATIALZONESTREET = "sigit_spatialzonestreet";
        public static final String SIGIT_TRANSACTION = "sigit_transaction";
        
        public static final String EMPTY = "";
    }
}
