package org.sigit.logic.viewer.toolbox;

import java.util.UUID;

import org.sigit.model.commons.IResponsibility;
import org.sigit.model.hnd.ladmshadow.Responsibility;
import org.sigit.model.ladm.administrative.LA_Responsibility;
import org.sigit.model.ladm.administrative.LA_ResponsibilityType;
import org.sigit.model.ladm.external.ExtParty;
import org.sigit.util.ShareValue;

public class ParcelResponsibility extends ParcelRRR<LA_ResponsibilityType> {
    public ParcelResponsibility(UUID transactionId, long presentationNo, ExtParty extParty) {
        super(transactionId, presentationNo,
                extParty,
                null,
                transactionId != null ? Responsibility.newResponsibility(null, presentationNo, false, false) : new LA_Responsibility());
    }
    public ParcelResponsibility(UUID transactionId, long presentationNo, ExtParty extParty, LA_ResponsibilityType rrrType, ShareValue share, IResponsibility<?,?> responsibility) {
        super(transactionId, presentationNo,
                extParty,
                rrrType,
                share,
                responsibility);
    }
}
