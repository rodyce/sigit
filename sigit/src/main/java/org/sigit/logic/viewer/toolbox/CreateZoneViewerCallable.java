package org.sigit.logic.viewer.toolbox;

import org.sigit.dao.hnd.cadastre.HND_SpatialZoneDAO;
import org.sigit.commons.geometry.GeometryOperations;
import org.sigit.logic.viewer.InteractiveViewerHelper;
import org.sigit.logic.viewer.InteractiveViewerHelper.CreateZoneMode;
import org.sigit.logic.viewer.toolbox.BorderHelper.Coord;
import org.sigit.model.commons.IParcel;
import org.sigit.model.commons.IProperty;
import org.sigit.model.commons.ISpatialZone;
import org.sigit.model.hnd.administrative.HND_RuleOperatorType;
import org.sigit.model.hnd.cadastre.HND_Layer;
import org.sigit.model.hnd.cadastre.HND_SpatialZone;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.io.WKTWriter;


public class CreateZoneViewerCallable implements ViewerCallable, Serializable {
    private static final long serialVersionUID = 1L;
    
    @Override
    public void afterMerge(IParcel parcel1, IProperty property1,
            IParcel parcel2, IProperty property2, Polygon newShape,
            Date timeStamp) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Transactional
    public void doBorderOperation(
            InteractiveViewerHelper interactiveViewerHelper) {
        HND_Layer workingLayer = interactiveViewerHelper.getSelectedLayer();
        BorderHelper bh = interactiveViewerHelper.getBorderHelper();
        
        Coordinate[] coords = new Coordinate[bh.getCurrentBorder().size() + 1];
        for (int i = 0; i < bh.getCurrentBorder().size(); i++) {
            Coord c = bh.getCurrentBorder().get(i);
            coords[i] = new Coordinate(c.getX(), c.getY());
        }
        coords[bh.getCurrentBorder().size()] = new Coordinate(coords[0]);
        
        LinearRing lr = GeometryOperations.geomFactory.createLinearRing(coords);
        Polygon initialPoly = GeometryOperations.geomFactory.createPolygon(lr, null);

        //check if initial polygon is ok
        checkPolygon(initialPoly, interactiveViewerHelper);

        Geometry newShape = null;
        List<HND_SpatialZone> touchingSpList;
        switch (workingLayer.getLayerType()) {
        case PARCEL:
            //DO THIS IF WE ARE CURRENTLY WITH A PARCEL LAYER....
            //obtain all polygons touching out initial polygon (p0)
            touchingSpList = HND_SpatialZoneDAO.loadSpatialZonesByLayerAndGeom(
                    workingLayer, initialPoly, HND_RuleOperatorType.DISTANCE);
            
            if (touchingSpList.size() > 0) {
                WKTWriter wktWriter = new WKTWriter();
                //obtain union of all touching zones plus p0
                Geometry gUnion = (Geometry) initialPoly.clone();
                for (ISpatialZone sz : touchingSpList) {
                    Geometry geom = sz.getShape();
                    if (geom != null && geom instanceof Polygon)
                        gUnion = gUnion.union(sz.getShape());
                }
                System.out.println("gUnion: " + wktWriter.write(gUnion));
                
                Geometry allEnvelope = gUnion.getEnvelope();
                List<HND_SpatialZone> allTouchingSpList = HND_SpatialZoneDAO.loadSpatialZonesByLayerAndGeom(
                        workingLayer, allEnvelope, HND_RuleOperatorType.DISTANCE);
                
                Geometry gAllUnion = (Geometry) initialPoly.clone();
                Geometry gAllRest = GeometryOperations.geomFactory.createPolygon(null, null);
                for (ISpatialZone sz : allTouchingSpList) {
                    Geometry geom = sz.getShape();
                    if (geom != null && geom instanceof Polygon)
                        gAllRest = gAllRest.union(sz.getShape());
                }
                gAllRest = gAllRest.difference(lr);
                gAllUnion = initialPoly.union(gAllRest);
                System.out.println("gAllUnion: " + wktWriter.write(gAllUnion));
                System.out.println("gAllRest: " + wktWriter.write(gAllRest));
                
                //eliminate any internal rings (holes)
                if (gAllUnion instanceof Polygon) {
                    Polygon allPolygon = polygonWithoutHoles((Polygon) gAllUnion);
                    gAllUnion = allPolygon;
                }
                else if (gAllUnion instanceof MultiPolygon) {
                    MultiPolygon allMultiPolygon = multiPolygonWithoutHoles((MultiPolygon) gAllUnion);
                    gAllUnion = allMultiPolygon;
                }
                
                //Calculate the difference between allPolygon and original spatial zones
                newShape = gAllUnion.difference(gAllRest);
                System.out.println("newShape: " + wktWriter.write(newShape));
            }
            else
                newShape = initialPoly;
            break;
        case BUILDING:
            //BUILDING LAYER... BUILDINGS CAN'T OVERLAP AND THEY MUST BE POLYGONS
            //obtain all polygons touching out initial polygon (p0)
            touchingSpList = HND_SpatialZoneDAO.loadSpatialZonesByLayerAndGeom(
                    workingLayer, initialPoly, HND_RuleOperatorType.DISTANCE);
            
            for (HND_SpatialZone sz : touchingSpList)
                if (sz.getShape().intersection(initialPoly).getArea() > 0)
                    throw new IllegalStateException(
                            interactiveViewerHelper.getResBundle()
                            .loadMessage("dataentry.create_zone.overlapping_geom"));

            newShape = initialPoly;
            break;
        case SPATIAL_ZONE:
        case STREET:
            //REGULAR SPATIAL ZONE. NO RESTRICTIONS.
            newShape = initialPoly;
            break;
        case RASTER:
            throw new IllegalStateException("Cannot work with raster layer here");
        }


        if (newShape != null) {
            //check if new shape is ok
            checkPolygon(newShape, interactiveViewerHelper);

            if (interactiveViewerHelper.getCreateZoneMode() == CreateZoneMode.SET_SHAPE_FIELD) {
                interactiveViewerHelper.getCreatedZoneGeoms().clear();
                interactiveViewerHelper.setShapeField(newShape);
            }
            interactiveViewerHelper.getCreatedZoneGeoms().add(newShape);
            bh.clearBoundary();
        }
    }
    
    
    
    private Polygon polygonWithoutHoles(Polygon polygon) {
        if (polygon.getNumInteriorRing() > 0) {
            LinearRing extRing = GeometryOperations.geomFactory.createLinearRing(polygon.getExteriorRing().getCoordinates());
            polygon = GeometryOperations.geomFactory.createPolygon(extRing, null);
        }
        return polygon;
    }
    
    private MultiPolygon multiPolygonWithoutHoles(MultiPolygon multiPolygon) {
        boolean hasHoles = false;
        Polygon poly;
        for (int i = 0; i < multiPolygon.getNumGeometries(); i++) {
            poly = (Polygon) multiPolygon.getGeometryN(i);
            hasHoles = poly.getNumInteriorRing() > 0;
            if (hasHoles) break;
        }
        
        if (hasHoles) {
            List<Polygon> polyList = new ArrayList<Polygon>();
            for (int i = 0; i < multiPolygon.getNumGeometries(); i++) {
                poly = (Polygon) multiPolygon.getGeometryN(i);
                poly = polygonWithoutHoles(poly);
                polyList.add(poly);
            }
            Polygon[] polygons = polyList.toArray(new Polygon[0]);
            MultiPolygon mp = GeometryOperations.geomFactory.createMultiPolygon(polygons);
            
            return mp;
        }

        return multiPolygon;
    }
    
    private void checkPolygon(Geometry geom, InteractiveViewerHelper ivh) {
        if ( !(geom instanceof Polygon) )
            throw new IllegalStateException(ivh.getResBundle().loadMessage("dataentry.create_zone.no_valid_geom"));
        
        if ( !((Polygon) geom).isValid() )
            throw new IllegalStateException(ivh.getResBundle().loadMessage("dataentry.create_zone.no_valid_geom"));
        
        if ( !((Polygon) geom).isSimple() )
            throw new IllegalStateException(ivh.getResBundle().loadMessage("dataentry.create_zone.no_valid_geom"));
    }
}
