package org.sigit.logic.viewer.toolbox;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.deegree.geometry.Envelope;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.Transaction;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.FeatureCollections;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.sigit.commons.di.CtxComponent;
import org.sigit.commons.geometry.GeometryOperations;
import org.sigit.dao.hnd.cadastre.HND_SpatialZoneDAO;
import org.sigit.interop.dbf.DBFFieldNames;
import org.sigit.interop.dbf.PartyDBF;
import org.sigit.interop.dbf.PropertyDBF;
import org.sigit.interop.dbf.Property_SpatialZoneDBF;
import org.sigit.interop.dbf.ResponsibilitiesDBF;
import org.sigit.interop.dbf.RestrictionsDBF;
import org.sigit.interop.dbf.RightsDBF;
import org.sigit.logic.general.GeneralHelper;
import org.sigit.logic.viewer.InteractiveViewerHelper;
import org.sigit.logic.viewer.InteractiveViewerHelper.LayerTheme;
import org.sigit.logic.viewer.InteractiveViewerHelper.StyleNames;
import org.sigit.logic.viewer.MapHelper;
import org.sigit.model.commons.IRRR;
import org.sigit.model.commons.ISpatialZone;
import org.sigit.model.hnd.administrative.HND_LegalPerson;
import org.sigit.model.hnd.administrative.HND_NaturalPerson;
import org.sigit.model.hnd.administrative.HND_Property;
import org.sigit.model.hnd.administrative.HND_RuleOperatorType;
import org.sigit.model.hnd.cadastre.HND_LandUse;
import org.sigit.model.hnd.cadastre.HND_Layer;
import org.sigit.model.hnd.cadastre.HND_LayerType;
import org.sigit.model.hnd.cadastre.HND_Parcel;
import org.sigit.model.hnd.cadastre.HND_SpatialZone;
import org.sigit.model.ladm.external.ExtParty;
import org.springframework.transaction.annotation.Transactional;

import com.vividsolutions.jts.geom.Geometry;


public class LayerHelper implements Serializable {
    private static final long serialVersionUID = -4875408467949573597L;

    private static final String UTF8 = "UTF-8";

    private static final String DBF_EXT = ".DBF";

    private static final String SPATIALZONE = "SPATIALZONE";
    private static final String PARCEL = "PARCEL";
    private static final String PROPERTY_SPATIALZONE = "PROPERTY_SPATIALZONE";
    private static final String PROPERTY = "PROPERTY";
    private static final String RIGHTS = "RIGHTS";
    private static final String RESTRICTIONS = "RESTRICTIONS";
    private static final String RESPONSIBILITIES = "RESPONSIBILITIES";
    private static final String PARTY = "PARTY";
    
    private static final String[] FILE_NAMES = { SPATIALZONE,
        PARCEL, PROPERTY_SPATIALZONE, PROPERTY, RIGHTS,
        RESTRICTIONS, RESPONSIBILITIES, PARTY
    };


    private InteractiveViewerHelper interactiveViewerHelper;
    private GeneralHelper generalHelper;
    private boolean useExtents = false;
    private Extents extents;
    private List<LayerDisplayDatum> layerDisplayData;
    private boolean allowChangeActiveLayer = true;
    


    public LayerHelper(InteractiveViewerHelper ivh, GeneralHelper gh) {
        interactiveViewerHelper = ivh;
        generalHelper = gh;
    }

    
    public boolean isUseExtents() {
        return useExtents;
    }
    public void setUseExtents(boolean useExtents) {
        this.useExtents = useExtents;
    }

    public Extents getExtents() {
        if (extents == null)
            extents = new Extents();
        return extents;
    }
    public void setExtents(Extents extents) {
        this.extents = extents;
    }
    
    public boolean isAllowChangeActiveLayer() {
        return allowChangeActiveLayer;
    }
    public void setAllowChangeActiveLayer(boolean allowChangeActiveLayer) {
        this.allowChangeActiveLayer = allowChangeActiveLayer;
    }


    private String getOriginalDBFFileName(String fileName) {
        for (String fname : FILE_NAMES) {
            if (fileName.startsWith(fname)) {
                int idx = fileName.lastIndexOf('.');
                if (idx >= 0) {
                    String ext = fileName.substring(idx);
                    return fname + ext;
                }
                else
                    return fname;
            }
        }
        return fileName;
    }
    
    private List<File> generateFiles(List<HND_SpatialZone> spatialZoneList)
    throws IOException, Exception {
        Property_SpatialZoneDBF propSzDBF = new Property_SpatialZoneDBF(
                File.createTempFile(PROPERTY_SPATIALZONE, DBF_EXT));
        propSzDBF.open();
        
        PropertyDBF propDBF = new PropertyDBF(
                File.createTempFile(PROPERTY, DBF_EXT));
        propDBF.open();
        
        RightsDBF rightsDBF = new RightsDBF(
                File.createTempFile(RIGHTS, DBF_EXT));
        rightsDBF.open();
        
        RestrictionsDBF restrictionsDBF = new RestrictionsDBF(
                File.createTempFile(RESTRICTIONS, DBF_EXT));
        restrictionsDBF.open();
        
        ResponsibilitiesDBF responsibilitiesDBF = new ResponsibilitiesDBF(
                File.createTempFile(RESPONSIBILITIES, DBF_EXT));
        responsibilitiesDBF.open();
        
        PartyDBF partyDBF = new PartyDBF(
                File.createTempFile(PARTY, DBF_EXT));
        partyDBF.open();

        for (HND_SpatialZone spatialZone : spatialZoneList) {
            HND_Property property = spatialZone.getProperty();
            if (property != null) {
                /*
                propSzDBF.addRecord(spatialZone.getSuID(), property.getuID());
                propDBF.addRecord(property.getuID(),
                        property.getRegistration() != null ? property.getRegistration().getCode() : null,
                        property.getRegistration() != null ? property.getRegistration().getTome() : null,
                        property.getRegistration() != null ? property.getRegistration().getFolio() : null,
                        property.getRegistration() != null ? property.getRegistration().getAnnotationNumber() : null);*/
                
                for (IRRR<?,?> rrr : property.getRrr()) {
                    ExtParty extParty = rrr.getParty().getExtParty();
                    /*
                    if (rrr instanceof IRight)
                        rightsDBF.addRecord(rrr.getRID(), property.getuID(),
                                extParty.getExtPID(), rrr.getDescription(),
                                rrr.getShare().getNumerator(), rrr.getShare().getDenominator(),
                                rrr.getType().toString());
                    else if (rrr instanceof IRestriction)
                        restrictionsDBF.addRecord(rrr.getRID(), property.getuID(),
                                extParty.getExtPID(), rrr.getDescription(),
                                rrr.getShare().getNumerator(), rrr.getShare().getDenominator(),
                                rrr.getType().toString(), ((IRestriction<?,?>) rrr).getPartyRequired());
                    else
                        responsibilitiesDBF.addRecord(rrr.getRID(), property.getuID(),
                                extParty.getExtPID(), rrr.getDescription(),
                                rrr.getShare().getNumerator(), rrr.getShare().getDenominator(),
                                rrr.getType().toString());
                    */
                        
                    if (extParty instanceof HND_NaturalPerson) {
                        HND_NaturalPerson natPersonParty = (HND_NaturalPerson) extParty;
                        partyDBF.addNaturalPerson((long) extParty.getExtPID().hashCode(),
                                natPersonParty.getIdentity(),
                                natPersonParty.getRtn(),
                                'N',
                                natPersonParty.getName(),
                                natPersonParty.getAddressDescription(),
                                natPersonParty.getPhones(),
                                natPersonParty.getEmails(),
                                natPersonParty.getFirstName1(),
                                natPersonParty.getFirstName2(),
                                natPersonParty.getLastName1(),
                                natPersonParty.getLastName2(),
                                natPersonParty.getGender(),
                                natPersonParty.getNationality(),
                                natPersonParty.getDob(),
                                natPersonParty.getMaritalStatus());
                    }
                    else if (extParty instanceof HND_LegalPerson) {
                        HND_LegalPerson legalPersonParty = (HND_LegalPerson) extParty;
                        partyDBF.addLegalPerson((long) extParty.getExtPID().hashCode(),
                                null,
                                legalPersonParty.getRtn(),
                                'N',
                                legalPersonParty.getName(),
                                legalPersonParty.getAddressDescription(),
                                legalPersonParty.getPhones(),
                                legalPersonParty.getEmails());
                    }
                }
            }
        }
        
        propSzDBF.close();
        propDBF.close();
        rightsDBF.close();
        restrictionsDBF.close();
        responsibilitiesDBF.close();
        partyDBF.close();
        
        
        
        List<File> resultFiles = new ArrayList<File>();
        resultFiles.add(propSzDBF.getFile());
        resultFiles.add(propDBF.getFile());
        resultFiles.add(rightsDBF.getFile());
        resultFiles.add(restrictionsDBF.getFile());
        resultFiles.add(responsibilitiesDBF.getFile());
        resultFiles.add(partyDBF.getFile());
        
        return resultFiles;
    }

    @Transactional
    public String downloadSelectedLayer() {
        Date date = new Date();
        //The file name to be downloaded will contain the date in the format below
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String dateText = String.format("%d.%02d.%02d.%02d.%02d.%02d.%04d",
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH) + 1,
                cal.get(Calendar.DAY_OF_MONTH),
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                cal.get(Calendar.SECOND),
                cal.get(Calendar.MILLISECOND));

        try {
            List<HND_SpatialZone> spatialZoneList = null;
            //If the user did not specify a clipping extent, then download the whole layer
            //TODO Limit the number of downloaded shapes to 100 when viewer is in transaction mode
            if (!useExtents)
                spatialZoneList = HND_SpatialZoneDAO.loadSpatialZonesByLayer(interactiveViewerHelper.getSelectedLayer());
            else {
                Geometry filter = GeometryOperations.rectangleFromBox(extents.getX1(), extents.getY1(), extents.getX2(), extents.getY2());
                spatialZoneList = HND_SpatialZoneDAO.loadSpatialZonesByLayerAndGeom(interactiveViewerHelper.getSelectedLayer(), filter, HND_RuleOperatorType.WITHIN);
            }
            List<File> filesToZip = generateShapeFiles(spatialZoneList);
            
            filesToZip.addAll(generateFiles(spatialZoneList));


            if (filesToZip != null && filesToZip.size() > 0) {
                FacesContext facesContext = FacesContext.getCurrentInstance();
                facesContext.responseComplete();
                
                ExternalContext externalContext = facesContext.getExternalContext();
                HttpServletResponse response = (HttpServletResponse) externalContext
                        .getResponse();

                response.reset(); // Some JSF component library or some Filter might
                                    // have set some headers in the buffer
                                    // beforehand. We want to get rid of them, else
                                    // it may collide.
                response.setContentType("application/x-download");
                response.setHeader(
                        "Content-disposition",
                        String.format(
                                "attachment; filename=\"sigit_data_%s.zip\"",
                                dateText
                        )
                );

                BufferedOutputStream output = null;

                try {
                    output = new BufferedOutputStream(response.getOutputStream());
                    writeZipFile(filesToZip, output);
                    
                    for (File file : filesToZip)
                        file.delete();
                    
                } finally {
                    if (null != output)
                        output.close();
                }

            }
        }
        catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
            FacesContext.getCurrentInstance().addMessage("",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "oops", "oops"));
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
            FacesContext.getCurrentInstance().addMessage("",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "oops", "oops"));
        }
        catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage("",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "oops", "oops"));
        }
        finally {
            //TODO Erase any produced file
        }

        return null;
    }
    
    private void writeZipFile(final List<File> inputFiles, OutputStream outStream) {
        final int BUFFER = 4192;

        try {
            ZipOutputStream out = new ZipOutputStream(outStream);
            BufferedInputStream origin = null;
            byte data[] = new byte[BUFFER];
            for (File file : inputFiles) {
                if (file.getName().endsWith(".zip")) continue;
                
                try {
                    FileInputStream fi = new FileInputStream(file);
                    origin = new BufferedInputStream(fi, BUFFER);
                    
                    ZipEntry entry = new ZipEntry(getOriginalDBFFileName(file.getName()));
                    out.putNextEntry(entry);

                    int count;
                    while ((count = origin.read(data, 0, BUFFER)) != -1)
                        out.write(data, 0, count);
                    origin.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static SimpleFeatureType createSpatialZoneFeatureType(HND_SpatialZone spatialZone) {
        Class<? extends Geometry> clazz = spatialZone.getShape().getClass();
        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setName("Location");
        builder.setCRS(DefaultGeographicCRS.WGS84);
        
        //Defines the arributes for a Parcel
        //Attributes must be set in this order
        //TODO: Know what to do when just a spatial zone
        
        builder.add(DBFFieldNames.SPATIAL_ZONE.ZoneId, Long.class);
        builder.add(DBFFieldNames.SPATIAL_ZONE.Shape, clazz);
        builder.add(DBFFieldNames.SPATIAL_ZONE.ZoneName, String.class);
        builder.add(DBFFieldNames.SPATIAL_ZONE.LocationInCountry, String.class);
        builder.add(DBFFieldNames.SPATIAL_ZONE.GeometryPerimeter, Double.class);
        builder.add(DBFFieldNames.SPATIAL_ZONE.MeasuredPerimeter, Double.class);
        builder.add(DBFFieldNames.SPATIAL_ZONE.DocumentedPerimeter, Double.class);
        builder.add(DBFFieldNames.SPATIAL_ZONE.GeometryArea, Double.class);
        builder.add(DBFFieldNames.SPATIAL_ZONE.MeasuredArea, Double.class);
        builder.add(DBFFieldNames.SPATIAL_ZONE.DocumentedArea, Double.class);
        builder.add(DBFFieldNames.SPATIAL_ZONE.ProposedLandUse, String.class);
        builder.add(DBFFieldNames.SPATIAL_ZONE.CurrentLandUse, String.class);
        builder.add(DBFFieldNames.SPATIAL_ZONE.GeomHash, String.class);
        
        //C'est la part du Parcel
        if (spatialZone instanceof HND_Parcel) {
            builder.add(DBFFieldNames.SPATIAL_ZONE.PARCEL.FieldTab, Long.class);
            builder.add(DBFFieldNames.SPATIAL_ZONE.PARCEL.MunicipalKey, String.class);
            builder.add(DBFFieldNames.SPATIAL_ZONE.PARCEL.CadastralKey, String.class);
            builder.add(DBFFieldNames.SPATIAL_ZONE.PARCEL.NeighborhoodName, String.class);
            builder.add(DBFFieldNames.SPATIAL_ZONE.PARCEL.AccessWay1, String.class);
            builder.add(DBFFieldNames.SPATIAL_ZONE.PARCEL.AccessWay2, String.class);
            builder.add(DBFFieldNames.SPATIAL_ZONE.PARCEL.HouseNumber, String.class);
            builder.add(DBFFieldNames.SPATIAL_ZONE.PARCEL.DocumentedBuiltArea, Double.class);
            builder.add(DBFFieldNames.SPATIAL_ZONE.PARCEL.GroundBuiltArea, Double.class);
            builder.add(DBFFieldNames.SPATIAL_ZONE.PARCEL.CommercialAppraisal, BigDecimal.class);
            builder.add(DBFFieldNames.SPATIAL_ZONE.PARCEL.FiscalAppraisal, BigDecimal.class);
            builder.add(DBFFieldNames.SPATIAL_ZONE.PARCEL.BalanceDue, BigDecimal.class);
            builder.add(DBFFieldNames.SPATIAL_ZONE.PARCEL.TaxationStatus, String.class);
        }
        // build the type
        final SimpleFeatureType ZONE = builder.buildFeatureType();

        return ZONE;
    }
    
    private String spatialZoneHash(ISpatialZone spatialZone) {
        return "" + spatialZone.hashCode();
    }
    private String spatialZoneGeomHash(ISpatialZone spatialZone) {
        return "" + spatialZone.getShape().hashCode();
    }
    private String nullToString(Object obj) {
        if (obj != null)
            return obj.toString();
        return "";
    }

    private List<File> generateShapeFiles(List<HND_SpatialZone> spatialZoneList) {
        List<File> resultFiles = null;

        SimpleFeatureCollection collection = FeatureCollections.newCollection();
        SimpleFeatureType TYPE = null;
        SimpleFeatureBuilder featureBuilder = null;
        
        for (HND_SpatialZone spatialZone : spatialZoneList) {
            if (TYPE == null)
                TYPE = createSpatialZoneFeatureType(spatialZone);
            if (featureBuilder == null)
                featureBuilder = new SimpleFeatureBuilder(TYPE);
            
            featureBuilder.add(spatialZone.getSuID());
            featureBuilder.add(spatialZone.getShape());
            featureBuilder.add(spatialZone.getZoneName());
            featureBuilder.add(spatialZone.getLocationInCountry());
            featureBuilder.add(spatialZone.getGeometryPerimeter());
            featureBuilder.add(spatialZone.getMeasuredPerimeter());
            featureBuilder.add(spatialZone.getDocumentedPerimeter());
            featureBuilder.add(spatialZone.getGeometryArea());
            featureBuilder.add(spatialZone.getMeasuredArea());
            featureBuilder.add(spatialZone.getDocumentedArea());
            featureBuilder.add(spatialZone.getLandUse() != null ? spatialZone.getLandUse().getCompleteCode() : "");
            featureBuilder.add(spatialZoneGeomHash(spatialZone));
            
            if (spatialZone instanceof HND_Parcel) {
                HND_Parcel parcel = (HND_Parcel) spatialZone;
                
                featureBuilder.add(parcel.getFieldTab());
                featureBuilder.add(parcel.getMunicipalKey());
                featureBuilder.add(parcel.getCadastralKey());
                featureBuilder.add(parcel.getNeighborhood());
                featureBuilder.add(parcel.getAccessWay1());
                featureBuilder.add(parcel.getAccessWay2());
                featureBuilder.add(parcel.getHouseNumber());
                featureBuilder.add(parcel.getDocumentedBuiltArea());
                featureBuilder.add(parcel.getGroundBuiltArea());
                featureBuilder.add(parcel.getCommercialAppraisal());
                featureBuilder.add(parcel.getFiscalAppraisal());
                featureBuilder.add(parcel.getTaxationBalanceDue());
                featureBuilder.add(nullToString(parcel.getTaxationStatus()));
            }

            SimpleFeature feature = featureBuilder.buildFeature(null);
            collection.add(feature);
        }
        
        try {
            //Creates a new file in the temporary-file directory
            final File newFile = File.createTempFile(PARCEL, ".shp");

            ShapefileDataStoreFactory dataStoreFactory = new ShapefileDataStoreFactory();

            Map<String, Serializable> params = new HashMap<String, Serializable>();
            try {
                params.put("url", newFile.toURI().toURL());
                params.put("create spatial index", Boolean.FALSE);
            } catch (MalformedURLException ex) {
                System.err.println("URL mal formado: " + ex.getMessage());
            }

            ShapefileDataStore newDataStore = (ShapefileDataStore) dataStoreFactory
                    .createNewDataStore(params);
            newDataStore.createSchema(TYPE);

            Transaction transaction = new DefaultTransaction("create");

            String typeName = newDataStore.getTypeNames()[0];
            SimpleFeatureSource featureSource = newDataStore
                    .getFeatureSource(typeName);

            if (featureSource instanceof SimpleFeatureStore) {
                SimpleFeatureStore featureStore = (SimpleFeatureStore) featureSource;

                featureStore.setTransaction(transaction);
                try {
                    featureStore.addFeatures(collection);
                    transaction.commit();
                    
                    File dir = new File(newFile.getParent());
                    File[] resultFilesArray = dir.listFiles(new FilenameFilter() {
                        @Override
                        public boolean accept(File dir, String name) {
                            String shpName = newFile.getName();
                            return name.startsWith(shpName.substring(0, shpName
                                    .length() - 4));
                        }
                        
                    });
                    resultFiles = new ArrayList<File>();
                    
                    //TODO: corregir lo de los nombres!
                    for (File f : resultFilesArray)
                        resultFiles.add(f);

                } catch (Exception problem) {
                    problem.printStackTrace();
                    transaction.rollback();

                } finally {
                    transaction.close();
                }

            } else {
                System.out.println(typeName
                        + " does not support read/write access");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultFiles;
    }
    
    public void download() throws IOException {
        /*
        FacesContext facesContext = FacesContext.getCurrentInstance();
        File[] shapeFiles = null;
        try {
            //Paso 1: Obtener lista de entidades que satisfagan el query espacial
            String gid = transactionController.getGid();
            List<Countries> geomList = findTouches(Integer.valueOf(gid).intValue());
            
            //Paso 2: Generar archivos shape de la geometria
            shapeFiles = generateShapeFiles(geomList);
            
            //Paso 3: Escribir al stream de salida ZIP los archivos shape generados
            ExternalContext externalContext = facesContext.getExternalContext();
            HttpServletResponse response = (HttpServletResponse) externalContext
                    .getResponse();

            response.reset(); // Some JSF component library or some Filter might
                                // have set some headers in the buffer
                                // beforehand. We want to get rid of them, else
                                // it may collide.
            response.setContentType("application/x-download");
            response.setHeader("Content-disposition",
                    "attachment; filename=\"shape.zip\"");

            BufferedOutputStream output = null;

            try {
                output = new BufferedOutputStream(response.getOutputStream());

                writeZipFile(shapeFiles, output);
            } finally {
                if (null != output)
                    output.close();
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            //marcar los archivos shape generados para ser eliminados
            //al terminar la instancia de la Java VM
            for (File f : shapeFiles)
                f.deleteOnExit();
            
            facesContext.responseComplete();
        }
        */
    }
    
    

    public List<LayerDisplayDatum> getLayerDisplayData() {
        if (layerDisplayData == null) {
            layerDisplayData = new ArrayList<LayerDisplayDatum>();
            for (HND_Layer layer : interactiveViewerHelper.getLayerList()) {
                LayerDisplayDatum ldd = new LayerDisplayDatum();
                ldd.setLayer(layer);
                ldd.setVisible(true);
                
                Envelope envelope = interactiveViewerHelper.getWMSClient().getBoundingBox(
                        interactiveViewerHelper.getGeneralHelperInstance().getWorkingSRS(),
                        generalHelper.layerFullQualifiedName(layer));

                ldd.setExtents( envelope2Extents( envelope ) );
                
                layerDisplayData.add(ldd);
            }
            
            
            //Add raster layer if exists
            String rasterLayerFQName = interactiveViewerHelper.getGeneralHelperInstance().getRasterLayerName();
            if ( interactiveViewerHelper.getMapHelper().getNamedLayers().contains( rasterLayerFQName )) {
                LayerDisplayDatum ldd = new LayerDisplayDatum();
                
                //Create detached layer instance to represent raster layer
                HND_Layer rasterLayer = new HND_Layer();
                String[] fqNameElems = rasterLayerFQName.split(":", 2);
                rasterLayer.setWmsNamespace(fqNameElems[0]);
                rasterLayer.setWmsLayerName(fqNameElems[1]);
                rasterLayer.setName("ORTHOPHOTO (RASTER)");
                rasterLayer.setLayerType(HND_LayerType.RASTER);
                
                ldd.setLayer(rasterLayer);
                ldd.setVisible(false);
                
                Envelope envelope = interactiveViewerHelper.getWMSClient().getBoundingBox(
                        interactiveViewerHelper.getGeneralHelperInstance().getWorkingSRS(),
                        interactiveViewerHelper.getGeneralHelperInstance().getRasterLayerName());

                ldd.setExtents( envelope2Extents( envelope ) );
                
                layerDisplayData.add(ldd);
            }
        }
        return layerDisplayData;
    }
    public void setLayerDisplayData(List<LayerDisplayDatum> layerDisplayData) {
        this.layerDisplayData = layerDisplayData;
    }
    
    private Extents envelope2Extents(Envelope envelope) {
        Extents extents = new Extents();
        extents.setX1( envelope.getMin().get0() );
        extents.setY1( envelope.getMin().get1() );
        extents.setX2( envelope.getMax().get0() );
        extents.setY2( envelope.getMax().get1() );
        
        return extents;
    }
    
    public String legendGraphicURL(String styleName, String ruleName) {
        String layer = generalHelper.layerFullQualifiedName(interactiveViewerHelper.getSelectedLayer());

        String url = "";
        try {
            GeneralHelper gh = interactiveViewerHelper.getGeneralHelperInstance();
            url = String.format(gh.getRequestResourcePath() + "/legendgraphic?layer=%s&styleName=%s",
                    URLEncoder.encode(layer, UTF8), URLEncoder.encode(styleName, UTF8));
            
            if (ruleName != null)
                url += String.format("&ruleName=%s", URLEncoder.encode(ruleName, UTF8));

            synchronized (gh) {
                if (!gh.isStyleRefreshed()) {
                    url += "&refresh=true";
                    if (styleName.equals(StyleNames.SIGIT_LANDUSE))
                        gh.setStyleRefreshed(true);
                }
            }
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return url;
    }
    
    public boolean isLandUseThemeSelected() {
        return interactiveViewerHelper.getSelectedLayerTheme() == LayerTheme.LAND_USE;
    }
    
    private List<HND_LandUse> currentLayerLandUses;
    private HND_Layer lastLayer;
    public List<HND_LandUse> getCurrentLayerLandUses() {
        if ( lastLayer != interactiveViewerHelper.getSelectedLayer() || currentLayerLandUses == null ) {
            lastLayer = interactiveViewerHelper.getSelectedLayer();
            currentLayerLandUses = new ArrayList<HND_LandUse>();
            for (HND_SpatialZone sz : HND_SpatialZoneDAO.loadSpatialZonesWithLandUseByLayer(interactiveViewerHelper.getSelectedLayer())) {
                HND_LandUse lu = sz.getLandUse();
                if ( lu != null && !currentLayerLandUses.contains(lu) )
                    currentLayerLandUses.add(lu);
            }
        }
        return currentLayerLandUses;
    }
    public void setCurrentLayerLandUseCodes(List<HND_LandUse> currentLayerLandUseCodes) {
        this.currentLayerLandUses = currentLayerLandUseCodes;
    }
    
    private static List<HND_LandUse> getLayerLandUses(HND_Layer layer) {
        List<HND_LandUse> luList = new ArrayList<HND_LandUse>();
        for (HND_SpatialZone sz : HND_SpatialZoneDAO.loadSpatialZonesWithLandUseByLayer(layer)) {
            HND_LandUse lu = sz.getLandUse();
            if (lu != null && !luList.contains(lu))
                luList.add(lu);
        }
        
        return luList;
    }
    
    private List<HND_Layer> visibleLayers;
    public List<HND_Layer> getVisibleLayers() {
        if (visibleLayers == null) {
            visibleLayers = new ArrayList<HND_Layer>();
            for (LayerDisplayDatum ldd : getLayerDisplayData())
                if (ldd != null && ldd.isVisible() && ldd.getLayer() != null)
                    visibleLayers.add(ldd.getLayer());
        }
        return visibleLayers;
    }
    public void setVisibleLayers(List<HND_Layer> visibleLayers) {
        this.visibleLayers = visibleLayers;
    }

    public List<ThemeLayerGroup> getVisibleLayerThemeGroups() {
        List<ThemeLayerGroup> tlgs = new ArrayList<ThemeLayerGroup>();
        Map<String, ThemeLayerGroup> tlgMap = new HashMap<String, ThemeLayerGroup>();
        for (HND_Layer layer : getVisibleLayers()) {
            if (layer.getLayerType() == HND_LayerType.RASTER) continue;
            
            LayerTheme lt = interactiveViewerHelper.getSelectedThemePerLayerMap().get(layer);
            String styleName = InteractiveViewerHelper.getStyleName(lt, layer.getLayerType());
            ThemeLayerGroup tlg = tlgMap.get(styleName);
            if (tlg == null) {
                if (!lt.name().equals(LayerTheme.DEFAULT_THEME.name()))
                    tlg = new ThemeLayerGroup(lt, interactiveViewerHelper.getResBundle().loadMessage(lt.name()), styleName);
                else
                    tlg = new ThemeLayerGroup(lt,
                            styleName.equals(StyleNames.SIGIT_LANDUSE) ?
                                    interactiveViewerHelper.getResBundle().loadMessage(LayerTheme.LAND_USE.name()) :
                                    "",
                            styleName);
                tlgMap.put(styleName, tlg);
            }
            tlg.addLayer(layer);
        }
        tlgs.addAll(tlgMap.values());
        
        return tlgs;
    }
    
    public void initializeThemeGroups() {
        int remainingSymbols = 10;
        int symbolCount;
        
        //put the landuse themes at the end of the list
        List<ThemeLayerGroup> tlgList = getVisibleLayerThemeGroups();
        List<ThemeLayerGroup> tlgListAux = new ArrayList<ThemeLayerGroup>();
        for (int i = 0; i < tlgList.size();) {
            if (tlgList.get(i).styleName.equals(StyleNames.SIGIT_LANDUSE)) {
                tlgListAux.add(tlgList.get(i));
                tlgList.remove(i);
            }
            else
                i++;
        }
        tlgList.addAll(tlgListAux);
        
        visibleLayerSmallThemeGroups = new ArrayList<ThemeLayerGroup>();
        visibleLayerAdditionalThemeGroups = new ArrayList<ThemeLayerGroup>();
        for (ThemeLayerGroup tlg : tlgList) {
            symbolCount = 0;
            if (tlg.styleName.equals(StyleNames.SIGIT_LANDUSE))
                for (HND_Layer layer : tlg.getLayers())
                    symbolCount += tlg.layerLandUses(layer).size();
            if (symbolCount < remainingSymbols) {
                visibleLayerSmallThemeGroups.add(tlg);
                remainingSymbols -= symbolCount;
            }
            else
                visibleLayerAdditionalThemeGroups.add(tlg);
        }
    }

    private List<ThemeLayerGroup> visibleLayerSmallThemeGroups;
    public List<ThemeLayerGroup> getVisibleLayerSmallThemeGroups() {
        if (visibleLayerSmallThemeGroups == null) {
            initializeThemeGroups();
        }
        return visibleLayerSmallThemeGroups;
    }
    private List<ThemeLayerGroup> visibleLayerAdditionalThemeGroups;
    public List<ThemeLayerGroup> getVisibleLayerAdditionalThemeGroups() {
        if (visibleLayerAdditionalThemeGroups == null) {
            initializeThemeGroups();
        }
        return visibleLayerAdditionalThemeGroups;
    }

    //gets the visibility value and resets to null the mapscript so it is recalculated
    //with the new visibility configuration
    public boolean isVisibleAndReset(LayerDisplayDatum ldd) {
        MapHelper mh = (MapHelper) CtxComponent.getInstance("mapHelper");
        mh.setMapScript(null);
        mh.setDataEntryMapScript(null);
        
        return ldd.isVisible();
    }
    
    public static class ThemeLayerGroup {
        private static final List<HND_LandUse> emptyLUList = new ArrayList<HND_LandUse>();
        private LayerTheme theme;
        private String themeTitle;
        private String styleName;
        private Map<HND_Layer, List<HND_LandUse>> layerToLandUseMap = new HashMap<HND_Layer, List<HND_LandUse>>();
        
        private String title;
        
        public ThemeLayerGroup(LayerTheme theme, String themeTitle, String styleName) {
            if (theme == null || themeTitle == null || styleName == null) throw new NullPointerException("ThemeLayerGroup params cannot be null");
            
            this.theme = theme;
            this.themeTitle = themeTitle;
            this.styleName = styleName;
        }
        
        public LayerTheme getTheme() {
            return theme;
        }
        public String getStyleName() {
            return styleName;
        }
        public List<HND_Layer> getLayers() {
            return new ArrayList<HND_Layer>(layerToLandUseMap.keySet());
        }
        @Transactional
        public void addLayer(HND_Layer layer) {
            if (!layerToLandUseMap.containsKey(layer)) {
                List<HND_LandUse> luList = emptyLUList;
                if (styleName.equals(StyleNames.SIGIT_LANDUSE))
                    luList = LayerHelper.getLayerLandUses(layer);
                
                layerToLandUseMap.put(layer, luList);
                
                title = null;
            }
        }
        public List<HND_LandUse> layerLandUses(HND_Layer layer) {
            if (layerToLandUseMap.containsKey(layer))
                return layerToLandUseMap.get(layer);
            return emptyLUList;
        }
        public String getTitle() {
            if (title == null) {
                StringBuilder sb = new StringBuilder(100);
                if (themeTitle.length() > 0)
                    sb.append(themeTitle);
                if (getLayers().size() > 0) {
                    if (themeTitle.length() > 0)
                        sb.append(" - ");
                    for (HND_Layer layer : getLayers()) {
                        sb.append(layer.getName());
                        sb.append(", ");
                    }
                    sb.setLength(sb.length() - 2);
                }
                
                title = sb.toString();
            }
            return title;
        }
        
        
        @Override
        public String toString() {
            return getTitle();
        }
    }

    public static class Extents {
        private double X1 = 0.0;
        private double Y1 = 0.0;
        private double X2 = 0.0;
        private double Y2 = 0.0;
        
        
        public Double getX1() {
            return X1;
        }
        public void setX1(Double x1) {
            X1 = x1;
        }
        
        public Double getY1() {
            return Y1;
        }
        public void setY1(Double y1) {
            Y1 = y1;
        }
        
        public Double getX2() {
            return X2;
        }
        public void setX2(Double x2) {
            X2 = x2;
        }
        
        public Double getY2() {
            return Y2;
        }
        public void setY2(Double y2) {
            Y2 = y2;
        }
    }


    public class LayerDisplayDatum {
        private HND_Layer layer;
        private boolean visible;
        private Extents extents;
        
        public boolean isSelected() {
            return layer == interactiveViewerHelper.getSelectedLayer();
        }
        public void setSelected(boolean selected) {
            if (selected)
                interactiveViewerHelper.setSelectedLayer(layer);
        }
        
        public HND_Layer getLayer() {
            return layer;
        }
        public void setLayer(HND_Layer layer) {
            this.layer = layer;
        }
        
        public boolean isVisible() {
            return visible;
        }
        public void setVisible(boolean visible) {
            if (this.visible != visible) {
                this.visible = visible;
                
                MapHelper mh = (MapHelper) CtxComponent.getInstance("mapHelper");
                mh.setMapScript(null);
                mh.setDataEntryMapScript(null);
                
                setVisibleLayers(null);
            }
        }

        public Extents getExtents() {
            return extents;
        }
        public void setExtents(Extents extents) {
            this.extents = extents;
        }
    }
}
