package org.sigit.logic.viewer.toolbox;

import org.sigit.logic.ladmshadow.Util;
import org.sigit.logic.workflow.transaction.DataEntryHelper;
import org.sigit.model.hnd.administrative.HND_SpatialZoneInTransaction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UndoHelper implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private DataEntryHelper dataEntryHelper;
    
    public UndoHelper(DataEntryHelper dataEntryHelper) {
        this.dataEntryHelper = dataEntryHelper;
    }
    
    public String applyUndo() {
        //TODO: Yet to finish
        //Util.undoLastOperation(dataEntryHelper.getPresentationNo());
        return null;
    }
    
    public String applyUndoAll() {
        //List<ParcelData> parcelDataList = dataEntryHelper.getTransactionHelper().getParcelDataList();
        List<UUID> parcelIdList = new ArrayList<>();
        for (HND_SpatialZoneInTransaction szit : dataEntryHelper.getTransactionHelper()
                .getTransaction().getSpatialZoneInTransactions())
            parcelIdList.add(szit.getSpatialZoneId());
        //for (ParcelData pd : parcelDataList)
        //    parcelIdList.add(Long.valueOf(pd.getSuID()));
        
        Util.undoAll(dataEntryHelper.getPresentationNo(), parcelIdList);
        return null;
    }

}
