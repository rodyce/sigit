package org.sigit.logic.viewer.toolbox;

import java.util.UUID;

import org.sigit.model.commons.IRestriction;
import org.sigit.model.hnd.ladmshadow.Restriction;
import org.sigit.model.ladm.administrative.LA_Restriction;
import org.sigit.model.ladm.administrative.LA_RestrictionType;
import org.sigit.model.ladm.external.ExtParty;
import org.sigit.util.ShareValue;

public class ParcelRestriction extends ParcelRRR<LA_RestrictionType> {
    //if presentationId is null then we use shadow schema because we are in
    //the context of a municipal transaction
    public ParcelRestriction(UUID transactionId, long presentationNo, ExtParty extParty) {
        super(transactionId, presentationNo,
                extParty,
                null,
                transactionId != null ? Restriction.newRestriction(null, presentationNo, false, false) : new LA_Restriction());
    }
    public ParcelRestriction(UUID transactionId, long presentationNo, ExtParty extParty, LA_RestrictionType rrrType, ShareValue share, IRestriction<?,?> restriction) {
        super(transactionId, presentationNo,
                extParty,
                rrrType,
                share,
                restriction);
    }
}
