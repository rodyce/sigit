package org.sigit.logic.viewer.toolbox;

import java.util.Date;

import com.vividsolutions.jts.geom.Polygon;

import org.sigit.logic.viewer.InteractiveViewerHelper;
import org.sigit.model.commons.IParcel;
import org.sigit.model.commons.IProperty;

public interface ViewerCallable {
    void afterMerge(IParcel parcel1, IProperty property1,
            IParcel parcel2, IProperty property2,
            Polygon newShape, Date timeStamp);
    
    void doBorderOperation(
            InteractiveViewerHelper interactiveViewerHelper);
}
