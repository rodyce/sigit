package org.sigit.logic.viewer;

import org.sigit.dao.hnd.cadastre.HND_LayerDAO;
import org.sigit.logic.general.GeneralHelper;
import org.sigit.logic.viewer.InteractiveViewerHelper.LayerTheme;
import org.sigit.logic.viewer.toolbox.LayerHelper;
import org.sigit.model.commons.ISpatialZone;
import org.sigit.model.hnd.cadastre.HND_Layer;
import org.sigit.model.hnd.cadastre.HND_LayerType;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringEscapeUtils;
import org.deegree.geometry.Envelope;
import org.deegree.protocol.wms.client.WMSClient111;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import com.vividsolutions.jts.geom.Geometry;

@Component("mapHelper")
@Lazy
@Scope(value="sigit-conversation", proxyMode=ScopedProxyMode.TARGET_CLASS)
public class MapHelper implements Serializable {
    private static final long serialVersionUID = 1L;
    
    public static final String NAME = "mapHelper";
    
    public static final String ORTHOPHOTO_LAYER_NAME = "orthophoto";
    public static final String PARCEL_LAYER_NAME = "parcel";
    public static final String STREET_LAYER_NAME = "spatialzonestreet";
    //TODO capas intermedias de reportes...
    public static final String CENTROID_LAYER_NAME = "spatial_unit__reference_point";
    public static final String BUILDING_LAYER_NAME = "buildingunit";
    public static final String ZONING_LAYER_NAME = "spatialzone";
    
    public static final String[] allLayers = {
        ORTHOPHOTO_LAYER_NAME, PARCEL_LAYER_NAME, STREET_LAYER_NAME,
        CENTROID_LAYER_NAME, BUILDING_LAYER_NAME, ZONING_LAYER_NAME
    };
    
    private static final Map<String, Integer> layerZOrder;
    
    static {
        layerZOrder = new HashMap<String, Integer>();
        layerZOrder.put(ORTHOPHOTO_LAYER_NAME, 100);
        layerZOrder.put(PARCEL_LAYER_NAME, 200);
        layerZOrder.put(STREET_LAYER_NAME, 200);
        layerZOrder.put(CENTROID_LAYER_NAME, 290);
        layerZOrder.put(BUILDING_LAYER_NAME, 300);
        layerZOrder.put(ZONING_LAYER_NAME, 400);
    }
    
    public static int getLayerZOrder(String layerName) {
        return layerZOrder.get(layerName) != null ? layerZOrder.get(layerName) : 1000;
    }

    
    private String mapOptions;
    private String mapScript;
    private String dataEntryMapScript;
    private List<HND_Layer> layerList;
    private boolean parcelLayerOnly;
    
    private boolean showEditControls;
    private boolean highlightParcelList;
    
    private List<? extends ISpatialZone> features;
    private List<? extends ISpatialZone> readOnlyFeatures;
    
    private String selectedLayerFQName;
    
    private WMSClient111 localWMSClient;
    private List<String> namedLayers;
    

    @Autowired
    private GeneralHelper generalHelper;

    @Resource
    private InteractiveViewerHelper interactiveViewerHelper;
    
    
    public MapHelper() {
        this.parcelLayerOnly = false;
        this.showEditControls = true;
        this.highlightParcelList = true;
    }
    
    public List<String> getNamedLayers() {
        if (namedLayers == null)
            namedLayers = getWMSClient().getNamedLayers();
        return namedLayers;
    }

    public String customExtentsMapOptions(boolean parcelLayerOnly, Geometry customMapExtents) {
        if (this.mapOptions == null || this.mapOptions.equals("") || this.parcelLayerOnly != parcelLayerOnly) {
            this.parcelLayerOnly = parcelLayerOnly;
            
            com.vividsolutions.jts.geom.Envelope env = customMapExtents.getEnvelope().getEnvelopeInternal();
            
            if (env != null) {
                this.mapOptions = String.format("{ controls:[], " +
                        "maxExtent: new OpenLayers.Bounds(%f, %f, %f, %f), " +
                        //"minResolution: 0.1, " +
                        "maxResolution: 75, " +
                        //"projection: new OpenLayers.Projection('EPSG:32616'), " +
                        //"displayProjection: new OpenLayers.Projection('EPSG:32616'), " +
                        "units: 'm' }",
                        
                        env.getMinX(),
                        env.getMinY(),
                        env.getMaxX(),
                        env.getMaxY());
            }
            else
                this.mapOptions = "";
        }
        return this.mapOptions;
    }

    @Transactional
    public String mapOptions(boolean parcelLayerOnly) {
        if (this.mapOptions == null || this.mapOptions.equals("") || this.parcelLayerOnly != parcelLayerOnly) {
            this.parcelLayerOnly = parcelLayerOnly;
            List<String> boundsInclLayers = new ArrayList<String>();
            
            if (getNamedLayers() == null || getNamedLayers().size() == 0)
                return null;

            String layerName;
            for (HND_Layer layer : getLayerList()) {
                if (!parcelLayerOnly || layer.getLayerType() == HND_LayerType.PARCEL) {
                    layerName = generalHelper.layerFullQualifiedName(layer);
                    if (namedLayers.contains(layerName))
                        boundsInclLayers.add(layerName);
                }
            }
            
            Envelope env = getWMSClient().getBoundingBox(generalHelper.getWorkingSRS(), boundsInclLayers);

            if (env != null) {
                this.mapOptions = String.format("{ controls:[], " +
                        "maxExtent: new OpenLayers.Bounds(%f, %f, %f, %f), " +
                        //"minResolution: 0.1, " +
                        "maxResolution: 75, " +
                        //"projection: new OpenLayers.Projection('EPSG:32616'), " +
                        //"displayProjection: new OpenLayers.Projection('EPSG:32616'), " +
                        "units: 'm' }",
                        
                        env.getMin().get0(),
                        env.getMin().get1(),
                        env.getMax().get0(),
                        env.getMax().get1());
            }
            else
                this.mapOptions = "";
        }
        return this.mapOptions;
    }
    
    @Transactional
    public String getDataEntryMapScript() {
        if (dataEntryMapScript != null) return dataEntryMapScript;
        
        StringBuilder sb = new StringBuilder(
                //"var click = new OpenLayers.Control.Click();" +
                //"mapEditor.addControl(click);" +
                //"click.activate();" +
                "var vector_style = new OpenLayers.Style({'strokeColor': '#ff0000','strokeWidth': 3});" +
                "var vector_style_map = new OpenLayers.StyleMap({'default': vector_style});" +
                "vlayer.styleMap = vector_style_map;" +
                "var select_feature_control = new OpenLayers.Control.SelectFeature(parcelFeaturesLayer,{multiple: false,toggle: true,multipleKey: 'shiftKey'});" +
                "mapEditor.addControl(select_feature_control);" +
                "select_feature_control.id = 'parcelSelectControl';"
        );
        
        addLayers(sb, false);
        addRasterLayer(sb, 0);
        

        if (interactiveViewerHelper.getShowTopographicTxnLayer())
            addTopographicTxnLayer(sb);

        addLayerStyles(sb);
        
        
        addInitialFeatures(sb);
        
        addSnapCapability(sb);

        sb.append(
                "var vertexStyle = {" +
                "    cursor: 'pointer'," +
                "    fillColor: '#787878'," +
                "    fillOpacity: .8," +
                "    fontColor: '#343434'," +
                "    pointRadius: 14," +
                "    strokeColor: '#232323'," +
                "    strokeDashstyle: 'dot'," +
                "    strokeWidth: 3" +
                "};" +
                "polyLayer.style = vertexStyle;" +
                "mapEditor.setLayerIndex(polyLayer, 99);" +

                "mapEditor.setLayerIndex(vlayer, 98);" +
                "doZoom();"
        );

        return dataEntryMapScript = sb.toString();
    }
    public void setDataEntryMapScript(String dataEntryMapScript) {
        this.dataEntryMapScript = dataEntryMapScript;
    }

    public String getMapScript() {
        if (mapScript != null) return mapScript;
        
        StringBuilder sb = new StringBuilder(
                "var select_feature_control = new OpenLayers.Control.SelectFeature(parcelFeaturesLayer,{multiple: false,toggle: true,multipleKey: 'shiftKey'});" +
                "mapEditor.addControl(select_feature_control);" +
                "select_feature_control.id = 'parcelSelectControl';"
        );

        addLayers(sb, true);
        
        if (interactiveViewerHelper.getShowTopographicTxnLayer())
            addTopographicTxnLayer(sb);
        
        addSimpleLayerStyles(sb);
        addRasterLayer(sb, 0);
        addSnapCapability(sb);
        
        addInitialFeatures(sb);
        
        addMapPanelControls(sb);
        
        sb.append(
                String.format(
                        "changeWorkingLayer('%s');" +
                        "mapEditor.setLayerIndex(polyLayer, 10003);" +
                        "mapEditor.setLayerIndex(vlayer, 10002);" +
                        "mapEditor.setLayerIndex(parcelFeaturesLayer, 10001);" +
                        "mapEditor.setLayerIndex(neighborParcelLayer, 10000);" +
                        "doZoom();",
                        
                        generalHelper.layerFullQualifiedName(interactiveViewerHelper.getSelectedLayer())
                )
        );

        return mapScript = sb.toString();
    }
    public void setMapScript(String mapScript) {
        this.mapScript = mapScript;
    }
    
    /////////////// MINI MAP UTILITY FUNCTIONS //////////////////
    private String miniMapScript;
    private List<ISpatialZone> miniMapHighlightedSpatialZones;
    private List<HND_LayerType> miniMapVisibleLayerTypes;
    private List<ISpatialZone> getMiniMapHighlightedSpatialZones() {
        if (miniMapHighlightedSpatialZones == null)
            miniMapHighlightedSpatialZones = new ArrayList<ISpatialZone>();
        return miniMapHighlightedSpatialZones;
    }
    public void setMiniMapHighlightedSpatialZone(ISpatialZone highlightedSpatialZone) {
        getMiniMapHighlightedSpatialZones().clear();
        getMiniMapHighlightedSpatialZones().add(highlightedSpatialZone);
    }
    public void addMiniMapHighlightedSpatialZone(ISpatialZone highlightedSpatialZone) {
        getMiniMapHighlightedSpatialZones().add(highlightedSpatialZone);
    }
    private List<HND_LayerType> getMiniMapVisibleLayerTypes() {
        if (miniMapVisibleLayerTypes == null)
            miniMapVisibleLayerTypes = new ArrayList<HND_LayerType>();
        return miniMapVisibleLayerTypes;
    }
    public void setMiniMapVisibleLayerTypes(HND_LayerType lt) {
        getMiniMapVisibleLayerTypes().clear();
        getMiniMapVisibleLayerTypes().add(lt);
    }
    public void addMiniMapVisibleLayerTypes(HND_LayerType lt) {
        getMiniMapVisibleLayerTypes().add(lt);
    }
    
    @Transactional
    public String getMiniMapScript() {
        if (miniMapScript == null) {
            StringBuffer sb = new StringBuffer(
                    "var vector_style = new OpenLayers.Style({'strokeColor': '#ff0000','strokeWidth': 3});" +
                    "var vector_style_map = new OpenLayers.StyleMap({'default': vector_style});" +
                    "vlayer.styleMap = vector_style_map;" +
                    
                    "var parcel_style = new OpenLayers.Style({" +
                    "    'fillColor': '#669933'," +
                    "    'fillOpacity': .8," +
                    "    'graphicName': 'square'," +
                    "    'label': '${label}'," +
                    "    'pointRadius': 16," +
                    "    'strokeColor': '#aaee77'," +
                    "    'strokeDashstyle': 'solid'," +
                    "    'strokeWidth': 4" +
                    "});" +
                    "" +
                    "var parcel_style_map = new OpenLayers.StyleMap( {'default': parcel_style} );" +
    
                    "parcelFeaturesLayer.styleMap = parcel_style_map;" +
                    
                    "var parcelGML, geom;"
            );
            
            sb.append(
                "parcelGML = '" + generalHelper.escapeJS(generalHelper.getSpatialZonesGML(getMiniMapHighlightedSpatialZones(), 0)) + "';" +
                "geom = new OpenLayers.Format.GML().read(parcelGML);" +
                "parcelFeaturesLayer.addFeatures(geom);"
            );
            
            for (HND_Layer layer : generalHelper.getLayerList()) {
                if (!generalHelper.layerFullQualifiedName(layer).equals(generalHelper.getPrimaryWMSLayer())) {
                    //checks if layer in db schema is also registered in WMS
                    
                    if (getNamedLayers().contains(generalHelper.layerFullQualifiedName(layer))
                            && ( getMiniMapVisibleLayerTypes().size() == 0 || getMiniMapVisibleLayerTypes().contains(layer.getLayerType()) )) {
                        //include only specified layer types if at least one is
                        sb.append(
                                generalHelper.drawLayerJS(
                                        "mapEditor",
                                        layer,
                                        MapHelper.getLayerZOrder(layer.getWmsLayerName()),
                                        true,
                                        InteractiveViewerHelper.getStyleName(
                                                LayerTheme.DEFAULT_THEME,
                                                layer.getLayerType())) );
                    }
                }
            }
            miniMapScript = sb.toString();
        }
        return miniMapScript;
    }
    public void invalidateMiniMapScript() {
        miniMapScript = null;
    }
    /////////////// MINI MAP UTILITY FUNCTIONS //////////////////



    private void addLayers(StringBuilder sb, boolean includeInfoControl) {
        for (LayerHelper.LayerDisplayDatum ldd : interactiveViewerHelper.getLayerHelper().getLayerDisplayData()) {
            HND_Layer layer = ldd.getLayer();
            if (layer.getLayerType() != HND_LayerType.RASTER) {
                if (!parcelLayerOnly || layer.getLayerType() == HND_LayerType.PARCEL)
                    if (getNamedLayers().contains(generalHelper.layerFullQualifiedName(layer)))
                        sb.append(
                                generalHelper.drawLayerJS(
                                        "mapEditor",
                                        layer,
                                        layerZOrder.get(layer.getWmsLayerName()),
                                        ldd.isVisible(),
                                        InteractiveViewerHelper.getStyleName(
                                                LayerTheme.DEFAULT_THEME,
                                                layer.getLayerType())));
    
                if (includeInfoControl && generalHelper.layerFullQualifiedName(layer).equals(getSelectedLayerFQName())) {
                    sb.append(String.format(
                        "var infoControl = new OpenLayers.Control.WMSGetFeatureInfo({" +
                        "    url: '%s'," +
                        "    title: 'test'," +
                        "    layers: [wms]," +
                        "    infoFormat: 'application/vnd.ogc.gml'," +
                        "    queryVisible: true," +
                        "    allowSelection: true," +
                        "});",
    
                        generalHelper.getWmsUrl()
                    ));
    
                    if (highlightParcelList) {
                        sb.append(
                            "infoControl.events.register('getfeatureinfo', this, updateHighlightedParcelList);" +
                            "infoControl.events.register('nogetfeatureinfo', this, unHighlightAllParcels);"
                        );
                    }
    
                    sb.append(
                        "infoControl.id = 'infoControl';" +                
                        "mapEditor.addControl(infoControl);" +
                        "infoControl.activate();"
                    );
                }
            }
        }
    }
    
    private void addSimpleLayerStyles(StringBuilder sb) {
        sb.append(
                "var vector_style = new OpenLayers.Style({'strokeColor': '#ff0000','strokeWidth': 3});" +
                "var vector_style_map = new OpenLayers.StyleMap({'default': vector_style});" +
                "vlayer.styleMap = vector_style_map;" +
                
                "var parcel_style = new OpenLayers.Style({" +
                "    'fillColor': '#669933'," +
                "    'fillOpacity': .8," +
                "    'graphicName': 'square'," +
                "    'label': '${zonename}'," +
                "    'pointRadius': 16," +
                "    'strokeColor': '#aaee77'," +
                "    'strokeDashstyle': 'solid'," +
                "    'strokeWidth': 4" +
                "});" +
                "" +
                "" +
                
                "var parcel_style_select = new OpenLayers.Style({" +
                "    'fillColor': '#FF0000'," +
                "    'fillOpacity': .8," +
                "    'graphicName': 'square'," +
                "    'label': '${zonename}'," +
                "    'pointRadius': 16," +
                "    'strokeColor': '#343434'," +
                "    'strokeDashstyle': 'solid'," +
                "    'strokeWidth': 4" +
                "});" +
                "var parcel_style_map = new OpenLayers.StyleMap({'default': parcel_style, 'select': parcel_style_select});" +
                "var readonly_parcel_style_map = new OpenLayers.StyleMap({'default': parcel_style});" +
                
                "parcelFeaturesLayer.styleMap = parcel_style_map;" +
                "neighborParcelLayer.styleMap = readonly_parcel_style_map;"
        );
    }
    
    private void addTopographicTxnLayer(StringBuilder sb) {
        final String layerName = generalHelper.getTopographicTxnsWMSLayerName();
        if ( getWMSClient().hasLayer( layerName ) ) {
            sb.append(String.format(
                    "var wms = new OpenLayers.Layer.WMS(" +
                    "    '%s'," +
                    "    '%s'," +
                    "    {layers: '%s', transparent: true, format: 'image/png'}," +
                    "    {projection: '%s'});" +
                    "wms.id = '%s';" +
                    "%s.addLayer(wms);",
                    
                    layerName,
                    generalHelper.getWmsUrl(),
                    layerName,
                    generalHelper.getWorkingSRS(),
                    layerName,
                    "mapEditor"
            ));
        }
        else
            ; //TODO: Agregar WARNING LOG

    }
    
    private void addLayerStyles(StringBuilder sb) {
        sb.append(
                "var vector_style = new OpenLayers.Style({'strokeColor': '#ff0000','strokeWidth': 3});" +
                "var vector_style_map = new OpenLayers.StyleMap({'default': vector_style});" +
                "vlayer.styleMap = vector_style_map;" +
                
                "var parcel_style = new OpenLayers.Style({" +
                "    'fillColor': '#669933'," +
                "    'fillOpacity': .8," +
                "    'graphicName': 'square'," +
                "    'label': '${label}'," +
                "    'pointRadius': 16," +
                "    'strokeColor': '#aaee77'," +
                "    'strokeDashstyle': 'solid'," +
                "    'strokeWidth': 4" +
                "});" +
                "" +
                "" +
                
                "var parcel_style_select = new OpenLayers.Style({" +
                "    'fillColor': '#3BB9FF'," +
                "    'fillOpacity': .8," +
                "    'graphicName': 'square'," +
                "    'label': '${label}'," +
                "    'pointRadius': 16," +
                "    'strokeColor': '#343434'," +
                "    'strokeDashstyle': 'solid'," +
                "    'strokeWidth': 4" +
                "});" +
                "var parcel_style_map = new OpenLayers.StyleMap({'default': parcel_style, 'select': parcel_style_select});" +
                "var readonly_parcel_style_map = new OpenLayers.StyleMap({'default': parcel_style});" +
                
                "var parcel_symbolizer_lookup = {" +
                "    '0': {" +
                "        'fillColor': '#669933', 'fillOpacity': .5, 'strokeColor': '#aaee77', 'strokeWidth': 3, 'pointRadius': 8, 'label': '${label}'" +
                "    }," +
                "    '1': {" +
                "        'fillColor': '#FF0000', 'fillOpacity': .5, 'strokeColor': '#aaee77', 'strokeWidth': 3, 'pointRadius': 8, 'label': '${label}'" +
                "    }" +
                "};" +
                
                "parcel_style_map.addUniqueValueRules('default', 'neighborLevel', parcel_symbolizer_lookup);" +
                "parcelFeaturesLayer.styleMap = parcel_style_map;" +
                "neighborParcelLayer.styleMap = readonly_parcel_style_map;"
        );
    }
    
    private void addRasterLayer(StringBuilder sb, int layerIndex) {
        String rasterLayerName = generalHelper.getRasterLayerName();

        if ( getWMSClient().getNamedLayers().contains(rasterLayerName) ) {
            sb.append(String.format(
                    "var ortho = new OpenLayers.Layer.WMS(" +
                    "    '%s'," +
                    "    '%s'," +
                    "    {layers: '%s', transparent: true, format: 'image/png'}," +
                    "    {projection: '%s'});" +
                    "ortho.id = '%s';" +
                    "ortho.setVisibility(false);" +
                    "mapEditor.addLayer(ortho);" +
                    "mapEditor.setLayerIndex(ortho, %d);",
                    
                    rasterLayerName,
                    generalHelper.getWmsUrl(),
                    rasterLayerName,
                    generalHelper.getWorkingSRS(),
                    rasterLayerName,
                    layerIndex
            ));
        }
    }
    
    private void addInitialFeatures(StringBuilder sb) {
        if (features != null || readOnlyFeatures != null) {
            String allSZGml;
            sb.append("var allSZGml, spatialZoneWKT, geom;");
            
            if (features != null) {
                allSZGml = generalHelper.getSpatialZonesGML(features, 0);
                sb.append(mapScriptUtil(allSZGml, 0));
            }

            if (readOnlyFeatures != null) {
                allSZGml = generalHelper.getSpatialZonesGML(readOnlyFeatures, 1);
                sb.append(mapScriptUtil(allSZGml, 1));
            }
        }
    }
    private String mapScriptUtil(String gml, int neighborLevel) {
        return String.format("allSZGml = '%s';" +
                "geom = new OpenLayers.Format.GML().read(allSZGml);" +
                (neighborLevel == 0 ? "parcelFeaturesLayer" : "neighborParcelLayer") +
                ".addFeatures(geom);",
                
                StringEscapeUtils.escapeJavaScript(gml));
    }

    private void addSnapCapability(StringBuilder sb) {
        sb.append(
                "snap = new OpenLayers.Control.Snapping({" +
                "    layer: polyLayer," +
                "    targets: [polyLayer, parcelFeaturesLayer, vlayer]," +
                "    greedy: false" +
                "});" +
                "snap.activate();"
                
                /*
                "var panel = new OpenLayers.Control.Panel({" +
                "    displayClass: 'olControlEditingToolbar'" +
                "});" +
                */
                /*
                "draw = new OpenLayers.Control.DrawFeature(" +
                "    polyLayer, OpenLayers.Handler.Polygon," +
                "    {displayClass: 'olControlDrawFeaturePoint', title: '" + generalHelper.getResBundle().loadMessage("map.draw_feature") + "', handlerOptions: {holeModifier: 'altKey'}}" +
                ");" +
                "modify = new OpenLayers.Control.ModifyFeature(" +
                "    polyLayer, {displayClass: 'olControlModifyFeature', title: '" + generalHelper.getResBundle().loadMessage("map.modify_feature") + "'}" +
                ");"
                */            
        );
    }
    
    private void addMapPanelControls(StringBuilder sb) {
        sb.append(
                "var btnSelection = new OpenLayers.Control.Button({title: 'AAA', displayClass: 'selectionButton', type: OpenLayers.Control.TYPE_BUTTON," +
                "    trigger: function(){ var infoControl = getControl('infoControl');" +
                "        infoControl.activate();" +
                "        navigationControl.deactivate();" +
                "        zoomBoxControl.deactivate();}});" +

                "var btnQuickSelection = new OpenLayers.Control.Button({title: 'BBB', displayClass: 'quickSelectionButton', type: OpenLayers.Control.TYPE_TOGGLE," +
                "    eventListeners: {" +
                "        'activate': function() {highlightOnlyOne = true;}," +
                "        'deactivate': function() {highlightOnlyOne = false;}}});" +

                "mapPanel.addControls([btnSelection, btnQuickSelection]);");
    }


    private List<HND_Layer> getLayerList() {
        if (layerList == null) {
            if (interactiveViewerHelper != null)
                layerList = interactiveViewerHelper.getLayerList();
            else
                layerList = HND_LayerDAO.loadLayers(true);
        }
        return layerList;
    }

    public boolean isShowEditControls() {
        return showEditControls;
    }
    public void setShowEditControls(boolean showEditControls) {
        this.showEditControls = showEditControls;
    }

    public boolean isHighlightParcelList() {
        return highlightParcelList;
    }
    public void setHighlightParcelList(boolean highlightParcelList) {
        this.highlightParcelList = highlightParcelList;
    }

    public boolean isParcelLayerOnly() {
        return parcelLayerOnly;
    }
    public void setParcelLayerOnly(boolean parcelLayerOnly) {
        this.parcelLayerOnly = parcelLayerOnly;
    }
    
    
    public List<? extends ISpatialZone> getFeatures() {
        return features;
    }
    public void setFeatures(List<? extends ISpatialZone> features) {
        this.features = features;
    }

    public List<? extends ISpatialZone> getReadOnlyFeatures() {
        return readOnlyFeatures;
    }
    public void setReadOnlyFeatures(List<? extends ISpatialZone> readOnlyFeatures) {
        this.readOnlyFeatures = readOnlyFeatures;
    }


    public String getSelectedLayerFQName() {
        if (selectedLayerFQName == null) {
            String fqn = generalHelper.layerFullQualifiedName(interactiveViewerHelper.getSelectedLayer());
            selectedLayerFQName = fqn != null ? fqn : "";
        }
        return selectedLayerFQName;
    }
    public void setSelectedLayerFQName(String selectedLayerFQName) {
        this.selectedLayerFQName = selectedLayerFQName;
    }
    
    
    private WMSClient111 getWMSClient() {
        if (interactiveViewerHelper != null)
            return interactiveViewerHelper.getWMSClient();
        
        if (localWMSClient == null) {
            try {
                localWMSClient = new WMSClient111(new URL(generalHelper.getWmsCapabilitiesUrl111()));
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return localWMSClient;
    }
}
