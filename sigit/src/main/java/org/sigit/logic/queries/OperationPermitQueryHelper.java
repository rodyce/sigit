package org.sigit.logic.queries;

import org.sigit.dao.hnd.administrative.HND_OperationPermitDAO;
import org.sigit.logic.workflow.permits.OperationPermitHelper;
import org.sigit.model.hnd.administrative.HND_OperationPermit;
import org.sigit.model.hnd.administrative.HND_OperationPermitType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Scope;

@Component("operationPermitQueryHelper")
@Scope(value="sigit-conversation")
public class OperationPermitQueryHelper extends QueryHelper<HND_OperationPermit> implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
    @Autowired
    private OperationPermitHelper operationPermitHelper;

    private HND_OperationPermit selected;
    
    
    public OperationPermitQueryHelper() {
        setSearchSelector(QueryOption.BY_PRESENTATION_NO.name());
    }

    @Override
    @Transactional
    public List<HND_OperationPermit> getQueryResultList() {
        try {
            if (queryResultList == null && getSearchText() != null) {
                switch (getQueryOption()) {
                case BY_CURRENT_STATE:
                    Boolean approved = getSearchText().equalsIgnoreCase("IN_PROCESS") ? null : getSearchText().equalsIgnoreCase("APPROVED");
                    queryResultList = HND_OperationPermitDAO.loadOperationPermitsByCurrentState(approved);
                    break;
                case BY_PRESENTATION_NO:
                    queryResultList = new ArrayList<HND_OperationPermit>();
                    HND_OperationPermit permit = HND_OperationPermitDAO.loadOperationPermitByPresentationNo( Long.parseLong(getSearchText()) );
                    if (permit != null)
                        queryResultList.add( permit );
                    break;
                case BY_TYPE:
                    HND_OperationPermitType permitType = HND_OperationPermitType.valueOf(getSearchText());
                    queryResultList = HND_OperationPermitDAO.loadOperationPermitsByType(permitType);
                    break;
                default:
                    queryResultList = new ArrayList<HND_OperationPermit>();
                    break;
                }
            }
        }
        catch (Throwable t) {
            FacesContext.getCurrentInstance().addMessage(
                    "",
                    new FacesMessage(
                            FacesMessage.SEVERITY_ERROR,
                            t.getMessage(),
                            ""
                    )
            );
            t.printStackTrace();
        }
        
        return queryResultList;
    }

    @Override
    public HND_OperationPermit getSelected() {
        if (selected == null) return null;
        
        return operationPermitHelper.getPermit();
    }

    @Override
    public void setSelected(HND_OperationPermit selected) {
        if (this.selected != selected) {
            this.selected = selected;
            operationPermitHelper.setPermit(selected);
        }
    }

    public boolean isApproved() {
        return getSelected() != null &&
                getSelected().getApproved() != null &&
                getSelected().getApproved();
    }

    @Override
    public QuickSearchTextControlType getQuickSearchTextControlType() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String doNewSearch() {
        return doNewSearch("/queries/operationPermitSearchResult.xhtml", "/queries/operationPermits.xhtml");
    }


    public QueryOption getQueryOption() {
        String ss = getSearchSelector();
        if (ss == null || ss.trim().equals("")) return null;
        
        return QueryOption.valueOf(getSearchSelector());
    }
    
    public static enum QueryOption {
        BY_PRESENTATION_NO,
        BY_CURRENT_STATE,
        BY_TYPE
    }

    public void destroy() {
    }
}
