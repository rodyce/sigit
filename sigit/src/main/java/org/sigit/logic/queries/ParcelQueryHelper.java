package org.sigit.logic.queries;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.sigit.dao.hnd.cadastre.HND_ParcelDAO;
import org.sigit.logic.general.GeneralHelper;
import org.sigit.logic.general.SpatialAnalysisHelper;
import org.sigit.logic.viewer.MapHelper;
import org.sigit.model.hnd.cadastre.HND_LayerType;
import org.sigit.model.hnd.cadastre.HND_Parcel;
import org.sigit.model.hnd.cadastre.HND_TaxationStatusType;
import org.sigit.model.ladm.spatialunit.LA_RequiredRelationshipSpatialUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.vividsolutions.jts.geom.Geometry;


@Component("parcelQueryHelper")
@Scope(value="sigit-conversation")

public class ParcelQueryHelper extends QueryHelper<HND_Parcel> implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private String searchSelector = QueryOption.BY_CADASTRAL_KEY.name();
    private String searchText;
    
    private List<HND_Parcel> selectedParcelPredecessors;
    private List<HND_Parcel> selectedParcelSuccessors;
    
    private String mapOptions;
    private String mapScript;
    
    private HND_Parcel selected;
    
    
    @Autowired
    private SpatialAnalysisHelper spatialAnalysisHelper;
    
    @Autowired
    private GeneralHelper generalHelper;
    
    @Autowired
    private MapHelper mapHelper;

    
    @Override
    public String getSearchSelector() {
        return searchSelector;
    }
    @Override
    public void setSearchSelector(String searchSelector) {
        this.searchSelector = searchSelector;
    }
    
    @Override
    public String getSearchText() {
        return searchText;
    }
    @Override
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }
    
    @Override
    @Transactional
    public List<HND_Parcel> getQueryResultList() {
        if (queryResultList == null && searchText != null) {
            switch (getQueryOption()) {
            case BY_FIELD_TAB:
                queryResultList = HND_ParcelDAO.loadParcelsByFieldTab(searchText);
                break;
            case BY_CADASTRAL_KEY:
                queryResultList = HND_ParcelDAO.loadParcelsByCadastralKey(searchText);
                break;
            case BY_MUNICIPAL_KEY:
                queryResultList = HND_ParcelDAO.loadParcelsByMunicipalKey(searchText);
                break;
            case IN_TRANSACTION:
                boolean inTransaction = searchText.equalsIgnoreCase("YES");
                queryResultList = HND_ParcelDAO.loadParcelsByIsInTransaction(inTransaction);
                break;
            case BY_TAX_STATUS:
                HND_TaxationStatusType taxStatusType = HND_TaxationStatusType.valueOf(searchText);
                queryResultList = HND_ParcelDAO.loadParcelsByTaxStatus(taxStatusType);
                break;
            default:
                queryResultList = HND_ParcelDAO.loadParcelsByFieldTab(searchText);
            }
        }
        return queryResultList;
    }
    @Override
    public void setQueryResultList(List<HND_Parcel> queryResultList) {
        this.queryResultList = queryResultList;
    }
    
    @Override
    public HND_Parcel getSelected() {
        if (selected == null) {
            if (getQueryResultList() != null && getQueryResultList().size() > 0) {
                setSelected(getQueryResultList().get(0));
            }
        }
        return selected;
    }
    @Override
    public void setSelected(HND_Parcel selected) {
        if (this.selected != selected) {
            this.selected = selected;
            spatialAnalysisHelper.setSpatialZone(this.selected);
            selectedParcelPredecessors = null;
            selectedParcelSuccessors = null;
            mapOptions = null;
            mapScript = null;
        }
    }
    
    @Override
    public String doNewSearch() {
        return doNewSearch("/queries/parcelSearchResult.xhtml", "/queries/parcels.xhtml");
    }
    

    public List<HND_Parcel> getSelectedParcelPredecessors() {
        HND_Parcel hndParcel;
        if (selectedParcelPredecessors == null) {
            selectedParcelPredecessors = new ArrayList<HND_Parcel>();
            for (LA_RequiredRelationshipSpatialUnit rrsu : getSelected().getRequiredRelationshipSpatialUnits1()) {
                if (rrsu.getSpatialUnit1() instanceof HND_Parcel) {
                    hndParcel = (HND_Parcel) rrsu.getSpatialUnit2();
                    if (!hndParcel.isAlive())
                        selectedParcelPredecessors.add(hndParcel);
                }
            }
        }
        return selectedParcelPredecessors;
    }
    public void setSelectedParcelPredecessors(List<HND_Parcel> selectedParcelPredecessors) {
        this.selectedParcelPredecessors = selectedParcelPredecessors;
    }
    
    public List<HND_Parcel> getSelectedParcelSuccessors() {
        HND_Parcel hndParcel;
        if (selectedParcelSuccessors == null) {
            selectedParcelSuccessors = new ArrayList<HND_Parcel>();
            for (LA_RequiredRelationshipSpatialUnit rrsu : getSelected().getRequiredRelationshipSpatialUnits2()) {
                if (rrsu.getSpatialUnit2() instanceof HND_Parcel) {
                    hndParcel = (HND_Parcel) rrsu.getSpatialUnit1();
                    selectedParcelSuccessors.add(hndParcel);
                }
            }
        }
        return selectedParcelSuccessors;
    }
    public void setSelectedParcelSuccessors(List<HND_Parcel> selectedParcelSuccessors) {
        this.selectedParcelSuccessors = selectedParcelSuccessors;
    }
    
    public String getMapOptions() {
        if (selected == null) return "";
        
        if (mapOptions == null) {
            double x1, y1, x2, y2;
            
            List<Geometry> envelopeList = new ArrayList<Geometry>();
            envelopeList.add(selected.getShape().getEnvelope());

            double[] dd = generalHelper.calcZoomEnvelope(envelopeList);
            x1 = dd[0]; y1 = dd[1]; x2 = dd[2]; y2 = dd[3];

            mapOptions = String.format("{ controls:[], " +
                    "maxExtent: new OpenLayers.Bounds(%f, %f, %f, %f), " +
                    "maxResolution: 26.280468750000182, " +
                    "projection: new OpenLayers.Projection('%s'), " +
                    "displayProjection: new OpenLayers.Projection('%s'), " +
                    "units: 'm' }",
                    
                    x1, y1, x2, y2,
                    generalHelper.getWorkingSRS(),
                    generalHelper.getWorkingSRS());
        }
        
        return mapOptions;
    }

    public String mapScript(String mapControlName) {
        if (mapScript == null) {
            mapHelper.invalidateMiniMapScript();
            
            mapHelper.setMiniMapHighlightedSpatialZone(getSelected());
            mapHelper.setMiniMapVisibleLayerTypes(HND_LayerType.PARCEL);
            mapScript = mapHelper.getMiniMapScript();
        }
        return mapScript;
    }
    
    @Override
    public QuickSearchTextControlType getQuickSearchTextControlType() {
        QuickSearchTextControlType qstct = null;
        switch (getQueryOption()) {
        case BY_FIELD_TAB:
        case BY_MUNICIPAL_KEY:
        case BY_CADASTRAL_KEY:
            qstct = QuickSearchTextControlType.TEXT_BOX;
            break;
        case IN_TRANSACTION:
        case BY_TAX_STATUS:
            qstct = QuickSearchTextControlType.VALUE_LIST;
            break;
        }
        
        return qstct;
    }

    public QueryOption getQueryOption() {
        String ss = getSearchSelector();
        if (ss == null || ss.trim().equals("")) return null;
        
        return QueryOption.valueOf(getSearchSelector());
    }
    
    public static enum QueryOption {
        BY_FIELD_TAB,
        BY_MUNICIPAL_KEY,
        BY_CADASTRAL_KEY,
        IN_TRANSACTION,
        BY_TAX_STATUS
    }
}
