package org.sigit.logic.queries;

import java.io.Serializable;
import java.util.List;

public abstract class QueryHelper<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    private String searchSelector;
    private String searchText;
    private String activeSection;

    protected List<T> queryResultList;


    
    public String getSearchSelector() {
        return searchSelector;
    }
    public void setSearchSelector(String searchSelector) {
        this.searchSelector = searchSelector;
    }
    
    public String getSearchText() {
        return searchText;
    }
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }
    
    public String getActiveSection() {
        return activeSection;
    }
    public void setActiveSection(String activeSection) {
        this.activeSection = activeSection;
    }
    
    
    public abstract List<T> getQueryResultList();
    
    public void setQueryResultList(List<T> queryResultList) {
        this.queryResultList = queryResultList;
    }
    
    
    public abstract T getSelected();
    
    public abstract void setSelected(T selected);
    
    public abstract QuickSearchTextControlType getQuickSearchTextControlType();

    public abstract String doNewSearch();

    public int min(int a, int b) {
        return a < b ? a : b;
    }
    
    public int max(int a, int b) {
        return a > b ? a : b;
    }
    
    protected String doNewSearch(String searchResultView, String resultView) {
        queryResultList = null;
        setSelected(null);
        
        List<T> queryResult = getQueryResultList();
        if (queryResult != null) {
            if (queryResult.size() > 1)
                return searchResultView;
            else if (queryResult.size() == 1)
                setSelected(queryResult.get(0));
        }
            
        return resultView;
    }

}
