package org.sigit.logic.queries;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.sigit.dao.hnd.administrative.HND_BuildingPermitDAO;
import org.sigit.logic.workflow.permits.BuildingPermitHelper;
import org.sigit.model.hnd.administrative.HND_BuildingPermit;
import org.sigit.model.hnd.administrative.HND_BuildingPermitType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component("buildingPermitQueryHelper")
@Scope(value="sigit-conversation")
public class BuildingPermitQueryHelper extends QueryHelper<HND_BuildingPermit> implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private HND_BuildingPermit selected;

    @Autowired
    private BuildingPermitHelper buildingPermitHelper;
    
    
    public BuildingPermitQueryHelper() {
        setSearchSelector(QueryOption.BY_PRESENTATION_NO.name());
    }

    @Override
    @Transactional
    public List<HND_BuildingPermit> getQueryResultList() {
        try {
            if (queryResultList == null && getSearchText() != null) {
                switch (getQueryOption()) {
                case BY_CURRENT_STATE:
                    Boolean approved = getSearchText().equalsIgnoreCase("IN_PROCESS") ? null : getSearchText().equalsIgnoreCase("APPROVED");
                    queryResultList = HND_BuildingPermitDAO.loadBuildingPermitsByCurrentState(approved);
                    break;
                case BY_PRESENTATION_NO:
                    queryResultList = new ArrayList<HND_BuildingPermit>();
                    HND_BuildingPermit permit = HND_BuildingPermitDAO.loadBuildingPermitByPresentationNo( Long.parseLong(getSearchText()) );
                    if (permit != null)
                        queryResultList.add( permit );
                    break;
                case BY_TYPE:
                    HND_BuildingPermitType permitType = HND_BuildingPermitType.valueOf(getSearchText());
                    queryResultList = HND_BuildingPermitDAO.loadBuildingPermitsByType(permitType);
                    break;
                default:
                    queryResultList = new ArrayList<HND_BuildingPermit>();
                    break;
                }
            }
        }
        catch (Throwable t) {
            FacesContext.getCurrentInstance().addMessage(
                    "",
                    new FacesMessage(
                            FacesMessage.SEVERITY_ERROR,
                            t.getMessage(),
                            ""
                    )
            );
            t.printStackTrace();
        }
        
        return queryResultList;
    }

    @Override
    public HND_BuildingPermit getSelected() {
        return selected;
    }
    @Override
    public void setSelected(HND_BuildingPermit selected) {
        if (this.selected != selected) {
            this.selected = selected;
            buildingPermitHelper.setPermit(selected);
        }
    }

    public boolean isApproved() {
        return getSelected() != null &&
                getSelected().getApproved() != null &&
                getSelected().getApproved();
    }

    @Override
    public QuickSearchTextControlType getQuickSearchTextControlType() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String doNewSearch() {
        return doNewSearch("/queries/buildingPermitSearchResult.xhtml", "/queries/buildingPermits.xhtml");
    }

    
    public QueryOption getQueryOption() {
        String ss = getSearchSelector();
        if (ss == null || ss.trim().equals("")) return null;
        
        return QueryOption.valueOf(getSearchSelector());
    }
    
    public static enum QueryOption {
        BY_PRESENTATION_NO,
        BY_CURRENT_STATE,
        BY_TYPE
    }
    
    public void destroy() {
    }
}
