package org.sigit.logic.workflow.transaction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.sigit.commons.di.CtxComponent;
import org.sigit.dao.hnd.administrative.HND_TransactionMetaDataDAO;
import org.sigit.dao.hnd.ladmshadow.ParcelDAO;
import org.sigit.dao.hnd.ladmshadow.PropertyDAO;
import org.sigit.i18n.ResourceBundleHelper;
import org.sigit.logic.general.GeneralHelper;
import org.sigit.logic.ladm.PropertyUtil;
import org.sigit.logic.viewer.InteractiveViewerHelper;
import org.sigit.logic.viewer.MapHelper;
import org.sigit.logic.viewer.toolbox.ViewerCallable;
import org.sigit.model.commons.IParcel;
import org.sigit.model.commons.IProperty;
import org.sigit.model.commons.ISpatialZone;
import org.sigit.model.hnd.administrative.HND_TransactionMetaData;
import org.sigit.model.hnd.ladmshadow.Parcel;
import org.sigit.model.hnd.ladmshadow.Property;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.io.WKTWriter;

@Component("dataEntryHelper")
@Scope(value="sigit-conversation")
public class DataEntryHelper implements ViewerCallable, Serializable {
    private static final long serialVersionUID = 1L;

    private TransactionHelper transactionHelper;
    
    private List<SelectItem> siRequestedParcels;

    private UUID transactionId;
    private long presentationNo;
    
    private List<Property> requestedProperties;
    private List<Property> neighborProperties;
    private List<Parcel> requestedParcels;
    private List<Parcel> neighborParcels;
    
    private List<String> presentationWkts = new ArrayList<String>();
    private ResourceBundleHelper resBundle;
    
    private GeneralHelper generalHelper = (GeneralHelper) CtxComponent.getInstance("generalHelper");
    private MapHelper mapHelper = (MapHelper) CtxComponent.getInstance(MapHelper.NAME);
    private InteractiveViewerHelper interactiveViewerHelper = (InteractiveViewerHelper) CtxComponent.getInstance(InteractiveViewerHelper.NAME);
    
    private boolean canMutateRRR = true;
    private boolean canSplit = true;
    private boolean canMerge = true;
    private boolean canEditData = true;
    
    
    @PostConstruct
    @Transactional
    void init() {
        transactionHelper = (TransactionHelper) CtxComponent.getInstance("transactionHelper");
        transactionId = transactionHelper.getTransactionId();
        presentationNo = transactionHelper.getPresentationNo();
        
        interactiveViewerHelper.setTransactionId(transactionId);
        interactiveViewerHelper.getMergeHelper().setViewerCallable(this);
        interactiveViewerHelper.setSelectedZone( getRequestedParcels().get(0) );
        interactiveViewerHelper.addAditionalReRenderId("drawParcelFunctions");
        interactiveViewerHelper.addAditionalReRenderId("requestedParcelList");
        interactiveViewerHelper.setSplitCallable(new DataEntrySplitViewerCallable(this));
        interactiveViewerHelper.setAllowEditing(true);

        mapHelper.setHighlightParcelList(false);
        mapHelper.setShowEditControls(true);
        
        HND_TransactionMetaData tmd = HND_TransactionMetaDataDAO.loadRequestMetaDataByRequestType(
                transactionHelper.getTransaction().getRequestType());
        if (tmd != null) {
            canMutateRRR = tmd.isCanMutateRRR();
            canSplit = tmd.isCanSplit();
            canMerge = tmd.isCanMerge();
            canEditData = tmd.isCanEditData();
        }
        
        updatePresentationWkts();
    }
    
    
    public UUID getTransactionId() {
        return transactionId;
    }
    
    public long getPresentationNo() {
        return presentationNo;
    }

    public ISpatialZone getSelectedZone() {
        return interactiveViewerHelper.getSelectedZone();
    }
    
    
    public Double getEnvelopeLeft() {
        return interactiveViewerHelper.getEnvelopeLeft();
    }
    
    public Double getEnvelopeBottom() {
        return interactiveViewerHelper.getEnvelopeBottom();
    }
    
    public Double getEnvelopeRight() {
        return interactiveViewerHelper.getEnvelopeRight();
    }
    
    public Double getEnvelopeTop() {
        return interactiveViewerHelper.getEnvelopeTop();
    }

    
    @Transactional
    public List<Property> getRequestedProperties() {
        if (requestedProperties == null) {
            if (this.transactionId != null)
                requestedProperties = PropertyDAO.loadRequestedPropertiesByPresentationNo(this.presentationNo);
            else
                requestedProperties = new ArrayList<Property>();
        }
        
        return requestedProperties;
    }
    
    public void setRequestedProperties(List<Property> requestedProperties) {
        this.requestedProperties = requestedProperties;
    }
    
    
    @Transactional
    public List<Property> getNeighborProperties() {
        if (neighborProperties == null) {
            neighborProperties = PropertyDAO.loadNeighborPropertiesByPresentationNo(this.presentationNo);
        }
        
        return neighborProperties;
    }
    
    public void setNeighborProperties(List<Property> neighborProperties) {
        this.neighborProperties = neighborProperties;
    }
    
    
    @Transactional
    public List<Parcel> getRequestedParcels() {
        if (requestedParcels == null) {
            requestedParcels = ParcelDAO.loadRequestedParcelsByPresentationNo(this.presentationNo);
        }
        
        return requestedParcels;
    }
    
    public void setRequestedParcels(List<Parcel> requestedParcels) {
        this.requestedParcels = requestedParcels;
    }
    
    
    @Transactional
    public List<Parcel> getNeighborParcels() {
        if (neighborParcels == null) {
            neighborParcels = ParcelDAO.loadNeighborParcelsByPresentationNo(this.presentationNo);
        }
        
        return neighborParcels;
    }
    
    public void setNeighborParcels(List<Parcel> neighborParcels) {
        this.neighborParcels = neighborParcels;
    }
    
    
    @Transactional
    public String getRequestedParcelsGML() {
        return generalHelper.getSpatialZonesGML(this.getRequestedParcels(), 0);
    }
    
    @Transactional
    public String getNeighborParcelsGML() {
        return generalHelper.getSpatialZonesGML(this.getNeighborParcels(), 1);
    }
    
    
    public TransactionHelper getTransactionHelper() {
        return transactionHelper;
    }
    
    
    public ResourceBundleHelper getResBundle() {
        if (resBundle == null) {
            resBundle = generalHelper.getResBundle();
        }
        return resBundle;
    }

    private void updatePresentationWkts() {
        presentationWkts.clear();

        List<Property> propertyList = PropertyDAO.loadPropertiesByPresentationNo(this.presentationNo);
        WKTWriter wktWriter = new WKTWriter();
        for (Property prop : propertyList) {
            for (Polygon p : PropertyUtil.polygonsFromProperty(prop))
                presentationWkts.add(wktWriter.write(p));
        }
    }

    
    
    public List<SelectItem> getSiRequestedParcels() {
        if (siRequestedParcels == null) {
            siRequestedParcels = new ArrayList<SelectItem>();
            /*
            for (Parcel p : getRequestedParcels())
                siRequestedParcels.add(new SelectItem(p, parcelText(p)));
                */
        }
        return siRequestedParcels;
    }
    
    public boolean isEditing() {
        return interactiveViewerHelper.isEditing();
    }

    
    public void afterMerge(IParcel parcel1, IProperty property1,
            IParcel parcel2, IProperty property2, Polygon newShape,
            Date timeStamp) {
        setRequestedParcels(null);
        setNeighborParcels(null);
    }

    
    public void doBorderOperation(
            InteractiveViewerHelper interactiveViewerHelper) {
    }

    
    public boolean isCanMutateRRR() {
        return canMutateRRR;
    }

    
    public boolean isCanSplit() {
        return canSplit;
    }

    
    public boolean isCanMerge() {
        return canMerge;
    }

    
    public boolean isCanEditData() {
        return canEditData;
    }

}
