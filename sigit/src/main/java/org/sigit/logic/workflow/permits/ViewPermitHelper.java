package org.sigit.logic.workflow.permits;

import java.io.Serializable;

import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

@Component("viewPermitHelper")
@Scope(value="sigit-conversation")
public class ViewPermitHelper implements Serializable {
    private static final long serialVersionUID = 1L;
    
    boolean renderData = false;


    public boolean isRenderData() {
        return renderData;
    }
    public void setRenderData(boolean renderData) {
        this.renderData = renderData;
    }
}
