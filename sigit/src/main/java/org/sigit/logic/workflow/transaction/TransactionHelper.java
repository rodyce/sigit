package org.sigit.logic.workflow.transaction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.sigit.commons.conversation.annotations.Begin;
import org.sigit.commons.di.CtxComponent;
import org.sigit.commons.exception.DataValidationException;
import org.sigit.commons.exception.LockedException;
import org.sigit.commons.exception.SigitException;
import org.sigit.dao.SigitDAO;
import org.sigit.dao.hnd.administrative.HND_MunicipalTransactionDAO;
import org.sigit.dao.hnd.cadastre.HND_ParcelDAO;
import org.sigit.dao.ladm.administrative.LA_AdministrativeSourceDAO;
import org.sigit.dao.ladm.external.ExtPartyDAO;
import org.sigit.logic.ladmshadow.Util;
import org.sigit.logic.queries.TransactionQueryHelper;
import org.sigit.logic.workflow.ArchiveRepository;
import org.sigit.logic.workflow.AttachedFileData;
import org.sigit.logic.workflow.BusinessProcess;
import org.sigit.logic.workflow.ProcessConfig;
import org.sigit.logic.workflow.Task;
import org.sigit.logic.workflow.WorkflowHelperBase;
import org.sigit.logic.workflow.annotations.BeginTask;
import org.sigit.logic.workflow.annotations.EndTask;
import org.sigit.model.commons.ISpatialZone;
import org.sigit.model.hnd.administrative.HND_ActivityType;
import org.sigit.model.hnd.administrative.HND_MunicipalTransaction;
import org.sigit.model.hnd.administrative.HND_MunicipalTransactionType;
import org.sigit.model.hnd.administrative.HND_SpatialZoneInTransaction;
import org.sigit.model.hnd.administrative.HND_TransactionSubType;
import org.sigit.model.hnd.cadastre.HND_Parcel;
import org.sigit.model.hnd.cadastre.HND_SpatialZone;
import org.sigit.model.hnd.special.HND_Lock;
import org.sigit.model.ladm.administrative.LA_AdministrativeSource;
import org.sigit.model.ladm.external.ExtParty;
import org.sigit.view.NavigationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component("transactionHelper")
@Scope(value="sigit-conversation")
public class TransactionHelper extends WorkflowHelperBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private static final Logger log = LoggerFactory.getLogger(TransactionHelper.class);

    private HND_MunicipalTransaction transaction;
    private UUID transactionId;
    private long presentationNo;
    
    private ExtParty party;
    private ExtParty favoredParty;
    
    private boolean attachInfo;
    private boolean finished = false;
    
    private Set<HND_SpatialZone> requestedZoneSet;
    private List<HND_SpatialZone> requestedZoneList;
    
    private TransactionLogicHelper transactionLogicHelper = new TransactionLogicHelper(this);
    private HND_MunicipalTransactionType requestType;
    private HND_TransactionSubType requestSubType;
    private List<SelectItem> requestSubTypes;


    @Autowired
    private NavigationHelper navigationHelper;
    
    @Autowired
    private ArchiveRepository archiveRepository;

    @Transactional
    public HND_MunicipalTransaction getTransaction() {
        if (transaction == null) {
            if (presentationNo == 0)
                transaction = new HND_MunicipalTransaction();
            else {
                transaction = HND_MunicipalTransactionDAO.loadRequestByPresentationNo(presentationNo);
                transactionId = transaction.getId();
            }
        }
        return transaction;
    }
    public void setTransaction(HND_MunicipalTransaction transaction) {
        this.transaction = transaction;
    }

    public ExtParty getParty() {
        return party;
    }
    public void setParty(ExtParty party) {
        this.party = party;
    }

    public ExtParty getFavoredParty() {
        return favoredParty;
    }
    public void setFavoredParty(ExtParty favoredParty) {
        this.favoredParty = favoredParty;
    }
    
    public UUID getTransactionId() {
        return transactionId;
    }
    public void setTransactionId(UUID transactionId) {
        this.transactionId = transactionId;
    }
    
    public long getPresentationNo() {
        return presentationNo;
    }
    public void setPresentationNo(long presentationNo) {
        this.presentationNo = presentationNo;
    }

    public String getPresentationNoStr() {
        return String.valueOf(presentationNo);
    }
    public void setPresentationNoStr(String presentationNoStr) {
        this.presentationNo = Long.parseLong(presentationNoStr);
    }
    
    public String getPaddedPresentationNo() {
        return String.format("%06d", getPresentationNo());
    }
    
    
    public UUID getPartyId() {
        if (party != null)
            return party.getExtPID(); 
        return null;
    }
    
    
    public String getPartyFormalIdentity() {
        if (party != null)
            return party.getFormalIdentity();
        return "";
    }
    public void setPartyFormalIdentity(String partyFormalIdentity) {
    }
    
    
    public String getPartyName() {
        if (party != null)
            return party.getName();
        return "";
    }
    
    
    public UUID getFavoredPartyId() {
        if (favoredParty != null)
            return favoredParty.getExtPID();
        return null;
    }
    
    public String getFavoredPartyFormalIdentity() {
        if (favoredParty != null)
            return favoredParty.getFormalIdentity();
        return "";
    }
    
    public String getFavoredPartyName() {
        if (favoredParty != null)
            return favoredParty.getName();
        return "";
    }
    
    public Set<HND_SpatialZone> getRequestedZoneSet() {
        if (requestedZoneSet == null) {
            requestedZoneSet = new HashSet<HND_SpatialZone>();
            if (getTransaction().getSpatialZoneInTransactions().size() > 0)
                for (HND_SpatialZoneInTransaction szit : getTransaction().getSpatialZoneInTransactions())
                    if (szit.isUserRequested())
                        requestedZoneSet.add(szit.getSpatialZone());
        }
        return requestedZoneSet;
    }
    public void setRequestedZoneSet(Set<HND_SpatialZone> requestedZoneSet) {
        this.requestedZoneSet = requestedZoneSet;
    }
    
    
    @Transactional
    public List<HND_SpatialZone> getRequestedZoneList() {
        if (requestedZoneList == null) {
            requestedZoneList = new ArrayList<HND_SpatialZone>();
            requestedZoneList.addAll(getRequestedZoneSet());
        }
        return requestedZoneList;
    }
    public void setRequestedZoneList(List<HND_SpatialZone> requestedZoneList) {
        this.requestedZoneList = requestedZoneList;
    }
    
    /*
    public void addParcelData() {
        if (selectedParcelData != null) {
            if (!getParcelDataList().contains(selectedParcelData))
                getParcelDataList().add(selectedParcelData);
        }
    }
    public String deleteSelectedParcelData() {
        parcelDataList.remove(currentRow);
        return null;
    }
    */
    
    
    @Transactional
    public List<LA_AdministrativeSource> getFileSources() {
        if (fileSources == null) {
            //TODO: we could add an ordering criterion
            fileSources = new ArrayList<LA_AdministrativeSource>();
            for (LA_AdministrativeSource source : getTransaction().getSources())
                fileSources.add(source);
        }
        
        return fileSources;
    }
    
    
    @Transactional
    public void acceptSelectedFile() {
        if (transaction != null && transaction.getPresentationNo() == null) {
            //If there is no presentation number, then...
            if ("0".equals(editMode)) { //agregando
                if (null == attachedFiles)
                    attachedFiles = new ArrayList<AttachedFileData>();
                attachedFiles.add(selectedFile);
            }
            else if ("1".equals(editMode)) { //editando
                attachedFiles.set(currentRow, selectedFile);
            }
        } else if (transaction != null) {
            //If there IS presentation number, then this transaction was already submitted
            //NOTE: We will only use internal archives
            LA_AdministrativeSource newAdminSource = archiveRepository.addToRepository(appOptions, selectedFile, false);
            SigitDAO.save(newAdminSource);
            transaction.getSources().add(newAdminSource);

            HND_MunicipalTransactionDAO.save(transaction);
        }
        
        setSelectedFile(null);
        fileSources = null;
    }
    
    
    public boolean isAttachInfo() {
        return attachInfo;
    }
    
    public void setAttachInfo(boolean attachInfo) {
        this.attachInfo = attachInfo;
    }
    
    public boolean isFinished() {
        return finished;
    }
    public void setFinished(boolean finished) {
        this.finished = finished;
    }
    
    
    public String hasAttachedFiles() {
        if (attachInfo)
            return "si_adjunta";
        
        return "no_adjunta";
    }
    
    private void setPresentationData() {
        HND_MunicipalTransaction hndTransaction = getTransaction();
        
        ExtParty extParty = ExtPartyDAO.loadExtPartyByExtPID(getPartyId());
        
        
        //Phase 1: Set the general data
        hndTransaction.setCurrentActivity(HND_ActivityType.PRESENTATION);
        hndTransaction.setPresentationDate(new Date());
        hndTransaction.setExtParty(extParty);
        hndTransaction.setReceptionistUserName(credentials.getUsername());
        hndTransaction.setReceptionistFullName(credentials.getLoggedUserFullName());
        
        //Phase 2: Set attached files data
        if (getAttachedFiles() != null) {
            Set<LA_AdministrativeSource> adminSources = hndTransaction.getSources();
            if (adminSources == null) {
                adminSources = new HashSet<LA_AdministrativeSource>();
                hndTransaction.setSources(adminSources);
            }
            
            for (AttachedFileData afd : attachedFiles) {
                LA_AdministrativeSource source = archiveRepository.addToRepository(appOptions, afd, false);
                LA_AdministrativeSourceDAO.save(source);
                adminSources.add(source);
            }
        }
        
        //Phase 3: Save transaction
        hndTransaction = HND_MunicipalTransactionDAO.save(hndTransaction);
        HND_MunicipalTransactionDAO.flush();
        HND_MunicipalTransactionDAO.refresh(hndTransaction);
        setTransactionId(hndTransaction.getId());
    }

    private void setParcelDataToTransaction(HND_MunicipalTransaction hndTransaction) {
        HND_SpatialZoneInTransaction newSpatialZoneInTransaction;
        Set<HND_SpatialZone> nearParcelSet = new HashSet<HND_SpatialZone>();
        Set<HND_SpatialZone> completeSpatialZoneSet = new HashSet<HND_SpatialZone>();
        for (HND_SpatialZone reqSpatialZone : getRequestedZoneSet()) {
            for (HND_SpatialZone neighborParcel : HND_ParcelDAO.loadNeighborsByHNDParcel((HND_Parcel)reqSpatialZone)) {
                nearParcelSet.add(neighborParcel);
                nearParcelSet.addAll(HND_ParcelDAO.loadNeighborsByHNDParcel((HND_Parcel)neighborParcel));
            }
        }
        completeSpatialZoneSet.addAll(requestedZoneSet);
        completeSpatialZoneSet.addAll(nearParcelSet);
        
        //synchronize parcel in transactions with the complete parcel set
        //it involves adding and deleting from parcel-in-transactions set
        
        //1: Delete from parcel-in-transaction set the parcels
        //        not in the complete-parcel set
        Set<HND_SpatialZoneInTransaction> szitToDelete = new HashSet<HND_SpatialZoneInTransaction>();
        for (HND_SpatialZoneInTransaction szit : transaction.getSpatialZoneInTransactions()) {
            if (!completeSpatialZoneSet.contains(szit.getSpatialZone()))
                szitToDelete.add(szit);
        }
        for (HND_SpatialZoneInTransaction szit : szitToDelete) {
            transaction.getSpatialZoneInTransactions().remove(szit);
            szit.getSpatialZone().getSpatialZoneInTransactions().remove(szit);
            SigitDAO.delete(szit);
        }
        
        //2: Add to parcel-in-transaction set the parcels
        //        present in the complete-parcel set
        int neighborLevel;
        for (HND_SpatialZone hndSpatialZone : completeSpatialZoneSet) {
            if (hndSpatialZone != null && !isSpatialZoneInTransaction(hndSpatialZone, hndTransaction)) {
                newSpatialZoneInTransaction = new HND_SpatialZoneInTransaction();
                newSpatialZoneInTransaction.setTransaction(hndTransaction);
                newSpatialZoneInTransaction.setSpatialZone(hndSpatialZone);
                
                if (requestedZoneSet.contains(hndSpatialZone))
                    neighborLevel = 0;
                else {
                    neighborLevel = 1;
                    for (HND_SpatialZone reqSpatialZone : requestedZoneSet)
                        if (!hndSpatialZone.getShape().intersects(reqSpatialZone.getShape()))
                            neighborLevel = 2;
                }
                
                newSpatialZoneInTransaction.setNeighborLevel(neighborLevel);
                
                hndTransaction.getSpatialZoneInTransactions().add(newSpatialZoneInTransaction);
                hndSpatialZone.getSpatialZoneInTransactions().add(newSpatialZoneInTransaction);
            } else {
                log.warn("spatial zone already in transaction" + hndSpatialZone.getLabel());
            }
        }
    }
    
    private boolean isSpatialZoneInTransaction(ISpatialZone spatialZone, HND_MunicipalTransaction transaction) {
        for (HND_SpatialZoneInTransaction pit : transaction.getSpatialZoneInTransactions()) {
            if (pit.getSpatialZone().getSuID() == spatialZone.getSuID())
                return true;
        }
        
        return false;
    }

    private void setAnalysisData() {
        getTransaction().setAnalystUserName(credentials.getUsername());
        getTransaction().setAnalystFullName(credentials.getLoggedUserFullName());
    }

    private void setDataEntryData() {
        getTransaction().setEditorUserName(credentials.getUsername());
        getTransaction().setEditorFullName(credentials.getLoggedUserFullName());
    }
    
    private void setApprovalData(boolean denyTransaction) {
        getTransaction().setApproverUserName(credentials.getUsername());
        getTransaction().setApproverFullName(credentials.getLoggedUserFullName());
        
        if (denyTransaction)
            getTransaction().setApproved(false);
    }

    
    
    //@CreateProcess(definition=TRANSACTION_WORKFLOW, processKey="#{transactionHelper.transactionId}")
    @Transactional
    public String beginProcess() {
        setPresentationData();
        BusinessProcess.instance().createProcess(ProcessConfig.TRANSACTION_WORKFLOW, transaction);
        this.finished = true;
        
        return navigationHelper.navigateToHome();
    }
    
    
    @Override
    public List<Task> getTaskList() {
        if (taskList == null) {
            taskList = BusinessProcess.instance().getTaskList(ProcessConfig.TRANSACTION_WORKFLOW);
        }
        return taskList;
    }
    @Override
    public List<Task> getPooledTaskList() {
        if (pooledTaskList == null) {
            pooledTaskList = BusinessProcess.instance().getPooledTaskList(ProcessConfig.TRANSACTION_WORKFLOW);
        }
        return pooledTaskList;
    }


    
    @BeginTask
    @Begin(join=true)
    @Transactional
    public String startTask() {
        setTransaction(HND_MunicipalTransactionDAO.loadRequestById(
                getSelectedTask().getTransactionId()));
        
        setTransactionId(this.transaction.getId());
        setParty(this.transaction.getExtParty());
        
        BusinessProcess.instance().resumeProcess(ProcessConfig.TRANSACTION_WORKFLOW,
                getSelectedTask().getId());
        
        return navigationHelper.navigateTo(ProcessConfig.instance().getViewId(
                ProcessConfig.TRANSACTION_WORKFLOW, getSelectedTask().getName()));
    }
    
    @EndTask
    @Transactional
    public String endTask() {
        return navigationHelper.navigateToHome();
    }

    
    @EndTask
    @Transactional
    public String endPresentation() {
        setPresentationData();
        BusinessProcess.instance().endTask();
        return endTask();
    }
    
    
    @Transactional
    public String save() {
        HND_MunicipalTransactionDAO.save(transaction);
        return null;
    }
    
    @Transactional
    public String pause(boolean doSave) {
        if (doSave) save();
        return navigationHelper.navigateToHome();
    }
    
    
    @Transactional
    public String endAnalysisOK() throws SigitException {
        try {
            //1: Set analysis step specific data. These are analyst username and full name
            setAnalysisData();
    
            //2: Set the parcel data, neighbors and neighbor's neighbors in spatial zones in transaction list
            setParcelDataToTransaction(getTransaction());
            
            //2.5: Validates that a classification was chosen, at least 1 was zone was selected, dictamen 
            //and observations were filled
            checkAnalysisData();

            //3: validates that none of the spatial units are locked
            checkNoLockedSZInTransaction();
    
            List<UUID> parcelIdList = new ArrayList<>();
            for (HND_SpatialZoneInTransaction szit : getTransaction().getSpatialZoneInTransactions()) {
                //Save current HND_SpatialZoneInTransaction entity
                SigitDAO.save(szit);
                
                if (szit.isUserRequested()) {
                    parcelIdList.add(szit.getSpatialZone().getSuID());
                }
                
                //lock the spatial zone
                szit.getSpatialZone().getLock().setLockLevel(HND_Lock.neighborLevel2LockLevel( szit.getNeighborLevel() ));
                szit.getSpatialZone().getLock().setLockingTransaction(getTransaction());
                
                SigitDAO.save(szit.getSpatialZone());
            }
            
            SigitDAO.save(getTransaction());
            
            //Copy parcel data to shadow schema. Include neighbors and create snapshot.
            Util.copyParcelDataWithRelatedParcels( presentationNo, parcelIdList, true, true );
            
            BusinessProcess.instance().endTask("analysis_ok");
            return endTask();
        }
        catch (SigitException e) {
            FacesContext.getCurrentInstance().addMessage("",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            interactiveViewerHelper.getResBundle().loadMessage(e.getExceptionBundle()),
                            e.getMessage()));
        }
        
        return null;
    }
    private void checkAnalysisData() throws SigitException {
        if (getTransaction().getRequestType() == null)
            throw new DataValidationException(
                    getResBundle().loadMessage("analysis.validation.no_classification"));
        
        if (getTransaction().getSpatialZoneInTransactions().size() == 0)
            throw new DataValidationException(
                    getResBundle().loadMessage("analysis.validation.no_spatialzones"));
    }
    private void checkNoLockedSZInTransaction() throws SigitException {
        HND_Lock hndLock;
        for (HND_SpatialZoneInTransaction szit : getTransaction().getSpatialZoneInTransactions()) {
            hndLock = szit.getSpatialZone().getLock();
            if (hndLock.isLocked())
                throw new LockedException(
                        generalHelper.formatText(getResBundle().loadMessage("fmt.lock.detail"),
                                generalHelper.getZoneUsefulName(szit.getSpatialZone()),
                                hndLock.getLockingTransaction().getId()));
        }
    }
    
    
    @EndTask(transition="analysis_not_ok")
    @Transactional
    public String endAnalysisDenied() {
        setAnalysisData();
        getTransaction().setCompletionDate(new Date());
        HND_MunicipalTransactionDAO.merge(getTransaction());
        BusinessProcess.instance().endTask("analysis_not_ok");
        return endTask();
    }
    
    
    @EndTask(transition="dataentry_ok")
    @Transactional
    public String endDataEntry() {
        setDataEntryData();
        BusinessProcess.instance().endTask("dataentry_ok");
        return endTask();
    }
    
    
    @EndTask(transition="dataentry_back2analysis")
    @Transactional
    public String dataEntryBackToAnalysis() {
        BusinessProcess.instance().endTask("dataentry_back2analysis");
        return endTask();
    }

    
    //@EndTask(transition="approval_ok")
    @Transactional
    public String endApproval() {
        setApprovalData(false);
        
        BusinessProcess.instance().endTask("approval_ok");
        
        SigitDAO.flush();

        TransactionQueryHelper tqh = (TransactionQueryHelper) CtxComponent.getInstance("transactionQueryHelper");
        if (tqh != null) {
            tqh.setSelected(getTransaction());
            return "/queries/transactions.xhtml";
        }
        return endTask();
    }
    
    
    @EndTask(transition="approval_denied")
    @Transactional
    public String endApprovalDenied() {
        setApprovalData(true);
        BusinessProcess.instance().endTask("approval_denied");
        return endTask();
    }

    
    @Transactional
    public String endProcess(boolean approved, String responseData) {
        //unlock the spatial zones
        for (HND_SpatialZoneInTransaction szit : getTransaction().getSpatialZoneInTransactions()) {
            szit.getSpatialZone().getLock().unlock();
            SigitDAO.save(szit.getSpatialZone());
        }
        transactionLogicHelper.applyPresentationChanges(true, responseData);

        return null;
    }
    
    
    @EndTask(transition="approval_back2analysis")
    @Transactional
    public String approvalBackToAnalysis() {
        BusinessProcess.instance().endTask("approval_denied");
        return endTask();
    }
    
    
    @EndTask(transition="approval_back2dataentry")
    @Transactional
    public String approvalBackToDataEntry() {
        BusinessProcess.instance().endTask("approval_back2dataentry");
        return endTask();
    }
    
    
    
    public HND_MunicipalTransactionType getRequestType() {
        return getTransaction().getRequestType();
    }
    
    public void setRequestType(HND_MunicipalTransactionType requestType) {
        if (this.requestType != requestType) {
            this.requestType = requestType;
            getTransaction().setRequestType(requestType);
            requestSubTypes = null;
        }
    }
    
    
    
    public HND_TransactionSubType getRequestSubType() {
        return getTransaction().getRequestSubType();
    }
    
    public void setRequestSubType(HND_TransactionSubType requestSubType) {
        if (this.requestSubType != requestSubType) {
            this.requestSubType = requestSubType;
            getTransaction().setRequestSubType(requestSubType);
        }
    }
    public List<SelectItem> getRequestSubTypes() {
        //TODO: Eliminar mas adelante si no se ocupa
        if (requestSubTypes == null) {
            requestSubTypes = new ArrayList<SelectItem>();
        }
        return requestSubTypes;
    }

    
    
    public String selectZonesFromMap() {
        if (getTransaction() != null) {
            getRequestedZoneSet().addAll(interactiveViewerHelper.getHighlightedSpatialZoneList());
            setRequestedZoneList(null);
        }
        
        return null;
    }

    public String excludeSelectedZone(HND_SpatialZone zone) {
        getRequestedZoneSet().remove(zone);
        setRequestedZoneList(null);
        return null;
    }
}
