package org.sigit.logic.workflow;

import java.util.List;

import org.sigit.model.ladm.administrative.LA_AdministrativeSource;

public interface WorkflowHelper extends AttachFileHelper {

    List<LA_AdministrativeSource> getFileSources();

    String getEditMode();

    void setEditMode(String editMode);

    int getCurrentRow();

    void setCurrentRow(int currentRow);

    String beginProcess();

    String startTask();

    String endTask();
    
    String pause(boolean doSaveS);
    
    String save();

    List<Task> getTaskList();

    List<Task> getPooledTaskList();

    Task getSelectedTask();

    void setSelectedTask(Task selectedTask);
    
    String assignToCurrentUser();
    
}
