package org.sigit.logic.workflow.permits;

import java.io.Serializable;

import org.sigit.logic.viewer.InteractiveViewerHelper;
import org.sigit.logic.viewer.InteractiveViewerHelper.BorderEditMode;
import org.sigit.logic.viewer.InteractiveViewerHelper.CreateZoneMode;

import org.sigit.commons.di.CtxComponent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

@Scope(value="sigit-conversation")
@Component("digitizationHelper")
public class DigitizationHelper implements Serializable {
    private static final long serialVersionUID = 1L;

    @Autowired
    private InteractiveViewerHelper interactiveViewerHelper;
    
    @Autowired
    private BuildingPermitHelper buildingPermitHelper;

    public void create() {
        InteractiveViewerHelper ivh = getInteractiveViewerHelper();
        
        ivh.setShowRightsToolbox(false);
        ivh.setShowRestrictionsToolbox(false);
        ivh.setShowResponsibilitiesToolbox(false);
        ivh.setShowDataToolbox(false);
        ivh.setShowSpatialAnalysisToolbox(false);
        ivh.setShowHelpToolbox(false);
        ivh.setShowTitle(false);
        ivh.setBorderEditMode(BorderEditMode.CREATE);
        ivh.setShowSaveModificationButtons(true);
        ivh.setAllowEditing(true);
        
        ivh.setBorderShapeExpr("buildingPermitHelper.permit.buildingUnit.shape");
        ivh.setCreateZoneMode(CreateZoneMode.SET_SHAPE_FIELD);

        ivh.setSelectedZone(getBuildingPermitHelper().getPermit().getSelectedZone());
    }
    
    public InteractiveViewerHelper getInteractiveViewerHelper() {
        if (interactiveViewerHelper == null)
            interactiveViewerHelper = (InteractiveViewerHelper) CtxComponent
            .getInstance("interactiveViewerHelper");
        return interactiveViewerHelper;
    }

    public BuildingPermitHelper getBuildingPermitHelper() {
        if (buildingPermitHelper == null)
            buildingPermitHelper = (BuildingPermitHelper) CtxComponent
            .getInstance("buildingPermitHelper");
        return buildingPermitHelper;
    }
    
    
}
