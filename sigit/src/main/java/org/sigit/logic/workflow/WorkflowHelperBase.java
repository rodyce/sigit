package org.sigit.logic.workflow;

import java.util.ArrayList;
import java.util.List;

import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.sigit.i18n.ResourceBundleHelper;
import org.sigit.logic.general.GeneralHelper;
import org.sigit.logic.general.SpatialAnalysisHelper;
import org.sigit.logic.security.Credentials;
import org.sigit.logic.security.Identity;
import org.sigit.logic.viewer.InteractiveViewerHelper;
import org.sigit.logic.workflow.AttachedFileData;
import org.sigit.model.ladm.administrative.LA_AdministrativeSource;
import org.sigit.util.AppOptions;
import org.sigit.view.NavigationHelper;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class WorkflowHelperBase implements WorkflowHelper {
    protected List<AttachedFileData> attachedFiles;
    protected List<LA_AdministrativeSource> fileSources;
    protected String editMode = "0"; //0: agregando, 1: editando
    protected AttachedFileData selectedFile;
    protected boolean attachInfo = true;
    protected int currentRow;
    private Task selectedTask;

    protected List<Task> taskList;
    protected List<Task> pooledTaskList;


    @Autowired
    protected AppOptions appOptions;

    @Autowired
    protected Credentials credentials;
    
    @Autowired
    protected InteractiveViewerHelper interactiveViewerHelper;
    
    @Autowired
    protected SpatialAnalysisHelper spatialAnalysisHelper;
    
    @Autowired
    protected GeneralHelper generalHelper;
    
    @Autowired
    protected Identity identity;
    
    @Autowired
    protected NavigationHelper navigationHelper;

    @Autowired
    private ResourceBundleHelper resBundle;


    @Override
    public List<AttachedFileData> getAttachedFiles() {
        return attachedFiles;
    }
    @Override
    public void setAttachedFiles(List<AttachedFileData> attachedFiles) {
        this.attachedFiles = attachedFiles;
    }
    
    @Override
    public abstract List<LA_AdministrativeSource> getFileSources();


    @Override
    public String getEditMode() {
        return editMode;
    }
    @Override
    public void setEditMode(String editMode) {
        this.editMode = editMode;
    }
    
    @Override
    public AttachedFileData getSelectedFile() {
        return selectedFile;
    }
    @Override
    public void setSelectedFile(AttachedFileData selectedFile) {
        this.selectedFile = selectedFile;
    }
    
    @Override
    public boolean isAttachInfo() {
        return attachInfo;
    }
    @Override
    public void setAttachInfo(boolean attachInfo) {
        this.attachInfo = attachInfo;
    }
    
    @Override
    public int getCurrentRow() {
        return currentRow;
    }
    @Override
    public void setCurrentRow(int currentRow) {
        this.currentRow = currentRow;
    }
    
    @Override
    public Task getSelectedTask() {
        return selectedTask;
    }
    @Override
    public void setSelectedTask(Task selectedTask) {
        this.selectedTask = selectedTask;
    }
    
    @Override
    public abstract String hasAttachedFiles();
    @Override
    public abstract void acceptSelectedFile();

    @Override
    public void deleteSelectedFile() {
        attachedFiles.remove(currentRow);
    }

    //TODO: Finish implementation
    @Override
    public void attachFileListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        if (null == attachedFiles)
            attachedFiles = new ArrayList<AttachedFileData>();
        
        AttachedFileData afd = new AttachedFileData();
        afd.setFileName(item.getName());
        afd.setData(item.getData());
        
        setSelectedFile(afd);
    }
    
    @Override
    public abstract String beginProcess();
    
    @Override
    public abstract String startTask();
    
    @Override
    public abstract String endTask();
    
    @Override
    public abstract List<Task> getTaskList();
    
    @Override
    public abstract List<Task> getPooledTaskList();

    @Override
    public String assignToCurrentUser() {
        BusinessProcess.instance().assignTaskToCurrentUser(getSelectedTask());
        refreshTaskLists();
        return null;
    }
    
    protected void refreshTaskLists() {
        taskList = null;
        pooledTaskList = null;
    }
    
    protected ResourceBundleHelper getResBundle() {
        if (resBundle == null) {
            resBundle = new ResourceBundleHelper();
        }
        return resBundle;
    }

}
