package org.sigit.logic.workflow.transaction;

import org.sigit.logic.viewer.InteractiveViewerHelper;
import org.sigit.logic.viewer.toolbox.SplitViewerCallable;

public class DataEntrySplitViewerCallable extends SplitViewerCallable {
    private static final long serialVersionUID = 1L;
    
    private DataEntryHelper dataEntryHelper;

    public DataEntrySplitViewerCallable(DataEntryHelper dataEntryHelper) {
        this.dataEntryHelper = dataEntryHelper;
    }
    
    @Override
    public void doBorderOperation(
            InteractiveViewerHelper interactiveViewerHelper) {
        super.doBorderOperation(interactiveViewerHelper);
        dataEntryHelper.setRequestedParcels(null);
        dataEntryHelper.setNeighborParcels(null);
    }
}
