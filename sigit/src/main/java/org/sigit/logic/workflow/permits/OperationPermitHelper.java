package org.sigit.logic.workflow.permits;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.faces.model.SelectItem;

import org.sigit.commons.conversation.annotations.Begin;
import org.sigit.commons.conversation.annotations.End;
import org.sigit.commons.di.CtxComponent;
import org.sigit.dao.hnd.administrative.HND_OperationPermitDAO;
import org.sigit.dao.hnd.administrative.HND_PermitRuleGroupDAO;
import org.sigit.logic.queries.OperationPermitQueryHelper;
import org.sigit.logic.workflow.BusinessProcess;
import org.sigit.logic.workflow.ProcessConfig;
import org.sigit.logic.workflow.Task;
import org.sigit.logic.workflow.annotations.BeginTask;
import org.sigit.logic.workflow.annotations.CreateProcess;
import org.sigit.logic.workflow.annotations.EndTask;
import org.sigit.model.hnd.administrative.HND_OperationPermit;
import org.sigit.model.hnd.administrative.HND_OperationPermitType;
import org.sigit.model.hnd.administrative.HND_PermitRuleGroup;
import org.sigit.model.hnd.administrative.HND_PermitType;
import org.sigit.model.hnd.cadastre.HND_BuildingUnit;
import org.sigit.model.hnd.cadastre.HND_LandUse;
import org.sigit.model.hnd.cadastre.HND_Parcel;
import org.sigit.model.hnd.cadastre.HND_SpatialZone;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component("operationPermitHelper")
@Scope(value="sigit-conversation")
public class OperationPermitHelper extends PermitHelper<HND_OperationPermit> implements Serializable {
    private static final long serialVersionUID = -5452696846360406096L;
    
    private List<SelectItem> permitTypeValueList;
    
    
    @Override
    public HND_OperationPermit getPermit() {
        if (permit == null) {
            permit = new HND_OperationPermit();
            permit.setRuleGroup(getPermitRuleGroup());
        }
        return (HND_OperationPermit) permit;
    }
    @Override
    public void setPermit(HND_OperationPermit permit) {
        if (this.permit != permit) {
            this.permit = permit;
            setAttachedFiles(null);
            setParty(null);
        }
    }
    
    @Override
    public List<Task> getTaskList() {
        if (taskList == null) {
            taskList = BusinessProcess.instance().getTaskList(ProcessConfig.OPERATION_PERMIT_PROCESS);
        }
        return taskList;
    }
    @Override
    public List<Task> getPooledTaskList() {
        if (pooledTaskList == null) {
            pooledTaskList = BusinessProcess.instance().getPooledTaskList(ProcessConfig.OPERATION_PERMIT_PROCESS);
        }
        return pooledTaskList;
    }
    
    
    @End
    @CreateProcess(definition=ProcessConfig.OPERATION_PERMIT_PROCESS, processKey="#{operationPermitHelper.permitId}")
    @Override
    @Transactional
    public String beginProcess() {
        HND_OperationPermit hndPermit = getPermit();
        
        beginProcessAux(hndPermit, HND_PermitType.OPERATION_PERMIT);
        
        HND_OperationPermitDAO.save(hndPermit);
        setPermitId(hndPermit.getId());

        BusinessProcess.instance().createProcess(ProcessConfig.OPERATION_PERMIT_PROCESS, hndPermit);

        return navigationHelper.navigateTo("/permits/operation/activities");
    }


    @Override
    @BeginTask
    @Begin(join=true)
    @Transactional
    public String startTask() {
        setPermit(HND_OperationPermitDAO.loadOperationPermitByID(
                getSelectedTask().getTransactionId()));
        
        setPermitId(this.permit.getId());
        setParty(this.permit.getExtParty());
        
        BusinessProcess.instance().resumeProcess(ProcessConfig.OPERATION_PERMIT_PROCESS,
                getSelectedTask().getId());
        
        return navigationHelper.navigateTo(ProcessConfig.instance().getViewId(
                ProcessConfig.OPERATION_PERMIT_PROCESS, getSelectedTask().getName()));
    }

    @EndTask(transition="approval_back2analysis")
    public String approvalBack2Analysis() {
        BusinessProcess.instance().endTask("approval_back2analysis");
        return "home";
    }
    
    public List<SelectItem> getPermitTypeValueList() {
        if (permitTypeValueList == null) {
            permitTypeValueList = new ArrayList<SelectItem>();
            permitTypeValueList.add(new SelectItem(null, "(seleccionar)"));
            for (HND_OperationPermitType pt : HND_OperationPermitType.values())
                permitTypeValueList.add(new SelectItem(pt, resBundle.loadMessage(pt.name())));
        }
        return permitTypeValueList;
    }
    
    @Override
    @Transactional
    public List<HND_PermitRuleGroup> getPermitRuleGroupList() {
        if (permitRuleGroupList == null) {
            permitRuleGroupList = HND_PermitRuleGroupDAO.loadPermitRuleGroupsByType(HND_PermitType.OPERATION_PERMIT);
        }
        return permitRuleGroupList;
    }
    @Override
    public List<SelectItem> getPermitRuleGroupItemList() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public boolean isAdminAnalysisOK() {
        final List<?>[] listArr = { getLandUseRestrictingSZList() };
        
        for (List<?> l : listArr)
            if (!listOK(l))
                return false;
        
        return true;
    }
    
    
    @Override
    public HND_PermitRuleGroup getPermitRuleGroup() {
        if (permitRuleGroup == null) {
            permitRuleGroup = generalHelper.getSystemConfiguration().getOperationPermitRuleGroup();
        }
        return permitRuleGroup;
    }
    
    
    @Override
    public HND_LandUse getLandUse() {
        return getPermit().getLandUse();
    }
    @Override
    public List<HND_LandUse> getOtherLandUses() {
        return getPermit().getOtherLandUses();
    }
    
    @Override
    public void addLandUse() {
        if (selectedLandUse == null) return;
        
        List<HND_LandUse> landUses = getPermit().getOtherLandUses();
        if (!landUses.contains(selectedLandUse))
            landUses.add(selectedLandUse);
    }
    @Override
    public void deleteLandUse(HND_LandUse landUse) {
        if (landUse == null) return;
        
        List<HND_LandUse> landUses = getPermit().getOtherLandUses();
        landUses.remove(landUse);
    }

    public String selectParcelOrBuildingFromMap() {
        HND_SpatialZone spatialZone = (HND_SpatialZone) interactiveViewerHelper.getSelectedZone();
        if (spatialZone == null) {
            List<HND_SpatialZone> listSZ = interactiveViewerHelper.getHighlightedSpatialZoneList();
            if (listSZ != null && listSZ.size() > 0)
                spatialZone = listSZ.get(0);
        }
        
        if (spatialZone instanceof HND_Parcel || spatialZone instanceof HND_BuildingUnit)
            getPermit().setSelectedZone(spatialZone);
        
        return null;
    }
    
    @Override
    public String endApprovalTask() {
        String defaultReturn = super.endApprovalTask();
        
        OperationPermitQueryHelper opqh = (OperationPermitQueryHelper) CtxComponent.getInstance("operationPermitQueryHelper");
        if (opqh != null) {
            opqh.setSelected(getPermit());
            return navigationHelper.navigateTo("/queries/operationPermits.xhtml");
        }
        
        return navigationHelper.navigateTo(defaultReturn);
    }
    
    @Override
    public Set<HND_LandUse> getAllPermitLandUses() {
        Set<HND_LandUse> permitLandUses = listToSet(getPermit().getOtherLandUses());
        permitLandUses.add(getPermit().getLandUse());

        return permitLandUses;
    }
}
