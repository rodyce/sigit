package org.sigit.logic.workflow.permits;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;

import org.sigit.dao.SigitDAO;
import org.sigit.dao.hnd.administrative.HND_PermitDAO;
import org.sigit.dao.hnd.cadastre.HND_LayerDAO;
import org.sigit.dao.hnd.cadastre.HND_SpatialZoneDAO;
import org.sigit.dao.ladm.administrative.LA_AdministrativeSourceDAO;
import org.sigit.dao.ladm.external.ExtArchiveDAO;
import org.sigit.dao.ladm.external.ExtPartyDAO;
import org.sigit.i18n.ResourceBundleHelper;
import org.sigit.logic.general.GeneralHelper;
import org.sigit.logic.general.SpatialAnalysisHelper;
import org.sigit.logic.general.SpatialAnalysisHelper.SpatialAnalysisResult;
import org.sigit.logic.security.Credentials;
import org.sigit.logic.workflow.ArchiveRepository;
import org.sigit.logic.workflow.AttachedFileData;
import org.sigit.logic.workflow.BusinessProcess;
import org.sigit.logic.workflow.WorkflowHelperBase;
import org.sigit.logic.workflow.annotations.EndTask;
import org.sigit.model.commons.IExtArchive;
import org.sigit.model.commons.ISpatialZone;
import org.sigit.model.hnd.administrative.HND_Permit;
import org.sigit.model.hnd.administrative.HND_PermitObservation;
import org.sigit.model.hnd.administrative.HND_PermitRuleGroup;
import org.sigit.model.hnd.administrative.HND_PermitType;
import org.sigit.model.hnd.administrative.HND_RuleActionType;
import org.sigit.model.hnd.administrative.HND_SpatialRule;
import org.sigit.model.hnd.cadastre.HND_BuildingMaterial;
import org.sigit.model.hnd.cadastre.HND_LandUse;
import org.sigit.model.hnd.cadastre.HND_Layer;
import org.sigit.model.hnd.cadastre.HND_LayerType;
import org.sigit.model.hnd.cadastre.HND_Parcel;
import org.sigit.model.hnd.cadastre.HND_SpatialZone;
import org.sigit.model.ladm.administrative.LA_AdministrativeSource;
import org.sigit.model.ladm.external.ExtParty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


public abstract class PermitHelper<P extends HND_Permit> extends WorkflowHelperBase {
    protected P permit;
    protected UUID permitId;
    
    private ExtParty party;
    protected HND_PermitRuleGroup permitRuleGroup;

    private String cashiersCode;
    private BigDecimal payment;

    private boolean approveTransaction;

    private List<HND_SpatialRule> restrictingRuleList = new ArrayList<HND_SpatialRule>();
    private List<HND_Layer> buildingLayers;

    protected HND_LandUse selectedLandUse;
    
    protected List<HND_PermitRuleGroup> permitRuleGroupList;
    protected List<SelectItem> permitRuleGroupItemList;
    protected List<HND_PermitObservation> permitObservations;
    
    protected List<HND_SpatialZone> numberOfFloorsRestrictingSZList;
    protected List<HND_SpatialZone> buildingAreaRestrictingSZList;
    protected List<HND_SpatialZone> buildingMaterialRestrictingSZList;
    protected List<HND_SpatialZone> landUseRestrictingSZList;
    
    protected Map<HND_SpatialZone, List<HND_BuildingMaterial>> szBuildingMaterials;
    protected Map<HND_SpatialZone, List<HND_LandUse>> szLandUses;


    @Autowired
    protected Credentials credentials;
    
    @Autowired
    protected ResourceBundleHelper resBundle;
    
    @Autowired
    protected GeneralHelper generalHelper;

    @Autowired
    protected SpatialAnalysisHelper spatialAnalysisHelper;
    
    @Autowired
    protected ArchiveRepository archiveRepository;
    
    private PermitRuleGroupConverter permitRuleGroupConverter;
    private boolean renderViewRequestPanel = false;

    
    public abstract P getPermit();
    public abstract void setPermit(P permit);
    
    public abstract HND_PermitRuleGroup getPermitRuleGroup();
    public abstract Set<HND_LandUse> getAllPermitLandUses();
    
    public UUID getPermitId() {
        return permitId;
    }
    public void setPermitId(UUID permitId) {
        this.permitId = permitId;
    }
    
    public ExtParty getParty() {
        if (party == null && getPermit() != null)
            party = getPermit().getExtParty();
        return party;
    }
    public void setParty(ExtParty party) {
        if (this.party != party) {
            this.party = party;
            if (party != null && getPermit() != null)
                getPermit().setExtParty(party);
        }
    }
    
    public UUID getPartyId() {
        return getParty() != null ? getParty().getExtPID() : null;
    }
    
    public String getPartyNationalIdentity() {
        return getParty() != null ? getParty().getFormalIdentity() : "";
    }
    public void setPartyNationalIdentity(String partyNationalIdentity) {
    }
    
    public String getPartyName() {
        return getParty() != null ? getParty().getName() : "";
    }
    
    public String getSelectedZoneName() {
        return (permit != null && permit.getSelectedZone() != null) ? generalHelper.formatZoneNameText(permit.getSelectedZone()) : null;
    }
    public void setSelectedZoneName(String selectedZoneName) {
    }
    
    public String getCashiersCode() {
        return cashiersCode;
    }
    public void setCashiersCode(String cashiersCode) {
        this.cashiersCode = cashiersCode;
    }
    
    public BigDecimal getPayment() {
        return payment;
    }
    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }
    
    public boolean isRenderViewRequestPanel() {
        return renderViewRequestPanel;
    }
    public void setRenderViewRequestPanel(boolean renderViewRequestPanel) {
        this.renderViewRequestPanel = renderViewRequestPanel;
    }
    
    @Transactional
    public void acceptSelectedFile() {
        if (selectedFile == null) return;
        
        if (permit != null && permit.getId() == null) {
            if ("0".equals(editMode)) { //agregando
                if (null == attachedFiles)
                    attachedFiles = new ArrayList<AttachedFileData>();
                attachedFiles.add(selectedFile);
            }
            else if ("1".equals(editMode)) { //editando
                attachedFiles.set(currentRow, selectedFile);
            }
        }
        else if (permit != null) {
            if ("0".equals(editMode)) {
                LA_AdministrativeSource newSource = archiveRepository.addToRepository(appOptions, selectedFile, false);
                LA_AdministrativeSourceDAO.save(newSource);
                permit.getSources().add(newSource);
                setAttachedFiles(null);
            }
            else if ("1".equals(editMode)) {
                LA_AdministrativeSource source = null;
                //Find the corresponding administrative source for the specified archive UUID
                for (LA_AdministrativeSource src : permit.getSources()) {
                    if (src.getsID().equals(selectedFile.getSourceId())) {
                        source = src;
                        break;
                    }
                }
                if (source != null) {
                    source.setType(selectedFile.getAdminSourceType());
                    source.getArchive().setDescription(selectedFile.getDescripcion());
                    LA_AdministrativeSourceDAO.save(source);
                }
            }
            
            HND_PermitDAO.save(permit);
        }
        
        setSelectedFile(null);
        fileSources = null;
    }

    
    public String hasAttachedFiles() {
        if (attachInfo)
            return "attaches";
        
        return "does_not_attach";
    }
    
    public List<LA_AdministrativeSource> getFileSources() {
        if (fileSources == null) {
            //TODO: we could add an ordering criterion
            fileSources = new ArrayList<LA_AdministrativeSource>();
            if (permit != null && permit.getSources() != null)
                for (LA_AdministrativeSource source : permit.getSources())
                    fileSources.add(source);
        }
        
        return fileSources;
    }
    
    public List<HND_PermitObservation> getPermitObservations() {
        if (permitObservations == null) {
            permitObservations = new ArrayList<HND_PermitObservation>();
            if (permit != null && permit.getPermitObservations() != null)
                for (HND_PermitObservation po : permit.getPermitObservations())
                    permitObservations.add(po);
        }
        return permitObservations;
    }
    
    public String selectZoneFromMap() {
        ISpatialZone sz = interactiveViewerHelper.getSelectedZone();
        if (permit != null && sz instanceof HND_Parcel) {
            permit.setSelectedZone( (HND_SpatialZone) sz );
        }
        return null;
    }
    
    public boolean isApproveTransaction() {
        return approveTransaction;
    }
    public void setApproveTransaction(boolean approveTransaction) {
        this.approveTransaction = approveTransaction;
    }
    
    @EndTask
    public String endTask() {
        BusinessProcess.instance().endTask();
        return navigationHelper.navigateToHome();
    }
    
    @EndTask
    public String endPresentationTask() {
        permit.setReceptionistUserName(credentials.getUsername());
        permit.setReceptionistFullName(credentials.getLoggedUserFullName());
        
        BusinessProcess.instance().endTask();

        return navigationHelper.navigateToHome();
    }
    
    @EndTask
    public String endAssessmentsTask() {
        if (!spatialAnalysisHelper.getSpatialRuleToAnalysisMap().isEmpty()) {
            //new spatial analysis was made, so need to clear current observations
            permit.getPermitObservations().clear();
            for (HND_PermitObservation po : getSarToPermitObsMap().values())
                permit.getPermitObservations().add(po);
        }
        
        permit.setAnalystUserName(credentials.getUsername());
        permit.setAnalystFullName(credentials.getLoggedUserFullName());
        
        HND_PermitDAO.save(permit);

        BusinessProcess.instance().endTask();

        return navigationHelper.navigateToHome();
    }
    
    @Transactional
    public String endApprovalTask() {
        permit.setApproved(approveTransaction);
        permit.setApproverUserName(credentials.getUsername());
        permit.setApproverFullName(credentials.getLoggedUserFullName());
        
        //The permit is approved. Set the corresponding life time begin to
        //associated sources and zones
        final Date issueDate = new Date();
        permit.setIssueDate(issueDate);
        for (LA_AdministrativeSource source : permit.getSources()) {
            source.setAcceptance(issueDate);
            source.setBeginLifespanVersion(issueDate);
            IExtArchive archive = source.getArchive();
            if (archive != null) {
                archive.setAcceptance(issueDate);
                ExtArchiveDAO.save(archive);
            }
            LA_AdministrativeSourceDAO.save(source);
        }
        
        //set no pending permit to the underlying permit's spatial zone
        permit.getSelectedZone().setPendingPermitType(null);
        
        HND_PermitDAO.save(permit);
        
        BusinessProcess.instance().endTask();

        return navigationHelper.navigateToHome();
    }
    
    @Override
    @Transactional
    public String pause(boolean doSave) {
        if (doSave) save();
        return navigationHelper.navigateToHome();
    }
    
    @Override
    @Transactional
    public String save() {
        HND_PermitDAO.save(permit);
        return null;
    }
    
    @EndTask(transition="back_to_assessments")
    @Transactional
    public String endApprovalBackToAssessments() {
        permit.setApproverUserName(credentials.getUsername());
        permit.setApproverFullName(credentials.getLoggedUserFullName());

        HND_PermitDAO.save(permit);
        
        BusinessProcess.instance().endTask("back_to_assessments");

        return navigationHelper.navigateToHome();
    }
    
    @Override
    @Transactional
    public List<AttachedFileData> getAttachedFiles() {
        if (super.getAttachedFiles() == null) {
            if (getPermit() != null && getPermit().getSources() != null && getPermit().getSources().size() > 0) {
                attachedFiles = new ArrayList<AttachedFileData>();
                for (LA_AdministrativeSource source : getPermit().getSources()) {
                    AttachedFileData afd = new AttachedFileData();
                    afd.setAdminSourceType(source.getType());
                    afd.setId(source.getArchive().getsID());
                    afd.setSourceId(source.getsID());
                    afd.setFileName(source.getArchive().getName());
                    afd.setDescripcion(source.getArchive().getDescription());
                    afd.setArchive(source.getArchive());
                    
                    attachedFiles.add(afd);
                }
            }
        }
        return super.getAttachedFiles();
    }

    protected void beginProcessAux(HND_Permit hndPermit, HND_PermitType permitType) {
        if (permitType == null) throw new NullPointerException("permitType is required");
        
        hndPermit.setRequestDate(new Date());
        hndPermit.setReceptionistUserName(credentials.getUsername());
        hndPermit.setReceptionistFullName(credentials.getLoggedUserFullName());

        ExtParty extParty = ExtPartyDAO.loadExtPartyByExtPID(getPartyId());
        hndPermit.setExtParty(extParty);
        
        if ( hndPermit.getSelectedZone() != null )
            hndPermit.getSelectedZone().setPendingPermitType(permitType);
        
        List<AttachedFileData> attachedFiles = getAttachedFiles();
        if (attachedFiles != null) {
            Set<LA_AdministrativeSource> adminSources = hndPermit.getSources();
            if (adminSources == null) {
                adminSources = new HashSet<LA_AdministrativeSource>();
                hndPermit.setSources(adminSources);
            }
            
            LA_AdministrativeSource adminSource;
            for (AttachedFileData afd : attachedFiles) {
                adminSource = archiveRepository.addToRepository(appOptions, afd, false);
                LA_AdministrativeSourceDAO.save(adminSource);
                adminSources.add(adminSource);
            }
        }
    }

    @Transactional
    public List<SelectItem> getPermitRuleGroupItemList() {
        if (permitRuleGroupItemList == null) {
            permitRuleGroupItemList = new ArrayList<SelectItem>();
            permitRuleGroupItemList.add(new SelectItem(null, "(seleccionar)"));
            for (HND_PermitRuleGroup prg : getPermitRuleGroupList())
                permitRuleGroupItemList.add(new SelectItem(prg, prg.getName() + " - " + prg.getDescription()));
        }
        return permitRuleGroupItemList;
    }
    
    public List<HND_SpatialRule> getRuleList() {
        return spatialAnalysisHelper.getRuleGroupRules();
    }
    
    
    /**List used in administrative analysis*/
    public String gotoAdminAnalysis() {
        buildingAreaRestrictingSZList = null;
        buildingMaterialRestrictingSZList = null;
        numberOfFloorsRestrictingSZList = null;
        landUseRestrictingSZList = null;
        szBuildingMaterials = null;
        szLandUses = null;
        
        return "next";
    }
    
    public abstract boolean isAdminAnalysisOK();
    
    protected boolean listOK(List<?> list) {
        return list == null || list.isEmpty();
    }
    /**END*/
    
    /**Functions for Spatial Analysis*/
    public String gotoSpatialAnalysis() {
        spatialAnalysisHelper.setSpatialZone(getPermit().getSelectedZone());
        spatialAnalysisHelper.setRuleGroup(getPermitRuleGroup());
        spatialAnalysisHelper.setDistance(null);
        
        Set<HND_LandUse> additionaLUSet = new HashSet<HND_LandUse>();
        additionaLUSet.add(getLandUse());
        additionaLUSet.addAll(getOtherLandUses());
        spatialAnalysisHelper.setLhsAdditionalLUSet(additionaLUSet);
        
        spatialAnalysisHelper.doNewSpatialAnalysis();
        
        return "next";
    }
    
    public boolean isSpatialAnalysisOK() {
        final List<?>[] sarLists = { spatialAnalysisHelper.getDistanceResults(),
                spatialAnalysisHelper.getOverlapsResults(),
                spatialAnalysisHelper.getWithinResults(),
                spatialAnalysisHelper.getTouchesResults(),
                spatialAnalysisHelper.getCrossesResults(),
                spatialAnalysisHelper.getContainsResults()
                };
        
        restrictingRuleList.clear();
        for (List<?> sarList : sarLists)
            for (Object sar : sarList)
                if (sar != null)
                    for (HND_SpatialRule sr : ((SpatialAnalysisResult) sar).getSpatialRuleSet())
                        if (sr.getAction() == HND_RuleActionType.MINUS)
                            if (!restrictingRuleList.contains(sr))
                                restrictingRuleList.add(sr);
        
        return restrictingRuleList.size() == 0;
    }
    public List<HND_SpatialRule> getRestrictingRuleList() {
        return restrictingRuleList;
    }
    
    public abstract HND_LandUse getLandUse();
    public abstract List<HND_LandUse> getOtherLandUses();
    /**END*/
    
    public String goBack() {
        return "back";
    }
    
    @EndTask
    @Transactional
    public String endAssessments() {
        getPermit().setAnalystUserName(credentials.getUsername());
        getPermit().setAnalystFullName(credentials.getLoggedUserFullName());
        SigitDAO.save(getPermit());
        
        BusinessProcess.instance().endTask();
        
        return navigationHelper.navigateToHome();
    }
    
    @EndTask
    @Transactional
    public String endDigitization() {
        getPermit().setEditorUserName(credentials.getUsername());
        getPermit().setEditorFullName(credentials.getLoggedUserFullName());
        SigitDAO.save(getPermit());
        
        BusinessProcess.instance().endTask();
        
        return navigationHelper.navigateToHome();
    }
    
    public List<HND_SpatialZone> getLandUseRestrictingSZList() {
        if (landUseRestrictingSZList == null) {
            landUseRestrictingSZList = new ArrayList<HND_SpatialZone>();
            List<HND_SpatialZone> szCandidateList = HND_SpatialZoneDAO
                    .loadSpatialZonesContainedInSZ(
                        getPermit().getSelectedZone());
            if (szCandidateList != null && szCandidateList.size() > 0) {
                Set<HND_LandUse> permitLandUses = getAllPermitLandUses();
                
                for (HND_SpatialZone hndSZ : szCandidateList) {
                    Set<HND_LandUse> szLandUses = listToSet(hndSZ.getOtherLandUses());
                    szLandUses.add(hndSZ.getLandUse());
                    
                    List<HND_LandUse> incompatLUs = new ArrayList<HND_LandUse>();
                    if (!areLandUseSetsCompatible(permitLandUses, szLandUses, incompatLUs)) {
                        landUseRestrictingSZList.add(hndSZ);
                        for (HND_LandUse lu : incompatLUs) {
                            if (!szLandUses(hndSZ).contains(lu)) {
                                szLandUses(hndSZ).add(lu);
                            }
                        }
                    }
                }
            }
        }
        return landUseRestrictingSZList;
    }
    public void setLandUseRestrictingSZList(
            List<HND_SpatialZone> landUseRestrictingSZList) {
        this.landUseRestrictingSZList = landUseRestrictingSZList;
    }

    public List<HND_LandUse> szLandUses(HND_SpatialZone sz) {
        if (szLandUses == null) {
            szLandUses = new HashMap<HND_SpatialZone, List<HND_LandUse>>();
        }
        List<HND_LandUse> lus = szLandUses.get(sz);
        if (lus == null) {
            lus = new ArrayList<HND_LandUse>();
            szLandUses.put(sz, lus);
        }
        return lus;
    }

    protected boolean areLandUseSetsCompatible(Set<HND_LandUse> permitLandUses, Set<HND_LandUse> szLandUses,
            List<HND_LandUse> szIncompatibleLandUses) {
        boolean retval = true;
        for (HND_LandUse permitLU : permitLandUses)
            for (HND_LandUse szLU : szLandUses)
                if (!generalHelper.areLandUsesCompatible(permitLU, szLU)) {
                    if (szIncompatibleLandUses != null)
                        szIncompatibleLandUses.add(szLU);
                    else
                        return false;
                    
                    retval = false;
                }
        
        return retval;
    }
    
    protected <T> Set<T> listToSet(List<T> list) {
        if (list == null) return null;
        
        Set<T> newSet = new HashSet<T>();
        for (T t : list)
            if (t != null)
                newSet.add(t);
        
        return newSet;
    }
    

    private Map<SpatialAnalysisResult, HND_PermitObservation> getSarToPermitObsMap() {
        Map<SpatialAnalysisResult, HND_PermitObservation> sarToPermitObsMap = new HashMap<SpatialAnalysisResult, HND_PermitObservation>();
        HND_PermitObservation permitObservation;
        for (HND_SpatialRule spatialRule : getRuleList()) {
            List<SpatialAnalysisResult> sarList = spatialAnalysisHelper.getSpatialRuleToAnalysisMap().get(spatialRule);
            for (SpatialAnalysisResult sar : sarList) {
                permitObservation = new HND_PermitObservation();
                permitObservation.setPermit(permit);
                permitObservation.setSpatialRuleName(spatialRule.getCode());
                permitObservation.setSpatialRuleDescription(spatialRule.getDescription());
                permitObservation.setSpatialRuleDetails(generalHelper.spatialRuleDetailsText(spatialRule));
                permitObservation.setAction(spatialRule.getAction());
                permitObservation.setTargetSpatialZone(sar.getRhsSpatialZone());
                permitObservation.setAnalystObservation(sar.getAnalystObservation());

                sarToPermitObsMap.put(sar, permitObservation);
            }
        }
        
        return sarToPermitObsMap;
    }

    
    public Converter getPermitRuleGroupConverter() {
        if (permitRuleGroupConverter == null) {
            permitRuleGroupConverter = new PermitRuleGroupConverter();
        }
        return permitRuleGroupConverter;
    }
    
    @Transactional
    public List<HND_Layer> getBuildingLayers() {
        if (buildingLayers == null) {
            buildingLayers = HND_LayerDAO.loadLayersByType(HND_LayerType.BUILDING, true);
            if (buildingLayers == null)
                buildingLayers = new ArrayList<HND_Layer>();
        }
        return buildingLayers;
    }


    public HND_LandUse getSelectedLandUse() {
        return selectedLandUse;
    }
    public void setSelectedLandUse(HND_LandUse selectedLandUse) {
        this.selectedLandUse = selectedLandUse;
    }
    public String getSelectedLandUseName() {
        return generalHelper.landUseName(getLandUse());
    }
    public void setSelectedLandUseName(String selectedLandUseName) {
    }
    public abstract void addLandUse();
    public abstract void deleteLandUse(HND_LandUse landUse);
    
    public abstract List<HND_PermitRuleGroup> getPermitRuleGroupList();

    
    public class PermitRuleGroupConverter implements Converter {
        @Override
        @Transactional
        public Object getAsObject(FacesContext context, UIComponent component,
                String value) {
            for (HND_PermitRuleGroup prg : getPermitRuleGroupList())
                if (String.valueOf(prg.getId()).equals(value))
                    return prg;
            return null;
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component,
                Object value) {
            if (value == null) return "";
            return String.valueOf(((HND_PermitRuleGroup) value).getId());
        }
    }
}

