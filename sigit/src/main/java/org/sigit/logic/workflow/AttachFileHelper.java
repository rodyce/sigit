package org.sigit.logic.workflow;

import java.util.List;

import org.richfaces.event.FileUploadEvent;

public interface AttachFileHelper {

    String getEditMode();

    void setEditMode(String editMode);

    List<AttachedFileData> getAttachedFiles();

    void setAttachedFiles(List<AttachedFileData> attachedFiles);

    AttachedFileData getSelectedFile();

    void setSelectedFile(AttachedFileData selectedFile);

    boolean isAttachInfo();

    void setAttachInfo(boolean attachInfo);

    String hasAttachedFiles();

    void acceptSelectedFile();

    void deleteSelectedFile();

    void attachFileListener(FileUploadEvent event);

}
