package org.sigit.logic.workflow.permits;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.sigit.commons.conversation.annotations.Begin;
import org.sigit.commons.conversation.annotations.End;
import org.sigit.dao.SigitDAO;
import org.sigit.dao.hnd.administrative.HND_BuildingPermitDAO;
import org.sigit.dao.hnd.administrative.HND_PermitRuleGroupDAO;
import org.sigit.dao.hnd.cadastre.HND_SpatialZoneDAO;
import org.sigit.logic.workflow.BusinessProcess;
import org.sigit.logic.workflow.ProcessConfig;
import org.sigit.logic.workflow.Task;
import org.sigit.logic.workflow.annotations.BeginTask;
import org.sigit.logic.workflow.annotations.CreateProcess;
import org.sigit.model.hnd.administrative.HND_BuildingPermit;
import org.sigit.model.hnd.administrative.HND_PermitRuleGroup;
import org.sigit.model.hnd.administrative.HND_PermitType;
import org.sigit.model.hnd.cadastre.HND_BuildingMaterial;
import org.sigit.model.hnd.cadastre.HND_LandUse;
import org.sigit.model.hnd.cadastre.HND_Parcel;
import org.sigit.model.hnd.cadastre.HND_SpatialZone;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component("buildingPermitHelper")
@Scope(value="sigit-conversation")
public class BuildingPermitHelper extends PermitHelper<HND_BuildingPermit> implements Serializable {
    private static final long serialVersionUID = -3421722459928810842L;
    
    private HND_BuildingMaterial selectedBuildingMaterial;
    
    
    @Override
    public HND_BuildingPermit getPermit() {
        if (permit == null) {
            permit = new HND_BuildingPermit();
            permit.setRuleGroup(getPermitRuleGroup());
            
            if (getBuildingLayers().size() == 1)
                permit.getBuildingUnit().setLevel(getBuildingLayers().get(0));
        }
        return (HND_BuildingPermit) permit;
    }
    @Override
    public void setPermit(HND_BuildingPermit permit) {
        this.permit = permit;
    }
    
    public String selectParcelFromMap() {
        HND_SpatialZone spatialZone = null;
        
        if (interactiveViewerHelper.getSelectedParcel() != null)
            spatialZone = (HND_Parcel) interactiveViewerHelper.getSelectedParcel();
        else if (interactiveViewerHelper.getHighlightedSpatialZoneList() != null
                && interactiveViewerHelper.getHighlightedSpatialZoneList().size() > 0
                && interactiveViewerHelper.getHighlightedSpatialZoneList().get(0) instanceof HND_Parcel)
            spatialZone = (HND_Parcel) interactiveViewerHelper.getHighlightedSpatialZoneList().get(0);
        
        if (spatialZone != null)
            getPermit().setSelectedZone(spatialZone);
        
        return null;
    }
    
    public HND_BuildingMaterial getSelectedBuildingMaterial() {
        return selectedBuildingMaterial;
    }
    public void setSelectedBuildingMaterial(
            HND_BuildingMaterial selectedBuildingMaterial) {
        this.selectedBuildingMaterial = selectedBuildingMaterial;
    }
    public void addBuildingMaterial() {
        if (selectedBuildingMaterial == null) return;
        
        List<HND_BuildingMaterial> buildingMaterials = getPermit().getBuildingUnit().getBuildingMaterials();
        if (!buildingMaterials.contains(selectedBuildingMaterial))
            buildingMaterials.add(selectedBuildingMaterial);
    }
    public void deleteBuildingMaterial(HND_BuildingMaterial buildingMaterial) {
        if (buildingMaterial == null) return;
        
        List<HND_BuildingMaterial> buildingMaterials = getPermit().getBuildingUnit().getBuildingMaterials();
        buildingMaterials.remove(buildingMaterial);
    }
    
    @Override
    public void addLandUse() {
        if (selectedLandUse == null) return;
        
        List<HND_LandUse> landUses = getPermit().getBuildingUnit().getOtherLandUses();
        if (!landUses.contains(selectedLandUse))
            landUses.add(selectedLandUse);
    }
    @Override
    public void deleteLandUse(HND_LandUse landUse) {
        if (landUse == null) return;
        
        List<HND_LandUse> landUses = getPermit().getBuildingUnit().getOtherLandUses();
        landUses.remove(landUse);
    }
    
    
    public List<HND_SpatialZone> getBuildingAreaRestrictingSZList() {
        if (buildingAreaRestrictingSZList == null) {
            buildingAreaRestrictingSZList = HND_SpatialZoneDAO
                    .loadSpatialZonesBySZAndLessBuiltAreaAllowed(
                            getPermit().getSelectedZone(),
                            getPermit().getBuildingUnit().getBuiltArea());
        }
        return buildingAreaRestrictingSZList;
    }
    public void setBuildingAreaRestrictingSZList(
            List<HND_SpatialZone> buildingAreaRestrictingSZList) {
        this.buildingAreaRestrictingSZList = buildingAreaRestrictingSZList;
    }
    
    public List<HND_SpatialZone> getNumberOfFloorsRestrictingSZList() {
        if (numberOfFloorsRestrictingSZList == null) {
            numberOfFloorsRestrictingSZList = HND_SpatialZoneDAO
                    .loadSpatialZonesBySZAndLessNumberOfFloorsAllowed(
                        getPermit().getSelectedZone(),
                        getPermit().getBuildingUnit().getNumberOfFloors());
        }
        return numberOfFloorsRestrictingSZList;
    }
    public void setNumberOfFloorsRestrictingSZList(
            List<HND_SpatialZone> numberOfFloorsRestrictingSZList) {
        this.numberOfFloorsRestrictingSZList = numberOfFloorsRestrictingSZList;
    }
    
    public List<HND_SpatialZone> getBuildingMaterialRestrictingSZList() {
        if (buildingMaterialRestrictingSZList == null) {
            buildingMaterialRestrictingSZList = new ArrayList<HND_SpatialZone>();
            
            List<HND_SpatialZone> szCandidateList = HND_SpatialZoneDAO
                    .loadSpatialZonesByIntersectingSZ(
                        getPermit().getSelectedZone());
            if (szCandidateList != null && szCandidateList.size() > 0)
                for (HND_SpatialZone hndSZ : szCandidateList)
                    for (HND_BuildingMaterial bm : hndSZ.getPermitNorm().getForbiddenBuildingMaterials())
                        //for each forbidden building material....
                        if (getPermit().getBuildingUnit().getBuildingMaterials().contains(bm)) {
                            if (!buildingMaterialRestrictingSZList.contains(hndSZ))
                                buildingMaterialRestrictingSZList.add(hndSZ);
                            if (!szBuildingMaterials(hndSZ).contains(bm))
                                szBuildingMaterials(hndSZ).add(bm);
                        }
        }
        return buildingMaterialRestrictingSZList;
    }
    public void setBuildingMaterialRestrictingSZList(
            List<HND_SpatialZone> buildingMaterialRestrictingSZList) {
        this.buildingMaterialRestrictingSZList = buildingMaterialRestrictingSZList;
    }
    
    public List<HND_BuildingMaterial> szBuildingMaterials(HND_SpatialZone sz) {
        if (szBuildingMaterials == null) {
            szBuildingMaterials = new HashMap<HND_SpatialZone, List<HND_BuildingMaterial>>();
        }
        List<HND_BuildingMaterial> bms = szBuildingMaterials.get(sz);
        if (bms == null) {
            bms = new ArrayList<HND_BuildingMaterial>();
            szBuildingMaterials.put(sz, bms);
        }
        return bms;
    }
    
    @Override
    public boolean isAdminAnalysisOK() {
        final List<?>[] listArr = { getBuildingAreaRestrictingSZList(), getBuildingMaterialRestrictingSZList(),
                getNumberOfFloorsRestrictingSZList(), getLandUseRestrictingSZList() };
        
        for (List<?> l : listArr)
            if (!listOK(l))
                return false;
        
        return true;
    }
    
    @Override
    public List<Task> getTaskList() {
        if (taskList == null) {
            taskList = BusinessProcess.instance().getTaskList(ProcessConfig.BUILDING_PERMIT_PROCESS);
        }
        return taskList;
    }
    @Override
    public List<Task> getPooledTaskList() {
        if (pooledTaskList == null) {
            pooledTaskList = BusinessProcess.instance().getPooledTaskList(ProcessConfig.BUILDING_PERMIT_PROCESS);
        }
        return pooledTaskList;
    }

    
    @End
    @CreateProcess(definition=ProcessConfig.BUILDING_PERMIT_PROCESS, processKey="#{buildingPermitHelper.permitId}")
    @Override
    @Transactional
    public String beginProcess() {
        HND_BuildingPermit hndPermit = getPermit();
        
        beginProcessAux(hndPermit, HND_PermitType.BUILDING_PERMIT);
        
        if (hndPermit.getBuildingUnit() != null) SigitDAO.save(hndPermit.getBuildingUnit());
        HND_BuildingPermitDAO.save(hndPermit);
        setPermitId(hndPermit.getId());

        BusinessProcess.instance().createProcess(ProcessConfig.BUILDING_PERMIT_PROCESS, hndPermit);

        return navigationHelper.navigateTo("/permits/building/activities");
    }

    @Override
    @BeginTask
    @Begin(join=true)
    @Transactional
    public String startTask() {
        setPermit(HND_BuildingPermitDAO.loadBuildingPermitByPresentationNo(
                getSelectedTask().getPresentationNo()));
        
        setPermitId(this.permit.getId());
        setParty(this.permit.getExtParty());
        
        BusinessProcess.instance().resumeProcess(ProcessConfig.BUILDING_PERMIT_PROCESS,
                getSelectedTask().getId());
        
        return navigationHelper.navigateTo(ProcessConfig.instance().getViewId(
                ProcessConfig.BUILDING_PERMIT_PROCESS, getSelectedTask().getName()));
    }

    @Override
    @Transactional
    public List<HND_PermitRuleGroup> getPermitRuleGroupList() {
        if (permitRuleGroupList == null) {
            permitRuleGroupList = HND_PermitRuleGroupDAO.loadPermitRuleGroupsByType(HND_PermitType.BUILDING_PERMIT);
        }
        return permitRuleGroupList;
    }
    
    @Override
    public HND_PermitRuleGroup getPermitRuleGroup() {
        if (permitRuleGroup == null) {
            permitRuleGroup = generalHelper.getSystemConfiguration().getBuildingPermitRuleGroup();
        }
        return permitRuleGroup;
    }
    
    
    @Override
    public HND_LandUse getLandUse() {
        return getPermit().getBuildingUnit().getLandUse();
    }
    @Override
    public List<HND_LandUse> getOtherLandUses() {
        return getPermit().getBuildingUnit().getOtherLandUses();
    }

    @Override
    public Set<HND_LandUse> getAllPermitLandUses() {
        Set<HND_LandUse> permitLandUses = listToSet(getPermit().getBuildingUnit().getOtherLandUses());
        permitLandUses.add(getPermit().getBuildingUnit().getLandUse());

        return permitLandUses;
    }
}

