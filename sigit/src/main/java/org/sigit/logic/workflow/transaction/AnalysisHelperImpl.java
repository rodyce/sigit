package org.sigit.logic.workflow.transaction;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("analysisHelper")
@Scope(value="sigit-conversation")
public class AnalysisHelperImpl implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
    public AnalysisHelperImpl() {
    }
}
