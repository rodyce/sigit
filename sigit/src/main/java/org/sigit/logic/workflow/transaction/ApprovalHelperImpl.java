package org.sigit.logic.workflow.transaction;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import org.sigit.commons.di.CtxComponent;
import org.sigit.dao.hnd.ladmshadow.ParcelDAO;
import org.sigit.model.hnd.ladmshadow.Parcel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component("approvalHelper")
@Scope(value="sigit-conversation")
public class ApprovalHelperImpl implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private TransactionHelper transactionHelper;
    private long presentationNo;
    
    private List<Parcel> beforeParcelList;
    private List<Parcel> afterParcelList;
    
    
    public ApprovalHelperImpl() {
        transactionHelper = (TransactionHelper)CtxComponent.getInstance("transactionHelper");
        presentationNo = transactionHelper.getPresentationNo();
    }

    @Transactional
    public List<Parcel> getBeforeParcelList() {
        if (beforeParcelList == null) {
            beforeParcelList = ParcelDAO.loadBeforeParcelsByPresentationNo(presentationNo);
        }
        return beforeParcelList;
    }
    public void setBeforeParcelList(List<Parcel> beforeParcelList) {
        this.beforeParcelList = beforeParcelList;
    }
    
    @Transactional
    public List<Parcel> getAfterParcelList() {
        if (afterParcelList == null) {
            afterParcelList = ParcelDAO.loadRequestedParcelsByPresentationNo(presentationNo);
        }
        return afterParcelList;
    }
    public void setAfterParcelList(List<Parcel> afterParcelList) {
        this.afterParcelList = afterParcelList;
    }
    
    public List<Parcel> getParcelList(String listIdx) {
        if (listIdx.equals("0"))
            return getBeforeParcelList();
        
        return getAfterParcelList();
    }
}
