package org.sigit.logic.security;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.sigit.i18n.ResourceBundleHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component("identity")
@Scope("session")
public class Identity implements Serializable {
    private static final long serialVersionUID = 1L;
    private static Logger log = LoggerFactory.getLogger(Identity.class.getName());
    
    @Autowired
    private Credentials credentials;
    
    @Autowired
    @Qualifier("authenticationManager")
    private transient ProviderManager authManager;
    
    @Autowired
    private HttpSession session;

    @Autowired
    private ResourceBundleHelper resBundle;

    private boolean loggedIn = false;

    @Transactional
    public String login() {
        Authentication authentication = new UsernamePasswordAuthenticationToken(
                credentials.getUsername(), credentials.getPassword());
        
        try {
            authentication = authManager.authenticate(authentication);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            loggedIn = true;
            return "/home";
        } catch (BadCredentialsException ex) {
            log.error(String.format("%s. [%s]",
                    resBundle.loadMessage("txt.login.invalid_credentials"),
                    credentials.getUsername()));
            FacesContext.getCurrentInstance().addMessage(
                    "",
                    new FacesMessage(
                            FacesMessage.SEVERITY_ERROR,
                            resBundle.loadMessage("txt.login.invalid_credentials"),
                            ""
                    )
            );
        } catch (LockedException ex) {
            log.error(String.format("%s. [%s]",
                    resBundle.loadMessage("txt.login.account_locked"),
                    credentials.getUsername()));
            FacesContext.getCurrentInstance().addMessage(
                    "",
                    new FacesMessage(
                            FacesMessage.SEVERITY_ERROR,
                            resBundle.loadMessage("txt.login.account_locked"),
                            ""
                    )
            );
        }
        
        loggedIn = false;
        return "/login";
    }
    
    public String logout() {
        return "/logout?faces-redirect=true";
    }
    
    public void doLogout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        session.invalidate();
        loggedIn = false;
    }
    
    public boolean isLoggedIn() {
        //Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        //return auth != null && auth.isAuthenticated();
        return loggedIn;
    }
    
    public boolean hasRole(String role) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        for (GrantedAuthority grantedAuth : auth.getAuthorities()) {
            if (role.equals(grantedAuth.getAuthority())) {
                return true;
            }
        }
        return false;
    }
    
    
    public String getUsername() {
        return loggedIn ? credentials.getUsername() : "";
    }
    
    public String getLoggedUserFullName() {
        return loggedIn ? credentials.getLoggedUserFullName() : "";
    }
}
