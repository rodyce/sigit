const pgp = require('pg-promise')();
var fs = require('file-system');
const {LargeObjectManager} = require('pg-large-object');
require('dotenv').config();

// If you are on a high latency connection and working with
        // large LargeObjects, you should increase the buffer size.
        // The buffer should be divisible by 2048 for best performance
        // (2048 is the default page size in PostgreSQL, see LOBLKSIZE)
const BUFFER_SIZE = 16384;
const BATCH_SIZE = 10;

// Database connection details;
const cn = {
    host: process.env.HOST, // 'localhost' is the default;
    port: process.env.PORT, // 5432 is the default;
    database: process.env.DATA_BASE,
    user: 'sigitmaster',
    password: process.env.PASSWORD
};

const SELECT_SQL = 'SELECT ea.name, ea.id as uuid FROM ladm_external.extarchive ea';
const INSERT_ARCHIVE_INTERNAL_SQL = 'INSERT INTO hnd_special.archiveinternalrepository(data, id)values($1, $2)';

const db = pgp(cn); // database instance;

function determinateStatusFile(name){

    var completePath = process.env.DOC_PATH + name;

    if (fs.existsSync(completePath)) {
        return true;
    }else{
        //console.warn(`file with name ${name} does not exists in file system!`);
        return false;
    }
}

async function insertLargeObject(batch){

    console.log("batch to create:");
    console.log(batch);

    await db.tx(async tx =>{
        // creating a sequence of transaction querie BEGIN:
        const man = new LargeObjectManager({pgPromise: tx});

        try{
            for(const node of batch){
                
                const [oid, stream] = await man.createAndWritableStreamAsync(BUFFER_SIZE);

                console.log(`Creating a large object for document ${node.name}, with oid: ${oid}.`);
                
                const fileStream = fs.createReadStream(process.env.DOC_PATH + node.name);
                fileStream.pipe(stream);

                try{
                    await db.none(INSERT_ARCHIVE_INTERNAL_SQL, [oid, node.uuid]);
                }catch(error){
                    console.error('INSERT ERROR:', error);
                    throw error;
                }
            }
        }catch(e){
            console.error(e);
            throw e;
        }
    })
}

function isLastItem(node, array){
    var last = array[array.length - 1];
    return node == last;
}
 
db.any(SELECT_SQL, [])
.then(data => {
    var arrSlot = [];
    data.forEach(node => {
        if(determinateStatusFile(node.name)){
            arrSlot.push(node);
        }

        if(arrSlot.length == BATCH_SIZE || isLastItem(node, data)){
            insertLargeObject(arrSlot);
            arrSlot = [];
        }
    });
})
.catch(error => {
    console.log('ERROR:', error); // print the error;
})


