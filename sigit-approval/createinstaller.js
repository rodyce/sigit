const electronInstaller = require('electron-winstaller');
const path = require('path')

getInstallerConfig();

async function getInstallerConfig () {
    try {
        console.log('creating windows installer');
        const rootPath = path.join('./')
        const outPath = path.join(rootPath, 'release-builds')
    
        await electronInstaller.createWindowsInstaller({
          appDirectory: path.join(outPath, 'SigitApproval-win32-ia32/'),
          outputDirectory: path.join(outPath, 'windows-installer'),
          authors: 'Le Bard Ruiz',
          exe: 'SigitApproval.exe',
        });
        console.log('It worked!');
    } catch (e) {
        console.log(`No dice: ${e.message}`);
    }
}

