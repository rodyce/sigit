import React, { Component } from 'react';
import './App.css';
import ReactDOM from 'react-dom';
import { Table, Divider, Modal, Button, Input, Form, Row, Col, Icon, Popconfirm} from 'antd';
import MaskedInput from 'react-maskedinput';
import 'antd/dist/antd.css';
const { confirm } = Modal;

require('dotenv').config();

const API_URL_BASE = process.env.REACT_APP_BASE_URL;
const PAGINATION_NUMBER = process.env.REACT_APP_PAGINATION_NUMBER;
const TEXT_MASK = process.env.REACT_APP_TEXT_MASK;


import {
  layer, control, //name spaces
  Controls, Map, Layers   //objects
} from "react-openlayers";

const CUSTOM_STATUS = {
  'DELIVER_FOR_EXTERNAL_APPROVAL': 'ENTREGAR PARA APROBACIÓN EXTERNA',
  'EXTERNAL_APPROVAL': 'APROBACIÓN EXTERNA'
}

const CUSTOM_SOURCE_TYPE = {
  'DEED': 'Escritura',
  'AGRI_CONSENT': 'Beneplácito agrícola',
  'OTHER': "Otro"
}

const CUSTOM_TRANSACTION_TYPE = {
  'MERGERS_PARTITIONS': 'Reuniones y Particiones'
}

const RRR_TYPE = {
  'OWNERSHIP': 'Propiedad',
  'OWNERSHIP_ASSUMED': 'Presunta propiedad'
}

// Presentation header module
class TransactionHeader extends Component{
  constructor(props){
    super(props);
  }

  readCurrentStatus(currentSatus){
    return CUSTOM_STATUS[currentSatus.trim()];
  }

  readtransactionType(type){
    return CUSTOM_TRANSACTION_TYPE[type.trim()];
  }

  render(){

    var transaction = this.props.data;

    return(
      <div style={{padding:"50px", width: '100%'}}>
        <div style={{float:"left"}}>
          <h2 style={{"border-bottom":"1px solid #ebedf0", "text-align": "center"}}>CONSULTA DE TRANSACCIONES</h2>
          <div style={{ width: '50%', padding:"5%", float:"left"}}>
          <h4 style={{"border-bottom":"1px solid #ebedf0"}}>Datos de la transacción</h4>
            <div className="item">
              <span>Fecha de presentación: </span>
              <label>{parseLocalDate(transaction["sigit:presentationDate"])}</label>
            </div>
            <div className="item">
              <span>Número de presentación: </span>
              <label>{transaction["sigit:presentationId"]}</label>
            </div>
            <div className="item">
              <span>No. de identidad de la persona que presenta: </span>
              <label>{transaction["sigit:presenterIdentity"]}</label>
            </div>
            <div className="item">
              <span>Nombre completo de la persona que presenta: </span>
              <label>{transaction["sigit:presenterName"]}</label>
            </div>
            <div className="item">
              <span>Descripción de la transacción: </span>
              <label>{transaction["sigit:description"]}</label>
            </div>
            <div className="item">
              <span>Tipo de solicitud segun analista: </span>
              <label>{this.readtransactionType(transaction["sigit:transactionType"])}</label>
            </div>
            <div className="item">
              <span>Dictamen sobre solicitud: </span>
              <label>{transaction["sigit:analysisDictum"]}</label>
            </div>
            <div className="item">
              <span>Observaciones adicionales	: </span>
              <label>{transaction["sigit:additionalObservations"]}</label>
            </div>
          </div>

          <div style={{ width: '50%', padding:"5%", float:"left"}}>
          <h4 style={{"border-bottom":"1px solid #ebedf0"}}>Estado y responsables de la transacción</h4>
            <div className="item">
              <span>Actividad actual: </span>
              <label>{this.readCurrentStatus(transaction["sigit:currentActivity"])}</label>
            </div>
            <div className="item">
              <span>Recepcionista de documentos: </span>
              <label>{transaction["sigit:receptionistFullName"]}</label>
            </div>
            <div className="item">
              <span>Aprobador: </span>
              <label>{transaction["sigit:approverUserName"]}</label>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

// Presentation administrative sources module
class AdministrativeSources extends Component {

  constructor(props) {
    super(props);

    this.headers  = [
      {
        title: 'Fecha',
        dataIndex: 'submission',
        key: 'submission'
      },
      {
        title: 'Tipo',
        dataIndex: 'type',
        key: 'type',
        render: text => (
          this.readCurrentType(text)
        )
      },
      {
        title: 'Descripción',
        dataIndex: 'description',
        key: 'description',
        render: text => (
          <span>
            <a href={ API_URL_BASE + "sigit/services/docs/" + text}>{text}</a>
            <Divider type="vertical" />
            <a href={ API_URL_BASE + "sigit/services/docs/" + text}>Ver</a>
          </span>
        ),
      }
    ];
  }

  readCurrentType(type){
    return CUSTOM_SOURCE_TYPE[type.trim()];
  }

  render() {
    try{
      return(
          <div class="resources-table">
            <h2>Fuentes documentales</h2>
            <Table {...{pagination: this.props.data.length >= PAGINATION_NUMBER}} 
              columns={this.headers} dataSource={this.props.data} />
          </div>
      );
    }catch(e){
      console.error(e);
    }
    
  }
  
}

// Presentation geometry module
class TransactionGeometry extends Component{

  constructor(props) {
    super(props);

    this.state = {
      data: [], 
    };
  }

  componentDidMount(){
    this.setState({
      data: this.props.data
    });
  }

  render(){
    return(
      <div style={{width: '100%', float:"left", marginBottom: "50px"}}>
        <h2 style={{"border-bottom":"1px solid #ebedf0", "text-align": "center"}}>Estado antes y después de la transacción</h2>
          <div ref="mapContainer" style={{float:"left", width: '50%', padding:"10"}}>
          <Map view={{center: [0, 0], zoom: 2}} >
            <Layers>
              <layer.Tile/>
              <layer.Vector/>
            </Layers>
            <Controls attribution={false} zoom={true}>
              <control.Rotate />
              <control.ScaleLine />
              <control.FullScreen />
              <control.OverviewMap />
              <control.ZoomSlider />
              <control.ZoomToExtent />
              <control.Zoom />
            </Controls>
          </Map>
          </div>
          <div ref="mapContainer" style={{float:"left", width: '50%', padding:"10"}}>
          <Map view={{center: [0, 0], zoom: 2}} >
            <Layers>
              <layer.Tile/>
              <layer.Vector/>
            </Layers>
            <Controls attribution={false} zoom={true}>
              <control.Rotate />
              <control.ScaleLine />
              <control.FullScreen />
              <control.OverviewMap />
              <control.ZoomSlider />
              <control.ZoomToExtent />
              <control.Zoom />
            </Controls>
          </Map>
          </div>
        </div>
    )
  }

}

// Presentation parcels module
class ParcelsView extends Component{
  constructor(props) {
    super(props);
  }

  getHeader(type){

    var header  = [
      {
        title: 'Entidad',
        dataIndex: 'entity',
        key: 'entity'
      },
      {
        title: 'Tipo de ' + type,
        dataIndex: 'type',
        key: 'type'
      },
      {
        title: 'Participación',
        dataIndex: 'participation',
        key: 'participation',
        align: 'center'
      }
    ];

    return header;
  }


  drawParcels(afterData, beforeData){

    var rightsHeader = this.getHeader("derecho");
    var restrictionsHeader = this.getHeader("restricción");
    var responsabilitiesHeader = this.getHeader("responsabilidad");

    var parcelsAfterView = [];
    var parcelsBeforeView = [];

    afterData.map(function(item, i){
      parcelsAfterView.push(
      <div className="drrContainer">
          <div style={{"border":"1px solid #ebedf0", padding:"20px", width: "100%"}}>
            <h4 style={{"border-bottom":"1px solid #ebedf0"}}>Parcela con clave municipal: {item.municipalKey}</h4>
            <div class="item-details">
              <div className="item">
                <span>Ficha de campo: </span>
                <label>{item.fieldTab}</label>
              </div>
              <div className="item">
                <span>Clave catastral: </span>
                <label>{item.cadastralKey}</label>
              </div>
            </div>
            <h4 style={{"border-bottom":"1px solid #ebedf0"}}>Derechos</h4>
            <Table {...{pagination: item.responsibilities >= PAGINATION_NUMBER}}  
              columns={rightsHeader} dataSource={item.rights} />
            <h4 style={{"border-bottom":"1px solid #ebedf0"}}>Restricciones</h4>
            <Table {...{pagination: item.responsibilities >= PAGINATION_NUMBER}} 
              columns={restrictionsHeader} dataSource={item.restrictions} />
            <h4 {...{pagination: item.responsibilities >= PAGINATION_NUMBER}} 
              style={{"border-bottom":"1px solid #ebedf0"}}>Responsabilidades</h4>
            <Table columns={responsabilitiesHeader} dataSource={item.responsibilities} />
          </div>
        </div>);
    });

    beforeData.map(function(item, i){
      parcelsBeforeView.push(
      <div className="drrContainer">
          <div style={{"border":"1px solid #ebedf0", padding:"20px", width: "100%"}}>
            <h4 style={{"border-bottom":"1px solid #ebedf0"}}>Parcela con clave municipal: {item.municipalKey}</h4>
            <div class="item-details">
              <div className="item">
                <span>Ficha de campo: </span>
                <label>{item.fieldTab}</label>
              </div>
              <div className="item">
                <span>Clave catastral: </span>
                <label>{item.cadastralKey}</label>
              </div>
            </div>
            <h4 style={{"border-bottom":"1px solid #ebedf0"}}>Derechos</h4>
            <Table {...{pagination: item.responsibilities >= PAGINATION_NUMBER}} 
              columns={rightsHeader} dataSource={item.rights} />
            <h4 style={{"border-bottom":"1px solid #ebedf0"}}>Restricciones</h4>
            <Table {...{pagination: item.responsibilities >= PAGINATION_NUMBER}} 
              columns={restrictionsHeader} dataSource={item.restrictions} />
            <h4 {...{pagination: item.responsibilities >= PAGINATION_NUMBER}} 
              style={{"border-bottom":"1px solid #ebedf0"}}>Responsabilidades</h4>
            <Table columns={responsabilitiesHeader} dataSource={item.responsibilities} />
          </div>
        </div>);
    })

    return (
      <div style={{width: '100%', float:"left"}}>
        <h2 style={{"border-bottom":"1px solid #ebedf0", "text-align": "center"}}>Derechos, Restricciones y Responsabilidades</h2>
        <div class="parcel parcelAfter">
          <h4 style={{"text-align": "center"}}>ANTES</h4>
          {parcelsBeforeView}
        </div>
        <div class="parcel parcelBefore">
          <h4 style={{"text-align": "center"}}>DESPUÉS</h4>
          {parcelsAfterView}
        </div>
      </div>
    );
  }

  render(){
    return (this.drawParcels(this.props.afterData, this.props.beforeData))
  }
}

// Component for render details without cadestarl key
class FormDetails extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: []
    };
  }

  componentDidMount() {
    this.props.onRef(this)
    this.setState({
      data: this.props.data
    });
  }

  componentWillUnmount() {
    this.props.onRef(undefined)
  }

  getFields(node) {
    const { getFieldDecorator } = this.props.form;
    const children = [];

    children.push(
      <Col span={8} style={{ display: 'block' }}>
          <Form.Item label={node.parcelData.fieldTab}></Form.Item>
      </Col>
    );

    children.push(
      <Col span={8} style={{ display: 'block' }}>
        <Form.Item label="">
          {getFieldDecorator("cadastralKey." + node.parcelData.featureId, {
            rules: [{ required: true, message: 'Requerido!' }],
          })(
            <MaskedInput mask={TEXT_MASK}
              name="card"
              render={(ref, props) => {
                return (<Input placeholder="Ingrese clave catastral" allowClear/>)
              }}
            />
          )}
        </Form.Item>
      </Col>
    );

    children.push(
      <Col span={8} style={{ display: 'block' }}>
        <Form.Item label="">
          {getFieldDecorator("observations." + node.parcelData.featureId, {
            rules: [{ required: false, message: 'Requerido!' }],
          })(
            <Input
              placeholder="Ingrese observaciones" allowClear 
            />,
          )}
        </Form.Item>
      </Col>
    );

    return children;
  }

  validateFields(){

    var response = {
      completed: false,
      values: {}
    }

    this.props.form.validateFields((err, values) => {

      if (!err) {
        console.log('Received values of form: ', values);
        response.completed = true;
        response.values = values;
      }else{
        return response;
      }

    });

    return response;
  };

  render() {
    var detailRows = [];

    detailRows.push(
      <div id="detail-header">
        <Col span={8} style={{ display: 'block' }}>
              <Form.Item label="Ficha de Campo"></Form.Item>
          </Col>
          <Col span={8} style={{ display: 'block' }}>
              <Form.Item label="Clave Catastral"></Form.Item>
          </Col>
          <Col span={8} style={{ display: 'block' }}>
              <Form.Item label="Observaciones"></Form.Item>
          </Col>
        </div>
    );

    this.state.data.map(item => (
      detailRows.push(<Row gutter={24}>{this.getFields(item)}</Row>)
    ))

    return (
      <Form className="details-form" >
        <div id="detail-content">{detailRows}</div>
      </Form>
    );
  }
}

// Component for render details without cadestarl key
class FormLogin extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: []
    };
  }

  componentDidMount() {
    this.props.onRef(this)
    this.setState({
      data: this.props.data
    });
  }

  componentWillUnmount() {
    this.props.onRef(undefined)
  }


  validateFields(){

    var response = {
      completed: false,
      values: {}
    }

    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        response.completed = true;
        response.values = values;
      }else{
        return response;
      }

    });

    return response;
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
      <Form className="login-form">
        <Form.Item>
          {getFieldDecorator('username', {
            rules: [{ required: true, message: 'Ingrese el usuario!' }],
          })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Usuario"
            />,
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Ingrese la contraseña!' }],
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="Contraseña"
            />,
          )}
        </Form.Item>
      </Form>
      </div>
    );
  }
}

// ----------------- Global Functions ------------------------

// Make parcel node
function makeParcel(node){
  var parcel = {
    cadastralKey: node["sigit:cadastralKey"] != '\"\"' ? 
      node["sigit:cadastralKey"] : null,
    municipalKey: null,
    fieldTab: node["sigit:fieldTab"],
    featureId: node["gml:id"],
    name: node["sigit:name"],
    rights: [],
    restrictions: [],
    responsibilities: []
  };

  addRRRNodes(parcel, node["sigit:RRR"]["sigit:Rights"], "Right");
  addRRRNodes(parcel, node["sigit:RRR"]["sigit:Restrictions"], "Restriction");
  addRRRNodes(parcel, node["sigit:RRR"]["sigit:Responsibilities"], "Responsibility");

  return parcel;
}

// Determine if an object is empty
function isEmpty(obj) {
  for(var key in obj) {
      if(obj.hasOwnProperty(key))
          return false;
  }
  return true;
}

// Make participation tag
function makeParticipation(share){

  if(share["sigit:denominator"] == share["sigit:numerator"]){
    return "1";
  }else{
    return share["sigit:numerator"] + "/" + share["sigit:denominator"];
  }

}

// Parse array for RRR by type
function addRRRNodes(parcel, element, mode){

  var rrrType = mode == "Right" ? 
    "rightType" : mode == "Restriction" 
    ? "restrictionType" : "responsibilityType";

  if(Array.isArray(element["sigit:" + mode])){

    element["sigit:" + mode].forEach(function(key) {

      var rrr = {
        key: key["gml:id"],
        entity: key["sigit:partyIdentity"],
        name: key["sigit:partyName"],
        type: RRR_TYPE[key["sigit:" + rrrType]],
        participation: makeParticipation(key["sigit:share"])
      }

      switch(mode){
        case "Right":
            parcel.rights.push(rrr);
          break;
        case "Restriction":
            parcel.restrictions.push(rrr);
          break;
        case "Responsibility":
            parcel.responsibilities.push(rrr);
          break;
      }
    });
  }else{

    if(isEmpty(element)){
      return;
    }

    var rrr = {
      key: element["sigit:" + mode]["gml:id"],
      entity: element["sigit:" + mode]["sigit:partyIdentity"],
      name: element["sigit:partyName"],
      type: RRR_TYPE[element["sigit:" + mode]["sigit:" + rrrType]],
      participation: makeParticipation(element["sigit:" + mode]["sigit:share"])
    }

    switch(mode){
      case "Right":
          parcel.rights.push(rrr);
        break;
      case "Restriction":
          parcel.restrictions.push(rrr);
        break;
      case "Responsibility":
          parcel.responsibilities.push(rrr);
        break;
    }
  }
}

function parseLocalDate(date){
  return new Date(date).toLocaleString();
}

function showDialog(message, error){
  confirm({
    title: "SIGIT - Confirmación",
    content: message,
    okType: error ? 'danger' : '',
    cancelButtonProps: {
      disabled: true,
    },
    onOk () {
    },
    onCancel() {
      
    },
  });
}

function confirmDialog(message, okCallback){
  confirm({
    title: "SIGIT - Confirmación",
    content: message,
    onOk() {
      okCallback();
    },
    onCancel() {
      
    },
  })
}

// ----------------------------------------------------------


// Main app controller component
class SigitRestController extends Component {

  _isMounted = false;
  _log_in = false;
  _currentAction = null;

	constructor(props) {
    super(props);
    
    this.onShowPresentation = this.onShowPresentation.bind(this);
    this.fetchService = this.fetchService.bind(this);
    this.onApprove = this.onApprove.bind(this);

    this.Actions = [
      {name:"Main"}, 
      {name:"Next"}, 
      {name:"Login"} 
    ];

    this.pagination = { position: 'bottom' };

    this.setAction(this.Actions[0]);

    this.main_grid_header  = [
      {
        title: 'Presentación',
        dataIndex: 'presentationId',
        key: 'presentationId'
      },
      {
        title: 'Fecha',
        dataIndex: 'presentationDate',
        key: 'presentationDate',
        render: (text) => (
          parseLocalDate(text)
        ),
      },
      {
        title: 'Acción',
        key: 'presentationId',
        render: (text, record) => (
          <span onClick={((e) => this.onShowPresentation(e, record.presentationId))}>
            <a href="javascript:;">Ver</a>
          </span>
        ),
      }
    ];

    this.state = {
      transactions: [], 
      parcelsAfterJson: [],
      currentResponse: null,
      currentUrl: null,
      requestFailed: false,
      main_url: "municipalTransaction",
      formLayout: 'horizontal',
      loading: false,
      usingIconLoading: true,
      presentationId: null,
      modalVisible: false,
      dialogResultOk: false,
      editingKey: '',
      confirmLoading: false,
      enabled_status: '',
      pagination_deliver: false,
      pagination_external: false,
      credentials: {
        username: "admin",
        password: "sigithn",
        user: {
          identity: "0801-1991-17174",
          name: "tester",
          department: "testing",
          contactPhone: "98345567",
          contactEmail: "test@gmail.com"
        }
      },
      bodyJson: {
        details: [
        ]
      },
      currentPresentationView: <p>SIGIT</p>
    };
  }

  componentDidMount() {
    this.loadMain();    
  }

  setAction(action){
    this._currentAction = action;
  }

  loadMain(){
    this._isMounted = true;

    if(this._log_in){
      this.setAction(this.Actions[0]);
      this.fetchService(this.state.main_url, "GET", {}, null);
    }else{
      this.setAction(this.Actions[2]);
      document.getElementById('loading').style.display = "none";
      this.forceUpdate();
    }
  }

  // Next presentation process
  onApplyNextPresentation = (e) => {
    this.setAction(this.Actions[1]);
    this.fetchServiceAndRender(this._currentAction, "municipalTransaction/next");
  }

  onShowPresentation = (e, presentationId) => {
    this._isMounted = false;

    this.fetchService("municipalTransaction/"+presentationId, "GET", {}, null).then(x=>{
      if(!this.state.requestFailed){

        var currentPrsentationView = this.renderCurrentPresentation(false);

        this.setState({
          modalVisible: true,
          currentPresentationView: currentPrsentationView
        });
      }
    });
  }

  async fetchServiceAndRender(action, api_url){
    this.fetchService(api_url, "GET", {}, action).then(()=>{
      if(!this.state.requestFailed){
        this.forceUpdate();
        console.log(this.state.currentResponse);
      }else{
        this.reloadInit();
      }
    });
  }

  updateIconLoading(loading){
    document.getElementById('loading')
      .style.display = loading ? "block" : "none";
  }

  // API client
  async fetchService(api_url, method, body, action){

    if(this.state.usingIconLoading){
      this.updateIconLoading(true);
    }
    
    this.state.currentUrl = api_url;
    var restConfiguration = null;

    if(method == "GET"){
      restConfiguration = { method: method }
    }else{
      restConfiguration = { 
        headers : { 
          'Content-Type': 'application/json',
          'Accept': 'application/json'
         }, 
        method: 'POST', 
        body: JSON.stringify(body)
      }
    }

    try{

      let response = await fetch(API_URL_BASE + 'sigit/services/v1/' + api_url, restConfiguration);
      let result = await response.json();
      this.state.requestFailed = !response.ok;

      if (!this.state.requestFailed) {
        if(this._isMounted){
          this.setAction(action != null ? action : this._currentAction);

          this.setState({
            transactions: result.transaction,
            currentResponse: result,
            enabled_status: 'disabled-main'
          });
        }else{
          this.setState({
            currentResponse: result,
            modalVisible: false,
            enabled_status: 'disabled-main'
          });
        }
      }else{
        showDialog(result.faultInfo.message, true);
      }

      this.updateIconLoading(false);

    }catch(e){
      showDialog("No se puede conectar, intente luego!", true);
      this.updateIconLoading(false);
      this.state.requestFailed = true;
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  // Parse administrative sources node
  parseAdministrativeSourceResponse(transaction){
    var sources = transaction["sigit:administrativeSources"]["sigit:AdministrativeSource"];
    var jsonData = [];

    if(Array.isArray(sources)){
      sources.forEach(function(element) {
        jsonData.push(
          {
            key: element["gml:id"],
            submission : parseLocalDate(element["sigit:submission"]),
            type: element["sigit:type"],
            description: element["sigit:sID"],
          }
        );
      });
    }else{
      if(!isEmpty(sources)){
        jsonData.push(
          {
            key: sources["gml:id"],
            submission : parseLocalDate(sources["sigit:submission"]),
            type: sources["sigit:type"],
            description: sources["sigit:sID"],
          }
        );
      }
    }
    return jsonData;
  }

  // Parse parcels node
  parseParcelsResponse(transaction, after_node){
    var parcels = [];
    var jsonData = [];

    if(after_node){
      parcels = transaction["sigit:parcelsAfterTransaction"]["sigit:Parcel"];
    }else{
      parcels = transaction["sigit:parcelsBeforeTransaction"]["sigit:Parcel"];
    }

    if(Array.isArray(parcels)){
      // Make parcel array
      parcels.forEach(function(element) {
        var p = makeParcel(element);
        jsonData.push(p);
      });
    }else{
      if(!isEmpty(parcels)){
        var parcel = makeParcel(parcels);
        jsonData.push(parcel);
      }
    }

    return jsonData;
  }

  // Make details node for json body
  makeDetailsForCadastralKey(parcels){
    var details = [];

    if(Array.isArray(parcels)){
      // Make parcel array
      parcels.forEach(function(node) {
        if(node.cadastralKey == null){
          details.push(
            {
              parcelData: {
                fieldTab: node.fieldTab,
                featureId: node.featureId,
                newCadastralKey: null,
                observations: null
              }
            }
          )
        }
      });
    }else{
      if(isEmpty(parcels)){
        return details;
      }

      if(String(parcels.cadastralKey) === ""){
        details.push(
          {
            parcelData: {
              fieldTab: parcels["sigit:fieldTab"],
              featureId: parcels["gml:id"],
              newCadastralKey: null,
              observations: null
            }
          }
        )
      }
    }

    return details;
  }

  handleCancel = e => {
    this.setState({
      modalVisible: false,
      dialogResultOk: false,
      usingIconLoading: true
    });
  };

  // Render main App
  reloadInit = () => {
    this.loadMain();
  }

  onClose = () =>{
    this.loadMain();
  }

  ScrollUp = () => {
    window.scrollTo(0, 0);
  }

  async onApprove(e){
    var modalResponse = this.formDetails.validateFields();

    if(modalResponse.completed){
      confirmDialog('¿Está seguro del proceso de aprobación?', ()=>{
        this.state.bodyJson.details.forEach(function(node){
          node.parcelData.newCadastralKey = modalResponse.values.cadastralKey[node.parcelData.featureId];
          node.parcelData.observations = modalResponse.values.observations[node.parcelData.featureId];
        });
        
        this.approveFinalWorkflowState();
      });
    }
  };

  // Attend las presentation process
  onAttendPresentation = () => {
    this.state.usingIconLoading = false;

    var body = {
      entityName: "IP",
      presentationId: this.state.presentationId,
      approved: true,
      approverData: {
        approverIdentity: "0801-1991-17174",
        approverName: "tester",
        department: "testing",
        contactPhone: "98345567",
        contactEmail: "test@gmail.com"
      },
      details: []
    }

    body.details = this.makeDetailsForCadastralKey(this.state.parcelsAfterJson);

    // If there are new details without cadastral key
    if(body.details.length > 0){
      // Set body for request
      this.setState({
        bodyJson: body,
        modalVisible: true
      });
    }else{// Apply status without details

      confirmDialog('¿Está seguro del proceso de aprobación?', ()=>{
        this.approveFinalWorkflowState();
      });
    }
  };

  // Final approvation process
  async approveFinalWorkflowState(){
    this._isMounted = false;
    
    this.fetchService("municipalTransaction/" + this.state.presentationId +"/entityName/PATH", null, 
      "post", this.state.bodyJson).then(()=>{
      //if request failed, load 
      if(!this.state.requestFailed){
        showDialog("proceso realizado correctamente", false);
        this.reloadInit();
      }else{
        showDialog("request fail", true);
      }
    });
  }

  // Global render
  render() {   
    ReactDOM.render(<Button id="loading-button" type="primary" shape="circle" loading />, 
     document.getElementById('loading'));

    return (
      this.rederData(this.index)
    )
  }

  // Split transaction list by activity
  splitTransactions(transactions, external_approval){
    var arr = [];
    transactions.forEach(function(key) {
      if(external_approval){
        if(String(key.currentActivity) === "EXTERNAL_APPROVAL"){
          arr.push(key);
        }
      }else{
        if(String(key.currentActivity) === "DELIVER_FOR_EXTERNAL_APPROVAL"){
          arr.push(key);
        }
      }
    });

    return arr;
  }

  // Render details without cadestral key
  renderDetails = () =>{
    const FormDetailsForCadestralKeys = Form
      .create({ name: 'details' })(FormDetails);

    return (
      <div>
        <FormDetailsForCadestralKeys 
          onRef={ref => (this.formDetails = ref)} 
          data={this.state.bodyJson.details}
        />
      </div>
    );
  }

  onLogin = (e) => {
    e.preventDefault();

    var modalResponse = this.formLogin.validateFields();

    if(modalResponse.completed){
      if(modalResponse.values.username == this.state.credentials.username &&
        modalResponse.values.password == this.state.credentials.password){

          this._log_in = true;
          this.loadMain();
        }else{
          showDialog("credenciales incorrectas!", true);
        }
    }
  }

  // Get render view for login
  renderLogin = () =>{
    const FormUserLogin = Form
      .create({ name: 'details' })(FormLogin);

    return (
      <div id="login-block">
        <FormUserLogin 
          onRef={ref => (this.formLogin = ref)} 
        />
        <Button
          lassName="login-form-button"
          type="primary"
          icon="check"
          onClick={this.onLogin.bind(this)}
          >
            Ingresar
        </Button>
      </div>
    );
  }

  // Get render data for current presentation view
  renderCurrentPresentation = (toApprove) =>{
    try{
      var transaction = this.state.currentResponse
      ["gml:FeatureCollection"]["gml:featureMembers"]["sigit:Transaction"];
      //Parse json data for administrative sources
      var jsonData = this.parseAdministrativeSourceResponse(transaction);
      //Parcel json data for administrative sources
      var parcelsAfterJson = this.parseParcelsResponse(transaction, true);
      //Parcel json data for administrative sources
      var parcelsBeforeJson = this.parseParcelsResponse(transaction, false);

      this.state.presentationId = transaction["sigit:presentationId"];

      this.state.parcelsAfterJson = parcelsAfterJson;
    }catch(e){
      return (<div>{e}</div>);
    }

    var approvationHeader = (
      <div>
        <Modal
          width="80%"
          title="Asignar Claves Catastrales"
          visible={this.state.modalVisible}
          confirmLoading={this.state.confirmLoading}
          onOk={this.onApprove}
          onCancel={this.handleCancel}
        >
          {this.renderDetails()}
        </Modal>
        <div id="approvation-header">
          <div id="header-actions">
            <Popconfirm
              placement="topRight"
              title="¿Está seguro de aprobar esta presentación?"
              onConfirm={this.onAttendPresentation}
              okText="Yes"
              cancelText="No"
            >
              <div id="action-bar">
                <Button
                  type="primary"
                  icon="poweroff"
                  onClick={this.ScrollUp}
                >
                  Aprobar
                </Button>
              </div>
              <div id="close-button">
                <Button
                  class="close"
                  type="danger"
                  icon="left"
                  onClick={this.onClose}
                >
                  Regresar
                </Button>
              </div>
            </Popconfirm>
          </div>
        </div>
      </div>
    );

    return(
      <div id="transaction-view" className={this.enabled_status}>
        {toApprove ? approvationHeader : ''}
        <TransactionHeader data={transaction}/>
        <AdministrativeSources data={jsonData}/>
        <TransactionGeometry/>
        <ParcelsView afterData={parcelsAfterJson} beforeData={parcelsBeforeJson}/>
      </div>
    )
  }

  // Render respective data by action
  rederData = () =>{
    //console.log(this.state.currentResponse);
    switch(this._currentAction.name){
      //Main
      case this.Actions[0].name:

        var deliver_data = this.state.transactions != null ?
          this.splitTransactions(this.state.transactions, true) : [];
        var external_data = this.state.transactions != null ?
          this.splitTransactions(this.state.transactions, false) : [];

        return (
          <div id="main">

          <Modal
            width="95%"
            style={{ top: 20 }}
            title="Detalle de transacción"
            visible={this.state.modalVisible}
            confirmLoading={this.state.confirmLoading}
            onOk={this.handleCancel}
            onCancel={this.handleCancel}
          >
            {this.state.currentPresentationView}
          </Modal>

          <Button
              type="primary"
              icon="poweroff"
              onClick={this.onApplyNextPresentation.bind(this)}
          >
            Atender Ultima
          </Button>
          <div class="main-data" >
            <h3>Actividades de Transacciones</h3>
            <div class="main-table">
            <Table {...{pagination: deliver_data.length >= PAGINATION_NUMBER}} columns={this.main_grid_header} dataSource={external_data} />
            </div>
          </div>
          <div {...this.state} class="main-data">
            <h3>Actividades pendientes por atender</h3>
            <div class="main-table">
            <Table {...{pagination: external_data.length >= PAGINATION_NUMBER}} columns={this.main_grid_header} dataSource={deliver_data} />
            </div>
          </div>
          </div>
        )
        
      // Next presentation
      case this.Actions[1].name:
        //Current transaction response
        return this.renderCurrentPresentation(true);
      case this.Actions[2].name:
        return this.renderLogin();
    }
  }
}

ReactDOM.render(
	<SigitRestController action="Main" />, 
	document.getElementById('root')
);

export default SigitRestController;
