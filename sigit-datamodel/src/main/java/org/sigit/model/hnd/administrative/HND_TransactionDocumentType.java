package org.sigit.model.hnd.administrative;

public enum HND_TransactionDocumentType {
    
    DEED,
    
    RESOLUTION,
    
    NOTARIAL_ACT,
    
    AWARDING_ACT,
    
    PRIVATE_DOCUMENT,
    
    ANNOTATION_FULL_CERTIFICATION
    
}
