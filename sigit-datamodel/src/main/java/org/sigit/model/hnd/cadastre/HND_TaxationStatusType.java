package org.sigit.model.hnd.cadastre;

public enum HND_TaxationStatusType {
    SOLVENT,
    
    INSOLVENT,
    
    UNKNOWN
}
