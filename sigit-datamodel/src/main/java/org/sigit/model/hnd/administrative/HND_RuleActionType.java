package org.sigit.model.hnd.administrative;

public enum HND_RuleActionType {
    PLUS,
    
    MINUS,
    
    ASTERISK
}
