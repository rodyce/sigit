package org.sigit.model.hnd.administrative;

public enum HND_TransactionType {
    BUILDING_PERMIT,
    
    MUNICIPAL_TRANSACTION,
    
    OPERATION_PERMIT,
    
    TOPOGRAPHIC_MAINTENANCE
}
