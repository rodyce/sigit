package org.sigit.model.hnd.administrative;

public enum HND_ActivityType {
    PRESENTATION,
    
    ANALYSIS,
    
    ASSESSMENTS,
    
    DATAENTRY,
    
    APPROVAL,
    
    DELIVER_FOR_EXTERNAL_APPROVAL,
    
    EXTERNAL_APPROVAL,
    
    RETURN,
    
    END
}
