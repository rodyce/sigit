package org.sigit.model.hnd.administrative;

public enum HND_ComparisonOperatorType {
    LT,
    
    LE,
    
    EQ,
    
    GE,
    
    GT
}
