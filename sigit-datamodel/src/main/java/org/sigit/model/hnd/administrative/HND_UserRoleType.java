package org.sigit.model.hnd.administrative;

public enum HND_UserRoleType {
    RECEPTIONIST,
    
    ANALYST,
    
    EDITOR,
    
    MANAGER,
    
    APPROVER,
    
    SURVEYING_TECHNICIAN,
    
    EXTERNAL_QUERIER,
    
    INTERNAL_QUERIER,
    
    SYSTEM_ADMINISTRATOR
}
