package org.sigit.model.hnd.cadastre;

public enum HND_LayerType {
    SPATIAL_ZONE,
    
    PARCEL,
    
    BUILDING,
    
    STREET,
    
    RASTER
}
