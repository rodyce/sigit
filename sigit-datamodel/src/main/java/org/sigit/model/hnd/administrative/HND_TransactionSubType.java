package org.sigit.model.hnd.administrative;

public enum HND_TransactionSubType {
    SELL_OR_BUY,
    
    DONATION,
    
    BARTER,
    
    JUDICIAL_ADJUDICATION,
    
    MORTGAGING,
    
    EMBARGOING,
    
    UNMORTGAGING,
    
    UNEMBARGOING,
    
    SALE_OF_PORTION,
    
    DISMEMBERMENT,
    
    RIGHTS_MUTATION
}
