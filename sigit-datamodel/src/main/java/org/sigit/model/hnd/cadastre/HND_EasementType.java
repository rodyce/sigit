package org.sigit.model.hnd.cadastre;

public enum HND_EasementType {
    ELECTRIC_PIPELINE,
    
    AQUEDUCT,
    
    TRANSIT
}
