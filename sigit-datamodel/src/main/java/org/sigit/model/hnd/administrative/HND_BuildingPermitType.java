package org.sigit.model.hnd.administrative;

public enum HND_BuildingPermitType {
    TYPE1_RESIDENTIAL,
    
    TYPE2_RESHUFFLE,
    
    TYPE3_FENCE,
    
    TYPE4_COMMERCIAL_INDUSTRIAL
}
