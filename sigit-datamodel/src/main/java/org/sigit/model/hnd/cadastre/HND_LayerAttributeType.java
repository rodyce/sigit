package org.sigit.model.hnd.cadastre;

public enum HND_LayerAttributeType {
    INTEGER,
    
    DECIMAL,
    
    STRING,
    
    DATE
}
