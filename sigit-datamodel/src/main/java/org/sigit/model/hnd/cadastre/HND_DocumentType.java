package org.sigit.model.hnd.cadastre;

public enum HND_DocumentType {
    PUBLIC,
    
    PRIVATE,
    
    NO_DOCUMENT
}
