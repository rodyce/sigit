package org.sigit.model.hnd.cadastre;

public enum HND_LandUseType {
    ADAMEC_APPROACH,
    
    ANDERSON,
    
    CHAPING_AND_KAISER,
    
    COLOMBIAN,
    
    TOPOGRAFICA,
    
    DUHAMEL,
    
    ECE_UN,
    
    EURO_STAT,
    
    ISIC_REV2,
    
    ISIC_REV3,
    
    ITC,
    
    KLECKER_1981,
    
    MUECHER,
    
    REMMELZWAAL_APPROACH,
    
    USGS
}
