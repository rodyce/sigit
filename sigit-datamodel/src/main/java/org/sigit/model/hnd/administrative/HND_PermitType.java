package org.sigit.model.hnd.administrative;

public enum HND_PermitType {
    BUILDING_PERMIT,
    
    OPERATION_PERMIT
}
