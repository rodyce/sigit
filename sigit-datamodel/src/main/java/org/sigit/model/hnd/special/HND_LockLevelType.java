package org.sigit.model.hnd.special;

public enum HND_LockLevelType {
    UNLOCKED,
    
    LOCKED_EXPLICIT,
    
    LOCKED_IMPLICIT,
    
    LOCKED_IMPLICIT_2
}
