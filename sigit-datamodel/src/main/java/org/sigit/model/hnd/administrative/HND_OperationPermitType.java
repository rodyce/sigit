package org.sigit.model.hnd.administrative;

public enum HND_OperationPermitType {
    TYPE1_NO_RISK,
    
    TYPE2_MEDIUM_RISK,
    
    TYPE3_HIGH_RISK
}
