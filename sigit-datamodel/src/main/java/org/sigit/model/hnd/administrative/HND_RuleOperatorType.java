package org.sigit.model.hnd.administrative;

public enum HND_RuleOperatorType {
    DISTANCE,
    
    OVERLAP,
    
    WITHIN,
    
    TOUCH,
    
    CROSS,
    
    CONTAINS,
    
    CROSSED_BY,
    
    DISJOINT
}
