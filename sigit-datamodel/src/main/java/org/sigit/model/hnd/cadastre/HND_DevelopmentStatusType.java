package org.sigit.model.hnd.cadastre;

public enum HND_DevelopmentStatusType {
    UNKNOWN,
    
    VACANT,
    
    IMPROVED
}
