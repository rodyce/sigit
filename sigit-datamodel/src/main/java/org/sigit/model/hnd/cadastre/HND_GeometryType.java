package org.sigit.model.hnd.cadastre;

public enum HND_GeometryType {
    POINT,
    
    LINE,
    
    POLYGON
}
