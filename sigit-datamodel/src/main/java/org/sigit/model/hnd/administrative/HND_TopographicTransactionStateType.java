package org.sigit.model.hnd.administrative;

public enum HND_TopographicTransactionStateType {
    
    INITIATED,
    
    FINISHED
    
}
