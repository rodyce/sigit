package org.sigit.model.hnd.administrative;

public enum HND_MunicipalTransactionType {
    TITLE_DEED_ADJUDICATION,
    
    LIEN,
    
    CANCELLATION,
    
    TRANSFER_OF_OWNERSHIP,
    
    SPECIAL_ACT_CONTRACT,
    
    DONATION,
    
    INHERITANCE,
    
    CHANGES_CORRECTIONS,
    
    MERGERS_PARTITIONS,
    
    VARIOUS
}
