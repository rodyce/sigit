package org.sigit.model.ladm.spatialunit;

public enum LA_DimensionType {
    _0D,
    
    _1D,
    
    _2D,
    
    _3D,
    
    LIMINAL
}
