package org.sigit.model.ladm.spatialunit.surveyingandrepresentation;

public enum LA_SpatialSourceType {
    FIELD_SKETCH,
    
    GNSS_SURVEY,
    
    ORTHO_PHOTO,
    
    RELATIVE_MEASUREMENT,
    
    TOPO_MAP,
    
    VIDEO
    
}
