package org.sigit.model.ladm.administrative;

public enum LA_AdministrativeSourceType {
    AGREEMENT,
    
    AGRI_CONSENT,
    
    AGRI_LEASE,
    
    AGRI_NOTARY_STATEMENT,
    
    COURT_ORDER,
    
    DEED,
    
    MORTGAGE,
    
    PROCLAMATION,
    
    TITLE,
    
    ORIGINAL_MEASURE,
    
    OTHER
}
