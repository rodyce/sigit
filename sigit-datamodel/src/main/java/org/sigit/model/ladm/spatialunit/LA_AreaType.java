package org.sigit.model.ladm.spatialunit;

public enum LA_AreaType {
    CALCULATED_AREA,
    NON_OFFICIAL_AREA,
    OFFICIAL_AREA,
    SURVEYED_AREA
}
