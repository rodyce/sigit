package org.sigit.model.ladm.spatialunit.surveyingandrepresentation;

public enum LA_MonumentationType {
    BEACON,
    
    CORNER_STONE,
    
    MARKER,
    
    NOT_MARKED
    
}
