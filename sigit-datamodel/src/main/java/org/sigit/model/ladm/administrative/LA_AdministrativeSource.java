package org.sigit.model.ladm.administrative;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.sigit.model.commons.IAdministrativeSource;
import org.sigit.model.ladm.party.LA_Party;
import org.sigit.model.ladm.special.LA_Source;

@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="LA_AdministrativeSource", schema="ladm_administrative")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorValue("LA_AdministrativeSource")
@PrimaryKeyJoinColumn(name="LA_SourceID", referencedColumnName="ID")
public class LA_AdministrativeSource extends LA_Source implements IAdministrativeSource, Serializable {
    private static final long serialVersionUID = 1L;
    
    private LA_AvailabilityStatusType availabilityStatus;
    private String text;
    private LA_AdministrativeSourceType type;
    private java.util.Set<LA_BAUnit> units = new java.util.HashSet<LA_BAUnit>();
    private java.util.Set<LA_RRR> rrr = new java.util.HashSet<LA_RRR>();
    private java.util.Set<LA_Party> conveyor = new java.util.HashSet<LA_Party>();
    
    
    @Column(name="AvailabilityStatus", nullable=true)
    @Enumerated(EnumType.STRING)
    public LA_AvailabilityStatusType getAvailabilityStatus() {
        return availabilityStatus;
    }
    public void setAvailabilityStatus(LA_AvailabilityStatusType value) {
        this.availabilityStatus = value;
    }
    
    @Column(name="Text", nullable=true)
    public String getText() {
        return text;
    }
    public void setText(String value) {
        this.text = value;
    }
    
    @Column(name="Type", nullable=true)
    @Enumerated(EnumType.STRING)
    public LA_AdministrativeSourceType getType() {
        return type;
    }
    public void setType(LA_AdministrativeSourceType value) {
        this.type = value;
    }
    
    @ManyToMany(targetEntity=LA_BAUnit.class)
    @org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})
    @JoinTable(name="LA_BAUnit_LA_AdministrativeSource", schema="ladm_administrative", joinColumns={ @JoinColumn(name="LA_AdministrativeSourceLA_SourceID") }, inverseJoinColumns={ @JoinColumn(name="LA_BAUnitID") })
    @org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)
    public java.util.Set<LA_BAUnit> getUnits() {
        return units;
    }
    public void setUnits(java.util.Set<LA_BAUnit> value) {
        this.units = value;
    }
    
    @ManyToMany(targetEntity=LA_RRR.class)
    @org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})
    @JoinTable(name="LA_RRR_LA_AdministrativeSource", schema="ladm_administrative", joinColumns={ @JoinColumn(name="LA_AdministrativeSourceLA_SourceID") }, inverseJoinColumns={ @JoinColumn(name="LA_RRRID") })
    @org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)
    public java.util.Set<LA_RRR> getRrr() {
        return rrr;
    }
    public void setRrr(java.util.Set<LA_RRR> value) {
        this.rrr = value;
    }
    
    @ManyToMany(targetEntity=LA_Party.class)
    @org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})
    @JoinTable(name="LA_Party_LA_AdministrativeSource", schema="ladm_party", joinColumns={ @JoinColumn(name="LA_AdministrativeSourceLA_SourceID") }, inverseJoinColumns={ @JoinColumn(name="LA_PartyID") })
    @org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)
    public java.util.Set<LA_Party> getConveyor() {
        return conveyor;
    }
    public void setConveyor(java.util.Set<LA_Party> value) {
        this.conveyor = value;
    }
    
    public String toString() {
        return super.toString();
    }
    
}
