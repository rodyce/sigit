package org.sigit.model.ladm.party;

public enum LA_GroupPartyType {
    ASSOCIATION,
    
    BAUNIT_GROUP,
    
    FAMILIY,
    
    TRIBE
}
