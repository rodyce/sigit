package org.sigit.model.ladm.spatialunit;

import java.util.UUID;

import org.sigit.model.commons.ISpatialUnit;

public interface LA_LegalSpaceBuildingUnit extends ISpatialUnit {
    public UUID getBuildingUnitID();
    
    public LA_BuildingUnitType getType();
    public void setType(LA_BuildingUnitType value);
}
