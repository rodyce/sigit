package org.sigit.model.ladm.spatialunit;

public enum LA_UtilityNetworkStatusType {
    IN_USE,
    
    OUT_OF_USE,
    
    PLANNED
    
}
