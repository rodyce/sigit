package org.sigit.model.ladm.spatialunit;

public enum LA_StructureType {
    POINT,
    
    POLYGON,
    
    TEXT,
    
    TOPOLOGICAL,
    
    UNSTRUCTURED_LINE
    
}
