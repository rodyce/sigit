package org.sigit.model.ladm.spatialunit;

public enum LA_VolumeType {
    CALCULATED_VOLUME,
    NON_OFFICIAL_VOLUME,
    OFFICIAL_VOLUME,
    SURVEYED_VOLUME
}
