package org.sigit.model.ladm.administrative;

public enum LA_RightType {
    AGRI_ACTIVITY,
    
    COMMON_OWNERSHIP,
    
    CUSTOMARY_TYPE,
    
    FIRE_WOOD,
    
    FISHING,
    
    GRAZING,
    
    INFORMAL_OCCUPATION,
    
    LEASE,
    
    LIFE_ESTATE,
    
    OCCUPATION,
    
    OWNERSHIP,
    
    OWNERSHIP_ASSUMED,
    
    REMAINDER_ESTATE,
    
    STATE_OWNERSHIP,
    
    SUPERFICIES,
    
    TENANCY,
    
    USUFRUCT,
    
    WATER_RIGHTS,
    
    
    OTHER
    
}
