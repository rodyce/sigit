package org.sigit.model.ladm.administrative;

public enum LA_ResponsibilityType {
    MONUMENT_MAINTENANCE,
    
    WATERWAY_MAINTENANCE,
    
    
    OTHER
}
