package org.sigit.model.ladm.spatialunit;

public enum LA_LevelContentType {
    BUILDING,
    
    CUSTOMARY,
    
    INFORMAL,
    
    MIXED,
    
    NETWORK,
    
    PRIMARY_RIGHT,
    
    RESPONSIBILITY,
    
    RESTRICTION
    
}
