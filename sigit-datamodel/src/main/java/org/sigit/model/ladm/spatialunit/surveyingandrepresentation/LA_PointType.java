package org.sigit.model.ladm.spatialunit.surveyingandrepresentation;

public enum LA_PointType {
    CONTROL,
    
    NO_SOURCE,
    
    SOURCE
    
}
