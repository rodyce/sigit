package org.sigit.model.ladm.external;

public enum ExtTaxType {
    BUILDING,
    
    LAND,
    
    REAL_ESTATE
    
}
