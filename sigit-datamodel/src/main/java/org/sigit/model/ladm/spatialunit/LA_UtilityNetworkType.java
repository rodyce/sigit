package org.sigit.model.ladm.spatialunit;

public enum LA_UtilityNetworkType {
    CHEMICALS,
    
    ELECTRICITY,
    
    GAS,
    
    OIL,
    
    TELECOMMUNICATION,
    
    WATER
    
}
