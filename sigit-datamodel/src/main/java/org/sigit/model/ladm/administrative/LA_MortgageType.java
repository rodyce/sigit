package org.sigit.model.ladm.administrative;

public enum LA_MortgageType {
    LEVEL_PAYMENT,
    
    LINEAR,
    
    MICRO_CREDIT
    
}
