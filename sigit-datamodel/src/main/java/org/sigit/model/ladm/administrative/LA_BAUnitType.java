package org.sigit.model.ladm.administrative;

public enum LA_BAUnitType {
    ADMINISTRATIVE_AREA,
    
    BASIC_PARCEL,
    
    BASIC_PROPERTY_UNIT,
    
    LEASED_UNIT,
    
    PROPERTY_RIGHT_UNIT
    
}
