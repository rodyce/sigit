package org.sigit.model.ladm.spatialunit;

public enum LA_RegisterType {
    ALL,
    
    FOREST,
    
    MINING,
    
    PUBLIC_SPACE,
    
    RURAL,
    
    URBAN
    
}
