package org.sigit.model.ladm.spatialunit;

//TODO: Complete the declaration using the ISO19125:2 _Type
public enum ISO19125_Type {
    ST_EQUALS,
    
    ST_DISJOINT,
    
    ST_TOUCHES,
    
    ST_WITHIN,
    
    ST_OVERLAPS,
    
    ST_CROSSES,
    
    ST_INTERSECTS,
    
    ST_CONTAINS,
    
    ST_RELATE
}
