package org.sigit.model.ladm.spatialunit.surveyingandrepresentation;

public enum LA_InterpolationType {
    END,
    
    ISOLATED,
    
    MID,
    
    MID_ARC,
    
    START
    
}
