package org.sigit.model.ladm.administrative;

public enum LA_AvailabilityStatusType {
    ARCHIVE_CONVERTED,
    
    ARCHIVE_DESTROYED,
    
    ARCHIVE_INCOMPLETE,
    
    ARCHIVE_UNKNOWN
}
