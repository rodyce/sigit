package org.sigit.model.ladm.administrative;

public enum LA_RestrictionType {
    ADMIN_PUBLIC_SERVITUDE,
    
    MONUMENT,
    
    MORTGAGE,
    
    NO_BUILDING,
    
    SERVITUDE,
    
    HISTORIC_PRESERVATION,
    
    LIMITED_ACCESS,
    
    
    OTHER
}
