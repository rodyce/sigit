package org.sigit.model.ladm.party;

public enum LA_PartyType {
    BAUNIT,
    GROUP,
    NATURAL_PERSON,
    NON_NATURAL_PERSON
}
