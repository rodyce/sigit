package org.sigit.model.commons;

import java.util.UUID;

import org.sigit.model.ladm.administrative.LA_RestrictionType;


public interface IRestriction<P extends IParty, B extends IBAUnit> extends IRRR<P, B> {
    public LA_RestrictionType getType();
    public int restrictionsHash();
    public UUID getExtPID();
    
    public boolean getPartyRequired();
}
