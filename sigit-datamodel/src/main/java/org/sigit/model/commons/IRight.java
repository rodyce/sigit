package org.sigit.model.commons;

import java.util.UUID;

import org.sigit.model.ladm.administrative.LA_RightType;


public interface IRight<P extends IParty, B extends IBAUnit> extends IRRR<P, B> {
    public LA_RightType getType();
    public int rightHash();
    public UUID getExtPID();
}
