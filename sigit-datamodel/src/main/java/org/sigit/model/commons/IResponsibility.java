package org.sigit.model.commons;

import java.util.UUID;

import org.sigit.model.ladm.administrative.LA_ResponsibilityType;


public interface IResponsibility<P extends IParty, B extends IBAUnit> extends IRRR<P, B> {
    public LA_ResponsibilityType getType();
    public int responsibilitiesHash();
    public UUID getExtPID();
}
