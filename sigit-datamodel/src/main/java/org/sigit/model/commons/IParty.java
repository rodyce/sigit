package org.sigit.model.commons;

import org.sigit.model.ladm.external.ExtParty;

public interface IParty {
    public ExtParty getExtParty();
}
