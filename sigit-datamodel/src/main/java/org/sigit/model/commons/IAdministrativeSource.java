package org.sigit.model.commons;

import org.sigit.model.ladm.administrative.LA_AdministrativeSourceType;
import org.sigit.model.ladm.administrative.LA_AvailabilityStatusType;
import org.sigit.model.ladm.administrative.LA_BAUnit;
import org.sigit.model.ladm.administrative.LA_RRR;
import org.sigit.model.ladm.party.LA_Party;


public interface IAdministrativeSource extends ISource {
    public LA_AvailabilityStatusType getAvailabilityStatus();
    public void setAvailabilityStatus(LA_AvailabilityStatusType value);
    
    public String getText();
    public void setText(String value);

    public LA_AdministrativeSourceType getType();
    public void setType(LA_AdministrativeSourceType value);

    public java.util.Set<LA_BAUnit> getUnits();
    public void setUnits(java.util.Set<LA_BAUnit> value);

    public java.util.Set<LA_RRR> getRrr();
    public void setRrr(java.util.Set<LA_RRR> value);

    public java.util.Set<LA_Party> getConveyor();
    public void setConveyor(java.util.Set<LA_Party> value);
}
