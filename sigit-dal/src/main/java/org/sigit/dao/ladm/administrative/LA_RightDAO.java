package org.sigit.dao.ladm.administrative;

import org.sigit.model.ladm.administrative.LA_Right;
import org.sigit.model.ladm.party.LA_Party;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class LA_RightDAO extends LA_RRRDAO {
    private static final long serialVersionUID = 1L;

    public static List<LA_Right> loadRightsByParty(LA_Party party) {
        return loadRightsByParty(getSession(), party);
    }
    
    @SuppressWarnings("unchecked")
    public static List<LA_Right> loadRightsByParty(Session session, LA_Party party) {
        Criteria criteria = session.createCriteria(LA_Right.class);
        criteria.add(Restrictions.eq("party", party));
        
        return criteria.list();
    }
}
