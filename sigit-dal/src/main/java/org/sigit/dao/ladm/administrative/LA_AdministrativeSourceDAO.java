package org.sigit.dao.ladm.administrative;

import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.sigit.dao.SigitDAO;
import org.sigit.model.ladm.administrative.LA_AdministrativeSource;

public class LA_AdministrativeSourceDAO extends SigitDAO<LA_AdministrativeSource> {
    private static final long serialVersionUID = 1L;

    protected LA_AdministrativeSourceDAO() {
    }
    
    
    public static LA_AdministrativeSource loadAdministrativeSourceByID(UUID id) {
        return loadAdministrativeSourceByID(getSession(), id);
    }
    
    public static LA_AdministrativeSource loadAdministrativeSourceByID(Session session, UUID id) {
        Criteria criteria = session.createCriteria(LA_AdministrativeSource.class);
        criteria.add(Restrictions.eq("sID", id));
        
        return (LA_AdministrativeSource) criteria.uniqueResult();
    }
}
