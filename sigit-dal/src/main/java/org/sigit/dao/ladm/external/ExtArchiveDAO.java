package org.sigit.dao.ladm.external;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.sigit.dao.SigitDAO;
import org.sigit.model.ladm.external.ExtArchive;

public class ExtArchiveDAO extends SigitDAO<ExtArchive> {
    private static final long serialVersionUID = 1L;

    public static List<ExtArchive> loadAll() {
        return loadAll(getSession());
    }
    public static ExtArchive loadExtArchiveByID(UUID id) {
        return loadExtArchiveByID(getSession(), id);
    }
    
    @SuppressWarnings("unchecked")
    public static List<ExtArchive> loadAll(Session session) {
        Criteria criteria = session.createCriteria(ExtArchive.class);
        
        return (List<ExtArchive>)criteria.list();
    }
    public static ExtArchive loadExtArchiveByID(Session session, UUID id) {
        Criteria criteria = session.createCriteria(ExtArchive.class);
        criteria.add(Restrictions.eq("sID", id));
        
        return (ExtArchive)criteria.uniqueResult();
    }
}
