package org.sigit.dao.ladm.external;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.sigit.model.ladm.external.ExtParty;
import org.sigit.dao.SigitDAO;

public class ExtPartyDAO extends SigitDAO<ExtParty> {
    private static final long serialVersionUID = 1L;

    public static List<ExtParty> loadExtParties() {
        return loadExtParties(getSession());
    }
    public static ExtParty loadExtPartyByExtPID(UUID extPID) {
        return loadExtPartyByExtPID(getSession(), extPID);
    }
    
    @SuppressWarnings("unchecked")
    public static List<ExtParty> loadExtParties(Session session) {
        Criteria criteria = session.createCriteria(ExtParty.class);
        
        return criteria.list();
    }
    public static ExtParty loadExtPartyByExtPID(Session session, UUID extPID) {
        Criteria criteria = session.createCriteria(ExtParty.class);
        criteria.add(Restrictions.eq("extPID", extPID));
        
        return (ExtParty)criteria.uniqueResult();
    }
}
