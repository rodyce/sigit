package org.sigit.dao.ladm.administrative;

import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.sigit.model.ladm.administrative.LA_BAUnit;
import org.sigit.dao.SigitDAO;

public class LA_BAUnitDAO extends SigitDAO<LA_BAUnit> {
    private static final long serialVersionUID = 1L;
    
    
    public static LA_BAUnit loadBAUnitByID(UUID baUnitId) {
        return loadBAUnitByID(getSession(), baUnitId);
    }
    
    public static LA_BAUnit loadBAUnitByID(Session session, UUID baUnitId) {
        Criteria criteria = session.createCriteria(LA_BAUnit.class);
        criteria.add(Restrictions.eq("uID", baUnitId));
        
        return (LA_BAUnit)criteria.uniqueResult();
    }

}
