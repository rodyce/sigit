package org.sigit.dao.ladm.party;

import org.sigit.dao.SigitDAO;
import org.sigit.model.ladm.external.ExtParty;
import org.sigit.model.ladm.party.LA_Party;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class LA_PartyDAO extends SigitDAO<ExtParty> {
    private static final long serialVersionUID = 1L;
    
    protected LA_PartyDAO() {
    }

    public static LA_Party loadPartyByExtParty(ExtParty extParty) {
        return loadPartyByExtParty(getSession(), extParty);
    }

    public static LA_Party loadPartyByExtParty(Session session, ExtParty extParty) {
        Criteria criteria = session.createCriteria(LA_Party.class);
        criteria.add(Restrictions.isNull("endLifespanVersion"));
        criteria.add(Restrictions.eq("extParty", extParty));

        return (LA_Party)criteria.uniqueResult();
    }
}
