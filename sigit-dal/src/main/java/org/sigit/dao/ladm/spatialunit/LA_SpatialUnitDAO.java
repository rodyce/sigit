package org.sigit.dao.ladm.spatialunit;

import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.sigit.model.ladm.spatialunit.LA_SpatialUnit;
import org.sigit.dao.SigitDAO;

public class LA_SpatialUnitDAO extends SigitDAO<LA_SpatialUnit> {
    private static final long serialVersionUID = 1L;

    public static LA_SpatialUnit loadSpatialUnitByID(UUID spatialUnitId) {
        return loadSpatialUnitByID(getSession(), spatialUnitId);
    }
    
    public static LA_SpatialUnit loadSpatialUnitByID(Session session, UUID spatialUnitId) {
        Criteria criteria = session.createCriteria(LA_SpatialUnit.class);
        criteria.add(Restrictions.eq("", spatialUnitId));
        
        return (LA_SpatialUnit) criteria.uniqueResult();
    }
}
