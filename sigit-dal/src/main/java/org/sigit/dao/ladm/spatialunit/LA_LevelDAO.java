package org.sigit.dao.ladm.spatialunit;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.sigit.dao.SigitDAO;
import org.sigit.model.ladm.spatialunit.LA_Level;

public class LA_LevelDAO extends SigitDAO<LA_Level> {
    private static final long serialVersionUID = -6468542136498599228L;

    public static LA_Level loadLevelByID(UUID id) {
        return loadLevelByID(getSession(), id);
    }
    public static List<LA_Level> loadLevels() {
        return loadLevels(getSession());
    }


    public static LA_Level loadLevelByID(Session session, UUID id) {
        Criteria criteria = session.createCriteria(LA_Level.class);
        criteria.add(Restrictions.eq("IID", id));
        
        return (LA_Level) criteria.uniqueResult();
    }
    @SuppressWarnings("unchecked")
    public static List<LA_Level> loadLevels(Session session) {
        Criteria criteria = session.createCriteria(LA_Level.class);
        criteria.addOrder(Order.asc("name"));
        
        return criteria.list();
    }
}
