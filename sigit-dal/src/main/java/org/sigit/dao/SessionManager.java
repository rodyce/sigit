package org.sigit.dao;


import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.sigit.commons.di.CtxComponent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("sigit-conversation")
public class SessionManager implements Serializable {
    private static final long serialVersionUID = 1L;
    static final String COMMONS_SF = "sigitSessionFactory";

    private Session session;
    
    protected static SessionFactory getSessionFactory(String factoryName) {
        return (SessionFactory) CtxComponent.getInstance(factoryName);
    }
    
    static SessionFactory getSessionFactory() {
        return getSessionFactory(COMMONS_SF);
    }
    
    protected static Session getSession() {
        return getSession(COMMONS_SF);
    }
    
    protected static Session getSession(String factoryName) {
        Session session = getSessionFactory(factoryName).openSession();
        session.setFlushMode(FlushMode.COMMIT);
        
        return session;
    }
    
    public static SessionManager instance() {
        return CtxComponent.getInstance(SessionManager.class);
    }

    @PostConstruct
    protected void init() {
        session = getSession();
    }
    
    public Session getCurrentSession() {
        return session;
    }
    
    @PreDestroy
    protected void destroy() {
        session.flush();
        session.clear();
        if (session.isOpen()) session.close();

        session = null;
    }
}
