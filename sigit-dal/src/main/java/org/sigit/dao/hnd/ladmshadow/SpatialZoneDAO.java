package org.sigit.dao.hnd.ladmshadow;

import org.sigit.model.hnd.ladmshadow.SpatialZone;

import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class SpatialZoneDAO extends SpatialUnitDAO {
    private static final long serialVersionUID = 1L;

    
    public static SpatialZone loadSpatialZoneByID(UUID id) {
        return loadSpatialZoneByID(getSession(), id);
    }
    public static SpatialZone loadSpatialZoneByID(Session session, UUID id) {
        Criteria criteria = session.createCriteria(SpatialZone.class);
        criteria.add(Restrictions.eq("suID", id));
        
        return (SpatialZone) criteria.uniqueResult();
    }

}
