package org.sigit.dao.hnd.administrative;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.sigit.dao.SigitDAO;
import org.sigit.model.hnd.administrative.HND_ActivityType;
import org.sigit.model.hnd.administrative.HND_MunicipalTransaction;
import org.sigit.model.hnd.administrative.HND_MunicipalTransactionType;
import org.sigit.model.hnd.administrative.HND_SpatialZoneInTransaction;

public class HND_MunicipalTransactionDAO extends SigitDAO<HND_MunicipalTransaction> {
    private static final long serialVersionUID = 1L;

    public static HND_MunicipalTransaction loadRequestById(UUID requestId) {
        return loadRequestById(getSession(), requestId);
    }
    public static HND_MunicipalTransaction loadRequestByPresentationNo(long presentationNo) {
        return loadRequestByPresentationNo(getSession(), presentationNo);
    }
    public static HND_MunicipalTransaction loadNextRequest() {
        return loadNextRequest(getSession());
    }
    public static List<HND_MunicipalTransaction> loadRequestsByCurrentState(Boolean approved) {
        return loadRequestsByCurrentState(getSession(), approved);
    }
    public static List<HND_MunicipalTransaction> loadRequestsByCurrentActivity(HND_ActivityType... activityTypes) {
        return loadRequestsByCurrentActivity(getSession(), activityTypes);
    }
    public static List<HND_MunicipalTransaction> loadRequestsByType(HND_MunicipalTransactionType requestType) {
        return loadRequestsByType(getSession(), requestType);
    }
    public static List<HND_SpatialZoneInTransaction> loadSpatialZonesInTransactions(HND_MunicipalTransaction trx) {
        return loadSpatialZonesInTransactions(getSession(), trx);
    }


    public static HND_MunicipalTransaction loadRequestById(Session session, UUID requestId) {
        Criteria criteria = session.createCriteria(HND_MunicipalTransaction.class);
        criteria.add(Restrictions.eq("id", requestId));
        
        return (HND_MunicipalTransaction) criteria.uniqueResult();
    }
    public static HND_MunicipalTransaction loadRequestByPresentationNo(Session session, long presentationNo) {
        Criteria criteria = session.createCriteria(HND_MunicipalTransaction.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        
        return (HND_MunicipalTransaction) criteria.uniqueResult();
    }
    public static HND_MunicipalTransaction loadNextRequest(Session session) {
        Date oldestDate = (Date) session.createCriteria(HND_MunicipalTransaction.class)
        .setProjection(Projections.min("startDate"))
        .add(Restrictions.eq("currentActivity", HND_ActivityType.DELIVER_FOR_EXTERNAL_APPROVAL))
        .uniqueResult();
        
        Criteria criteria = session.createCriteria(HND_MunicipalTransaction.class);
        criteria.add(Restrictions.eq("startDate", oldestDate));
        criteria.add(Restrictions.eq("currentActivity", HND_ActivityType.DELIVER_FOR_EXTERNAL_APPROVAL));
        
        return (HND_MunicipalTransaction) criteria.uniqueResult();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_MunicipalTransaction> loadRequestsByCurrentState(Session session, Boolean approved) {
        final String FIELD = "approved";
        Criteria criteria = session.createCriteria(HND_MunicipalTransaction.class);
        if (approved != null)
            criteria.add(Restrictions.eq(FIELD, approved));
        else
            criteria.add(Restrictions.isNull(FIELD));
        
        return criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_MunicipalTransaction> loadRequestsByCurrentActivity(Session session, HND_ActivityType... activityTypes) {
        Criteria criteria = session.createCriteria(HND_MunicipalTransaction.class);
        criteria.add(Restrictions.in("currentActivity", activityTypes));
        
        return criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_MunicipalTransaction> loadRequestsByType(Session session, HND_MunicipalTransactionType requestType) {
        Criteria criteria = session.createCriteria(HND_MunicipalTransaction.class);
        criteria.add(Restrictions.eq("requestType", requestType));
        
        return criteria.list();
    }
    public static List<HND_SpatialZoneInTransaction> loadSpatialZonesInTransactions(Session session, HND_MunicipalTransaction trx) {
        Criteria criteria = session.createCriteria(HND_MunicipalTransaction.class);
        criteria.add(Restrictions.eq("id", trx.getId()));
        HND_MunicipalTransaction tr = (HND_MunicipalTransaction) criteria.uniqueResult();
        
        return new ArrayList<HND_SpatialZoneInTransaction>( tr.getSpatialZoneInTransactions() );
    }
}
