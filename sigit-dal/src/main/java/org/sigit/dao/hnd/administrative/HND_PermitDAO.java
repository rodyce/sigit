package org.sigit.dao.hnd.administrative;

import java.io.Serializable;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.sigit.dao.SigitDAO;
import org.sigit.model.hnd.administrative.HND_Permit;


public class HND_PermitDAO extends SigitDAO<HND_Permit> implements Serializable {
    private static final long serialVersionUID = 1L;

    protected HND_PermitDAO() {
    }

    
    public static HND_Permit loadPermitByID(UUID permitId) {
        return loadPermitByID(getSession(), permitId);
    }

    public static HND_Permit loadPermitByID(Session session, UUID permitId) {
        Criteria criteria = session.createCriteria(HND_Permit.class);
        criteria.add(Restrictions.eq("id", permitId));
        
        return (HND_Permit) criteria.uniqueResult();
    }
}
