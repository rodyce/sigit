package org.sigit.dao.hnd.administrative;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;

import org.sigit.dao.SigitDAO;
import org.sigit.model.hnd.administrative.HND_SpatialRule;

public class HND_SpatialRuleDAO extends SigitDAO<HND_SpatialRule> {
    private static final long serialVersionUID = 1L;

    protected HND_SpatialRuleDAO() {
        
    }
    
    
    public static List<HND_SpatialRule> loadSpatialRules() {
        return loadSpatialRules(getSession());
    }


    @SuppressWarnings("unchecked")
    public static List<HND_SpatialRule> loadSpatialRules(Session session) {
        Criteria criteria = session.createCriteria(HND_SpatialRule.class);
        
        return (List<HND_SpatialRule>) criteria.list();
    }

}
