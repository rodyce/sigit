package org.sigit.dao.hnd.ladmshadow;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.spatial.criterion.SpatialRestrictions;

import com.vividsolutions.jts.geom.Polygon;

import org.sigit.model.hnd.ladmshadow.Parcel;
import org.sigit.dao.SigitDAO;


public class ParcelDAO extends SigitDAO<Parcel> {
    private static final long serialVersionUID = 1L;
    private static final String maxTimeStampHQL = "SELECT MAX(beginLifespanVersion) FROM Parcel WHERE presentationNo = :presentationNo";
    private static final String minTimeStampHQL = "SELECT MIN(beginLifespanVersion) FROM Parcel WHERE presentationNo = :presentationNo";
    private static final String deleteParcelsHQL = "DELETE FROM Parcel WHERE presentationNo = :presentationNo";
    private static final String deleteParcelsHQL2 = "DELETE FROM Parcel WHERE presentationNo = :presentationNo AND snapshot = :snapshot";

    protected ParcelDAO() {
    }

    public static Parcel loadParcelByID(UUID suId) {
        return loadParcelByID(getSession(), suId);
    }
    public static List<Parcel> loadParcelsByPresentationNo(long presentationNo) {
        return loadParcelsByPresentationNo(getSession(), presentationNo);
    }
    public static List<Parcel> loadBeforeParcelsByPresentationNo(long presentationNo) {
        return loadBeforeParcelsByPresentationNo(getSession(), presentationNo);
    }
    public static List<Parcel> loadRequestedParcelsByPresentationNo(long presentationNo) {
        return loadRequestedParcelsByPresentationNo(getSession(), presentationNo);
    }
    public static List<Parcel> loadNeighborParcelsByPresentationNo(long presentationNo) {
        return loadNeighborParcelsByPresentationNo(getSession(), presentationNo);
    }
    public static List<Parcel> loadParcelByPresentationIdAndLadmNo(long presentationNo, UUID ladmId) {
        return loadParcelByPresentationNoAndLadmId(getSession(), presentationNo, ladmId);
    }
    public static List<Parcel> loadNeighborsByParcel(Parcel parcel) {
        return loadNeighborsByParcel(getSession(), parcel);
    }
    public static List<Parcel> loadParcelsByBeginLifespanVersion(Date beginLifespanVersion) {
        return loadParcelsByBeginLifespanVersion(getSession(), beginLifespanVersion);
    }
    public static List<Parcel> loadParcelsByEndLifespanVersion(Date endLifespanVersion) {
        return loadParcelsByEndLifespanVersion(getSession(), endLifespanVersion);
    }
    public static List<Parcel> loadOriginalParcelsByPresentationNo(long presentationNo) {
        return loadOriginalParcelsByPresentationNo(getSession(), presentationNo);
    }
    public static List<Parcel> loadFinalParcelsByPresentationNo(long presentationNo) {
        return loadFinalParcelsByPresentationNo(getSession(), presentationNo);
    }
    public static List<Parcel> loadParcelsBeforeTransactionByPresentationNo(long presentationNo) {
        return loadParcelsBeforeTransactionByPresentationNo(getSession(), presentationNo);
    }
    public static List<Parcel> loadParcelsAfterTransactionByPresentationId(long presentationNo) {
        return loadParcelsAfterTransactionByPresentationNo(getSession(), presentationNo);
    }
    public static Parcel loadParcelAfterTransactionByPresentationNoAndSuID(long presentationNo, UUID suID) {
        return loadParcelAfterTransactionByPresentationNoAndSuID(getSession(), presentationNo, suID);
    }
    
    public static Date loadMaxTimeStamp(long presentationNo) {
        return loadMaxTimeStamp(getSession(), presentationNo);
    }
    public static Date loadMinTimeStamp(long presentationNo) {
        return loadMinTimeStamp(getSession(), presentationNo);
    }
    public static int deleteParcelsByPresentationNo(long presentationNo, boolean includeSnapshot) {
        return deleteParcelsByPresentationNo(getSession(), presentationNo, includeSnapshot);
    }
    public static int clearParcelsAssociationsByPresentationId(long presentationNo, boolean includeSnapshot) {
        return clearParcelsAssociationsByPresentationNo(getSession(), presentationNo, includeSnapshot);
    }
    
    
    public static Parcel loadParcelByID(Session session, UUID suId) {
        Criteria criteria = session.createCriteria(Parcel.class);
        criteria.add(Restrictions.eq("suID", suId));
        
        return (Parcel) criteria.uniqueResult();
    }
    @SuppressWarnings("unchecked")
    public static List<Parcel> loadParcelsByPresentationNo(Session session, long presentationNo) {
        Criteria criteria = session.createCriteria(Parcel.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        criteria.add(Restrictions.eq("snapshot", false));
        
        return (List<Parcel>) criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<Parcel> loadBeforeParcelsByPresentationNo(Session session, long presentationNo) {
        Criteria criteria = session.createCriteria(Parcel.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        criteria.add(Restrictions.eq("readOnly", false));
        criteria.add(Restrictions.eq("snapshot", true));
        
        return (List<Parcel>) criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<Parcel> loadRequestedParcelsByPresentationNo(Session session, long presentationNo) {
        Criteria criteria = session.createCriteria(Parcel.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        criteria.add(Restrictions.eq("readOnly", false));
        criteria.add(Restrictions.eq("snapshot", false));
        criteria.add(Restrictions.isNull("endLifespanVersion"));
        
        return (List<Parcel>) criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<Parcel> loadNeighborParcelsByPresentationNo(Session session, long presentationNo) {
        Criteria criteria = session.createCriteria(Parcel.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        criteria.add(Restrictions.eq("readOnly", true));
        criteria.add(Restrictions.eq("snapshot", false));
        criteria.add(Restrictions.isNull("endLifespanVersion"));
        
        return (List<Parcel>) criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<Parcel> loadParcelByPresentationNoAndLadmId(Session session, long presentationNo, UUID ladmId) {
        Criteria criteria = session.createCriteria(Parcel.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        criteria.add(Restrictions.eq("ladmId", ladmId));
        
        return (List<Parcel>) criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<Parcel> loadNeighborsByParcel(Session session, Parcel parcel) {
        Polygon filter = parcel.getShape();
        
        if (filter != null) {
            //Set<Parcel> parcelSet = new HashSet<Parcel>();
            
            Criteria criteria = session.createCriteria(Parcel.class);
            criteria.add(Restrictions.eq("snapshot", false));
            criteria.add(SpatialRestrictions.touches("shape", filter));
            
            return (List<Parcel>)criteria.list();
            /*
            @SuppressWarnings("unchecked")
            List<Parcel> touchingParcelList = (List<Parcel>)criteria.list();
            if (touchingParcelList != null)
                for (Parcel touchingP : touchingParcelList)
                    if (touchingP.getShape().intersection(filter).getLength() > 0.0001)
                        parcelSet.add(touchingP);
            
            return parcelSet;
            */
        }
        
        return null;
    }
    @SuppressWarnings("unchecked")
    public static List<Parcel> loadParcelsByBeginLifespanVersion(Session session, Date beginLifespanVersion) {
        Criteria criteria = session.createCriteria(Parcel.class);
        criteria.add(Restrictions.eq("snapshot", false));
        criteria.add(Restrictions.eq("beginLifespanVersion", beginLifespanVersion));
        
        return (List<Parcel>) criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<Parcel> loadParcelsByEndLifespanVersion(Session session, Date endLifespanVersion) {
        Criteria criteria = session.createCriteria(Parcel.class);
        criteria.add(Restrictions.eq("snapshot", false));
        criteria.add(Restrictions.eq("endLifespanVersion", endLifespanVersion));
        
        return (List<Parcel>) criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<Parcel> loadOriginalParcelsByPresentationNo(Session session, long presentationNo) {
        Criteria criteria = session.createCriteria(Parcel.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        criteria.add(Restrictions.eq("modified", true));
        criteria.add(Restrictions.eq("snapshot", false));
        criteria.add(Restrictions.eq("original", true));
        
        return (List<Parcel>) criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<Parcel> loadFinalParcelsByPresentationNo(Session session, long presentationNo) {
        Criteria criteria = session.createCriteria(Parcel.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        criteria.add(Restrictions.isNull("endLifespanVersion"));
        criteria.add(Restrictions.eq("snapshot", false));
        /*
        criteria.add(
                Restrictions.or(
                        Restrictions.and(
                                Restrictions.eq("modified", false),
                                Restrictions.eq("original", false)),
                        Restrictions.eq("modified", true)
                )
        );
        */
        
        return (List<Parcel>) criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<Parcel> loadParcelsBeforeTransactionByPresentationNo(Session session, long presentationNo) {
        Criteria criteria = session.createCriteria(Parcel.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        criteria.add(Restrictions.eq("readOnly", false));
        criteria.add(Restrictions.eq("snapshot", true));
        
        return (List<Parcel>) criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<Parcel> loadParcelsAfterTransactionByPresentationNo(Session session, long presentationNo) {
        Criteria criteria = session.createCriteria(Parcel.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        criteria.add(Restrictions.eq("readOnly", false));
        criteria.add(Restrictions.eq("snapshot", false));
        criteria.add(Restrictions.isNull("endLifespanVersion"));
        
        return (List<Parcel>) criteria.list();
    }
    public static Parcel loadParcelAfterTransactionByPresentationNoAndSuID(Session session, long presentationNo, UUID suID) {
        Criteria criteria = session.createCriteria(Parcel.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        criteria.add(Restrictions.eq("suID", suID));
        criteria.add(Restrictions.eq("readOnly", false));
        criteria.add(Restrictions.eq("snapshot", false));
        criteria.add(Restrictions.isNull("endLifespanVersion"));
        
        return (Parcel) criteria.uniqueResult();
    }
    
    
    
    
    public static Date loadMaxTimeStamp(Session session, long presentationNo) {
        return (Date) session.createQuery(maxTimeStampHQL)
            .setParameter("presentationNo", presentationNo)
            .uniqueResult();
    }
    public static Date loadMinTimeStamp(Session session, long presentationNo) {
        return (Date) session.createQuery(minTimeStampHQL)
            .setParameter("presentationNo", presentationNo)
            .uniqueResult();
    }
    public static int deleteParcelsByPresentationNo(Session session, long presentationNo, boolean includeSnapshot) {
        /*
        List<Parcel> parcels = loadParcelsByPresentationNo(presentationNo);

        Set<SpatialUnit> suSet;
        Property prop;
        
        for (Parcel p : parcels) {
            prop = p.getProperty();
            if (prop != null) {
                suSet = prop.getSpatialUnits();
                if (suSet != null)
                    suSet.clear();
                PropertyDAO.save(prop);
                p.setBaunits(null);
            }
            delete(p);
        }
        */

        if (includeSnapshot) {
            return session.createQuery(deleteParcelsHQL)
                .setParameter("presentationNo", presentationNo)
                .executeUpdate();
        }
        return session.createQuery(deleteParcelsHQL2)
            .setParameter("presentationNo", presentationNo)
            .setBoolean("snapshot", includeSnapshot)
            .executeUpdate();
        
        //return 0;
    }
    
    public static int clearParcelsAssociationsByPresentationNo(Session session, long presentationNo, boolean includeSnapshot) {
        Criteria criteria = session.createCriteria(Parcel.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        if (!includeSnapshot)
            criteria.add(Restrictions.eq("snapshot", false));
        
        ScrollableResults parcelCursor = null;
        
        int count = 0;
        try {
            parcelCursor = criteria.scroll();
            while (parcelCursor.next()) {
                Parcel parcel = (Parcel) parcelCursor.get(0);
                
                parcel.setPointOfReference(null);
                parcel.setBaunits(null);
                parcel.setElement(null);
                parcel.setWhole(null);
                parcel.setSpatialSources(null);
                parcel.setRelSpatialUnits1requiredrelationshipspatialunits(null);
                parcel.setRelSpatialUnits2requiredrelationshipspatialunits(null);
                parcel.setBfsMinus(null);
                parcel.setBfsPlus(null);
                parcel.setBfMinus(null);
                parcel.setBfPlus(null);
                
                SigitDAO.save(parcel);
                
                count++;
            }
            
            session.flush();
            session.clear();
        } finally {
            if (parcelCursor != null) parcelCursor.close();
        }
        
        return count;
    }

}
