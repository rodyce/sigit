package org.sigit.dao.hnd.administrative;

import org.sigit.dao.SigitDAO;
import org.sigit.model.hnd.administrative.HND_UserRole;

public class HND_UserRoleDAO extends SigitDAO<HND_UserRole> {
    private static final long serialVersionUID = 1L;
}
