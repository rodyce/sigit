package org.sigit.dao.hnd.special;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

import org.sigit.dao.SigitDAO;
import org.sigit.model.hnd.special.SystemConfiguration;

public class SystemConfigurationDAO extends SigitDAO<SystemConfiguration> {
    private static final long serialVersionUID = 1L;

    private SystemConfigurationDAO() {
    }


    public static SystemConfiguration loadSystemConfiguration() {
        return loadSystemConfiguration(getSession());
    }
    @SuppressWarnings("unchecked")
    public static SystemConfiguration loadSystemConfiguration(Session session) {
        Criteria criteria = session.createCriteria(SystemConfiguration.class);
        criteria.addOrder(Order.desc("id"));
        criteria.setMaxResults(1);
        
        List<SystemConfiguration> list = criteria.list();
        return list.get(0);
    }
}
