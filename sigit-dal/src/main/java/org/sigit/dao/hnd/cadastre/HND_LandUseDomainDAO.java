package org.sigit.dao.hnd.cadastre;

import org.sigit.dao.SigitDAO;
import org.sigit.model.hnd.cadastre.HND_LandUseDomain;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

public class HND_LandUseDomainDAO extends SigitDAO<HND_LandUseDomain> {
    private static final long serialVersionUID = 1L;

    public static List<HND_LandUseDomain> loadLandUseDomains() {
        return loadLandUseDomains(getSession());
    }


    @SuppressWarnings("unchecked")
    public static List<HND_LandUseDomain> loadLandUseDomains(Session session) {
        Criteria criteria = session.createCriteria(HND_LandUseDomain.class);
        criteria.addOrder(Order.asc("name"));
        
        return criteria.list();
    }
}
