package org.sigit.dao.hnd.administrative;

import org.sigit.dao.ladm.administrative.LA_BAUnitDAO;
import org.sigit.commons.geometry.GeometryOperations;
import org.sigit.model.hnd.cadastre.HND_Parcel;
import org.sigit.model.hnd.administrative.HND_Property;
import org.sigit.model.ladm.administrative.LA_BAUnit;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.spatial.criterion.SpatialRestrictions;
import com.vividsolutions.jts.geom.MultiPolygon;

public class HND_PropertyDAO extends LA_BAUnitDAO {
    private static final long serialVersionUID = 1L;
    
    protected HND_PropertyDAO() {
    }
    

    public static List<HND_Property> loadProperties() {
        return loadProperties(getSession());
    }
    public static HND_Property loadPropertyByID(UUID baUnitId) {
        return loadPropertyByID(getSession(), baUnitId);
    }
    public static Set<HND_Property> loadNeighborsByHNDProperty(HND_Property hndProperty) {
        return loadNeighborsByHNDProperty(getSession(), hndProperty);
    }
    

    @SuppressWarnings("unchecked")
    public static List<HND_Property> loadProperties(Session session) {
        Criteria criteria = session.createCriteria(HND_Property.class);
        
        return (List<HND_Property>)criteria.list();
    }
    
    public static HND_Property loadPropertyByID(Session session, UUID baUnitId) {
        Criteria criteria = session.createCriteria(HND_Property.class);
        criteria.add(Restrictions.eq("uID", baUnitId));
        
        return (HND_Property)criteria.uniqueResult();
    }
    
    public static Set<HND_Property> loadNeighborsByHNDProperty(Session session, HND_Property hndProperty) {
        MultiPolygon filter = GeometryOperations.getParcelsFromPropertyAsMultiPolygon(hndProperty);
        

        if (filter != null) {
            Set<HND_Property> hndPropertyList = new HashSet<HND_Property>();

            Criteria criteria = session.createCriteria(HND_Parcel.class);
            criteria.add(SpatialRestrictions.touches("shape", filter));
            
            @SuppressWarnings("unchecked")
            List<HND_Parcel> hndTouchingParcelList = (List<HND_Parcel>)criteria.list();
            if (hndTouchingParcelList != null) {
                for (HND_Parcel hndTouchingP : hndTouchingParcelList) {
                    //TODO: Parameterize if we want touching parcels with intersection = 0
                    /*
                    if (hndTouchingP.getShape().intersection(filter).getLength() > 0.0001) {
                        Set<LA_BAUnit> laBAUnitSet = hndTouchingP.getBaunits();
                        if (laBAUnitSet != null) {
                            for (LA_BAUnit laTouchingBAUnit : laBAUnitSet)
                                if (laTouchingBAUnit instanceof HND_Property)
                                    hndPropertyList.add((HND_Property)laTouchingBAUnit);
                        }
                    }
                    */
                    Set<LA_BAUnit> laBAUnitSet = hndTouchingP.getBaunits();
                    if (laBAUnitSet != null) {
                        for (LA_BAUnit laTouchingBAUnit : laBAUnitSet)
                            if (laTouchingBAUnit instanceof HND_Property)
                                hndPropertyList.add((HND_Property)laTouchingBAUnit);
                    }
                }
            }
            return hndPropertyList;
        }
        
        return null;
    }
}
