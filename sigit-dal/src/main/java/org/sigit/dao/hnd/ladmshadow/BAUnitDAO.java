package org.sigit.dao.hnd.ladmshadow;

import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.sigit.model.hnd.ladmshadow.BAUnit;
import org.sigit.dao.SigitDAO;

public class BAUnitDAO extends SigitDAO<BAUnit> {
    private static final long serialVersionUID = 1L;
    
    public static BAUnit loadBAUnitByLadmIdAndPresentationNo(UUID ladmId, long presentationNo) {
        return loadBAUnitByLadmIdAndPresentationNo(getSession(), ladmId, presentationNo);
    }
    public static BAUnit loadBAUnitByLadmIdAndPresentationNo(Session session, UUID ladmId, long presentationNo) {
        Criteria criteria = session.createCriteria(BAUnit.class);
        criteria.add(Restrictions.eq("ladmId", ladmId));
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        criteria.add(Restrictions.eq("snapshot", false));
        
        return (BAUnit)criteria.uniqueResult();
    }

}
