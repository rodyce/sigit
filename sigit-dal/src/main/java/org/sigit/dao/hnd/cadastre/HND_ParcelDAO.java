package org.sigit.dao.hnd.cadastre;

import org.sigit.model.hnd.cadastre.HND_Parcel;
import org.sigit.model.hnd.cadastre.HND_TaxationStatusType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.spatial.criterion.SpatialRestrictions;

import com.vividsolutions.jts.geom.Polygon;

public class HND_ParcelDAO extends HND_SpatialZoneDAO {
    private static final long serialVersionUID = 1L;

    public static List<HND_Parcel> loadParcels() {
        return loadParcels(getSession());
    }
    public static HND_Parcel loadParcelByID(UUID parcelId) {
        return loadParcelByID(getSession(), parcelId);
    }
    public static List<HND_Parcel> loadParcelsByFieldTab(String fieldTab) {
        return loadParcelsByFieldTab(getSession(), fieldTab);
    }
    public static List<HND_Parcel> loadParcelsByCadastralKey(String cadastralKey) {
        return loadParcelsByCadastralKey(getSession(), cadastralKey);
    }
    public static List<HND_Parcel> loadParcelsByMunicipalKey(String municipalKey) {
        return loadParcelsByMunicipalKey(getSession(), municipalKey);
    }
    public static List<HND_Parcel> loadParcelsByIsInTransaction(boolean isInTransaction) {
        return loadParcelsByIsInTransaction(getSession(), isInTransaction);
    }
    public static List<HND_Parcel> loadParcelsByTaxStatus(HND_TaxationStatusType statusType) {
        return loadParcelsByTaxStatus(getSession(), statusType);
    }
    public static List<HND_Parcel> loadParcelByTaxationLastUpdateAfter(Date date) {
        return loadParcelByTaxationLastUpdateAfter(getSession(), date);
    }
    public static List<HND_Parcel> loadNeighborsByHNDParcel(HND_Parcel hndParcel) {
        return loadNeighborsByHNDParcel(getSession(), hndParcel);
    }
    public static List<HND_Parcel> loadParcelPredecessors(HND_Parcel hndParcel) {
        return loadParcelPredecessors(getSession(), hndParcel);
    }
    public static List<HND_Parcel> loadParcelSuccessors(HND_Parcel hndParcel) {
        return loadParcelSuccessors(getSession(), hndParcel);
    }


    @SuppressWarnings("unchecked")
    public static List<HND_Parcel> loadParcels(Session session) {
        Criteria criteria = session.createCriteria(HND_Parcel.class);
        
        return (List<HND_Parcel>) criteria.list();
    }
    public static HND_Parcel loadParcelByID(Session session, UUID parcelId) {
        Criteria criteria = session.createCriteria(HND_Parcel.class);
        criteria.add(Restrictions.eq("suID", parcelId));
        
        return (HND_Parcel) criteria.uniqueResult();
    }    
    @SuppressWarnings("unchecked")
    public static List<HND_Parcel> loadParcelsByFieldTab(Session session, String fieldTab) {
        Criteria criteria = session.createCriteria(HND_Parcel.class);
        criteria.add(Restrictions.eq("fieldTab", fieldTab));
        
        return (List<HND_Parcel>) criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_Parcel> loadParcelsByCadastralKey(Session session, String cadastralKey) {
        Criteria criteria = session.createCriteria(HND_Parcel.class);
        criteria.add(Restrictions.eq("cadastralKey", cadastralKey));
        
        criteria.addOrder(Order.desc("beginLifespanVersion"));
        
        return (List<HND_Parcel>) criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_Parcel> loadParcelsByMunicipalKey(Session session, String municipalKey) {
        Criteria criteria = session.createCriteria(HND_Parcel.class);
        criteria.add(Restrictions.eq("municipalKey", municipalKey));
        
        return (List<HND_Parcel>) criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_Parcel> loadParcelsByIsInTransaction(Session session, boolean isInTransaction) {
        final String FIELD = "lock.lockingTransaction";
        Criteria criteria = session.createCriteria(HND_Parcel.class);
        if (isInTransaction)
            criteria.add(Restrictions.isNotNull(FIELD));
        else
            criteria.add(Restrictions.isNull(FIELD));
        
        return (List<HND_Parcel>) criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_Parcel> loadParcelsByTaxStatus(Session session, HND_TaxationStatusType taxStatus) {
        final String propName = "taxationBalanceDue";
        Criteria criteria = session.createCriteria(HND_Parcel.class);
        
        switch (taxStatus) {
        case INSOLVENT:
            criteria.add(
                    Restrictions.ne(propName, BigDecimal.ZERO));
            break;
        case SOLVENT:
            criteria.add(
                    Restrictions.eq(propName, BigDecimal.ZERO));
            break;
        case UNKNOWN:
            criteria.add(
                    Restrictions.isNull(propName));
            break;
        }
        
        return (List<HND_Parcel>) criteria.list();
    }
    
    @SuppressWarnings("unchecked")
    public static List<HND_Parcel> loadParcelByTaxationLastUpdateAfter(Session session, Date date) {
        final String PROP_NAME = "taxationInfoLastUpdate";
        Criteria criteria = session.createCriteria(HND_Parcel.class);
        
        criteria.add( Restrictions.isNull("endLifespanVersion") );
        criteria.add( Restrictions.ge(PROP_NAME, date) );
        
        return (List<HND_Parcel>) criteria.list();
    }
    
    public static List<HND_Parcel> loadNeighborsByHNDParcel(Session session, HND_Parcel hndParcel) {
        Polygon filter = hndParcel.getShape();
        
        if (filter != null) {
            List<HND_Parcel> hndParcelList = new ArrayList<HND_Parcel>();
            
            Criteria criteria = session.createCriteria(HND_Parcel.class);
            criteria.add(SpatialRestrictions.touches("shape", filter));
            
            @SuppressWarnings("unchecked")
            List<HND_Parcel> hndTouchingParcelList = (List<HND_Parcel>)criteria.list();
            if (hndTouchingParcelList != null)
                for (HND_Parcel hndTouchingP : hndTouchingParcelList)
                    //TODO: Parameterize if we want touching parcels with intersection > 0 included or not
                    //if (hndTouchingP.getShape().intersection(filter).getLength() > 0.0001)
                    hndParcelList.add(hndTouchingP);
            
            return hndParcelList;
        }
        
        return null;
    }
    @SuppressWarnings("unchecked")
    public static List<HND_Parcel> loadParcelPredecessors(Session session, HND_Parcel hndParcel) {
        Criteria criteria = session.createCriteria(HND_Parcel.class);
        criteria.add(Restrictions.eq("endLifespanVersion", hndParcel.getBeginLifespanVersion()));
        
        return (List<HND_Parcel>) criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_Parcel> loadParcelSuccessors(Session session, HND_Parcel hndParcel) {
        Criteria criteria = session.createCriteria(HND_Parcel.class);
        criteria.add(Restrictions.eq("beginLifespanVersion", hndParcel.getEndLifespanVersion()));
        
        return (List<HND_Parcel>) criteria.list();
    }
}
