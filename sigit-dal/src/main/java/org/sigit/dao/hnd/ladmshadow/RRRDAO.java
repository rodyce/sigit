package org.sigit.dao.hnd.ladmshadow;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.sigit.model.hnd.ladmshadow.BAUnit;
import org.sigit.model.hnd.ladmshadow.RRR;
import org.sigit.dao.SigitDAO;

public class RRRDAO extends SigitDAO<RRR> {
    private static final long serialVersionUID = 1L;
    
    private static final String deleteHQL = "DELETE RRR r WHERE r.baunit = :baUnit and r.presentationNo = :presentationNo";
    private static String deleteRRRsHQL = "DELETE FROM RRR WHERE presentationNo = :presentationNo";
    private static String deleteRRRsHQL2 = "DELETE FROM RRR WHERE presentationNo = :presentationNo AND snapshot = :snapshot";
    
    protected RRRDAO() {
    }
    
    public static List<RRR> loadRRRsByPresentationNo(long presentationNo) {
        return loadRRRsByPresentationNo(getSession(), presentationNo);
    }
    public static List<RRR> loadRRRsByPresentationNo(long presentationNo, boolean onlySnapshots) {
        return loadRRRsByPresentationNo(getSession(), presentationNo, onlySnapshots);
    }
    public static int clearRRRsAssociationsByPresentationNo(long presentationNo, boolean includeSnapshot) {
        return clearRRRsAssociationsByPresentationNo(getSession(), presentationNo, includeSnapshot);
    }
    public static int deleteRRRsByPresentationNo(long presentationNo, boolean includeSnapshot) {
        return deleteRRRsByPresentationNo(getSession(), presentationNo, includeSnapshot);
    }
    public static void deleteByBaUnitIdAndPresentationId(UUID baUnitId, long presentationNo) {
        deleteByBaUnitIdAndPresentationNo(getSession(), baUnitId, presentationNo);
    }

    
    @SuppressWarnings("unchecked")
    public static List<RRR> loadRRRsByPresentationNo(Session session, long presentationNo) {
        Criteria criteria = session.createCriteria(RRR.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        
        return (List<RRR>)criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<RRR> loadRRRsByPresentationNo(Session session, long presentationNo, boolean onlySnapshots) {
        Criteria criteria = session.createCriteria(RRR.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        if (onlySnapshots)
            criteria.add( Restrictions.eq("snapshot", onlySnapshots) );
        
        return (List<RRR>)criteria.list();
    }
    public static int clearRRRsAssociationsByPresentationNo(Session session, long presentationNo, boolean includeSnapshot) {
        Criteria criteria = session.createCriteria(RRR.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        if (!includeSnapshot)
            criteria.add(Restrictions.eq("snapshot", false));
        
        ScrollableResults rrrCursor = criteria.scroll();
        
        RRR rrr;
        int count = 0;
        while (rrrCursor.next()) {
            rrr = (RRR) rrrCursor.get(0);
            
            rrr.setParty(null);
            rrr.setBaunit(null);
            rrr.setAdminSources(null);
            
            if (++count % 100 == 0) {
                session.flush();
                session.clear();
            }
        }
        
        rrrCursor.close();

        return count;
    }
    public static int deleteRRRsByPresentationNo(Session session, long presentationNo, boolean includeSnapshot) {
        if (includeSnapshot) {
            return session.createQuery(deleteRRRsHQL)
                .setParameter("presentationNo", presentationNo)
                .executeUpdate();
            
        }
        return session.createQuery(deleteRRRsHQL2)
            .setParameter("presentationNo", presentationNo)
            .setBoolean("snapshot", false)
            .executeUpdate();
    }
    public static void deleteByBaUnitIdAndPresentationNo(Session session, UUID baUnitId, long presentationNo) {
        BAUnit baUnit = BAUnitDAO.loadBAUnitByLadmIdAndPresentationNo(session, baUnitId, presentationNo);
        
        Query q = session.createQuery(deleteHQL)
            .setParameter("baUnit", baUnit)
            .setParameter("presentationNo", presentationNo);
        
        q.executeUpdate();
    }
}
