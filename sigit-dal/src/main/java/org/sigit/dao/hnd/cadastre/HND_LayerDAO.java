package org.sigit.dao.hnd.cadastre;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.sigit.dao.SigitDAO;
import org.sigit.model.hnd.cadastre.HND_Layer;
import org.sigit.model.hnd.cadastre.HND_LayerType;

public class HND_LayerDAO extends SigitDAO<HND_Layer> {
    private static final long serialVersionUID = 1L;

    public static List<HND_Layer> loadLayers(boolean orderedByName) {
        return loadLayersOrderedByName(getSession(), orderedByName);
    }
    public static List<HND_Layer> loadLayersByType(HND_LayerType layerType, boolean orderedByName) {
        return loadLayersByType(getSession(), layerType, orderedByName);
    }
    
    @SuppressWarnings("unchecked")
    public static List<HND_Layer> loadLayersOrderedByName(Session session, boolean orderedByName) {
        Criteria criteria = session.createCriteria(HND_Layer.class);
        
        if (orderedByName)
            criteria.addOrder(Order.asc("name"));
        
        return criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_Layer> loadLayersByType(Session session, HND_LayerType layerType, boolean orderedByName) {
        Criteria criteria = session.createCriteria(HND_Layer.class);
        criteria.add(Restrictions.eq("layerType", HND_LayerType.BUILDING));
        
        if (orderedByName)
            criteria.addOrder(Order.asc("name"));
        
        return criteria.list();
    }
}
