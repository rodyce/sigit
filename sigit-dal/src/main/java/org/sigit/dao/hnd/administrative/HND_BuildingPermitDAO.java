package org.sigit.dao.hnd.administrative;

import org.sigit.model.hnd.administrative.HND_BuildingPermit;
import org.sigit.model.hnd.administrative.HND_BuildingPermitType;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class HND_BuildingPermitDAO extends HND_PermitDAO implements Serializable {
    private static final long serialVersionUID = 1L;

    protected HND_BuildingPermitDAO() {
    }
    
    public static HND_BuildingPermit loadBuildingPermitByID(UUID permitId) {
        return loadBuildingPermitByID(getSession(), permitId);
    }
    public static HND_BuildingPermit loadBuildingPermitByPresentationNo(long presentationNo) {
        return loadBuildingPermitByPresentationNo(getSession(), presentationNo);
    }
    public static List<HND_BuildingPermit> loadBuildingPermitsByCurrentState(Boolean approved) {
        return loadBuildingPermitsByCurrentState(getSession(), approved);
    }
    public static List<HND_BuildingPermit> loadBuildingPermitsByType(HND_BuildingPermitType permitType) {
        return loadBuildingPermitsByType(getSession(), permitType);
    }
    

    public static HND_BuildingPermit loadBuildingPermitByID(Session session, UUID permitId) {
        Criteria criteria = session.createCriteria(HND_BuildingPermit.class);
        criteria.add(Restrictions.eq("id", permitId));
        
        return (HND_BuildingPermit) criteria.uniqueResult();
    }
    public static HND_BuildingPermit loadBuildingPermitByPresentationNo(Session session, long presentationNo) {
        Criteria criteria = session.createCriteria(HND_BuildingPermit.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        
        return (HND_BuildingPermit) criteria.uniqueResult();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_BuildingPermit> loadBuildingPermitsByCurrentState(Session session, Boolean approved) {
        final String FIELD = "approved";
        Criteria criteria = session.createCriteria(HND_BuildingPermit.class);
        if (approved != null)
            criteria.add(Restrictions.eq(FIELD, approved));
        else
            criteria.add(Restrictions.isNull(FIELD));
        
        return criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_BuildingPermit> loadBuildingPermitsByType(Session session, HND_BuildingPermitType permitType) {
        Criteria criteria = session.createCriteria(HND_BuildingPermit.class);
        criteria.add(Restrictions.eq("type", permitType));
        
        return criteria.list();
    }
}
