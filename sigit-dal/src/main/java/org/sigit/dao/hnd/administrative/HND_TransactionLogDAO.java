package org.sigit.dao.hnd.administrative;

import java.util.List;
import java.util.UUID;
import java.sql.Timestamp;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.sigit.dao.SigitDAO;
import org.sigit.model.hnd.administrative.HND_ActivityType;
import org.sigit.model.hnd.administrative.HND_Transaction;
import org.sigit.model.hnd.administrative.HND_TransactionLog;
import org.sigit.model.hnd.administrative.HND_TransactionType;

public class HND_TransactionLogDAO extends SigitDAO<HND_TransactionLog> {
    private static final long serialVersionUID = 1L;

    public static HND_TransactionLog loadTransactionLogByID(UUID id) {
        return loadTransactionLogByID(getSession(), id);
    }

    public static HND_TransactionLog loadLastTransactionLogByPresentationNo(
            Long presentationNo) {
        return loadLastTransactionLogByPresentationNo(getSession(),
                presentationNo);
    }

    public static List<HND_TransactionLog> loadTransactionLogsByPresentationNo(
            Long presentationNo) {
        return loadTransactionLogsByPresentationNo(getSession(), presentationNo);
    }

    public static List<HND_TransactionLog> loadClaimedTransactionLogsByUsernameAndTransactionType(
            String username, HND_TransactionType trxType) {
        return loadClaimedTransactionLogsByUsernameAndTransactionType(getSession(), username, trxType);
    }

    public static List<HND_TransactionLog> loadUnclaimedTransactionLogsByTransactionType(
            HND_TransactionType trxType) {
        return loadUnclaimedTransactionLogsByTransactionType(getSession(), trxType);
    }

    public static List<HND_TransactionLog> loadTransactionLogsByTransactionAndType(
            HND_Transaction transaction, HND_ActivityType... activityTypes) {
        return loadTransactionLogsByTransactionAndType(getSession(),
                transaction, activityTypes);
    }

    public static HND_TransactionLog loadTransactionLogByID(Session session,
            UUID id) {
        Criteria criteria = session.createCriteria(HND_TransactionLog.class);
        criteria.add(Restrictions.eq("id", id));

        return (HND_TransactionLog) criteria.uniqueResult();
    }

    public static HND_TransactionLog loadLastTransactionLogByPresentationNo(
            Session session, Long presentationNo) {
        Timestamp maxDate = (Timestamp) session.createCriteria(HND_TransactionLog.class)
                .setProjection(Projections.max("dateActivity"))
                .createCriteria("transaction")
                .add(Restrictions.eq("presentationNo", presentationNo))
                .uniqueResult();

        Criteria criteria = session.createCriteria(HND_TransactionLog.class);

        return (HND_TransactionLog) criteria.add(Restrictions.eq("dateActivity", maxDate))
                .createCriteria("transaction")
                .add(Restrictions.eq("presentationNo", presentationNo))
                .uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public static List<HND_TransactionLog> loadTransactionLogsByPresentationNo(
            Session session, Long presentationNo) {
        Criteria criteria = session.createCriteria(HND_TransactionLog.class);
        criteria.addOrder(Order.desc("dateActivity"));

        return criteria.createCriteria("transaction")
                .add(Restrictions.eq("presentationNo", presentationNo)).list();
    }

    @SuppressWarnings("unchecked")
    public static List<HND_TransactionLog> loadClaimedTransactionLogsByUsernameAndTransactionType(
            Session session, String userName, HND_TransactionType trxType) {
        Criteria criteria = session.createCriteria(HND_TransactionLog.class);
        criteria.setFetchMode("claimingUser", FetchMode.JOIN);
        criteria.addOrder(Order.asc("dateActivity"));

        criteria.add(Restrictions.isNotNull("claimingUser"));
        criteria.add(Restrictions.isNull("dateFinished"));
        criteria.add(Restrictions.ne("activity", HND_ActivityType.END)); //not in END state
        criteria.add(Restrictions.eq("transactionType", trxType));

        return criteria.createCriteria("claimingUser")
                .add(Restrictions.eq("userName", userName)).list();
    }

    @SuppressWarnings("unchecked")
    public static List<HND_TransactionLog> loadUnclaimedTransactionLogsByTransactionType(
            Session session, HND_TransactionType trxType) {
        Criteria criteria = session.createCriteria(HND_TransactionLog.class);
        criteria.setFetchMode("transaction", FetchMode.JOIN);
        criteria.add(Restrictions.ne("activity", HND_ActivityType.END));
        criteria.add(Restrictions.or(Restrictions.isNull("claimingUser"),
                Restrictions.isNull("dateClaimed")));
        criteria.add(Restrictions.eq("transactionType", trxType));
        criteria.add(Restrictions.not(
                Restrictions.in("activity", new Object[] {
                        HND_ActivityType.DELIVER_FOR_EXTERNAL_APPROVAL,
                        HND_ActivityType.EXTERNAL_APPROVAL
                        })));

        criteria.addOrder(Order.asc("dateActivity"));

        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    public static List<HND_TransactionLog> loadTransactionLogsByTransactionAndType(
            Session session, HND_Transaction transaction,
            HND_ActivityType... activityTypes) {
        Criteria criteria = session.createCriteria(HND_TransactionLog.class);
        criteria.add(Restrictions.eq("transaction", transaction));
        criteria.add(Restrictions.in("activity", activityTypes));
        return criteria.list();
    }
}
