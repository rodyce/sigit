package org.sigit.dao.hnd.cadastre;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.sigit.dao.SigitDAO;
import org.sigit.model.hnd.cadastre.HND_BuildingType;


public class HND_BuildingTypeDAO extends SigitDAO<HND_BuildingType> {
    private static final long serialVersionUID = 1L;

    public static List<HND_BuildingType> loadBuildingTypes() {
        return loadBuildingTypes(getSession());
    }
    public static List<HND_BuildingType> loadBuildingTypesByName(String name) {
        return loadBuildingTypesByName(getSession(), name);
    }
    
    
    @SuppressWarnings("unchecked")
    public static List<HND_BuildingType> loadBuildingTypes(Session session) {
        Criteria criteria = session.createCriteria(HND_BuildingType.class);
        
        criteria.addOrder(Order.asc("name"));
        
        return criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_BuildingType> loadBuildingTypesByName(Session session, String name) {
        Criteria criteria = session.createCriteria(HND_BuildingType.class);
        
        criteria.add(Restrictions.ilike("name", name, MatchMode.START));
        
        criteria.addOrder(Order.asc("name"));
        
        return criteria.list();
    }
}
