package org.sigit.dao.hnd.special;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.sigit.dao.SigitDAO;
import org.sigit.model.hnd.special.HND_Counter;
import org.sigit.model.hnd.special.HND_CounterType;

public class HND_CounterDAO extends SigitDAO<HND_Counter> {
    private static final long serialVersionUID = -4000085757396192412L;

    protected HND_CounterDAO() {    
    }
    
    public synchronized static Long nextValue(HND_CounterType counterType) {
        return nextValue(getSession(), counterType);
    }
    
    @SuppressWarnings("unchecked")
    public synchronized static Long nextValue(Session session, HND_CounterType counterType) {
        Criteria criteria = session.createCriteria(HND_Counter.class);
        criteria.add(Restrictions.eq("type", counterType));
        
        HND_Counter hndCounter = null;
        List<HND_Counter> counterList = criteria.list();
        if (counterList != null && counterList.size() > 0) {
            hndCounter = counterList.get(0);
            for (int i = 1; i < counterList.size(); i++) {
                delete(counterList.get(i));
                counterList.set(i, null);
            }
        }
        if (hndCounter == null) {
            hndCounter = new HND_Counter();
            hndCounter.setType(counterType);
            hndCounter.setCount(1);
        }
        else {
            hndCounter.setCount(hndCounter.getCount() + 1);
        }
        
        save(hndCounter);
        
        return hndCounter.getCount();
    }
}
