package org.sigit.dao.hnd.ladmshadow;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.sigit.model.hnd.ladmshadow.Property;

import org.sigit.dao.SigitDAO;


public class PropertyDAO extends SigitDAO<Property> {
    private static final long serialVersionUID = 1L;
    
    private static final String deletePropertiesHQL = "DELETE FROM Property WHERE presentationNo = :presentationNo";
    private static final String deletePropertiesHQL2 = "DELETE FROM Property WHERE presentationNo = :presentationNo AND snapshot = :snapshot";

    
    protected PropertyDAO() {
    }
    
    public static Property loadPropertyByID(UUID baUnitId) {
        return loadPropertyByID(getSession(), baUnitId);
    }
    public static List<Property> loadPropertiesByPresentationNo(long presentationNo) {
        return loadPropertiesByPresentationNo(getSession(), presentationNo);
    }
    public static List<Property> loadRequestedPropertiesByPresentationNo(long presentationNo) {
        return loadRequestedPropertiesByPresentationNo(getSession(), presentationNo);
    }
    public static List<Property> loadNeighborPropertiesByPresentationNo(long presentationNo) {
        return loadNeighborPropertiesByPresentationNo(getSession(), presentationNo);
    }
    public static Property loadPropertyByPresentationIdAndLadmId(long presentationNo, UUID ladmId) {
        return loadPropertyByPresentationNoAndLadmId(getSession(), presentationNo, ladmId);
    }
    public static int deletePropertiesByPresentationNo(long presentationNo, boolean includeSnapshot) {
        return deletePropertiesByPresentationNo(getSession(), presentationNo, includeSnapshot);
    }
    public static int clearPropertiesAssociationsByPresentationNo(long presentationNo, boolean includeSnapshot) {
        return clearPropertiesAssociationsByPresentationNo(getSession(), presentationNo, includeSnapshot);
    }
    
    
    public static Property loadPropertyByID(Session session, UUID baUnitId) {
        Criteria criteria = session.createCriteria(Property.class);
        criteria.add(Restrictions.eq("uID", baUnitId));

        return (Property) criteria.uniqueResult();
    }
    
    @SuppressWarnings("unchecked")
    public static List<Property> loadPropertiesByPresentationNo(Session session, long presentationNo) {
        Criteria criteria = session.createCriteria(Property.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));

        return (List<Property>)criteria.list();
    }
    
    @SuppressWarnings("unchecked")
    public static List<Property> loadRequestedPropertiesByPresentationNo(Session session, long presentationNo) {
        Criteria criteria = session.createCriteria(Property.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        criteria.add(Restrictions.eq("readOnly", false));
        
        return (List<Property>)criteria.list();
    }
    
    @SuppressWarnings("unchecked")
    public static List<Property> loadNeighborPropertiesByPresentationNo(Session session, long presentationNo) {
        Criteria criteria = session.createCriteria(Property.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        criteria.add(Restrictions.eq("readOnly", true));
        
        return (List<Property>)criteria.list();
    }
    
    public static Property loadPropertyByPresentationNoAndLadmId(Session session, long presentationNo, UUID ladmId) {
        Criteria criteria = session.createCriteria(Property.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        criteria.add(Restrictions.eq("ladmId", ladmId));
        
        return (Property)criteria.uniqueResult();
    }

    public static int deletePropertiesByPresentationNo(Session session, long presentationNo, boolean includeSnapshot) {
        if (includeSnapshot) {
            return session.createQuery(deletePropertiesHQL)
                .setParameter("presentationNo", presentationNo)
                .executeUpdate();
        }
        return session.createQuery(deletePropertiesHQL2)
            .setParameter("presentationNo", presentationNo)
            .setBoolean("snapshot", includeSnapshot)
            .executeUpdate();
    }
    
    public static int clearPropertiesAssociationsByPresentationNo(Session session, long presentationNo, boolean includeSnapshot) {
        Criteria criteria = session.createCriteria(Property.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        if (!includeSnapshot)
            criteria.add(Restrictions.eq("snapshot", false));
        
        ScrollableResults propertyCursor = null;
        
        int count = 0;
        try {
            propertyCursor = criteria.scroll();
            while (propertyCursor.next()) {
                Property property = (Property) propertyCursor.get(0);
                
                property.setBaParty(null);
                property.setSpatialSources(null);
                property.setRrr(null);
                property.setBaunits1requiredrelationshipbaunits(null);
                property.setBaunits2requiredrelationshipbaunits(null);
                property.setSpatialUnits(null);
                property.setAdminSources(null);
                
                SigitDAO.save(property);
                
                count++;
            }
            
            session.flush();
            session.clear();
        } finally {
            if (propertyCursor != null) propertyCursor.close();
        }
        
        return count;
    }
}
