package org.sigit.dao.hnd.ladmshadow;

import org.sigit.model.hnd.ladmshadow.SpatialUnit;
import org.sigit.dao.SigitDAO;

public class SpatialUnitDAO extends SigitDAO<SpatialUnit> {
    private static final long serialVersionUID = 1L;
    
    protected SpatialUnitDAO() {
    }
}
