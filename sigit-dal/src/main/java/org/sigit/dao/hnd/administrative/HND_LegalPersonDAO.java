package org.sigit.dao.hnd.administrative;

import org.sigit.dao.ladm.external.ExtPartyDAO;
import org.sigit.model.hnd.administrative.HND_LegalPerson;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

public class HND_LegalPersonDAO extends ExtPartyDAO {
    private static final long serialVersionUID = 1L;

    public static List<HND_LegalPerson> loadLegalPersonParties() {
        return loadLegalPersonParties(getSession());
    }
    public static HND_LegalPerson loadLegalPersonByRTN(String rtn) {
        return loadLegalPersonByRTN(getSession(), rtn);
    }
    public static List<HND_LegalPerson> loadLegalPersonsByName(String txt) {
        return loadLegalPersonsByName(getSession(), txt);
    }
    
    @SuppressWarnings("unchecked")
    public static List<HND_LegalPerson> loadLegalPersonParties(Session session) {
        Criteria criteria = session.createCriteria(HND_LegalPerson.class);
        criteria.addOrder(Order.asc("name"));
        
        return (List<HND_LegalPerson>)criteria.list();
    }
    public static HND_LegalPerson loadLegalPersonByRTN(Session session, String rtn) {
        Criteria criteria = session.createCriteria(HND_LegalPerson.class);
        criteria.add(Restrictions.eq("rtn", rtn).ignoreCase());
        
        return (HND_LegalPerson)criteria.uniqueResult();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_LegalPerson> loadLegalPersonsByName(Session session, String txt) {
        String name = txt.trim();
        
        Criteria criteria = session.createCriteria(HND_LegalPerson.class);
        criteria.add(Restrictions.like("name", name, MatchMode.START).ignoreCase());
        criteria.addOrder(Order.asc("name"));
        
        return (List<HND_LegalPerson>)criteria.list();
        
    }
}
