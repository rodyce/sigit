package org.sigit.dao.hnd.administrative;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.sigit.model.hnd.administrative.HND_MunicipalTransactionType;
import org.sigit.model.hnd.administrative.HND_TransactionMetaData;
import org.sigit.dao.SigitDAO;

public class HND_TransactionMetaDataDAO extends SigitDAO<HND_TransactionMetaData> {
    private static final long serialVersionUID = 1L;
    
    public static List<HND_TransactionMetaData> loadRequestMetaData() {
        return loadRequestMetaData(getSession());
    }
    public static HND_TransactionMetaData loadRequestMetaDataByRequestType(HND_MunicipalTransactionType type) {
        return loadRequestMetaDataByRequestType(getSession(), type);
    }
    
    @SuppressWarnings("unchecked")
    public static List<HND_TransactionMetaData> loadRequestMetaData(Session session) {
        Criteria criteria = session.createCriteria(HND_TransactionMetaData.class);
        
        return (List<HND_TransactionMetaData>)criteria.list();
    }
    public static HND_TransactionMetaData loadRequestMetaDataByRequestType(Session session, HND_MunicipalTransactionType type) {
        Criteria criteria = session.createCriteria(HND_TransactionMetaData.class);
        criteria.add(Restrictions.eq("municipalTrxType", type));
        
        return (HND_TransactionMetaData) criteria.uniqueResult();
    }
}
