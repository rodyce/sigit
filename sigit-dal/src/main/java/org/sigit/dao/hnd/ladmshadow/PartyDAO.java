package org.sigit.dao.hnd.ladmshadow;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.sigit.dao.SigitDAO;
import org.sigit.model.hnd.ladmshadow.Party;
import org.sigit.model.ladm.external.ExtParty;

public class PartyDAO extends SigitDAO<Party> {
    private static final long serialVersionUID = 1L;
    
    protected PartyDAO() {
    }
    
    
    public static Party loadPartyByExtParty(ExtParty extParty) {
        return loadPartyByExtParty(getSession(), extParty);
    }
    public static List<Party> loadPartyByExtPartySubType(Class<ExtParty> cls) {
        return loadPartyByExtPartySubType(getSession(), cls);
    }
    
    public static Party loadPartyByExtParty(Session session, ExtParty extParty) {
        Criteria criteria = session.createCriteria(Party.class);
        criteria.add(Restrictions.eq("extParty", extParty));
        
        return (Party)criteria.uniqueResult();
    }
    @SuppressWarnings("unchecked")
    public static List<Party> loadPartyByExtPartySubType(Session session, Class<ExtParty> cls) {
        Criteria criteria = session.createCriteria(Party.class);
        criteria.add(Restrictions.eq("extParty.class.canonicalName", cls.getCanonicalName()));
        
        return (List<Party>)criteria.list();
    }
}
