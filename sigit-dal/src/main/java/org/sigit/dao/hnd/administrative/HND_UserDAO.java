package org.sigit.dao.hnd.administrative;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.sigit.model.hnd.administrative.HND_User;
import org.sigit.dao.SigitDAO;

public class HND_UserDAO extends SigitDAO<HND_User> {
    private static final long serialVersionUID = 1L;
    
    protected HND_UserDAO() {
        
    }
    
    
    public static List<HND_User> loadUsers() {
        return loadUsers(getSession());
    }
    public static List<HND_User> loadUsers(boolean active) {
        return loadUsers(getSession(), active);
    }
    public static HND_User loadUser(String userName) {
        return loadUser(getSession(), userName);
    }
    public static HND_User loadUser(String userName, String password) {
        return loadUser(getSession(), userName, password);
    }
    public static boolean userExists(String userName, String password) {
        return userExists(getSession(), userName, password);
    }
    
    
    @SuppressWarnings("unchecked")
    public static List<HND_User> loadUsers(Session session) {
        Criteria criteria = session.createCriteria(HND_User.class);
        
        return (List<HND_User>) criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_User> loadUsers(Session session, boolean active) {
        Criteria criteria = session.createCriteria(HND_User.class);
        criteria.add(Restrictions.eq("active", active));
        
        return (List<HND_User>) criteria.list();        
    }
    public static HND_User loadUser(Session session, String userName) {
        Criteria criteria = session.createCriteria(HND_User.class);
        criteria.add(Restrictions.eq("userName", userName));
        
        return (HND_User)criteria.uniqueResult();
    }
    public static HND_User loadUser(Session session, String userName, String password) {
        Criteria criteria = session.createCriteria(HND_User.class);
        criteria.add(Restrictions.eq("userName", userName));
        criteria.add(Restrictions.eq("password", password));
        
        return (HND_User)criteria.uniqueResult();
    }
    public static boolean userExists(Session session, String userName, String password) {
        return loadUser(session, userName, password) != null;
    }

}
