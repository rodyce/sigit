package org.sigit.dao.hnd.administrative;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.sigit.dao.SigitDAO;
import org.sigit.model.hnd.administrative.HND_BusinessType;


public class HND_BusinessTypeDAO extends SigitDAO<HND_BusinessType> {
    private static final long serialVersionUID = 1L;

    public static List<HND_BusinessType> loadBusinessTypes() {
        return loadBusinessTypes(getSession());
    }
    public static List<HND_BusinessType> loadBusinessTypesByName(String name) {
        return loadBusinessTypesByName(getSession(), name);
    }
    
    
    @SuppressWarnings("unchecked")
    public static List<HND_BusinessType> loadBusinessTypes(Session session) {
        Criteria criteria = session.createCriteria(HND_BusinessType.class);
        
        criteria.addOrder(Order.asc("name"));
        
        return criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_BusinessType> loadBusinessTypesByName(Session session, String name) {
        Criteria criteria = session.createCriteria(HND_BusinessType.class);
        
        criteria.add(Restrictions.ilike("name", name, MatchMode.START));
        
        criteria.addOrder(Order.asc("name"));
        
        return criteria.list();
    }
}

