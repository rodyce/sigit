package org.sigit.dao.hnd.cadastre;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.sigit.dao.SigitDAO;
import org.sigit.model.hnd.cadastre.HND_LandUse;
import org.sigit.model.hnd.cadastre.HND_LandUseDomain;

public class HND_LandUseDAO extends SigitDAO<HND_LandUse> {
    private static final long serialVersionUID = 1L;
    
    public static List<HND_LandUse> loadLandUses() {
        return loadLandUses(getSession());
    }
    public static HND_LandUse loadLandUseByID(UUID landUseId) {
        return loadLandUseByID(getSession(), landUseId);
    }
    public static List<HND_LandUse> loadLandUses(HND_LandUseDomain landUseDomain) {
        return loadLandUses(getSession(), landUseDomain);
    }

    public static List<HND_LandUse> loadLandUsesByLevel(HND_LandUseDomain landUseDomain, int level, boolean orderedByName) {
        return loadLandUsesByLevel(getSession(), landUseDomain, level, orderedByName);
    }
    public static List<HND_LandUse> loadLandUsesByParent(HND_LandUse parent, boolean orderedByName) {
        return loadLandUsesByParent(getSession(), parent, orderedByName);
    }
    
    
    @SuppressWarnings("unchecked")
    public static List<HND_LandUse> loadLandUses(Session session) {
        Criteria criteria = session.createCriteria(HND_LandUse.class);
        criteria.addOrder(Order.asc("completeCode"));
        return criteria.list();
    }
    public static HND_LandUse loadLandUseByID(Session session, UUID landUseId) {
        Criteria criteria = session.createCriteria(HND_LandUse.class);
        criteria.add(Restrictions.eq("id", landUseId));
        return (HND_LandUse) criteria.uniqueResult();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_LandUse> loadLandUses(Session session, HND_LandUseDomain landUseDomain) {
        Criteria criteria = session.createCriteria(HND_LandUse.class);
        criteria.add(Restrictions.eq("domain", landUseDomain));
        criteria.addOrder(Order.asc("completeCode"));
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    public static List<HND_LandUse> loadLandUsesByLevel(Session session, HND_LandUseDomain landUseDomain, int level, boolean orderedByName) {
        Criteria criteria = session.createCriteria(HND_LandUse.class);
        
        criteria.add(Restrictions.eq("domain", landUseDomain));
        criteria.add(Restrictions.eq("level", level));
        if (orderedByName)
            criteria.addOrder(Order.asc("name"));
        
        return criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_LandUse> loadLandUsesByParent(Session session, HND_LandUse parent, boolean orderedByName) {
        Criteria criteria = session.createCriteria(HND_LandUse.class);
        
        criteria.add(Restrictions.eq("parent", parent));
        if (orderedByName)
            criteria.addOrder(Order.asc("name"));
        
        return criteria.list();
    }
}

