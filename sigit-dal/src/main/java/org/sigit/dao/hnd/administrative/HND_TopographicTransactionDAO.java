package org.sigit.dao.hnd.administrative;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.spatial.criterion.SpatialRestrictions;
import org.sigit.dao.SigitDAO;
import org.sigit.model.hnd.administrative.HND_TopographicTransaction;
import org.sigit.model.hnd.administrative.HND_TopographicTransactionStateType;
import org.sigit.model.hnd.cadastre.HND_Layer;

import com.vividsolutions.jts.geom.Geometry;

public class HND_TopographicTransactionDAO extends SigitDAO<HND_TopographicTransaction> {
    private static final long serialVersionUID = 1L;


    public static List<HND_TopographicTransaction> loadInitiatedTopographicTransactionsByUserName(String userName) {
        return loadInitiatedTopographicTransactionsByUserName(getSession(), userName);
    }
    public static boolean isTopographicTransactionInProgressExtentOverlappingGeom(Geometry geom, HND_Layer workingLayer) {
        return isTopographicTransactionInProgressExtentOverlappingGeom(getSession(), geom, workingLayer);
    }


    @SuppressWarnings("unchecked")
    public static List<HND_TopographicTransaction> loadInitiatedTopographicTransactionsByUserName(Session session, String userName) {
        Criteria criteria = session.createCriteria(HND_TopographicTransaction.class);
        
        criteria.add(Restrictions.eq("state", HND_TopographicTransactionStateType.INITIATED));
        criteria.add(Restrictions.eq("editorUserName", userName));
        
        criteria.addOrder(Order.asc("startDate"));
        
        
        return criteria.list();
    }
    public static boolean isTopographicTransactionInProgressExtentOverlappingGeom(Session session, Geometry geom, HND_Layer workingLayer) {
        Criteria criteria = session.createCriteria(HND_TopographicTransaction.class);
        
        criteria.add(Restrictions.eq("state", HND_TopographicTransactionStateType.INITIATED));
        criteria.add(SpatialRestrictions.intersects("extents", geom));
        criteria.add(Restrictions.eq("workingLayer", workingLayer));

        return ((Long) criteria.setProjection(Projections.rowCount()).uniqueResult()) > 0;
    }
}
