package org.sigit.dao.hnd.special;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.sigit.dao.SigitDAO;
import org.sigit.model.hnd.special.ArchiveInternalRepository;
import org.sigit.model.ladm.external.ExtArchive;
import org.sigit.model.ladm.external.ExtArchiveKey;

public class ArchiveInternalRepositoryDAO extends SigitDAO<ArchiveInternalRepository> {
    private static final long serialVersionUID = 1L;

    protected ArchiveInternalRepositoryDAO() {
    }
    
    public static byte[] loadBytesByExtArchive(ExtArchive extArchive) {
        return loadBytesByExtArchive(getSession(), extArchive);
    }
    public static void saveData(ExtArchive extArchive, byte[] data) {
        saveData(getSession(), extArchive, data);
    }
    
    public static byte[] loadBytesByExtArchive(Session session, ExtArchive extArchive) {
        ExtArchiveKey extArchiveKey = new ExtArchiveKey();
        extArchiveKey.setExtArchive(extArchive);
        
        Criteria criteria = session.createCriteria(ArchiveInternalRepository.class);
        criteria.add(Restrictions.eq("extArchiveKeyId", extArchiveKey));
        
        ArchiveInternalRepository repository = (ArchiveInternalRepository)criteria.uniqueResult();
        
        return repository.getData();
    }
    public static void saveData(Session session, ExtArchive extArchive, byte[] data) {
        ExtArchiveKey extArchiveKey = new ExtArchiveKey();
        extArchiveKey.setExtArchive(extArchive);
        
        if (extArchive.getsID() == null) {
            save(extArchive);
        }
        
        ArchiveInternalRepository repository = new ArchiveInternalRepository();
        repository.setExtArchiveKeyId(extArchiveKey);
        repository.setData(data);
        
        save(repository);
    }
}
