package org.sigit.dao.hnd.administrative;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.sigit.dao.SigitDAO;
import org.sigit.model.hnd.administrative.HND_PermitRuleGroup;
import org.sigit.model.hnd.administrative.HND_PermitType;

public class HND_PermitRuleGroupDAO extends SigitDAO<HND_PermitRuleGroup> {
    private static final long serialVersionUID = 1L;

    protected HND_PermitRuleGroupDAO() {
    }
    
    public static List<HND_PermitRuleGroup> loadPermitRuleGroups() {
        return loadPermitRuleGroups(getSession());
    }
    public static List<HND_PermitRuleGroup> loadPermitRuleGroupsByType(HND_PermitType permitType) {
        return loadPermitRuleGroupsByType(getSession(), permitType);
    }
    
    
    @SuppressWarnings("unchecked")
    public static List<HND_PermitRuleGroup> loadPermitRuleGroups(Session session) {
        Criteria criteria = session.createCriteria(HND_PermitRuleGroup.class);
        
        return (List<HND_PermitRuleGroup>) criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_PermitRuleGroup> loadPermitRuleGroupsByType(Session session, HND_PermitType permitType) {
        Criteria criteria = session.createCriteria(HND_PermitRuleGroup.class);
        criteria.add(
                Restrictions.or(
                        Restrictions.eq("permitType", permitType),
                        Restrictions.isNull("permitType")
                )
        );
        
        return (List<HND_PermitRuleGroup>) criteria.list();
    }
}
