package org.sigit.dao.hnd.administrative;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.sigit.model.hnd.administrative.HND_NaturalPerson;
import org.sigit.model.ladm.party.LA_Party;
import org.sigit.dao.ladm.external.ExtPartyDAO;

public class HND_NaturalPersonDAO extends ExtPartyDAO {
    private static final long serialVersionUID = 1L;

    public static List<LA_Party> loadParties() {
        return loadParties(getSession());
    }
    public static List<HND_NaturalPerson> loadNaturalPersonParties() {
        return loadNaturalPersonParties(getSession());
    }
    public static HND_NaturalPerson loadNatPersonPartyByIDentity(String id) {
        return loadNatPersonPartyByIDentity(getSession(), id);
    }
    public static HND_NaturalPerson loadNatPersonPartyByRTN(String rtn) {
        return loadNatPersonPartyByRTN(getSession(), rtn);
    }
    public static List<HND_NaturalPerson> loadNatPersonPartiesByFirstNames(String txt) {
        return loadNatPersonPartiesByFirstNames(getSession(), txt);
    }
    public static List<HND_NaturalPerson> loadNatPersonPartiesByLastNames(String txt) {
        return loadNatPersonPartiesByLastNames(getSession(), txt);
    }

    @SuppressWarnings("unchecked")
    public static List<LA_Party> loadParties(Session session) {
        Criteria criteria = session.createCriteria(LA_Party.class);
        criteria.add(Restrictions.in("extParty", loadNaturalPersonParties()));
        
        return (List<LA_Party>)criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_NaturalPerson> loadNaturalPersonParties(Session session) {
        Criteria criteria = session.createCriteria(HND_NaturalPerson.class);
        addOrderCriteria(criteria);
        
        return (List<HND_NaturalPerson>) criteria.list();
    }
    public static HND_NaturalPerson loadNatPersonPartyByIDentity(Session session, String identity) {
        Criteria criteria = session.createCriteria(HND_NaturalPerson.class);
        criteria.add(Restrictions.eq("identity", identity));
        addOrderCriteria(criteria);
        
        return (HND_NaturalPerson) criteria.uniqueResult();
    }
    public static HND_NaturalPerson loadNatPersonPartyByRTN(Session session, String rtn) {
        Criteria criteria = session.createCriteria(HND_NaturalPerson.class);
        criteria.add(Restrictions.eq("rtn", rtn).ignoreCase());
        addOrderCriteria(criteria);
        
        return (HND_NaturalPerson) criteria.uniqueResult();
    }
    
    public static List<HND_NaturalPerson> loadNatPersonPartiesByFirstNames(Session session, String txt) {
        return loadNatPersonPartiesByNames(session, "firstName1", "firstName2", txt);
    }
    public static List<HND_NaturalPerson> loadNatPersonPartiesByLastNames(Session session, String txt) {
        return loadNatPersonPartiesByNames(session, "lastName1", "lastName2", txt);
    }
    
    @SuppressWarnings("unchecked")
    private static List<HND_NaturalPerson> loadNatPersonPartiesByNames(
            Session session, String field1, String field2, String txt) {
        Criteria criteria = session.createCriteria(HND_NaturalPerson.class);
        String[] names = txt.trim().split(" ", 2);
        
        if (names.length == 0) return new ArrayList<HND_NaturalPerson>();
        
        if (names.length > 1) {
            criteria.add(Restrictions.like(field1, names[0], MatchMode.EXACT).ignoreCase());
            criteria.add(Restrictions.like(field2, names[1], MatchMode.START).ignoreCase());
        }
        else
            criteria.add(Restrictions.like(field1, names[0], MatchMode.START).ignoreCase());
        
        addOrderCriteria(criteria);
        
        return (List<HND_NaturalPerson>) criteria.list();
    }
    
    private static void addOrderCriteria(Criteria criteria) {
        criteria.addOrder(Order.asc("lastName1"));
        criteria.addOrder(Order.asc("lastName2"));
        criteria.addOrder(Order.asc("firstName1"));
        criteria.addOrder(Order.asc("firstName2"));
    }
}
