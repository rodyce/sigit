package org.sigit.dao.hnd.administrative;

import org.sigit.model.hnd.administrative.HND_OperationPermit;
import org.sigit.model.hnd.administrative.HND_OperationPermitType;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class HND_OperationPermitDAO extends HND_PermitDAO {
    private static final long serialVersionUID = 1L;

    protected HND_OperationPermitDAO() {
    }
    
    public static HND_OperationPermit loadOperationPermitByID(UUID permitId) {
        return loadOperationPermitByID(getSession(), permitId);
    }
    public static HND_OperationPermit loadOperationPermitByPresentationNo(long presentationNo) {
        return loadOperationPermitByPresentationNo(getSession(), presentationNo);
    }
    public static List<HND_OperationPermit> loadOperationPermitsByCurrentState(Boolean approved) {
        return loadOperationPermitsByCurrentState(getSession(), approved);
    }
    public static List<HND_OperationPermit> loadOperationPermitsByType(HND_OperationPermitType permitType) {
        return loadOperationPermitsByType(getSession(), permitType);
    }

    public static HND_OperationPermit loadOperationPermitByID(Session session, UUID permitId) {
        Criteria criteria = session.createCriteria(HND_OperationPermit.class);
        criteria.add(Restrictions.eq("id", permitId));
        
        return (HND_OperationPermit) criteria.uniqueResult();
    }
    public static HND_OperationPermit loadOperationPermitByPresentationNo(Session session, long presentationNo) {
        Criteria criteria = session.createCriteria(HND_OperationPermit.class);
        criteria.add(Restrictions.eq("presentationNo", presentationNo));
        
        return (HND_OperationPermit) criteria.uniqueResult();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_OperationPermit> loadOperationPermitsByCurrentState(Session session, Boolean approved) {
        final String FIELD = "approved";
        Criteria criteria = session.createCriteria(HND_OperationPermit.class);
        if (approved != null)
            criteria.add(Restrictions.eq(FIELD, approved));
        else
            criteria.add(Restrictions.isNull(FIELD));
        
        return criteria.list();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_OperationPermit> loadOperationPermitsByType(Session session, HND_OperationPermitType permitType) {
        Criteria criteria = session.createCriteria(HND_OperationPermit.class);
        criteria.add(Restrictions.eq("type", permitType));
        
        return criteria.list();
    }
}
