package org.sigit.dao.hnd.cadastre;

import org.sigit.dao.ladm.spatialunit.LA_SpatialUnitDAO;
import org.sigit.model.hnd.administrative.HND_RuleOperatorType;
import org.sigit.model.hnd.cadastre.HND_Layer;
import org.sigit.model.hnd.cadastre.HND_SpatialZone;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.spatial.criterion.SpatialRestrictions;

import com.vividsolutions.jts.geom.Geometry;

public class HND_SpatialZoneDAO extends LA_SpatialUnitDAO {
    private static final long serialVersionUID = 1L;

    
    public static HND_SpatialZone loadSpatialZoneByID(UUID id) {
        return loadSpatialZoneByID(getSession(), id);
    }
    public static List<HND_SpatialZone> loadSpatialZonesByLayer(HND_Layer layer) {
        return loadSpatialZonesByLayer(getSession(), layer);
    }
    public static List<HND_SpatialZone> loadSpatialZonesWithLandUseByLayer(HND_Layer layer) {
        return loadSpatialZonesWithLandUseByLayer(getSession(), layer);
    }
    public static List<HND_SpatialZone> loadSpatialZonesByNameAndLayer(String zoneName, HND_Layer layer) {
        return loadSpatialZonesByNameAndLayer(getSession(), zoneName, layer);
    }
    public static List<HND_SpatialZone> loadSpatialZonesByGeom(Geometry theGeom, HND_RuleOperatorType... rot) {
        return loadSpatialZonesByGeom(getSession(), theGeom, rot);
    }
    public static List<HND_SpatialZone> loadSpatialZonesByLayerAndGeom(HND_Layer layer, Geometry theGeom, HND_RuleOperatorType rot) {
        return loadSpatialZonesByLayerAndGeom(getSession(), layer, theGeom, rot);
    }
    public static Set<HND_SpatialZone> loadNeighborsBySpatialZone(HND_SpatialZone hndSpatialZone) {
        return loadNeighborsBySpatialZone(getSession(), hndSpatialZone);
    }
    public static List<HND_SpatialZone> loadSpatialZonesBySZAndLessBuiltAreaAllowed(
            HND_SpatialZone hndSpatialZone, BigDecimal builtArea) {
        return loadSpatialZonesBySZAndLessBuiltAreaAllowed(getSession(), hndSpatialZone, builtArea);
    }
    public static List<HND_SpatialZone> loadSpatialZonesBySZAndLessNumberOfFloorsAllowed(HND_SpatialZone hndSpatialZone, int numberOfFloors) {
        return loadSpatialZonesBySZAndLessNumberOfFloorsAllowed(getSession(), hndSpatialZone, numberOfFloors);
    }
    public static List<HND_SpatialZone> loadSpatialZonesByIntersectingSZ(
            HND_SpatialZone hndSpatialZone) {
        return loadSpatialZonesByIntersectingSZ(getSession(), hndSpatialZone);
    }
    public static List<HND_SpatialZone> loadSpatialZonesContainedInSZ(
            HND_SpatialZone hndSpatialZone) {
        return loadSpatialZonesContainedInSZ(getSession(), hndSpatialZone);
    }
    public static HND_SpatialZone loadSpatialZoneWithNonNullGeom() {
        return loadSpatialZoneWithNonNullGeom(getSession());
    }
    
    
    public static HND_SpatialZone loadSpatialZoneByID(Session session, UUID id) {
        Criteria criteria = session.createCriteria(HND_SpatialZone.class);
        criteria.add(Restrictions.eq("suID", id));
        
        return (HND_SpatialZone) criteria.uniqueResult();
    }
    @SuppressWarnings("unchecked")
    public static List<HND_SpatialZone> loadSpatialZonesByLayer(Session session, HND_Layer layer) {
        Criteria criteria = session.createCriteria(HND_SpatialZone.class);
        criteria.add(Restrictions.eq("level", layer));
        
        return criteria.list();
    }
    
    @SuppressWarnings("unchecked")
    public static List<HND_SpatialZone> loadSpatialZonesWithLandUseByLayer(Session session, HND_Layer layer) {
        Criteria criteria = session.createCriteria(HND_SpatialZone.class)
                .setFetchMode("landUse", FetchMode.JOIN);
        criteria.add(Restrictions.eq("level", layer));
        
        return criteria.list();
    }
    
    @SuppressWarnings("unchecked")
    public static List<HND_SpatialZone> loadSpatialZonesByNameAndLayer(Session session, String zoneName, HND_Layer layer) {
        Criteria criteria = session.createCriteria(HND_SpatialZone.class);
        criteria.add(Restrictions.like("zoneName", zoneName, MatchMode.START).ignoreCase());
        criteria.add(Restrictions.eq("level", layer));
        
        return criteria.list();
    }
    
    @SuppressWarnings("unchecked")
    public static List<HND_SpatialZone> loadSpatialZonesByLayerAndGeom(Session session, HND_Layer layer,
            Geometry theGeom, HND_RuleOperatorType rot) {
        Criteria criteria = session.createCriteria(HND_SpatialZone.class);
        criteria.add(Restrictions.eq("level", layer));
        addSpatialRestriction(criteria, theGeom, rot);
        
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    public static List<HND_SpatialZone> loadSpatialZonesByGeom(Session session, Geometry theGeom, HND_RuleOperatorType... rot) {
        Criteria criteria = session.createCriteria(HND_SpatialZone.class);
        addSpatialRestriction(criteria, theGeom, rot[0]);
        
        return criteria.list();
    }
    
    private static void addSpatialRestriction(Criteria criteria, Geometry geom, HND_RuleOperatorType rot) {
        switch (rot) {
        case DISTANCE:
            criteria.add(SpatialRestrictions.intersects("shape", geom));
            break;
        case OVERLAP:
            criteria.add(SpatialRestrictions.overlaps("shape", geom));
            break;
        case WITHIN:
            criteria.add(SpatialRestrictions.within("shape", geom));
            break;
        case TOUCH:
            criteria.add(SpatialRestrictions.touches("shape", geom));
            break;
        case CROSS:
            criteria.add(SpatialRestrictions.crosses("shape", geom));
            break;
        case CONTAINS:
            criteria.add(SpatialRestrictions.contains("shape", geom));
            break;
        default:
            throw new IllegalArgumentException(); 
        }
    }

    public static Set<HND_SpatialZone> loadNeighborsBySpatialZone(Session session, HND_SpatialZone hndSpatialZone) {
        Geometry filter = hndSpatialZone.getShape();
        
        if (filter != null) {
            Set<HND_SpatialZone> hndSpatialZoneSet = new HashSet<HND_SpatialZone>();
            
            Criteria criteria = session.createCriteria(HND_SpatialZone.class);
            criteria.add(SpatialRestrictions.touches("shape", filter));
            
            @SuppressWarnings("unchecked")
            List<HND_SpatialZone> hndTouchingSpatialZoneList = (List<HND_SpatialZone>)criteria.list();
            if (hndTouchingSpatialZoneList != null)
                for (HND_SpatialZone hndTouchingSZ : hndTouchingSpatialZoneList)
                    //TODO: Parameterize if we want touching parcels with intersection > 0 included or not
                    //if (hndTouchingP.getShape().intersection(filter).getLength() > 0.0001)
                    hndSpatialZoneSet.add(hndTouchingSZ);
            
            return hndSpatialZoneSet;
        }
        
        return null;
    }
    
    
    @SuppressWarnings("unchecked")
    public static List<HND_SpatialZone> loadSpatialZonesBySZAndLessBuiltAreaAllowed(
            Session session, HND_SpatialZone hndSpatialZone, BigDecimal builtArea) {
        Geometry geomFilter = hndSpatialZone.getShape();
        List<HND_SpatialZone> hndSpatialZones = new ArrayList<HND_SpatialZone>();
        if (geomFilter != null) {
            Criteria criteria = session.createCriteria(HND_SpatialZone.class);
            criteria.add(SpatialRestrictions.intersects("shape", geomFilter));
            criteria.add(Restrictions.lt("maxBuiltArea", builtArea));
            
            hndSpatialZones = (List<HND_SpatialZone>)criteria.list();
        }
        
        return hndSpatialZones;
    }
    
    @SuppressWarnings("unchecked")
    public static List<HND_SpatialZone> loadSpatialZonesBySZAndLessNumberOfFloorsAllowed(
            Session session, HND_SpatialZone hndSpatialZone, int numberOfFloors) {
        Geometry geomFilter = hndSpatialZone.getShape();
        List<HND_SpatialZone> hndSpatialZones = new ArrayList<HND_SpatialZone>();
        if (geomFilter != null) {
            Criteria criteria = session.createCriteria(HND_SpatialZone.class);
            criteria.add(SpatialRestrictions.intersects("shape", geomFilter));
            criteria.add(Restrictions.lt("maxNumberOfFloors", numberOfFloors));
            
            hndSpatialZones = (List<HND_SpatialZone>)criteria.list();
        }
        
        return hndSpatialZones;
    }
    
    @SuppressWarnings("unchecked")
    public static List<HND_SpatialZone> loadSpatialZonesByIntersectingSZ(
            Session session, HND_SpatialZone hndSpatialZone) {
        Geometry geomFilter = hndSpatialZone.getShape();
        List<HND_SpatialZone> hndSpatialZones = new ArrayList<HND_SpatialZone>();
        if (geomFilter != null) {
            Criteria criteria = session.createCriteria(HND_SpatialZone.class);
            criteria.add(SpatialRestrictions.intersects("shape", geomFilter));
            
            hndSpatialZones = (List<HND_SpatialZone>)criteria.list();
        }
        
        return hndSpatialZones;
    }

    @SuppressWarnings("unchecked")
    public static List<HND_SpatialZone> loadSpatialZonesContainedInSZ(
            Session session, HND_SpatialZone hndSpatialZone) {
        Geometry geomFilter = hndSpatialZone.getShape();
        List<HND_SpatialZone> hndSpatialZones = new ArrayList<HND_SpatialZone>();
        if (geomFilter != null) {
            Criteria criteria = session.createCriteria(HND_SpatialZone.class);
            criteria.add(SpatialRestrictions.contains("shape", geomFilter));
            
            hndSpatialZones = (List<HND_SpatialZone>)criteria.list();
        }
        
        return hndSpatialZones;
    }

    public static HND_SpatialZone loadSpatialZoneWithNonNullGeom(Session session) {
        Criteria criteria = session.createCriteria(HND_SpatialZone.class);
        criteria.add(Restrictions.isNotNull("shape"));
        criteria.setMaxResults(1);
        
        return (HND_SpatialZone) criteria.uniqueResult();
    }
}

