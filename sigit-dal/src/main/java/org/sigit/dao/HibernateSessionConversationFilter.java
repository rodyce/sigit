package org.sigit.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.SessionFactory;
import org.hibernate.context.internal.ManagedSessionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HibernateSessionConversationFilter implements Filter {
    public static final String HIBERNATE_SESSION_KEY = "hibernateSession";  
    public static final String END_OF_CONVERSATION_FLAG = "endOfConversation";

    private static final Logger log = LoggerFactory.getLogger(HibernateSessionConversationFilter.class);
    
    private SessionFactory sf;
    private List<String> ignorePathList;
    private List<String> requestScopedPathList;
    
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        sf = SessionManager.getSessionFactory();
        
        ignorePathList = new ArrayList<>();
        String ignorePaths = filterConfig.getInitParameter("ignorePaths");
        if (ignorePaths != null) {
            for (String path : ignorePaths.split(",")) {
                String p = path.trim();
                if (!ignorePathList.contains(p)) {
                    ignorePathList.add(p);
                }
            }
        }
        
        requestScopedPathList = new ArrayList<>();
        String requestScopedPaths = filterConfig.getInitParameter("requestScopedPaths");
        if (requestScopedPaths != null) {
            for (String path : requestScopedPaths.split(",")) {
                String p = path.trim();
                if (!requestScopedPathList.contains(p)) {
                    requestScopedPathList.add(p);
                }
            }
        }
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        final String requestUri = ((HttpServletRequest) request).getRequestURI();
        log.debug("Serving URI: " + requestUri);
        
        for (String ignorePath : ignorePathList) {
            if (requestUri.startsWith(ignorePath)) {
                log.debug("Processing ignore path: " + ignorePath);
                chain.doFilter(request, response);
                return; //We are done. Avoid any further processing.
            }
        }
        
        for (String requestScopedPath : requestScopedPathList) {
            if (((HttpServletRequest) request).getRequestURI().startsWith(requestScopedPath)) {
                log.debug("Creating a new session for this HTTP request");
                Session session = null;
                try {
                    session = sf.openSession();
                    logOpenHibernateSession(session);
                    ManagedSessionContext.bind(session);
          
                    log.debug("Processing the event");
                    chain.doFilter(request, response);
                } finally {
                    log.debug("Unbinding Session after processing the HTTP request");
                    ManagedSessionContext.unbind(sf);
                    if (session != null) {
                        try {
                            session.close();
                            logCloseHibernateSession(session);
                        } catch (Exception ex) {
                            log.warn("Error trying to close Hibernate session after processing request", ex);
                        }
                    }
                }
                return; //We are done. Avoid any further processing.
            }
        }
          
        // Try to get a Hibernate Session from the HttpSession  
        HttpSession httpSession =  
                ((HttpServletRequest) request).getSession();

        synchronized(httpSession) {
            log.debug("Obtained session ID: " + httpSession.getId() + " for URI: " + requestUri);
    
            Session currentHibernateSession;
            Session disconnectedHibernateSession =  
                    (Session) httpSession.getAttribute(HIBERNATE_SESSION_KEY);  
      
            // Start a new conversation or in the middle?
            if (disconnectedHibernateSession == null) {
                log.debug(">>> New conversation");
                currentHibernateSession = sf.openSession();
                logOpenHibernateSession(currentHibernateSession);
                currentHibernateSession.setFlushMode(FlushMode.COMMIT);
            } else {  
                log.debug("< Continuing conversation");  
                currentHibernateSession = (Session) disconnectedHibernateSession;
    
                //Make sure we get an open Hibernate session. This should not
                //happen often, but it is important to check
                if (!currentHibernateSession.isOpen()) {
                    log.warn("Closed Hibernate session found. Opening a new one.");
                    currentHibernateSession = sf.openSession();
                    logOpenHibernateSession(currentHibernateSession);
                    currentHibernateSession.setFlushMode(FlushMode.COMMIT);
                }
            }  
            
            log.debug("Binding the current Session");
            ManagedSessionContext.bind(currentHibernateSession);

            try {
                log.debug("Processing the event");
                chain.doFilter(request, response);
            } catch (Exception ex) {
                log.debug("Exception caught processing request (doFilter). HTTP session: " +
                            httpSession.getId(), ex);
                currentHibernateSession = ManagedSessionContext.unbind(sf);
                log.debug(String.format("Closing the Session %s. HTTP session %s",
                        currentHibernateSession.hashCode(),
                        httpSession.getId()));
                try {
                    currentHibernateSession.close();
                    logCloseHibernateSession(currentHibernateSession);
                } catch (SessionException se) {
                    log.debug("Before handling exception, the session was already closed", se);
                }
                
                throw ex;
            }
    
            log.debug("Unbinding Session after processing");
            currentHibernateSession = ManagedSessionContext.unbind(sf);
      
            try {
                // End or continue the long-running conversation?
                if (httpSession.getAttribute(END_OF_CONVERSATION_FLAG) != null) {
                    log.debug("Closing the Session");
                    try {
                        currentHibernateSession.close();
                        logCloseHibernateSession(currentHibernateSession);
                    } catch (SessionException se) {
                        log.debug("Session was already closed", se);
                    }
      
                    log.debug("Cleaning Hibernate Session " + currentHibernateSession.hashCode() + " from HttpSession " + httpSession.getId());
                    httpSession.setAttribute(HIBERNATE_SESSION_KEY, null);
    
                    httpSession.setAttribute(END_OF_CONVERSATION_FLAG, null);
      
                    log.debug("<<< End of conversation");
                } else {
                    httpSession.setAttribute(HIBERNATE_SESSION_KEY, currentHibernateSession);
                    log.debug("Stored Session " + currentHibernateSession.hashCode() + " in the HttpSession ID: " + httpSession.getId());
    
                    log.debug("> Returning to user in conversation");
                }
            } catch (IllegalStateException ise) {
                //This will happen if the httpSession was invalidated during the servlet execution
                //There is no direct way to check for that in the Session interface.
    
                log.debug("The HTTP session was invalidated. Closing Hibernate Session.");
                currentHibernateSession.close();
                logCloseHibernateSession(currentHibernateSession);
                log.debug("<<< End of conversation");
            }
        }
        // Let others handle it... maybe another interceptor for exceptions?
        /*
        if (ex instanceof ServletException) {
            throw ex;
        } else {
            throw new ServletException(ex);
        }*/
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
    }
    
    private void logOpenHibernateSession(Session session) {
        if (log.isDebugEnabled()) {
            synchronized(log) {
                openCount++;
                log.debug("OPENED Hibernate session: " + session.hashCode() + ". Open count: " + openCount);
            }
        }
    }
    private void logCloseHibernateSession(Session session) {
        if (log.isDebugEnabled()) {
            synchronized(log) {
                closeCount++;
                log.debug("CLOSED Hibernate session: " + session.hashCode() + ". Close count: " + closeCount);
            }
        }
    }

    private long openCount = 0;
    private long closeCount = 0;
}
