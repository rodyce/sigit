package org.sigit.dao;


import java.io.Serializable;

import org.hibernate.NonUniqueObjectException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.sigit.commons.di.CtxComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class SigitDAO<E> implements Serializable {
    private static final long serialVersionUID = -7640581780642087885L;
    
    private static final Logger log = LoggerFactory.getLogger(SigitDAO.class);

    protected SigitDAO() {    
    }

    protected static SessionFactory getSessionFactory(String factoryName) {
        return (SessionFactory) CtxComponent.getInstance(factoryName);
    }
    static SessionFactory getSessionFactory() {
        return getSessionFactory(SessionManager.COMMONS_SF);
    }
    
    protected static Session getSession() {
        Session session = getSessionFactory().getCurrentSession();
        log.debug("Will return session with system id " + System.identityHashCode(session));
        return session;
    }
    
    public static <E> E create(Class<E> type) {
        try {
            return type.newInstance();
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public static <E> E save(E ormEntity) {
        try {
            getSession().saveOrUpdate(ormEntity);
        } catch (NonUniqueObjectException ex) {
            log.warn("Had to use merge due to non uniqueness", ex);
            return (E) getSession().merge(ormEntity);
        }
        return ormEntity;
    }
    
    public static <E> void persist(E ormEntity) {
        getSession().persist(ormEntity);
    }
    
    public static <E> void merge(E ormEntity) {
        Session session = getSession();
        session.merge(ormEntity);
    }
    
    public static <E> void update(E ormEntity) {
        Session session = getSession();
        session.update(ormEntity);
    }
    
    public static <E> void delete(E ormEntity) {
        getSession().delete(ormEntity);
    }
    
    public static <E> void evict(E ormEntity) {
        getSession().evict(ormEntity);
    }
    
    public static <E> void refresh(E ormEntity) {
        getSession().refresh(ormEntity);
    }
    
    public static void clear() {
        getSession().clear();
    }
    
    public static void flush() {
        getSession().flush();
    }
}
