package org.sigit.dao;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SigitHttpSessionListener implements HttpSessionListener {
    private static final Logger log = LoggerFactory.getLogger(SigitHttpSessionListener.class);

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        if (log.isDebugEnabled()) {
            log.debug("Session created with ID: " + se.getSession().getId());
        }
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        //Close the associated Hibernate session attribute, if any.
        Session hibernateSession = (Session) se.getSession().getAttribute(
                HibernateSessionConversationFilter.HIBERNATE_SESSION_KEY);
        
        if (hibernateSession == null) {
            if (log.isDebugEnabled()) {
                log.debug("No hibernate session found when destroying the HTTP session " + se.getSession().getId());
            }
            return;
        }
        
        try {
            hibernateSession.close();
            if (log.isDebugEnabled()) {
                log.debug(String.format("Closed Hibernate session %s when destroying HTTP sessino %s",
                        hibernateSession.hashCode(), se.getSession().getId()));
            }
        } catch (HibernateException hex) {
            log.warn("Exception caught when trying to close Hibernate session on HTTP session expiration", hex);
        }
    }

}
