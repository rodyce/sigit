import uuid
import locale
import sys
from subprocess import PIPE, Popen


# TODO Parameterize these settings!
g_hostname = 'localhost'
g_database = 'svd0407'
g_username = 'sigitmaster'
g_srid = 32616
g_psql_binary = 'C:\\Program Files\\PostgreSQL\\9.5\\bin\\psql.exe'
g_shp2pgsql_binary = 'C:\\Program Files\\PostgreSQL\\9.5\\bin\\shp2pgsql.exe'
g_default_file_path = '.\\DATA'

data_files = [
    'zone.shp',
    'parcel.dbf',
    'zone_property.dbf',
    'property.dbf',
    'rights.dbf',
    'party.dbf',
    'ludomain.dbf',
    'landuse.dbf'
]

# TODO The 'p99_export_postprocessing' stored procedure is not executed here since it affects the whole migrated data.
# It should only affect current input data.
stored_procedures = [
    'p00_export_preprocessing',
    'p01_export_preliminaries',
    'p02_export_landuses',
    'p03_export_parties',
    'p04_export_l1_parcels',
    'p04_export_l2_zoning',
    'p04_export_l3_building_units',
    'p04_export_l4_streets',
    'p05_export_rrr'
]

messages = {
    'en': {
        'MSG_IDENTIFIERS': 'IDENTIFIERS',
        'MSG_IN_TABLE': 'In table',
        'MSG_REPEATED_ZONES_IN_ZONE': 'Found repeated zone identifiers in ZONE table',
        'MSG_NO_LAND_USE_IN_ZONE': 'Land use has not been specified for one or more zones in ZONE',
        'MSG_INVALID_LAND_USE_IN_ZONE':
            'Invalid land use codes found in ZONE table. Check correct separators (commas, dots, etc.)',
        'MSG_REPEATED_ZONES_IN_PARCEL': 'Found repeated zone identifiers in PARCEL table',
        'MSG_REPEATED_ZONES_IN_BUILDING_UNIT': 'Found repeated zone identifiers in BUILDING_UNIT table',
        'MSG_REPEATED_ZONES_IN_STREET': 'Found repeated zone identifiers in STREET table',
        'MSG_REPEATED_PARTY_IN_PARTY': 'Found repeated party identifiers in PARTY table',
        'MSG_PERSON_TYPE_UNSPECIFIED': 'Person type must be "N" for Natural or "L" for Legal',

        'MSG_ORPHAN_PARCEL_NO_ZONE': 'Found orphan PARCEL(s) with no corresponding record in ZONE',
        'MSG_ORPHAN_BUILDING_UNIT_NO_ZONE': 'Found orphan BUILDING UNIT(s) with no corresponding record in ZONE',
        'MSG_ORPHAN_STREET_NO_ZONE': 'Found orphan STREET(s) with no corresponding record in ZONE',
        'MSG_ORPHAN_ZONE_PROPERTY_NO_ZONE': 'Found orphan zones in ZONE_PROPERTY with no corresponding record in ZONE',
        'MSG_ORPHAN_PROPERTY_NO_ZONE_PROPERTY': 'Found orphan PROPERTY(ies) with no corresponding record in'
                                                'ZONE_PROPERTY',
        'MSG_ORPHAN_RIGHT_NO_PROPERTY': 'Found orphan RIGHT(s) with no corresponding record in PROPERTY',
        'MSG_ORPHAN_RIGHT_NO_PARTY': 'Found orphan RIGHT(s) with no corresponding record in PARTY',

        'MSG_ORPHAN_RESTRICTION_NO_PROPERTY': 'Found orphan RESTRICTION(s) with no corresponding record in PROPERTY',
        'MSG_ORPHAN_RESTRICTION_NO_PARTY': 'Found orphan RESTRICTION(s) with no corresponding record in PARTY',

        'MSG_SHARE_NUMERATOR_LTE_0': 'The Numerator of the RRR share must be greater than zero (0). ',
        'MSG_SHARE_NUMERATOR_GT_DENOMINATOR':
            'The Numerator of the RRR share must be less or equal than the its Denominator. ',

        'MSG_INVALID_GEOM_IN_ZONE': 'Found zones with invalid geometry in ZONE table',

        'MSG_GEOM_NOT_WITHIN_EXTENTS': 'Found zones not within jurisdiction/allowed extents',

        'MSG_INVALID_LEVELID_VALUE': 'Found invalid values for "LEVELID" field in ZONE table. '
                                     'Valid values are from 1 to 4 inclusive.',
        'MSG_NO_PARCELS': 'No parcels found in ZONE table',
        'MSG_NO_BUILDING_UNITS': 'No building units found in ZONE table',
        'MSG_NO_STREETS': 'No streets found in ZONE table',
        'MSG_NO_ZONE_REMINDER': 'The field "LEVELID" in the ZONE data file must contain the type of zone as follows:\\n'
                                '1: Parcel\\n'
                                '2: Zoning\\n'
                                '3: Building unit\\n'
                                '4: Street\\n',

        'MSG_INVALID_FILL_OPACITY': 'Found invalid values for "FILLOPAC" (Fill Opacity) field in LANDUSE table. '
                                    'Valid values must be greater than 0.1.',
        'MSG_INVALID_STROKE_WIDTH': 'Found invalid values for "STROKEWIDT" (Stroke Width) field in LANDUSE table. '
                                    'Valid values must be greater than 0.05.',

        'MSG_ERROR_HAPPENED': 'One or more errors occurred during execution',
        'MSG_PROCESS_FINISHED': 'PROCESS FINISHED'
    },
    'es': {
        'MSG_IDENTIFIERS': 'IDENTIFICADORES',
        'MSG_IN_TABLE': 'En la tabla',
        'MSG_REPEATED_ZONES_IN_ZONE': 'Se encontraron identificadores de zona repetidos en la tabla ZONE',
        'MSG_NO_LAND_USE_IN_ZONE': 'No se ha especificado el tipo de uso en una o mas zonas espaciales en la tabla '
                                   'ZONE',
        'MSG_INVALID_LAND_USE_IN_ZONE':
            'Se encontraron codigos invalidos de tipos de uso en la tabla ZONE. '
            'Verifique separadores correctos (comas, puntos, etc.)',
        'MSG_REPEATED_ZONES_IN_PARCEL': 'Se encontraron identificadores de zona repetidos en la tabla PARCEL',
        'MSG_REPEATED_ZONES_IN_BUILDING_UNIT': 'Se encontraron identificadores de zona repetidos en la tabla '
                                               'BUILDING UNIT',
        'MSG_REPEATED_ZONES_IN_STREET': 'Se encontraron identificadores de zona repetidos en la tabla STREET',
        'MSG_REPEATED_PARTY_IN_PARTY': 'Se encontraron identificadores de PERSONA repetidos en la tabla PARTY',
        'MSG_PERSON_TYPE_UNSPECIFIED': 'El tipo de Persona debe ser "N" para Natural or "L" para Legal (juridica)',

        'MSG_ORPHAN_PARCEL_NO_ZONE': 'Se encontraron registros de parcelas huerfanos en la tabla PARCEL '
                                     'sin registro correspondiente en la tabla ZONE',
        'MSG_ORPHAN_BUILDING_UNIT_NO_ZONE': 'Se encontraron registros de edificios huerfanos en la tabla BUILDING_UNIT '
                                            'sin registro correspondiente en la tabla ZONE',
        'MSG_ORPHAN_STREET_NO_ZONE': 'Se encontraron registros de calles huerfanos en la tabla STREET '
                                     'sin registro correspondiente en la tabla ZONE',
        'MSG_ORPHAN_ZONE_PROPERTY_NO_ZONE': 'Se encontraron registros de zona huerfanos en la tabla ZONE_PROPERTY '
                                            'sin registro correspondiente en la tabla ZONE',
        'MSG_ORPHAN_PROPERTY_NO_ZONE_PROPERTY': 'Se encontraron registros de propiedades huerfanos en la tabla '
                                                'PROPERTY sin registro correspondiente en la tabla ZONE_PROPERTY',
        'MSG_ORPHAN_RIGHT_NO_PROPERTY': 'Se encontraton registros de derechos huerfanos en la tabla RIGHTS '
                                        'sin registro correspondiente en la tabla PROPERTY',
        'MSG_ORPHAN_RIGHT_NO_PARTY': 'Se encontraton registros de derechos huerfanos en la tabla RIGHTS '
                                     'sin registro correspondiente en la tabla PARTY',
        'MSG_ORPHAN_RESTRICTION_NO_PROPERTY': 'Se encontraton registros de restricciones huerfanos en la tabla '
                                              'RESTRICTION sin registro correspondiente en la tabla PROPERTY',
        'MSG_ORPHAN_RESTRICTION_NO_PARTY': 'Se encontraton registros de restricciones huerfanos en la tabla '
                                           'RESTRICTION sin registro correspondiente en la tabla PARTY',

        'MSG_SHARE_NUMERATOR_LTE_0': 'El numerador de la fraccion de RRR debe ser mayor que cero (0). ',
        'MSG_SHARE_NUMERATOR_GT_DENOMINATOR':
            'El numerador de la fraccion de RRR debe ser menor o igual que el denominador. ',

        'MSG_INVALID_GEOM_IN_ZONE': 'Se encontraron zonas con geometria invalida en la tabla ZONE',

        'MSG_GEOM_NOT_WITHIN_EXTENTS': 'Se encontraron zonas fuera de la jurisdiccion o extension permitida',

        'MSG_INVALID_LEVELID_VALUE': 'Se encontraron valores incorrectos para el campo "LEVELID" en la tabla ZONE. '
                                     'Los valores correctos son de 1 a 4 inclusive.',
        'MSG_NO_PARCELS': 'No se encontraron Parcelas en la tabla ZONE',
        'MSG_NO_BUILDING_UNITS': 'No se encontraron Edificaciones en la tabla ZONE',
        'MSG_NO_STREETS': 'No se encontraron Calles en la tabla ZONE',
        'MSG_NO_ZONE_REMINDER': 'El campo "LEVELID" en el archivo de datos ZONE debe contener el tipo de zona como '
                                'sigue:\\n'
                                '1: PARCELA\\n'
                                '2: ZONIFICACION\\n'
                                '3: EDIFICACION\\n'
                                '4: CALLE\\n',

        'MSG_INVALID_FILL_OPACITY': 'Se encontraron valores incorrectos para el campo "FILLOPAC" (Opacidad de Color) '
                                    'en la tabla LANDUSE. '
                                    'Los valores correctos deben ser mayores a 0.1.',
        'MSG_INVALID_STROKE_WIDTH': 'Se encontraron valores incorrectos para el campo "STROKEWIDT" (Anchura de trazo) '
                                    'en la tabla LANDUSE. '
                                    'Los valores correctos deben ser mayores a 0.05.',

        'MSG_ERROR_HAPPENED': u'Se encontraron uno o m\u00E1s errores de durante la ejecuci\u00F3n',
        'MSG_PROCESS_FINISHED': 'FIN DEL PROCESO'
    }
}

# Defaults to en locale language
locale_lang = 'en'

# Dictionary to get messages
locale_dict = messages[locale_lang]


def exec_psql(command, ignore_stderr = False):
    p = Popen([g_psql_binary, '-h', g_hostname, '-d', g_database, '-U', g_username, '-c', command],
              stdin=PIPE, stdout=PIPE, stderr=PIPE, bufsize=1)
    _, std_err = p.communicate()

    if not ignore_stderr:
        if std_err is not None and len(std_err.strip()) > 0:
            raise Exception(std_err)


def execute_sql_file(sql_file, schema_name):
    with open(sql_file) as a_file:
        command = a_file.read()
        command = command.replace('migration', schema_name)
        for msg_key, msg_value in locale_dict.iteritems():
            command = command.replace('#{' + msg_key + '}', msg_value)
    return command


# Execute shp2pgsql given the directory to find it (search $PATH)
# Normally, it is located in PostgreSQL bin directory
# shp2pgsql [<options>] <shapefile> [[<schema>.]<table>]
def exec_shp2pgsql(shape_file, schema, srid):
    hostname = g_hostname
    database = g_database
    username = g_username

    print 'vamos a ver'


    # Use the following parameters when calling shp2pgsql
    # -s <srid> Set the SRID field
    # -d Drops the table, then recreates it and populates
    # -c Creates a new table and populate it (default)
    # -g <geocolumn> Specify the name of the geometry column
    # [-m <filename> Specify a file containing a set of mappings of (long) column
    #                 names to 10 character DBF column names]
    # -S Generate simple geometries instead of MULTI geometries
    # -t '2D'
    # [-n Only import DBF file]

    # Since the schema is generated in every run, then we use the '-c' parameter
    shp2psql_exec_array = [g_shp2pgsql_binary, '-s', repr(str(srid)),
                           '-c', '-g', 'shape', '-S', '-t', '2D']
    # If the file extension does not end in SHP, take it as DBF only processing
    if not shape_file.upper().endswith('SHP'):
        shp2psql_exec_array += ['-n']

    for data_file in data_files:
        if shape_file.lower().endswith(data_file):
            table = data_file.split('.', 2)[0]
            shp2psql_exec_array += [shape_file, '{}.{}'.format(schema, table)]

    print str(shp2psql_exec_array)

    psql_exec_array = [g_psql_binary, '-h', hostname, '-d', database, '-U', username]

    p_shp2pgsql = Popen(shp2psql_exec_array, stdout=PIPE)
    p_psql = Popen(psql_exec_array, stdin=p_shp2pgsql.stdout, stdout=PIPE)
    p_shp2pgsql.stdout.close()  # Allow p_shp2pgsql to receive a SIGPIPE if p_psql exits.
    std_out, std_err = p_psql.communicate()

    # if std_out is not None:
    #     print 'STDOUT: ' + str(std_out)
    if std_err is not None:
        print 'STDERR: ' + std_err

    # err = p.stderr.read()
    # if len(err) > 0:
    #     print err
    #     raise Exception('Terminating due to error(s)')

    # Return the commands output to be executed by 'psql'
    return std_out


def main():
    reload(sys)
    sys.setdefaultencoding('utf8')

    working_file_path = g_default_file_path if len(sys.argv) < 2 \
        else sys.argv[1]

    sql_files = [
        'db_functions/gcd.sql',
        'db_functions/landuse_color_formula.sql',
        'db_functions/table_exists.sql',
        'db_functions/sigit_uuid_numeric.sql',
        'db_functions/sigit_uuid_text.sql',
        'db_functions/export_spatialzones.sql',
        'db_functions/p00_export_preprocessing.sql',
        'db_functions/p01_export_preliminaries.sql',
        'db_functions/p02_export_landuses.sql',
        'db_functions/p03_export_parties.sql',
        'db_functions/p04_export_l1_parcels.sql',
        'db_functions/p04_export_l2_zoning.sql',
        'db_functions/p04_export_l3_building_units.sql',
        'db_functions/p04_export_l4_streets.sql',
        'db_functions/p05_export_rrr.sql'
    ]

    lang, _ = locale.getdefaultlocale()

    global locale_lang, locale_dict
    locale_lang = lang[0:2]
    locale_dict = messages[locale_lang]

    schema_name = 'migrator_schema_' + str(uuid.uuid1())
    schema_name = schema_name.replace('-', '')

    try:
        # Create new temporal migration schema
        command = 'CREATE SCHEMA {};'.format(schema_name)
        exec_psql(command)

        # Export stored procedures code in the temporal migration schema
        for a_file in sql_files:
            print 'processing ' + a_file
            command = execute_sql_file(a_file, schema_name)
            exec_psql(command)

        # Export the data files. This includes the shapefile and the DBFs
        for data_file in data_files:
            exec_shp2pgsql(working_file_path + '\\' + data_file, schema_name, g_srid)

        # Execute stored the stored procedures to populate the real SIGIT's
        # tables
        command = 'BEGIN TRANSACTION;'
        for sp in stored_procedures:
            command += 'SELECT {}.{}();'.format(schema_name, sp)
        command += 'COMMIT;'
        command += 'END TRANSACTION;'
        exec_psql(command)
    except Exception as exc:
        print str(locale_dict['MSG_ERROR_HAPPENED']) + ':\n'
        print exc.message
    finally:
        # Clean temporary schema
        command = 'DROP SCHEMA {} CASCADE;'.format(schema_name)
        exec_psql(command, ignore_stderr=True)

        print locale_dict['MSG_PROCESS_FINISHED']

    return 0


main()
