CREATE OR REPLACE FUNCTION migration.p04_export_l2_zoning()
  RETURNS integer AS
$BODY$BEGIN
    PERFORM migration.export_spatialzones(2);


    RETURN 0;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
