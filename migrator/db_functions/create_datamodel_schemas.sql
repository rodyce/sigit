CREATE OR REPLACE FUNCTION migration.create_datamodel_schemas()
  RETURNS void AS
$BODY$BEGIN
    CREATE SCHEMA gis_layers AUTHORIZATION sigitmaster;
    CREATE SCHEMA hnd_administrative AUTHORIZATION sigitmaster;
    CREATE SCHEMA hnd_cadastre AUTHORIZATION sigitmaster;
    CREATE SCHEMA hnd_special AUTHORIZATION sigitmaster;
    CREATE SCHEMA ladm_administrative AUTHORIZATION sigitmaster;
    CREATE SCHEMA ladm_external AUTHORIZATION sigitmaster;
    CREATE SCHEMA ladm_party AUTHORIZATION sigitmaster;
    CREATE SCHEMA ladm_spatialunit AUTHORIZATION sigitmaster;
    CREATE SCHEMA ladm_spatialunit_surveyingandrepresentation;
    CREATE SCHEMA ladm_special AUTHORIZATION sigitmaster;
    CREATE SCHEMA ladmshadow AUTHORIZATION sigitmaster;
    CREATE SCHEMA spatialtest AUTHORIZATION sigitmaster;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
