CREATE OR REPLACE FUNCTION migration.p00_export_preprocessing()
  RETURNS integer AS
$BODY$/*
El pre proceso incluye todas aquellas operaciones cuyo fin es preparar el esquema de migration
para posteriormente ser procesado por el proceso de migration en si.

Esto incluye:
0.- Eliminacion de posibles duplicados en llaves de tablas de migration
1.- Eliminacion de zonas con geometria invalida
2.- Correccion de llaves foraneas dentro del esquema de migration
3.- Mapeo de tipos de derecho, restricciones y responsabilidades
*/

DECLARE
    v_duplicate_count INTEGER;
    v_jurisdictionshape GEOMETRY;
    bad_zone_ids CHARACTER VARYING;
    error_messages CHARACTER VARYING;
BEGIN
    error_messages = '';

    --======================================================================
    --Verify there are no duplicates in ZONE table
    SELECT COUNT(*) INTO v_duplicate_count
    FROM
    (
        SELECT zone_id
        FROM migration.zone
        GROUP BY zone_id
        HAVING COUNT(zone_id) > 1
    ) a;
    IF v_duplicate_count > 0 THEN
        SELECT array_to_string(array_agg(zone_id), ', ') INTO bad_zone_ids FROM migration.zone
        WHERE zone_id IN
        (
            SELECT zone_id
            FROM migration.zone
            GROUP BY zone_id
            HAVING COUNT(zone_id) > 1
        );

        error_messages = error_messages || E'#{MSG_REPEATED_ZONES_IN_ZONE}\n';
        error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
    END IF;

    --Verify there are no invalid zone types in ZONE table
    SELECT COUNT(*) INTO v_duplicate_count
    FROM migration.zone
    WHERE levelid NOT IN (1, 2, 3, 4);
    IF v_duplicate_count > 0 THEN
        error_messages = error_messages || E'#{MSG_INVALID_LEVELID_VALUE}\n';
        error_messages = error_messages || E'#{MSG_NO_ZONE_REMINDER}\n';
    END IF;

    --Verify the land use is specified for every spatial zone
    SELECT COUNT(*) INTO v_duplicate_count
    FROM migration.zone
    WHERE landuseid IS NULL;
    IF v_duplicate_count > 0 THEN
        SELECT array_to_string(array_agg(zone_id), ', ') INTO bad_zone_ids
        FROM migration.zone
        WHERE landuseid IS NULL;

        error_messages = error_messages || E'#{MSG_NO_LAND_USE_IN_ZONE}\n';
        error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
    END IF;

    --Verify that the land use specified exists in the land use domain
    SELECT COUNT(*) INTO v_duplicate_count
    FROM migration.zone z
    WHERE z.landuseid NOT IN(SELECT complcode FROM migration.landuse);
    IF v_duplicate_count > 0 THEN
        SELECT array_to_string(array_agg(DISTINCT z.landuseid), ', ') INTO bad_zone_ids
        FROM migration.zone z
        WHERE z.landuseid NOT IN(SELECT complcode FROM migration.landuse);

        error_messages = error_messages || E'#{MSG_INVALID_LAND_USE_IN_ZONE}\n';
        error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
    END IF;

    --Verify there are no duplicates in PARCEL table
    IF migration.table_exists('migration', 'parcel') THEN
        --Verify if there are zones with levelid = 1 (parcels)
        SELECT COUNT(*) INTO v_duplicate_count
        FROM migration.zone
        WHERE levelid = 1;
        IF v_duplicate_count = 0 THEN
            error_messages = error_messages || E'#{MSG_NO_PARCELS}\n';
            error_messages = error_messages || E'#{MSG_NO_ZONE_REMINDER}\n';
        END IF;

        SELECT COUNT(*) INTO v_duplicate_count
        FROM
        (
            SELECT zone_id
            FROM migration.parcel
            GROUP BY zone_id
            HAVING COUNT(zone_id) > 1
        ) a;
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(zone_id), ', ') INTO bad_zone_ids
            FROM migration.parcel
            WHERE zone_id IN
            (
                SELECT zone_id
                FROM migration.parcel
                GROUP BY zone_id
                HAVING COUNT(zone_id) > 1
            );

            error_messages = error_messages || E'#{MSG_REPEATED_ZONES_IN_PARCEL}\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;
    END IF;

    --Verify there are no duplicates in BUILDING UNIT table
    IF migration.table_exists('migration', 'building_unit') THEN
        --Verify if there are zones with levelid = 3 (building unit)
        SELECT COUNT(*) INTO v_duplicate_count
        FROM migration.zone
        WHERE levelid = 3;
        IF v_duplicate_count = 0 THEN
            error_messages = error_messages || E'#{MSG_NO_BUILDING_UNITS}\n';
            error_messages = error_messages || E'#{MSG_NO_ZONE_REMINDER}\n';
        END IF;

        SELECT COUNT(*) INTO v_duplicate_count
        FROM
        (
            SELECT zone_id
            FROM migration.building_unit
            GROUP BY zone_id
            HAVING COUNT(zone_id) > 1
        ) a;
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(zone_id), ', ') INTO bad_zone_ids
            FROM migration.building_unit
            WHERE zone_id IN
            (
                SELECT zone_id
                FROM migration.building_unit
                GROUP BY zone_id
                HAVING COUNT(zone_id) > 1
            );

            error_messages = error_messages || E'#{MSG_REPEATED_ZONES_IN_BUILDING_UNIT}\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;
    END IF;

    --Verify there are no duplicates in STREET table
    IF migration.table_exists('migration', 'street') THEN
        --Verify if there are zones with levelid = 4 (street)
        SELECT COUNT(*) INTO v_duplicate_count
        FROM migration.zone
        WHERE levelid = 4;
        IF v_duplicate_count = 0 THEN
            error_messages = error_messages || E'#{MSG_NO_STREETS}\n';
            error_messages = error_messages || E'#{MSG_NO_ZONE_REMINDER}\n';
        END IF;

        SELECT COUNT(*) INTO v_duplicate_count
        FROM
        (
            SELECT zone_id
            FROM migration.street
            GROUP BY zone_id
            HAVING COUNT(zone_id) > 1
        ) a;
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(zone_id), ', ') INTO bad_zone_ids
            FROM migration.street
            WHERE zone_id IN
            (
                SELECT zone_id
                FROM migration.street
                GROUP BY zone_id
                HAVING COUNT(zone_id) > 1
            );

            error_messages = error_messages || E'#{MSG_REPEATED_ZONES_IN_STREET}\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;
    END IF;

    --Verification in PARTY table
    IF migration.table_exists('migration', 'party') THEN
        --Verify there are no duplicates in PARTY table
        SELECT COUNT(*) INTO v_duplicate_count
        FROM
        (
            SELECT party_id
            FROM migration.party
            GROUP BY party_id
            HAVING COUNT(party_id) > 1
        ) a;
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(party_id), ', ') INTO bad_zone_ids
            FROM migration.party
            WHERE party_id IN
            (
                SELECT party_id
                FROM migration.party
                GROUP BY party_id
                HAVING COUNT(party_id) > 1
            );

            error_messages = error_messages || E'#{MSG_REPEATED_PARTY_IN_PARTY}\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;

        --Verify that type of person is specified (L: Legal Person, N: Natural Person)
        SELECT COUNT(*) INTO v_duplicate_count
        FROM migration.party
        WHERE type IS NULL OR type NOT IN ('N', 'L');
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(party_id), ', ') INTO bad_zone_ids
            FROM migration.party
            WHERE type IS NULL OR type NOT IN ('N', 'L');

            error_messages = error_messages || E'#{MSG_PERSON_TYPE_UNSPECIFIED}\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;
    END IF;
    --== END (I) ===========================================================


    --======================================================================
    --II.- Verify referential integrity within the migration schema

    --Verify there are no orphan parcels
    IF migration.table_exists('migration', 'parcel') THEN
        SELECT COUNT(*) INTO v_duplicate_count
        FROM migration.parcel p
        WHERE NOT EXISTS (SELECT z.zone_id FROM migration.zone z WHERE z.zone_id = p.zone_id);
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(zone_id), ', ') INTO bad_zone_ids
            FROM migration.parcel p
            WHERE NOT EXISTS (SELECT z.zone_id FROM migration.zone z WHERE z.zone_id = p.zone_id);

            error_messages = error_messages || E'#{MSG_ORPHAN_PARCEL_NO_ZONE}\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;
    END IF;

    --Verify there are no orphan building units
    IF migration.table_exists('migration', 'building_unit') THEN
        SELECT COUNT(*) INTO v_duplicate_count
        FROM migration.building_unit bu
        WHERE NOT EXISTS (SELECT z.zone_id FROM migration.zone z WHERE z.zone_id = bu.zone_id);
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(zone_id), ', ') INTO bad_zone_ids
            FROM migration.building_unit bu
            WHERE NOT EXISTS (SELECT z.zone_id FROM migration.zone z WHERE z.zone_id = bu.zone_id);

            error_messages = error_messages || E'#{MSG_ORPHAN_BUILDING_UNIT_NO_ZONE}\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;
    END IF;

    IF migration.table_exists('migration', 'street') THEN
        SELECT COUNT(*) INTO v_duplicate_count
        FROM migration.street s
        WHERE NOT EXISTS (SELECT z.zone_id FROM migration.zone z WHERE z.zone_id = s.zone_id);
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(zone_id), ', ') INTO bad_zone_ids
            FROM migration.street s
            WHERE NOT EXISTS (SELECT z.zone_id FROM migration.zone z WHERE z.zone_id = s.zone_id);

            error_messages = error_messages || E'#{MSG_ORPHAN_STREET_NO_ZONE}\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;
    END IF;

    IF migration.table_exists('migration', 'zone_property') THEN
        SELECT COUNT(*) INTO v_duplicate_count
        FROM migration.zone_property zp
        WHERE NOT EXISTS (SELECT z.zone_id FROM migration.zone z WHERE z.zone_id = zp.zone_id);
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(zone_id), ', ') INTO bad_zone_ids
            FROM migration.zone_property zp
            WHERE NOT EXISTS (SELECT z.zone_id FROM migration.zone z WHERE z.zone_id = zp.zone_id);

            error_messages = error_messages || E'#{MSG_ORPHAN_ZONE_PROPERTY_NO_ZONE}\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;
    END IF;

    --Esto se hace debido a que la relacion entre propiedad y zona se
    --esta manejando estrictamente de 1 a 1
    IF migration.table_exists('migration', 'property') THEN
        SELECT COUNT(*) INTO v_duplicate_count
        FROM migration.property p
        WHERE NOT EXISTS (SELECT zp.propertyid FROM migration.zone_property zp WHERE p.propertyid = zp.propertyid);
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(propertyid), ', ') INTO bad_zone_ids
            FROM migration.property p
            WHERE NOT EXISTS (SELECT zp.propertyid FROM migration.zone_property zp WHERE p.propertyid = zp.propertyid);

            error_messages = error_messages || E'#{MSG_ORPHAN_PROPERTY_NO_ZONE_PROPERTY}\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;
    END IF;

    -- Verify RIGHTS
    IF migration.table_exists('migration', 'rights') THEN
        -- Verify there are no RIGHTS with nonexistent PROPERTY
        SELECT COUNT(*) INTO v_duplicate_count
        FROM migration.rights r
        WHERE NOT EXISTS (SELECT propertyid FROM migration.property p WHERE p.propertyid = r.propertyid);
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(propertyid), ', ') INTO bad_zone_ids
            FROM migration.rights r
            WHERE NOT EXISTS (SELECT propertyid FROM migration.property p WHERE p.propertyid = r.propertyid);

            error_messages = error_messages || E'#{MSG_ORPHAN_RIGHT_NO_PROPERTY}\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;

        -- Verify there are no RIGHTS with nonexistent PARTY
        SELECT COUNT(*) INTO v_duplicate_count
        FROM migration.rights r
        WHERE NOT EXISTS (SELECT party_id FROM migration.party p WHERE p.party_id = r.party_id);
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(party_id), ', ') INTO bad_zone_ids
            FROM migration.rights r
            WHERE NOT EXISTS (SELECT party_id FROM migration.party p WHERE p.party_id = r.party_id);

            error_messages = error_messages || E'#{MSG_ORPHAN_RIGHT_NO_PARTY}\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;

        -- Verify there are no fraction's numerators less than than 0 (fraction must be > 0/1)
        SELECT COUNT(*) INTO v_duplicate_count
        FROM migration.rights r
        WHERE numerator <= 0;
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(propertyid), ', ') INTO bad_zone_ids
            FROM migration.rights r
            WHERE numerator <= 0;

            error_messages = error_messages || E'#{MSG_IN_TABLE}' || E' RIGHTS:\n';
            error_messages = error_messages || E'#{MSG_SHARE_NUMERATOR_LTE_0}\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;

        -- Verify there are no fraction's numerators greater than denominator (fraction must be <= 1/1)
        SELECT COUNT(*) INTO v_duplicate_count
        FROM migration.rights r
        WHERE numerator > denominato;
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(propertyid), ', ') INTO bad_zone_ids
            FROM migration.rights r
            WHERE numerator > denominato;

            error_messages = error_messages || E'#{MSG_IN_TABLE}' || E' RIGHTS:\n';
            error_messages = error_messages || E'#{MSG_SHARE_NUMERATOR_GT_DENOMINATOR}\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;
    END IF;

    -- Verify RESTRICTIONS
    IF migration.table_exists('migration', 'restrictions') THEN
        -- Verify there are no RESTRICTIONS with nonexistent PROPERTY
        SELECT COUNT(*) INTO v_duplicate_count
        FROM migration.restrictions r
        WHERE NOT EXISTS (SELECT propertyid FROM migration.property p WHERE p.propertyid = r.propertyid);
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(propertyid), ', ') INTO bad_zone_ids
            FROM migration.restrictions r
            WHERE NOT EXISTS (SELECT propertyid FROM migration.property p WHERE p.propertyid = r.propertyid);

            error_messages = error_messages || E'#{MSG_ORPHAN_RESTRICTION_NO_PROPERTY}\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;

        -- Verify there are no RIGHTS with nonexistent PARTY
        SELECT COUNT(*) INTO v_duplicate_count
        FROM migration.restrictions r
        WHERE NOT EXISTS (SELECT party_id FROM migration.party p WHERE p.party_id = r.party_id);
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(party_id), ', ') INTO bad_zone_ids
            FROM migration.restrictions r
            WHERE NOT EXISTS (SELECT party_id FROM migration.party p WHERE p.party_id = r.party_id);

            error_messages = error_messages || E'#{MSG_ORPHAN_RESTRICTION_NO_PARTY}\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;

        -- Verify there are no fraction's numerators less than than 0 (fraction must be > 0/1)
        SELECT COUNT(*) INTO v_duplicate_count
        FROM migration.restrictions r
        WHERE numerator <= 0;
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(propertyid), ', ') INTO bad_zone_ids
            FROM migration.restrictions r
            WHERE numerator <= 0;

            error_messages = error_messages || E'#{MSG_IN_TABLE}' || E' RESTRICTIONS:\n';
            error_messages = error_messages || E'#{MSG_SHARE_NUMERATOR_LTE_0}\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;

        -- Verify there are no fraction's numerators greater than denominator (fraction must be <= 1/1)
        SELECT COUNT(*) INTO v_duplicate_count
        FROM migration.restrictions r
        WHERE numerator > denominato;
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(propertyid), ', ') INTO bad_zone_ids
            FROM migration.restrictions r
            WHERE numerator > denominato;

            error_messages = error_messages || E'#{MSG_IN_TABLE}' || E' RESTRICTIONS:\n';
            error_messages = error_messages || E'#{MSG_SHARE_NUMERATOR_GT_DENOMINATOR}\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;
    END IF;

    -- TODO: Add case for Responsibilities
    --== END (II) ==========================================================


    --======================================================================
    --III.- Verify correct geometry
    UPDATE migration.zone
    SET shape = st_buffer(shape, 0.0);

    -- Verify
    SELECT COUNT(*) INTO v_duplicate_count
    FROM migration.zone
    WHERE NOT st_isvalid(shape);
    IF v_duplicate_count > 0 THEN
        SELECT array_to_string(array_agg(zone_id), ', ') INTO bad_zone_ids
        FROM migration.zone
        WHERE NOT st_isvalid(shape);

        error_messages = error_messages || E'#{MSG_INVALID_GEOM_IN_ZONE}' || E'\n';
        error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
    END IF;

    -- Verify that all polygons are within the SIGIT's instance jurisdiction/extents,
    -- if any is defined.
    SELECT jurisdictionshape INTO v_jurisdictionshape
    FROM hnd_special.systemconfiguration
    WHERE id = sigit_uuid(1);
    IF v_jurisdictionshape IS NOT NULL THEN
        SELECT COUNT(*) INTO v_duplicate_count
        FROM migration.zone
        WHERE NOT st_within(st_setsrid(shape, 32616), st_setsrid(v_jurisdictionshape, 32616));
        IF v_duplicate_count > 0 THEN
            SELECT array_to_string(array_agg(zone_id), ', ') INTO bad_zone_ids
            FROM migration.zone
            WHERE NOT st_within(shape, v_jurisdictionshape);

            error_messages = error_messages || E'#{MSG_GEOM_NOT_WITHIN_EXTENTS}' || E'\n';
            error_messages = error_messages || E'#{MSG_IDENTIFIERS}:\n' || bad_zone_ids || E'\n\n';
        END IF;
    END IF;
    --== END (III) =========================================================

    --======================================================================
    --IV.- Verify Land Use data
    -- 1.- Verify that color fill opacity is at least 0.1. Less than this would
    --     make the fill color invisible (completely translucent).
    SELECT COUNT(*) INTO v_duplicate_count
    FROM migration.landuse
    WHERE fillopac < 0.1;
    IF v_duplicate_count > 0 THEN
        error_messages = error_messages || E'#{MSG_INVALID_FILL_OPACITY}' || E'\n';
    END IF;

    -- 2.- Verify that the stroke width is at least 0.05. Less than that would
    --     make the polygon borders invisible.
    SELECT COUNT(*) INTO v_duplicate_count
    FROM migration.landuse
    WHERE strokewidt < 0.05;
    IF v_duplicate_count > 0 THEN
        error_messages = error_messages || E'#{MSG_INVALID_STROKE_WIDTH}' || E'\n';
    END IF;
    --== END (IV) =========================================================

    -- Raise exception with all error messages
    IF char_length(error_messages) > 0 THEN
        RAISE EXCEPTION '%', error_messages;
    END IF;


    -- 1.- Perform geometry corrections
    UPDATE migration.zone
    SET shape = st_force2d(shape);

    -- Use the first polygon of a multi polygon only.
    -- Needed because the shapeuploader app generates multi polygons only.
    UPDATE migration.zone
    SET shape = st_geometryn(shape, 1)
    WHERE st_numgeometries(shape) > 0;

    -- 2.- Establish the SRID
    --TODO: Use parameter to establish SRID
    UPDATE migration.zone
    SET shape = st_setsrid(shape, 32616);

    --======================================================================
    --3.- Mapeo de tipos de derecho, restricciones y responsabilidades
    --IF migration.table_exists('migration', 'rights') THEN
        --UPDATE migration.rights SET type='OWNERSHIP' WHERE type = 'DOMINIO PLENO';
        --UPDATE migration.rights SET type='OWNERSHIP_ASSUMED' WHERE type = 'DOMINIO UTIL';
        --UPDATE migration.rights SET type='LEASE' WHERE type = 'HABITACION';
        --UPDATE migration.rights SET type='REMAINDER_ESTATE' WHERE type = 'NUDAPROPIEDAD';
        --UPDATE migration.rights SET type='OCCUPATION' WHERE type = 'OCUPACION';
        --UPDATE migration.rights SET type='OTHER' WHERE type = 'OTRA';
        --UPDATE migration.rights SET type='TENANCY' WHERE type = 'POSESION';
        --UPDATE migration.rights SET type='INFORMAL_OCCUPATION' WHERE type = 'USO';
        --UPDATE migration.rights SET type='USUFRUCT' WHERE type = 'USUFRUCTO';
        --UPDATE migration.rights SET type='OTHER' WHERE type IS NULL;
    --END IF;


    --MAPEO DE TIPOS DE RESTRICCION
    --IF migration.table_exists('migration', 'restrictions') THEN
        --UPDATE migration.restrictions SET type='MORTGAGE' WHERE type='HIPOTECA';
        --UPDATE migration.restrictions SET type='ADMIN_PUBLIC_SERVITUDE' WHERE type='SERVIDUMBRE DE PASO';
    --END IF;
    --== FIN (3) ===========================================================

    RETURN 0;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
