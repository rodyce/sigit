CREATE OR REPLACE FUNCTION migration.p04_export_l3_building_units()
  RETURNS integer AS
$BODY$BEGIN
    IF migration.table_exists('migration', 'building_unit') THEN
        PERFORM migration.export_spatialzones(3);

        --===== DATOS PARA HND_BuildingUnit
        INSERT INTO hnd_cadastre.hnd_buildingunit(
            hnd_buildingunitid,
            numberoffloors,
            builtarea,
            value
        )
        SELECT sigit_uuid(bu.zone_id),
            COALESCE(bu.numfloors, 1),--por omision 1 piso
            bu.builtarea,
            bu.value
        FROM migration.building_unit bu
        WHERE NOT EXISTS(
                SELECT hnd_buildingunitid
                FROM hnd_cadastre.hnd_buildingunit
                WHERE hnd_buildingunitid = sigit_uuid(bu.zone_id)
            );
        --=================================================================

        -- For Parcels that contain building units, their "developmentStatus"
        -- field must be set to 'IMPROVED'
        UPDATE hnd_cadastre.hnd_parcel p
        SET developmentstatus = 'IMPROVED'
        FROM (
            SELECT a.hnd_spatialzoneid
            FROM (
                SELECT p.hnd_spatialzoneid, shape AS spatialzone_shape
                FROM hnd_cadastre.hnd_parcel p LEFT JOIN
                     hnd_cadastre.hnd_spatialzone psz ON p.hnd_spatialzoneid = psz.la_spatialunitid) a,

                (
                SELECT bu.hnd_buildingunitid, shape AS buildingunit_shape
                FROM hnd_cadastre.hnd_buildingunit bu LEFT JOIN
                     hnd_cadastre.hnd_spatialzone busz ON bu.hnd_buildingunitid = busz.la_spatialunitid) b
            WHERE st_contains(a.spatialzone_shape, b.buildingunit_shape)
        ) res_parcels
        WHERE p.hnd_spatialzoneid = res_parcels.hnd_spatialzoneid;
    END IF;


    RETURN 0;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
