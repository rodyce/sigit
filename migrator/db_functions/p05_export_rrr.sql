CREATE OR REPLACE FUNCTION migration.p05_export_rrr()
  RETURNS integer AS
$BODY$
DECLARE
    v_rrr_id_offset INTEGER;
BEGIN

    --===== DATOS PARA LA_BAUnit y HND_Property
    INSERT INTO ladm_administrative.la_baunit(id, beginlifespanversion, "type")
    SELECT sigit_uuid(z.zone_id), LOCALTIMESTAMP, 'ADMINISTRATIVE_AREA'
    FROM migration.zone z
    WHERE st_isvalid(z.shape) AND
        NOT EXISTS (
            SELECT id
            FROM ladm_administrative.la_baunit
            WHERE id = sigit_uuid(z.zone_id)
        );

    --TODO: Remove st_isvalid checks and do it in step 00
    INSERT INTO hnd_administrative.hnd_property(la_baunitid, hasregistrationdata)
    SELECT sigit_uuid(z.zone_id), false
    FROM migration.zone z
    WHERE st_isvalid(z.shape)
        AND NOT EXISTS (
            SELECT la_baunitid
            FROM hnd_administrative.hnd_property
            WHERE la_baunitid = sigit_uuid(z.zone_id)
        );
    --===========================================


    --==== Creacion de la relacion entre LA_BAUnit y LA_SpatialUnit */
    INSERT INTO ladm_administrative.la_baunit_la_spatialunit(la_baunitid, la_spatialunitid)
    SELECT sigit_uuid(z.zone_id), sigit_uuid(z.zone_id)
    FROM migration.zone z
    WHERE st_isvalid(shape) AND
        NOT EXISTS (
            SELECT la_baunitid, la_spatialunitid
            FROM ladm_administrative.la_baunit_la_spatialunit
            WHERE la_baunitid = sigit_uuid(z.zone_id) AND
                la_spatialunitid = sigit_uuid(z.zone_id)
        );
    --===========================================


    --=================================================================
    --Derechos y calculo en fracciones
    --Calculate RRR identifier offset
    SELECT COUNT(*)+1 INTO v_rrr_id_offset
    FROM ladm_administrative.la_rrr;

    INSERT INTO ladm_administrative.la_rrr(id, beginlifespanversion, share_numerator, share_denominator, sharecheck, la_baunitid, la_partyid)
    SELECT sigit_uuid(a.gid + v_rrr_id_offset),
        a.begin_date,
        a.porcenta / a.gcd AS num,
        a.sumporcenta / a.gcd AS den,
        TRUE,
        sigit_uuid(a.propertyid),
        sigit_uuid(a.party_id)
    FROM
        (
        SELECT a.gid, a.party_id, a.propertyid, a.porcenta, a.begin_date, b.sumporcenta, migration.gcd(a.porcenta, b.sumporcenta) gcd
        FROM
        (
            SELECT gid, party_id, propertyid, type, begin_date, CAST(numerator * 10000 AS int) porcenta
            FROM migration.rights
            WHERE numerator > 0
        ) a,
        (
            SELECT propertyid, type, COUNT(propertyid), CAST(SUM(denominato) * 10000 AS int) sumporcenta
            FROM migration.rights
            WHERE denominato > 0
            GROUP BY propertyid, type
        ) b
        WHERE a.propertyid = b.propertyid AND a.type = b.type
    ) a,
    hnd_administrative.hnd_property p
    WHERE sigit_uuid(a.propertyid) = p.la_baunitid;


    INSERT INTO ladm_administrative.la_right(la_rrrid, "type")
    SELECT sigit_uuid(r.gid + v_rrr_id_offset), r.type
    FROM migration.rights r, hnd_administrative.hnd_property p
    WHERE sigit_uuid(r.propertyid) = p.la_baunitid
        AND r.numerator > 0 AND r.denominato > 0;

    --===============================================================================


    RETURN 0;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
