CREATE OR REPLACE FUNCTION migration.p99_export_postprocessing(
    working_srid integer,
    wms_namespace text)
  RETURNS integer AS
$BODY$/*
PostProceso
Incluye todas las operaciones necesarias que se deben efectuar despues de haber migrado los datos.

1.- Correcciones geometricas
2.- Establecer SRID
3.- Actualizar namespace de capas
*/
BEGIN

    --TODO: !!Move this to the preprocessing step!!
    --===========================================================================
    -- 1.- Correcciones geometricas
    UPDATE ladm_spatialunit.la_spatialunit
    SET referencepoint = st_force2d(referencepoint);

    UPDATE hnd_cadastre.hnd_spatialzone
    SET shape = st_force2d(shape);

    --Utilizar solo el primer poligono de un multipoligono.
    --Necesario pq el shapeuploader solamente utiliza multipoligono
    UPDATE ladm_spatialunit.la_spatialunit
    SET referencepoint = st_geometryn(referencepoint, 1)
    WHERE st_numgeometries(referencepoint) > 0;

    UPDATE hnd_cadastre.hnd_spatialzone
    SET shape = st_geometryn(shape, 1)
    WHERE st_numgeometries(shape) > 0;
    --=== FIN(1) =================================================================


    --===========================================================================
    -- 2.- Establecer SRID
    UPDATE ladm_spatialunit.la_spatialunit
    SET referencepoint = st_setsrid(referencepoint, working_srid);

    UPDATE hnd_cadastre.hnd_spatialzone
    SET shape = st_setsrid(shape, working_srid);
    --=== FIN(2) =================================================================


    --==================================
    -- 3.- Actualizar namespace de capas
    UPDATE hnd_cadastre.hnd_layer
    SET wmsnamespace = wms_namespace;
    --=== FIN(3) =======================


    RETURN 0;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
