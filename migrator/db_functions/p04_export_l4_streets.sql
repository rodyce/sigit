CREATE OR REPLACE FUNCTION migration.p04_export_l4_streets()
  RETURNS integer AS
$BODY$BEGIN
    PERFORM migration.export_spatialzones(4);

    --===== DATOS PARA HND_Street

    RETURN 0;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
