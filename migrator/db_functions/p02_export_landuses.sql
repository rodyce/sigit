CREATE OR REPLACE FUNCTION migration.p02_export_landuses()
  RETURNS integer AS
$BODY$/*
Migracion de los tipos de uso con sus dominios

1.- Migracion de NOMBRES DE DOMINIO DE TIPO DE USO. IMPORTANTE: CAMBIAR! para recuperar estos datos desde esquema de migration
2.- Migracion de valores de tipos de uso.
*/

DECLARE
    v_duplicate_count INTEGER;

BEGIN
    --============== (1)  ==============--
    -- 1.- Migracion de NOMBRES DE DOMINIO DE TIPO DE USO.
    SELECT COUNT(*) INTO v_duplicate_count
    FROM hnd_cadastre.hnd_landusedomain;
    IF v_duplicate_count = 0 THEN
        INSERT INTO hnd_cadastre.hnd_landusedomain(id, name, description)
        SELECT sigit_uuid(id), name, descriptio FROM migration.ludomain;
    END IF;
    --============== FIN (1)  ==============--

    --============== (2) ==============--
    SELECT COUNT(*) INTO v_duplicate_count
    FROM hnd_cadastre.hnd_landuse;
    IF v_duplicate_count = 0 THEN
        INSERT INTO hnd_cadastre.hnd_landuse(id, code, subcode, completecode, "name", landusedomainid, "level", leaf, fillcolor, fillopacity, strokecolor, strokewidth, parentlanduseid)
        SELECT sigit_uuid(complcode), complcode, subcode, complcode, "name", sigit_uuid(domainid), "level",
        (SELECT COUNT(*) = 0 WHERE EXISTS(SELECT * FROM migration.landuse WHERE lu0.complcode = parentcode)) AS leaf,
        fillcolor, fillopac, strokecolo, strokewidt, sigit_uuid(parentcode)
        FROM migration.landuse AS lu0
        ORDER BY complcode;
    END IF;
    --============== FIN (2)  ==============--

    RETURN 0;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
