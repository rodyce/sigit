CREATE OR REPLACE FUNCTION migration.p01_export_preliminaries()
  RETURNS integer AS
$BODY$--PRELIMINARES:
--===============
/*
Los preliminares involucran aspectos que se ingresan en las tablas de SIGIT
sin depender de la informacion a migrar en si.

Esto incluye:
1.- Descriptores de capas
2.- Vistas utilizadas por el servicio WMS
3.- Configuracion de opciones del Visualizador por tipo de transaccion
4.- OPCIONAL: Insertar materiales de construccion de ejemplo
5.- OPCIONAL: Reglas espaciales de ejemplo
*/

DECLARE
    v_duplicate_count INTEGER;

BEGIN

    --TRUNCATE ladm_spatialunit.la_level CASCADE;
    --TRUNCATE hnd_administrative.hnd_transactionmetadata CASCADE;
    --TRUNCATE hnd_cadastre.hnd_buildingmaterial CASCADE;

    --======== CAPAS ========--
    --1.- Descriptores de capas

    /* Nombres de las capas */
    --If tables already contain data, do not attempt to insert rows
    SELECT COUNT(*) INTO v_duplicate_count
    FROM ladm_spatialunit.la_level;
    IF v_duplicate_count = 0 THEN
        INSERT INTO ladm_spatialunit.la_level(id, beginlifespanversion, "name", "type")
        VALUES (sigit_uuid(1), LOCALTIMESTAMP, 'Predio', 'PRIMARY_RIGHT');

        INSERT INTO ladm_spatialunit.la_level(id, beginlifespanversion, "name", "type")
        VALUES (sigit_uuid(2), LOCALTIMESTAMP, 'Zonificacion', 'PRIMARY_RIGHT');

        INSERT INTO ladm_spatialunit.la_level(id, beginlifespanversion, "name", "type")
        VALUES (sigit_uuid(3), LOCALTIMESTAMP, 'Edificacion', 'PRIMARY_RIGHT');

        INSERT INTO ladm_spatialunit.la_level(id, beginlifespanversion, "name", "type")
        VALUES (sigit_uuid(4), LOCALTIMESTAMP, 'Calle', 'PRIMARY_RIGHT');
    END IF;


    /* Espacios de nombre y nombres de las capas en servidor WMS */
    -- The WMS namespace is set in the post processing script (p99)
    SELECT COUNT(*) INTO v_duplicate_count
    FROM hnd_cadastre.hnd_layer;
    IF v_duplicate_count = 0 THEN
        INSERT INTO hnd_cadastre.hnd_layer(la_levelid, wmsnamespace, wmslayername, overlapsallowed, transactionrequired, historycontrol, geometrytype, layertype)
        VALUES(sigit_uuid(1), 'sigit', 'parcel', false, true, true, 'POLYGON', 'PARCEL');

        INSERT INTO hnd_cadastre.hnd_layer(la_levelid, wmsnamespace, wmslayername, overlapsallowed, transactionrequired, historycontrol, geometrytype, layertype)
        VALUES(sigit_uuid(2), 'sigit', 'spatialzone', true, false, false, 'POLYGON', 'SPATIAL_ZONE');

        INSERT INTO hnd_cadastre.hnd_layer(la_levelid, wmsnamespace, wmslayername, overlapsallowed, transactionrequired, historycontrol, geometrytype, layertype)
        VALUES(sigit_uuid(3), 'sigit', 'buildingunit', true, false, false, 'POLYGON', 'BUILDING');

        INSERT INTO hnd_cadastre.hnd_layer(la_levelid, wmsnamespace, wmslayername, overlapsallowed, transactionrequired, historycontrol, geometrytype, layertype)
        VALUES(sigit_uuid(4), 'sigit', 'spatialzonestreet', false, false, false, 'POLYGON', 'STREET');
    END IF;
    --==============  FIN (1)  ==============--


    --======== VISTAS PARA SERVICIO WMS ========--
    --======== 2.- Vistas utilizadas por el servicio WMS
    IF NOT migration.table_exists('gis_layers', 'parcel') THEN
        CREATE OR REPLACE VIEW gis_layers.parcel AS
        SELECT CAST(encode(a.id, 'hex') AS uuid)::text AS id,
        CASE
            WHEN c.cadastralkey IS NOT NULL THEN c.cadastralkey
            WHEN c.municipalkey IS NOT NULL THEN c.municipalkey
            ELSE c.fieldtab
        END AS zonename,
        b.shape,
        b.locklevel,
        b.pendingpermittype,
        CASE
            WHEN c.taxationbalancedue = 0::numeric THEN 'SOLVENT'::text
            WHEN c.taxationbalancedue > 0::numeric THEN 'INSOLVENT'::text
            ELSE 'UNKNOWN'::text
        END AS taxationstatus,
        'PARCEL'::character varying AS sztype,
        d.completecode AS landusecode,
        d.name AS landusename,
        d.fillcolor,
        d.fillopacity,
        d.strokecolor,
        d.strokewidth
        FROM ladm_spatialunit.la_spatialunit a
            LEFT JOIN hnd_cadastre.hnd_spatialzone b ON a.id = b.la_spatialunitid
            LEFT JOIN hnd_cadastre.hnd_parcel c ON b.la_spatialunitid = c.hnd_spatialzoneid
            LEFT JOIN hnd_cadastre.hnd_landuse d ON b.landuseid = d.id
        WHERE a.la_levelid = sigit_uuid(1) AND a.endlifespanversion IS NULL;
    END IF;

    IF NOT migration.table_exists('gis_layers', 'sigit_instance_szbox') THEN
        --TODO: Customize the SRID (32616 hard-coded here)
        CREATE OR REPLACE VIEW gis_layers.sigit_instance_szbox AS
        SELECT st_setsrid(st_extent(sz.shape)::geometry, 32616) AS szbox
        FROM hnd_cadastre.hnd_spatialzone sz;
    END IF;

    IF NOT migration.table_exists('gis_layers', 'sigit_instance_jurisdiction') THEN
        CREATE OR REPLACE VIEW gis_layers.sigit_instance_jurisdiction AS
        SELECT sc.jurisdictionshape
        FROM hnd_special.systemconfiguration sc
        WHERE sc.id = sigit_uuid(1);
    END IF;

    IF NOT migration.table_exists('gis_layers', 'sigit_instance_extent') THEN
        CREATE OR REPLACE VIEW gis_layers.sigit_instance_extent AS
        SELECT
            CASE
                WHEN jurisdictionshape IS NULL THEN szbox
                ELSE jurisdictionshape
            END AS extent
        FROM gis_layers.sigit_instance_szbox, gis_layers.sigit_instance_jurisdiction;
    END IF;

    IF NOT migration.table_exists('gis_layers', 'spatialzone') THEN
        CREATE OR REPLACE VIEW gis_layers.spatialzone AS
        SELECT CAST(encode(a.id, 'hex') AS uuid)::text AS id, b.zonename, b.shape, b.locklevel, b.pendingpermittype, 'SPATIAL_ZONE'::character varying AS sztype, d.completecode AS landusecode, d.name AS landusename, d.fillcolor, d.fillopacity, d.strokecolor, d.strokewidth
        FROM ladm_spatialunit.la_spatialunit a
        LEFT JOIN hnd_cadastre.hnd_spatialzone b ON a.id = b.la_spatialunitid
        LEFT OUTER JOIN hnd_cadastre.hnd_landuse d ON b.landuseid = d.id
        WHERE a.la_levelid = sigit_uuid(2) AND a.endlifespanversion IS NULL;
    END IF;

    IF NOT migration.table_exists('gis_layers', 'buildingunit') THEN
        CREATE OR REPLACE VIEW gis_layers.buildingunit AS
        SELECT CAST(encode(a.id, 'hex') AS uuid)::text AS id, b.zonename, b.shape, b.locklevel, b.pendingpermittype, 'BUILDING'::character varying AS sztype, d.completecode AS landusecode, d.name AS landusename, d.fillcolor, d.fillopacity, d.strokecolor, d.strokewidth
        FROM ladm_spatialunit.la_spatialunit a
        LEFT JOIN hnd_cadastre.hnd_spatialzone b ON a.id = b.la_spatialunitid
        LEFT OUTER JOIN hnd_cadastre.hnd_landuse d ON b.landuseid = d.id
        WHERE a.la_levelid = sigit_uuid(3) AND a.endlifespanversion IS NULL;
    END IF;

    IF NOT migration.table_exists('gis_layers', 'spatialzonestreet') THEN
        CREATE OR REPLACE VIEW gis_layers.spatialzonestreet AS
        SELECT CAST(encode(a.id, 'hex') AS uuid)::text AS id, b.zonename, b.shape, b.locklevel, b.pendingpermittype, 'STREET'::character varying AS sztype, d.completecode AS landusecode, d.name AS landusename, d.fillcolor, d.fillopacity, d.strokecolor, d.strokewidth
        FROM ladm_spatialunit.la_spatialunit a
        LEFT JOIN hnd_cadastre.hnd_spatialzone b ON a.id = b.la_spatialunitid
        LEFT OUTER JOIN hnd_cadastre.hnd_landuse d ON b.landuseid = d.id
        WHERE a.la_levelid = sigit_uuid(4) AND a.endlifespanversion IS NULL;
    END IF;


    IF NOT migration.table_exists('gis_layers', 'spatial_unit__reference_point') THEN
        CREATE OR REPLACE VIEW gis_layers.spatial_unit__reference_point AS
        SELECT CAST(encode(a.id, 'hex') AS uuid)::text AS id, c.fieldtab AS zonename, a.referencepoint AS shape, b.locklevel, 'CENTROID'::character varying AS sztype, NULL::character varying AS landusecode, NULL::character varying AS landusename, NULL::character varying AS fillcolor, NULL::character varying AS fillopacity, NULL::character varying AS strokecolor, NULL::character varying AS strokewidth
        FROM ladm_spatialunit.la_spatialunit a
        LEFT JOIN hnd_cadastre.hnd_spatialzone b ON a.id = b.la_spatialunitid
        LEFT JOIN hnd_cadastre.hnd_parcel c ON b.la_spatialunitid = c.hnd_spatialzoneid
        WHERE a.endlifespanversion IS NULL;
    END IF;

    IF NOT migration.table_exists('gis_layers', 'topographic_transaction') THEN
        CREATE OR REPLACE VIEW gis_layers.topographic_transaction AS
        SELECT tt.presentationno, CAST(encode(tt.workinglayerid, 'hex') AS uuid)::text AS workinglayerid, tt.presentationno AS zonename, tt.extents AS shape, 'TOPOGRAPHIC_TRANSACTION'::character varying AS sztype, NULL::character varying AS landusecode, NULL::character varying AS landusename, NULL::character varying AS fillcolor, NULL::character varying AS fillopacity, NULL::character varying AS strokecolor, NULL::character varying AS strokewidth
        FROM hnd_administrative.hnd_topographictransaction tt
        WHERE tt.state = 'INITIATED';
    END IF;

    -- Permisos de construccion en proceso de transaccion
    IF NOT migration.table_exists('gis_layers', 'pending_building_permit') THEN
        CREATE OR REPLACE VIEW gis_layers.pending_building_permit AS
        SELECT sz.shape
        FROM hnd_administrative.hnd_transaction t
        LEFT JOIN hnd_administrative.hnd_permit p ON t.id = p.id
        LEFT JOIN hnd_administrative.hnd_buildingpermit bp ON p.id = bp.id
        LEFT JOIN hnd_cadastre.hnd_buildingunit bu ON bp.buildingunitid = bu.hnd_buildingunitid
        LEFT JOIN hnd_cadastre.hnd_spatialzone sz ON bu.hnd_buildingunitid = sz.la_spatialunitid
        WHERE t.completiondate IS NULL;
    END IF;


    -- Permisos de operacion en proceso de transaccion
    IF NOT migration.table_exists('gis_layers', 'pending_operation_permit') THEN
        CREATE OR REPLACE VIEW gis_layers.pending_operation_permit AS
        SELECT sz.shape
        FROM hnd_administrative.hnd_transaction t
        LEFT JOIN hnd_administrative.hnd_permit p ON t.id = p.id
        LEFT JOIN hnd_administrative.hnd_operationpermit op ON p.id = op.id
        LEFT JOIN hnd_cadastre.hnd_spatialzone sz ON p.selectedzoneid = sz.la_spatialunitid
        WHERE t.completiondate IS NULL;
    END IF;
    --==============  FIN (2)  ==============--


    --======== 3.- Configuracion de opciones del Visualizador por tipo de transaccion
    SELECT COUNT(*) INTO v_duplicate_count
    FROM hnd_administrative.hnd_transactionmetadata;
    IF v_duplicate_count = 0 THEN
        INSERT INTO hnd_administrative.hnd_transactionmetadata(municipaltrxtype,canmutaterrr,canmerge,cansplit,caneditdata) VALUES('TITLE_DEED_ADJUDICATION', TRUE, FALSE, FALSE, TRUE);
        INSERT INTO hnd_administrative.hnd_transactionmetadata(municipaltrxtype,canmutaterrr,canmerge,cansplit,caneditdata) VALUES('LIEN', TRUE, FALSE, FALSE, TRUE);
        INSERT INTO hnd_administrative.hnd_transactionmetadata(municipaltrxtype,canmutaterrr,canmerge,cansplit,caneditdata) VALUES('CANCELLATION', TRUE, FALSE, FALSE, TRUE);
        INSERT INTO hnd_administrative.hnd_transactionmetadata(municipaltrxtype,canmutaterrr,canmerge,cansplit,caneditdata) VALUES('TRANSFER_OF_OWNERSHIP', TRUE, FALSE, FALSE, TRUE);
        INSERT INTO hnd_administrative.hnd_transactionmetadata(municipaltrxtype,canmutaterrr,canmerge,cansplit,caneditdata) VALUES('SPECIAL_ACT_CONTRACT', TRUE, FALSE, FALSE, TRUE);
        INSERT INTO hnd_administrative.hnd_transactionmetadata(municipaltrxtype,canmutaterrr,canmerge,cansplit,caneditdata) VALUES('DONATION', TRUE, FALSE, FALSE, TRUE);
        INSERT INTO hnd_administrative.hnd_transactionmetadata(municipaltrxtype,canmutaterrr,canmerge,cansplit,caneditdata) VALUES('INHERITANCE', TRUE, FALSE, FALSE, TRUE);
        INSERT INTO hnd_administrative.hnd_transactionmetadata(municipaltrxtype,canmutaterrr,canmerge,cansplit,caneditdata) VALUES('CHANGES_CORRECTIONS', TRUE, TRUE, TRUE, TRUE);
        INSERT INTO hnd_administrative.hnd_transactionmetadata(municipaltrxtype,canmutaterrr,canmerge,cansplit,caneditdata) VALUES('MERGERS_PARTITIONS', TRUE, TRUE, TRUE, TRUE);
        INSERT INTO hnd_administrative.hnd_transactionmetadata(municipaltrxtype,canmutaterrr,canmerge,cansplit,caneditdata) VALUES('VARIOUS', TRUE, TRUE, TRUE, TRUE);
    END IF;
    --==============  FIN (3)  ==============--


    --======== 4.- OPCIONAL: Insertar materiales de construccion de ejemplo
    SELECT COUNT(*) INTO v_duplicate_count
    FROM hnd_cadastre.hnd_buildingmaterial;
    IF v_duplicate_count = 0 THEN
        INSERT INTO hnd_cadastre.hnd_buildingmaterial(id, name) VALUES(sigit_uuid(1), 'GRAVA');
        INSERT INTO hnd_cadastre.hnd_buildingmaterial(id, name) VALUES(sigit_uuid(2), 'CEMENTO');
        INSERT INTO hnd_cadastre.hnd_buildingmaterial(id, name) VALUES(sigit_uuid(3), 'ASBESTO');
        INSERT INTO hnd_cadastre.hnd_buildingmaterial(id, name) VALUES(sigit_uuid(4), 'MADERA');
        INSERT INTO hnd_cadastre.hnd_buildingmaterial(id, name) VALUES(sigit_uuid(5), 'PLASTICO');
    END IF;
    --==============  FIN (4)  ==============--


    RETURN 0;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
