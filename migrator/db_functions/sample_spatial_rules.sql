CREATE OR REPLACE FUNCTION migration.sample_spatial_rules()
  RETURNS integer AS
$BODY$BEGIN
    INSERT INTO hnd_administrative.hnd_spatialrule(id, action, code, comparisonoperator, comparisonparametervalue, description, ruleoperator, hnd_landuseid1, hnd_landuseid2, la_levelid1, la_levelid2)
    VALUES(sigit_uuid(1), 'MINUS', 'R01', NULL, NULL,
        'Predio industrial no puede estar dentro de una zonificación residencial',
        'CONTAINS',
        sigit_uuid(5),
        NULL,
        sigit_uuid(1),
        sigit_uuid(4));

    INSERT INTO hnd_administrative.hnd_spatialrule(id, action, code, comparisonoperator, comparisonparametervalue, description, ruleoperator, hnd_landuseid1, hnd_landuseid2, la_levelid1, la_levelid2)
    VALUES(sigit_uuid(2), 'MINUS', 'R02', 'LT', 600,
        'Predio industrial no puede estar a una distancia menor de 600m de uno residencial',
        'DISTANCE',
        sigit_uuid(5),
        sigit_uuid(9),
        sigit_uuid(1),
        sigit_uuid(1));


    INSERT INTO hnd_administrative.hnd_permitrulegroup(id, description, name, permittype)
    VALUES(sigit_uuid(1), 'Reglas para permisos de construcción y operación', 'G01', NULL);


    RETURN 0;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
