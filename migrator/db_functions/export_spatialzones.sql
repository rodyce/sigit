CREATE OR REPLACE FUNCTION migration.export_spatialzones(p_level_id integer)
  RETURNS integer AS
$BODY$BEGIN
    --===== ESPACIAL: DATOS PARA LA_SpatialUnit, HND_SpatialZone
    INSERT INTO ladm_spatialunit.la_spatialunit(
        id,
        beginlifespanversion,
        dimension,
        extaddressid,
        label,
        referencepoint,
        la_levelid
    )
    SELECT sigit_uuid(z.zone_id),
        LOCALTIMESTAMP,
        '_2D',
        sigit_uuid(1),
        z.zone_name,
        st_centroid(z.shape),
        sigit_uuid(p_level_id)
    FROM migration.zone z
    WHERE z.levelid = p_level_id AND st_isvalid(z.shape) AND
        NOT EXISTS (
            SELECT id
            FROM ladm_spatialunit.la_spatialunit
            WHERE id = sigit_uuid(z.zone_id)
        );


    INSERT INTO hnd_cadastre.hnd_spatialzone(
        la_spatialunitid,
        zonename,
        locationincountry,
        measuredperimeter,
        measuredarea,
        documentedperimeter,
        documentedarea,

        maxbuiltarea,--max built area
        maxnumberoffloors,--max num of floors

        landuseid,
        proposedlanduseid,
        shape,

        locklevel
    )
    SELECT sigit_uuid(z.zone_id),
        zone_name,
        loccountry,
        measd_per,
        measd_area,
        docted_per,
        doctedarea,

        maxbuiltar,--max built area
        maxnofloor,--max num of floors

        sigit_uuid(landuseid),
        sigit_uuid(propluid),
        z.shape,

        'UNLOCKED'
    FROM migration.zone z
    WHERE z.levelid = p_level_id AND st_isvalid(z.shape) AND
        NOT EXISTS (
            SELECT la_spatialunitid
            FROM hnd_cadastre.hnd_spatialzone
            WHERE la_spatialunitid = sigit_uuid(z.zone_id)
        );


    RETURN 0;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
