CREATE OR REPLACE FUNCTION migration.p03_export_parties()
  RETURNS integer AS
$BODY$/*
Migracion de propietarios.
Personas Naturales y Personas Juridicas
*/

BEGIN

    -----------------------------------------------------------------------------------------------------------------------------------------------------------------
    --------------------------------------------------------------------- PROPIETARIOS ------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------------------------------------------------
    --===== Datos para las tablas ExtParty, HND_Person, HND_NauralPerson y LA_Party
    INSERT INTO ladm_external.extparty(id, name)
    SELECT sigit_uuid(p.party_id), p.name
    FROM migration.party p
    WHERE NOT EXISTS (
        SELECT id FROM ladm_external.extparty WHERE id = sigit_uuid(p.party_id)
    );

    INSERT INTO hnd_administrative.hnd_person(extpartyid, rtn)
    SELECT sigit_uuid(p.party_id), p.rtn
    FROM migration.party p
    WHERE NOT EXISTS (
        SELECT extpartyid FROM hnd_administrative.hnd_person WHERE extpartyid = sigit_uuid(p.party_id)
    );

    INSERT INTO hnd_administrative.hnd_naturalperson(extpartyid, firstname1, firstname2, lastname1, lastname2, dob, identity, maritalstatus, nationality, gender)
    SELECT sigit_uuid(party_id),
        COALESCE(first_nme1, '-'),
        first_nme2,
        COALESCE(last_name1, '-'),
        last_name2,
        dob,
        identity,
        marit_stat,
        nationali,
        gender
    FROM migration.party p
    WHERE upper(p.type) = 'N' AND
        NOT EXISTS (
            SELECT extpartyid FROM hnd_administrative.hnd_naturalperson WHERE extpartyid = sigit_uuid(p.party_id)
        );
--        first_nme1 is not null and
--        not exists(
--        select e.extpartyid
--        from hnd_administrative.hnd_naturalperson e
--        where e.extpartyid = p.id
--    );

    INSERT INTO ladm_party.la_party(id, beginlifespanversion, "role", "type", extpartyid)
    SELECT sigit_uuid(party_id), LOCALTIMESTAMP, 'CITIZEN', 'NATURAL_PERSON', sigit_uuid(party_id)
    FROM migration.party p
    WHERE upper(p.type) = 'N' AND
        NOT EXISTS (
            SELECT id FROM ladm_party.la_party WHERE id = sigit_uuid(p.party_id)
        );

    INSERT INTO ladm_party.la_party(id, beginlifespanversion, "role", "type", extpartyid)
    SELECT sigit_uuid(party_id), LOCALTIMESTAMP, 'OWNER', 'NON_NATURAL_PERSON', sigit_uuid(party_id)
    FROM migration.party p
    WHERE upper(p.type) = 'L' AND
        NOT EXISTS (
            SELECT id FROM ladm_party.la_party WHERE id = sigit_uuid(p.party_id)
        );
    -----------------------------------------------------------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------------------------------------------------


    RETURN 0;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
