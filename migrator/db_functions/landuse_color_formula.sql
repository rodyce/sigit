CREATE OR REPLACE FUNCTION migration.landuse_color_formula()
  RETURNS integer AS
$BODY$BEGIN

    --Definicion de formula para el color
    --OJO: se utilizo el id para el ultimo componente de color, esto es considerando a que los ids presentes son de a lo mas 2 digitos
    --TODO: SE ELIMINARA DESPUES YA QUE EL COLOR TIENE QUE VENIR YA DEFINIDO
    UPDATE migration.landuse
    SET fillcolor = '#'::character varying || complcode || trim(to_char(id, '09')), fillopac = 0.5, strokecolo = '#000000', strokewidt = 0.1;



    RETURN 0;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
