CREATE OR REPLACE FUNCTION sigit_uuid(p_input_number numeric)
  RETURNS bytea AS
$BODY$BEGIN

IF p_input_number IS NULL THEN
    RETURN NULL;
ELSE
    RETURN decode(replace(COALESCE(uuid_generate_v5(uuid_ns_oid(), to_char(p_input_number, '999999999999'))::text, uuid_generate_v4()::text), '-', ''), 'hex');
END IF;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
