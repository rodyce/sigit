CREATE OR REPLACE FUNCTION migration.sample_db_init(
    working_srid integer,
    wms_namespace text)
  RETURNS integer AS
$BODY$BEGIN
    --===== CONFIGURACION GENERAL DE SISTEMA (pendiente grupos de reglas para permisos de construccion y operacion)
    TRUNCATE hnd_special.systemconfiguration;
    DELETE FROM hnd_special.systemconfiguration;
    INSERT INTO hnd_special.systemconfiguration(id, municipalityname, srid, workingnamespace, buildingpermitrulegroupid, operationpermitrulegroupid, jurisdictionshape)
    VALUES(sigit_uuid(1), 'MUNICIPALIDAD DE ANTIGUO CUSCATLAN', working_srid, wms_namespace, NULL, NULL, NULL);
    --=====================================================

    --====== USUARIOS INICIALES DEL SISTEMA
    -- Debe ir aqui debido a que primero hay que tener los PARTIES
    DELETE FROM hnd_administrative.hnd_user WHERE username = 'admin';
    INSERT INTO hnd_administrative.hnd_user(id, username, password, active, analyst, approver, editor, externalquerier, internalquerier, manager, receptionist, surveyingtechnician, systemadministrator, partyid)
    --Password sigithn
    VALUES(sigit_uuid(1), 'admin', '0f2da6df03f25784ee5fa729f7e41724', TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, (SELECT extpartyid FROM hnd_administrative.hnd_person LIMIT 1));
    --==========================================================================================

    RETURN 0;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
