CREATE OR REPLACE FUNCTION migration.p04_export_l1_parcels()
  RETURNS integer AS
$BODY$/**/

DECLARE
    v_duplicate_count INTEGER;

BEGIN
    IF migration.table_exists('migration', 'parcel') THEN
        PERFORM migration.export_spatialzones(1);

        INSERT INTO hnd_cadastre.hnd_parcel(
            hnd_spatialzoneid,
            fieldtab,
            municipalkey,
            cadastralkey,
            documentedbuiltarea,
            neighborhood,
            accessway1,
            accessway2,
            housenumber,
            commercialappraisal,
            fiscalappraisal,
            taxationbalancedue,
            developmentstatus,

            access,
            businessclass,
            drainage,
            electricity,
            garbagetruck,
            heritage,
            landscapevalue,
            lighting,
            location,
            phone,
            sidewalks,
            streets,
            topography,
            tvcable,
            vulnerable,
            water
        )
        SELECT sigit_uuid(z.zone_id),
            p.field_tab,
            p.muni_key,
            p.cadtralkey,
            p.doc_b_area,
            p.neigh_name,
            p.aw1,
            p.aw2,
            p.house_no,
            p.comm_appr,
            p.fiscalappr,
            p.taxbaldue,
            CASE p.devstatus
                WHEN 1 THEN 'VACANT'
                WHEN 2 THEN 'IMPROVED'
                ELSE 'UNKNOWN'
            END,

            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false
        FROM migration.parcel p LEFT JOIN migration.zone z ON p.zone_id = z.zone_id
        WHERE st_isvalid(z.shape) AND
            NOT EXISTS(
                SELECT hnd_spatialzoneid
                FROM hnd_cadastre.hnd_parcel
                WHERE hnd_spatialzoneid = sigit_uuid(z.zone_id)
            );
    END IF;

    RETURN 0;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
