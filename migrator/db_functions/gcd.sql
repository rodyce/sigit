CREATE OR REPLACE FUNCTION migration.gcd(
    pu integer,
    pv integer)
  RETURNS integer AS
$BODY$
DECLARE
    res int;
    r int;
    u int;
    v int;
BEGIN
    u := pu;
    v := pv;
    LOOP
        IF v = 0 THEN
            res := u;
            EXIT;
        ELSE
            r := u % v;
            u := v;
            v := r;
        END IF;
    END LOOP;

    RETURN res;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
