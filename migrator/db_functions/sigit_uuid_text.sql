CREATE OR REPLACE FUNCTION sigit_uuid(p_input_text text)
  RETURNS bytea AS
$BODY$SELECT CASE
           WHEN p_input_text IS NULL THEN NULL
           ELSE decode(replace(COALESCE(uuid_generate_v5(uuid_ns_oid(), p_input_text)::text, uuid_generate_v4()::text), '-', ''), 'hex')
       END;$BODY$
  LANGUAGE sql VOLATILE
  COST 100;
