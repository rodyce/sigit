CREATE OR REPLACE FUNCTION migration.table_exists(
    p_schema_name text,
    p_table_name text)
  RETURNS boolean AS
$BODY$SELECT COUNT(*) > 0
FROM   information_schema.tables
WHERE  table_schema = p_schema_name
AND    table_name = p_table_name
$BODY$
  LANGUAGE sql VOLATILE
  COST 100;
