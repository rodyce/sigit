package org.sigit.commons.di;

import javax.servlet.ServletContext;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.context.WebApplicationContext;

public class CtxComponent implements ApplicationContextAware {
    private static ApplicationContext applicationContext;
    
    public static Object getInstance(String componentName) {
        validateAppContext();
        return applicationContext.getBean(componentName);
    }
    
    public static <T> T getInstance(Class<T> clazz) {
        validateAppContext();
        return applicationContext.getBean(clazz);
    }

    public static ApplicationContext getDispatcherServletApplicationContext() {
        ApplicationContext dispatcherAppContext = null;
        if (applicationContext instanceof WebApplicationContext &&
                ((WebApplicationContext)applicationContext).getServletContext() != null) {
            ServletContext sc = ((WebApplicationContext)applicationContext).getServletContext();
            dispatcherAppContext = CtxComponent.applicationContext = (ApplicationContext)
                        sc.getAttribute("org.springframework.web.servlet.FrameworkServlet.CONTEXT.spring-dispatcher");
        }
        
        if (dispatcherAppContext == null) {
            throw new IllegalStateException("Could not get dispatcher servlet application context");
        }
        
        return dispatcherAppContext;
    }
    
    @Override
    public void setApplicationContext(ApplicationContext applicationContext)
            throws BeansException {
        
        CtxComponent.applicationContext = applicationContext;

    }
    
    private static void validateAppContext() {
        if (applicationContext == null) {
            throw new IllegalStateException("Spring application context has not been initialized");
        }
    }
}
