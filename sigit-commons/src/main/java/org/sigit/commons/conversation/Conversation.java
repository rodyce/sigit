package org.sigit.commons.conversation;

import java.io.Serializable;
import java.util.UUID;

import org.sigit.commons.di.CtxComponent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component("conversation")
@Scope("sigit-conversation")
public class Conversation implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private UUID uuid = UUID.randomUUID();


    public static Conversation instance() {
        return (Conversation) CtxComponent.getInstance("conversation");
    }
    
    public String getId() {
        return uuid.toString();
    }
    
    public void begin() {
    }
    
    public void leave() {
    }
    
    public void end() {
    }
}
