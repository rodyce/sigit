package org.sigit.commons.conversation.scope;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.faces.application.NavigationCase;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.Scope;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import com.sun.faces.application.ApplicationAssociate;

public class ConversationScope implements Scope, Serializable, HttpSessionBindingListener {
    private static final long serialVersionUID = 1L;
    
    private static final Logger logger = LoggerFactory.getLogger(ConversationScope.class);
    private static final String SIMPLE_STATE_TRANSITION = "::";
    private static final String BIDIRECTIONAL_STATE_TRANSITION = ":::";
    
    private List<String> transitionDefinitions = new ArrayList<>();
    private Map<String, Set<String>> transitionMap;

    private final Map<String, Context> sessionContextMap = new HashMap<>();
    
    
    @Override
    public Object get(String name, ObjectFactory<?> objectFactory) {
        Context ctx = currentContext();
        
        startNewConversationIfNeeded(ctx);
        
        Object bean;
        if (!ctx.scopeMap.containsKey(name)) {
            bean = objectFactory.getObject();
            ctx.scopeMap.put(name, bean);
        } else {
            bean = ctx.scopeMap.get(name);
        }
        
        return bean;
    }

    @Override
    public Object remove(String name) {
        Context ctx = currentContext();
        
        CallbackControl cc = ctx.beanDestructionCallbackMap.get(name);
        if (cc != null) {
            cc.doCallback();
            ctx.beanDestructionCallbackMap.remove(name);
        }
        return ctx.scopeMap.remove(name);
    }

    @Override
    public void registerDestructionCallback(String name, Runnable callback) {
        Context ctx = currentContext();

        ctx.beanDestructionCallbackMap.put(name, new CallbackControl(name, callback));
        
        Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        if (!sessionMap.containsKey("sessionBindingListener")) {
            sessionMap.put("sessionBindingListener", this);
        }
    }

    @Override
    public Object resolveContextualObject(String key) {
        return null;
    }

    @Override
    public String getConversationId() {
        Context ctx = currentContext();
        return ctx.uuid.toString();
    }

    @Override
    public void valueBound(HttpSessionBindingEvent event) {
        if (logger.isDebugEnabled()) {
            logger.debug("ConversationScope bound at session id: " + event.getSession().getId());
        }
    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent event) {
        Context ctx = sessionContextMap.get(event.getSession().getId());
        if (ctx != null) {
            if (logger.isDebugEnabled()) {
                logger.debug("ConversationScope UNBOUND at session id: " + event.getSession().getId());
            }
        }
    }

    public List<String> getTransitionDefinitions() {
        return transitionDefinitions;
    }
    public void setTransitionDefinitions(List<String> transitionDefinitions) {
        this.transitionDefinitions = transitionDefinitions;
    }
    
    private void startNewConversationIfNeeded(Context ctx) {
        String viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
        
        if (!viewId.equals(ctx.lastViewId)) {
            if (transitionMap == null) {
                loadTransitionMap();
            }
            if (!transitionMap.containsKey(ctx.lastViewId)
                    || !transitionMap.get(ctx.lastViewId).contains(viewId)) {
                if (logger.isDebugEnabled()) {
                    logger.debug(String.format("STARTING new conversation. Transition (%s -> %s)", ctx.lastViewId, viewId));
                }
                startNewConversation(ctx);
            }
            ctx.lastViewId = viewId;
        }
    }
    
    private void startNewConversation(final Context ctx) {
        //Current conversation will end to start a new one
        
        //STEP 1: Call destruction callbacks
        if (ctx.beanDestructionCallbackMap != null) {
            for (Entry<String, CallbackControl> entry : ctx.beanDestructionCallbackMap.entrySet()) {
                CallbackControl cc = entry.getValue();
                if (cc != null) {
                    cc.doCallback();
                }
            }
            ctx.beanDestructionCallbackMap.clear();
        }
        ctx.beanDestructionCallbackMap = new ConcurrentHashMap<>();
        
        //STEP 2: Clear scope map
        if (ctx.scopeMap != null) {
            ctx.scopeMap.clear();
        }
        ctx.scopeMap = new ConcurrentHashMap<>();
        
        //TODO: This fails when session expires
        RequestAttributes attributes = RequestContextHolder.currentRequestAttributes();
        attributes.setAttribute("endOfConversation", true, RequestAttributes.SCOPE_SESSION);

        
        //STEP 2: New conversation identifier
        ctx.uuid = UUID.randomUUID();
    }
    
    private synchronized void loadTransitionMap() {
        if (transitionMap != null) {
            //Return early since another thread already initialized the transition map
            return;
        }
        transitionMap = new HashMap<>();
        for (String trDef : transitionDefinitions) {
            boolean bidirectional = false;
            String splitToken = null;
            if (trDef.contains(BIDIRECTIONAL_STATE_TRANSITION)) {
                splitToken = BIDIRECTIONAL_STATE_TRANSITION;
                bidirectional = true;
            } else if (trDef.contains(SIMPLE_STATE_TRANSITION)) {
                splitToken = SIMPLE_STATE_TRANSITION;
                bidirectional = false;
            }
            
            if (splitToken == null) continue;
            
            String[] tuple = trDef.split(splitToken);
            if (tuple.length < 2) continue;
            
            String fromState = tuple[0].trim();
            String[] toStates = tuple[1].split(",");
            registerTransition(fromState, toStates, bidirectional);
        }
        transitionDefinitions.clear();
        
        //register Faces transitions
        ApplicationAssociate appAssoc = ApplicationAssociate.getInstance(
                FacesContext.getCurrentInstance().getExternalContext());
        Map<String, Set<NavigationCase>> navigationCaseMap = appAssoc.getNavigationCaseListMappings();
        for (Map.Entry<String, Set<NavigationCase>> entry : navigationCaseMap.entrySet()) {
            for (NavigationCase nc : entry.getValue()) {
                registerTransition(nc.getFromViewId(),
                        new String[] {nc.getToViewId(FacesContext.getCurrentInstance())},
                        false);
            }
        }
    }
    
    private void registerTransition(String fromState, String[] toStates, boolean bidirectional) {
        fromState = fromState.trim();
        Set<String> aSet;
        if (!transitionMap.containsKey(fromState)) {
            aSet = new HashSet<>();
            transitionMap.put(fromState, aSet);
        } else {
            aSet = transitionMap.get(fromState);
        }
        for (String toState : toStates) {
            toState = toState.trim();
            aSet.add(toState);
            
            logger.debug(String.format("Registering transition %s -> %s", fromState, toState));
            
            if (bidirectional) {
                registerTransition(toState, new String[] {fromState}, false);
            }
        }
    }
    
    private String currentSessionId() {
        RequestAttributes attributes = RequestContextHolder.currentRequestAttributes();
        return attributes.getSessionId();
    }
    
    private Context currentContext() {
        Context ctx = sessionContextMap.get(currentSessionId());
        
        if (ctx == null) {
            ctx = new Context();
            sessionContextMap.put(currentSessionId(), ctx);
            startNewConversation(ctx);
        }
        
        return ctx;
    }
    
    private class CallbackControl implements Serializable {
        private static final long serialVersionUID = 1L;
        
        private String name;
        private Runnable callback;
        private boolean callbackCalled = false;
        
        CallbackControl(String name, Runnable callback) {
            this.name = name;
            this.callback = callback;
        }
        
        public synchronized void doCallback() {
            if (!callbackCalled) {
                logger.debug("Calling destruction callback for bean " + name);
                try {
                    callback.run();
                } finally {
                    callbackCalled = true;
                }
            }
        }
    }
    
    private class Context implements Serializable {
        private static final long serialVersionUID = 1L;
        
        private Map<String, Object> scopeMap;
        private Map<String, CallbackControl> beanDestructionCallbackMap;
        private String lastViewId;

        private UUID uuid = UUID.randomUUID();
    }
}
