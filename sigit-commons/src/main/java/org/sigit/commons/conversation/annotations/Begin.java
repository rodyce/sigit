package org.sigit.commons.conversation.annotations;

public @interface Begin {
    
    boolean join() default true;
    
}
