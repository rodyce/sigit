package org.sigit.commons.exception;

public class LockedException extends SigitException {
    private static final long serialVersionUID = 1L;

    public LockedException() {
        super();
    }
    
    public LockedException(String detail) {
        super(detail);
    }
}
