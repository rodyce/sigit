package org.sigit.commons.exception;

public abstract class SigitException extends Exception {
    private static final long serialVersionUID = -2656963232335235387L;

    public SigitException() {
        super();
    }
    
    public SigitException(String detail) {
        super(detail);
    }
    
    public String formattedMessage(Object... args) {
        return String.format(super.getMessage(), args);
    }

    public String getExceptionBundle() {
        return "exception." + this.getClass().getSimpleName();
    }
}
