package org.sigit.commons.exception;

public class DataValidationException extends SigitException {
    private static final long serialVersionUID = 1L;

    public DataValidationException() {
        super();
    }
    
    public DataValidationException(String detail) {
        super(detail);
    }
}
