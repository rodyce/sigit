package org.sigit.commons.exception;

public class NoEqualRightsException extends SigitException {
    private static final long serialVersionUID = 4479940431401345711L;

    public NoEqualRightsException() {
        super();
    }
    
    public NoEqualRightsException(String detail) {
        super(detail);
    }
}
