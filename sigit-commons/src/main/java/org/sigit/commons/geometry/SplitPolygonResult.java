package org.sigit.commons.geometry;


import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Polygon;

public class SplitPolygonResult {
    private Polygon originalPolygon;
    private Polygon differencedPolygon;
    private Polygon splitPolygon1;
    private Polygon splitPolygon2;
    private Coordinate firstBoundaryCoord;
    private Coordinate lastBoundaryCoord;
    private Polygon[] polygonArray;
    
    SplitPolygonResult(Polygon originalPolygon, Polygon differencedPolygon,
            Polygon splitPolygon1, Polygon splitPolygon2,
            Coordinate firstBoundaryCoord, Coordinate lastBoundaryCoord) {
        this.originalPolygon = originalPolygon;
        this.differencedPolygon = differencedPolygon;
        this.splitPolygon1 = splitPolygon1;
        this.splitPolygon2 = splitPolygon2;
        this.firstBoundaryCoord = firstBoundaryCoord;
        this.lastBoundaryCoord = lastBoundaryCoord;
        this.polygonArray = new Polygon[] {splitPolygon1, splitPolygon2};
    }

    public Polygon getOriginalPolygon() {
        return originalPolygon;
    }

    public Polygon getDifferencedPolygon() {
        return differencedPolygon;
    }

    public Polygon getSplitPolygon1() {
        return splitPolygon1;
    }

    public Polygon getSplitPolygon2() {
        return splitPolygon2;
    }

    public Coordinate getFirstBoundaryCoord() {
        return firstBoundaryCoord;
    }

    public Coordinate getLastBoundaryCoord() {
        return lastBoundaryCoord;
    }

    public Polygon[] getPolygonArray() {
        return polygonArray;
    }
}
