package org.sigit.commons.geometry.exception;

public class InvalidSplitException extends SplitException {
    private static final long serialVersionUID = -3077887591348455169L;

    public InvalidSplitException() {
        //TODO: Add proper message
        super("InvalidSplitException");
    }
    
    public InvalidSplitException(String detail) {
        super(detail);
    }
}
