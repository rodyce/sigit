package org.sigit.commons.geometry.exception;

public class NoSplitException extends SplitException {
    private static final long serialVersionUID = 6307875302954290077L;

    public NoSplitException() {
        //TODO: Add proper messages
        super("NoSplitException");
    }
    
    public NoSplitException(String detail) {
        super(detail);
    }
}
