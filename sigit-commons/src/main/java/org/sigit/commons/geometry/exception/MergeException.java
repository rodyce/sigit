package org.sigit.commons.geometry.exception;

public class MergeException extends GeometryOperationException {
    private static final long serialVersionUID = -3942757692533792545L;

    public MergeException() {
        super();
    }
    
    public MergeException(String detail) {
        super(detail);
    }
}
