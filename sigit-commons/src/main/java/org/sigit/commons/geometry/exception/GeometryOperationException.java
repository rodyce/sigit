package org.sigit.commons.geometry.exception;

import org.sigit.commons.exception.SigitException;

public abstract class GeometryOperationException extends SigitException {
    private static final long serialVersionUID = -4959764196635545125L;

    public GeometryOperationException() {
        super();
    }
    
    public GeometryOperationException(String detail) {
        super(detail);
    }
}
