package org.sigit.commons.geometry;

public class BearingAndDistance {
    private double radAngle;
    private double azimuth;
    private double distance;
    
    private String direction;
    private String bearing;
    
    public static BearingAndDistance newFromCoords(double x1, double y1, double x2, double y2) {
        return new BearingAndDistance(
                GeometryOperations.theta(x1, y1, x2, y2),
                GeometryOperations.distance(x1, y1, x2, y2));
    }
    
    private BearingAndDistance(double radAngle, double distance) {
        this.radAngle = radAngle;
        this.distance = distance;
        
        determineBearing();
    }

    public double getRadAngle() {
        return radAngle;
    }
    public double getAzimuth() {
        return azimuth;
    }
    public double getDistance() {
        return distance;
    }
    public String getDirection() {
        return direction;
    }
    
    public String getBearing() {
        return bearing;
    }
    public String getBearing(int decimalPrec) {
        return String.format("%f." + decimalPrec + "° %s", azimuth, direction);
    }
    
    public boolean isDefined() {
        return !bearing.equals("") && distance > 0;
    }
    
    @Override
    public String toString() {
        return bearing;
    }
    public String toString(int decimalPrec) {
        return getBearing(decimalPrec);
    }
    
    
    private void determineBearing() {
        final double degs = GeometryOperations.radToDeg(radAngle);
        
        if (Math.abs(degs - 0) < GeometryOperations.EPSILON || Math.abs(radAngle - 360) < GeometryOperations.EPSILON) {
            azimuth = 90;
            direction = "NE";
        }
        else if (Math.abs(degs - 90) < GeometryOperations.EPSILON) {
            azimuth = 0;
            direction = "N";
        }
        else if (Math.abs(degs - 180) < GeometryOperations.EPSILON) {
            azimuth = 90;
            direction = "NW";
        }
        else if (Math.abs(degs - 270) < GeometryOperations.EPSILON) {
            azimuth = 0;
            direction = "S";
        }
        else if (degs > 0 && degs < 90) {
            azimuth = 90 - degs;
            direction = "NE";
        }
        else if (degs > 90 && degs < 180) {
            azimuth = degs - 90;
            direction = "NW";
        }
        else if (degs > 180 && degs < 270) {
            azimuth = 270 - degs;
            direction = "SW";
        }
        else if (degs > 270 && degs < 360) {
            azimuth = degs - 270;
            direction = "SE";
        }
        
        bearing = String.format("%f° %s", azimuth, direction);
    }
}
