package org.sigit.commons.geometry.exception;

public abstract class SplitException extends GeometryOperationException {
    private static final long serialVersionUID = 2849818789122276303L;

    public SplitException() {
        super();
    }
    
    public SplitException(String detail) {
        super(detail);
    }
}
